Author: Jorge Maldonado Ventura
Category: Literature
Date: 2024-01-24 14:38
Lang: en
Slug: la-batalla-de-usonansk
Tags: privacy, prose, security, war
Title: The battle of Usonansk
Save_as: the-battle-of-Usonansk/index.html
URL: the-battle-of-Usonansk/
Image: <img src="/wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar.png" alt="" width="1280" height="853" srcset="/wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar.png 1280w, /wp-content/uploads/2023/11/soldiers-fighting-a-cyberwar-640x426.png 640w" sizes="(max-width: 1280px) 100vw, 1280px">

*This story is based on real events. Identifying information has been
distorted.*

“According to the sources we have, they plan to focus their forces in
this region”. Little did the generals know that their sources had been
intentionally poisoned with false information. Something quite different
happened.

It turns out that certain military and high-ranking people were doing
research on the region that was going to be attacked, leaving a digital
trail that allowed the enemy to confirm that they had swallowed their
false information. They had found weaknesses in the chain of command and
would not fail to exploit them. Some of these people were stubborn and
had long careers, so they had no interest in leaving their high
positions, despite their successive mistakes. Thus, they suffered social
engineering attacks.

The social circle of key people was highly monitored to obtain all kinds
of information, identify weak points, etc. The danger of direct
confrontation and the fear of escalating the conflict were some of the
weaknesses identified in the enemy.

On August 14, they received a surprise attack that went around the
world. Their hasty response was discussed, very thoroughly in
important offices. They were losing credibility and power by leaps and
bounds. No-one believed in victory. They were pulling their money out of
stocks and banks, out of those places they thought so safe.

They were severely infected by parasites who lived off them and let
the cat out of the bag, waging internal struggles... The huge giant had
feet of clay.
