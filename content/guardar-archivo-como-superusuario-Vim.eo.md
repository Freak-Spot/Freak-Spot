Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2023-02-26 11:00
Image: <img src="/wp-content/uploads/2023/02/Vim-averto-ŝanĝo-de-nurlegebla-dosiero.png" alt="" width="737" height="133" srcset="/wp-content/uploads/2023/02/Vim-averto-ŝanĝo-de-nurlegebla-dosiero.png 737w, /wp-content/uploads/2023/02/Vim-averto-ŝanĝo-de-nurlegebla-dosiero-368x66.png 368w" sizes="(max-width: 737px) 100vw, 737px">
CSS: asciinema-player.css
JS: asciinema-player.js (top)
Lang: eo
Save_as: konservi-dosieron-kiel-ĉefuzanto-en-Vim/index.html
URL: konservi-dosieron-kiel-ĉefuzanto-en-Vim/
Slug: guardar-archivo-como-superusuario-vim
Tags: konsiloj, dd, Neovim, sudo, ĉefuzanto, ruzo, Vim
Title: Konservi dosieron kiel ĉefuzanto en Vim

Se vi uzas Vim-on aŭ Neovim-on, eble iufoje vi ne povis konservi la
modifojn, kiujn vi faris en dosiero, ĉar vi ne havis skribpermeson aŭ
la dosiero ne apartenis al vi.

<!-- more -->

La plej simpla opcio estas memori uzi `sudo` antaŭ redakti doserion, sur
kiu ni ne havas permesojn (ili kutimas esti sistemajn agordajn
dosierojn). Sed kiam ni jam forgesis `sudo` kaj faris multajn modifojn,
la jenan ruzon ŝparos al ni ĉagrenon kaj tempon.

Unue ni devas analizi la situacion. Se la redaktilo ne lasas al ni
konservi la dosieron, tio estas ĉar ni ne havas permeson modifi ĝin. Ni
povas klopodi plenumante `:w!`, sed se la dosiero apartenas al alia
uzanto kaj ni ne havas skribpermeson, ĝi ne funkcios.

Se ĝi lasis al ni legi la dosieran enhavon, tio kion ni povas fari estas
konservi ĝin uzante alian dosiernomon. En Vim ni povas fari tion
plenumante `:w alia_nomo`. Sed ĉi tiu solvo ne estas tute kontntiga, ĉar
ni poste devus plenumi novan komandon por anstataŭigi la dosieron, kiun
ni estas redaktanta: `!sudo mv alia_nomo %`[^1]

Estas maniero atingi nian celon pli facile: uzante la `dd`-programon.
Ni simple devas plenumi `:w !sudo dd of=%`. Ĉi tiu komando funkcias
tiel:

- `:w !`. Kiel la dokumentaro de Vim klarigas (`:h w_c`), la komandoj,
  kiuj sekvas al ekkria signo estas plenumitaj uzante la enhavon de la
  bufro (tio estas, la dosiero) kiel implicitan enigon.
- `sudo` ebligas al ni plenumi la komandon per ĉefuzantaj permesoj.
- `dd of=%` prenas kiel enigon la implicitan enigon kaj skribas ĝian
  enhavon en la dosiero, kiun ni estas redaktanta.

<asciinema-player src="/asciicasts/konservi-dosieron-kiel-ĉefuzanto-en-Vim.cast">
Pardonu, asciinema-player ne funkcias sen Ĝavoskripto.
</asciinema-player>

Post la plenumo de la antaŭa komando ni devas enigi nian pasvorton.
Poste Vim sciigos al ni, ke la dosiero ŝanĝis kaj ke la bufro ankaŭ
estis modifita. Ni simple devas premi <kbd>O</kbd> kaj,
poste, <kbd>Eniga klavo</kbd> por daŭrigi.

Ĉar la ruzo povas esti malfacile memorebla, estas bona ideo aldoni la
jenan linion al la agorda dosiero de Vim:

    ::vim
    cmap w!! w !sudo dd of=%<Enter>

Per ĝi nur necesas plenumi `:w!!` la sekvan fojon, kiam ni havu ĉi tiun
problemon.

[^1]: `%` estas la samo kiel meti la vojon de la dosiero, kiun ni estas
  redaktanta (plenumu `:h %` por pli da informo). La `!` je la komenco de
  la komando diras al Vim, ke ĝi plenumu komandon uzante la sisteman
  interpretilon (plenumu `:h :!` por pli da informo).
