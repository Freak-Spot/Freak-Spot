Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2023-12-06 22:00
Lang: de
Slug: cómo-hacer-llamadas-de-teléfono-anónimas
Tags: Handy, Bildung, Sicherheit, Tor, Tutorial
Save_as: wie-man-anonym-telefoniert/index.html
URL: wie-man-anonym-telefoniert/
Title: Wie man anonym telefoniert
Image: <img src="/wp-content/uploads/2023/11/mujer-al-móvil.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/11/mujer-al-móvil.png 1920w, /wp-content/uploads/2023/11/mujer-al-móvil-960x540.png 960w, /wp-content/uploads/2023/11/mujer-al-móvil-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

## Methode 1: Ausleihen eines Mobiltelefons

Eine einfache Methode, einen anonymen Anruf zu tätigen, besteht darin,
sich von jemandem auf der Straße ein Mobiltelefon zu leihen. Wenn du
freundlich fragst und die Person davon überzeugst, dass du dein Handy
nicht bei dir hast und dass es sich um einen Notfall handelt, wirst du
wahrscheinlich kein Problem haben. Wenn du doch ein Problem hast,
kannst du auch eine Gegenleistung für den Gefallen anbieten.

<div style="background-color: #aa0909; color: #000007">
<p>Du solltest dir bewusst sein, dass <strong>das Telefonsystem keine
Privatsphäre bietet</strong>. Du kannst verschlüsselte Anrufe über das
Internet und über virtuelle private Netzwerke oder Tor tätigen, um mehr
Privatsphäre zu haben.</p>
</div>

Anrufe können von Telefongesellschaften und Geheimdiensten aufgezeichnet
werden. Wenn du also ein hohes Maß an Privatsphäre benötigst, kannst
du deine Stimme verzerren. Wenn du befürchtest, dass die Person, die
dir das Telefon geliehen hat, Informationen über deine Identität
(Gesichtszüge, Größe usw.) an eine andere Person weitergibt, kannst du
dich schützen, indem du dein Aussehen, deine übliche Kleidung und deine
Körpergröße für den Anruf änderst (farbige Kontaktlinsen tragen,
Plateauschuhe oder Absätze tragen, dich rasieren oder einen Bart wachsen
lassen, deine Frisur ändern usw.). In Fällen, in denen du noch mehr
Privatsphäre benötigst, kannst du eine andere Person für dich anrufen
lassen.

<a href="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png">
<img src="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png" alt="" width="1920" height="821" srcset="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png 1920w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-960x410.png 960w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-480x205.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

## Methode 2: Anonyme Beschaffung einer SIM-Karte

In vielen Ländern muss man beim Kauf einer SIM-Karte seine persönlichen
Daten angeben. Es ist möglich, diese Beschränkung umzugehen, indem du
eine SIM-Karte auf dem Schwarzmarkt kaufst. Eine andere Möglichkeit
besteht darin, eine Person, die dich nicht kennt, davon zu überzeugen,
eine Karte gegen Geld für dich zu kaufen.

Wenn du die Karte in ein Feature-Phone oder ein Handy mit einem
sicheren Betriebssystem einlegst, kannst du Anrufe tätigen, die nicht
mit deiner wirklichen Identität verbunden sind. Auch hier solltest du,
wenn du hohes Maß an Privatsphäre benötigst, äußerst vorsichtig sein:
Vergiss nicht, deine Stimme zu verzerren, den Anruf von einem Ort aus,
den du nicht oft besuchst, zu tätigen, den Anruf aus deiner Anrufliste
zu löschen usw.<!-- more --> **Sei dich bewusst, dass du mit dieser
Methode ein Verbrechen begehen könntest**[^1].

[^1]: In Spanien zum Beispiel ist Identitätsdiebstahl eine Straftat.
