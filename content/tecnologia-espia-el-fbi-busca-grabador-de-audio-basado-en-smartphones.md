Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2016-08-16 04:20
Lang: es
Modified: 2017-02-27 01:53
Slug: tecnologia-espia-el-fbi-busca-grabador-de-audio-basado-en-smartphones
Status: published
Tags: 29 de julio de 2016, aplicación oculta, backdoor, espionaje, FBI, GPS, grabación de audio, grabación de vídeo, localización GPS, oferta pública, puerta trasera, smartphone, telecomunicaciones, teléfonos inteligentes, teléfonos móviles, Estados Unidos de América
Title: El FBI quiere una aplicación oculta para grabar audio y vídeo de «smartphones»

El FBI trata de conseguir mediante una [oferta
pública](https://www.fbo.gov/index?s=opportunity&mode=form&id=635322b7f9951b6cee336c66f1257043&tab=core&_cview=0)
una aplicación para teléfonos inteligentes que permita grabar audio y
vídeo de forma encubierta, almacenar los datos localmente, mandar
automáticamente los datos a la oficina central del FBI, mandar
grabaciones en vivo a otros agentes del FBI y mostrar la ubicación GPS
del «usuario».

El [apéndice](https://privacysos.org/app/uploads/2016/08/Attachment_RFI_DJF-16-1200-N-0007_Technical_Requirements.pdf)
del «Audio Recorder Technical Requirements» (así ha llamado el FBI a la
aplicación espía) describe dos escenarios en los que el FBI podría usar
la aplicación:

> «Para el primer escenario, la aplicación estaría precargada en el
> teléfono y funcionaría de forma oculta. Cuando la aplicación esté
> activa, la persona que controle el escenario será capaz de permitir la
> grabación remotamente. La grabación se almacenará en el teléfono, y
> simultáneamente el audio en directo será enviado (a través de la red
> móvil) a un servidor del Gobierno ubicado en Quantico, Virginia. El
> almacenamiento y la transferencia de datos ocurrirá automáticamente
> sin ninguna acción por parte del usuario».

Al final del informe, el FBI dice que también quiere que la aplicación
sea capaz de hacer grabaciones de vídeo ocultas.

El sistema permitirá a los agentes «enviar el audio, vídeo y/o datos GPS
sin almacenarlos en el teléfono o en el servidor». Más importante aún,
«mientras esté en el modo de grabación de audio en directo, el teléfono
que esté siendo usado como una grabadora oculta deberá tener un medio
para ocultar su actividad. El fabricante puede implementar esto como una
aplicación falsa o una aplicación oculta que requiera una entrada
estándar (como un patrón de toques o arrastres) para hacerla visible, o
cualquier otro método a su elección».
