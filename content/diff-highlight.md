Author: Jorge Maldonado Ventura
Category: Edición de Textos
Date: 2020-08-16
Lang: es
Slug: diff-highlight-un-mejor-diff-para-git
Tags: diff, diff-highlight, Git, GNU/Linux, interfaz de línea de órdenes
Title: diff-highlight: un mejor diff para Git
Image: <img src="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.png" alt="" width="1502" height="505" srcset="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.png 1502w, /wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar-751x252.png 751w, /wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar-375x126.png 375w" sizes="(max-width: 1502px) 100vw, 1502px">

El programa de [diff](https://es.wikipedia.org/wiki/Diff) predeterminado
de Git deja en ocasiones mucho que desear...

<!-- more -->

<a href="/wp-content/uploads/2020/08/git_diff-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_diff-ejemplo.png" alt="" width="838" height="705" srcset="/wp-content/uploads/2020/08/git_diff-ejemplo.png 838w, /wp-content/uploads/2020/08/git_diff-ejemplo-419x352.png 419w" sizes="(max-width: 838px) 100vw, 838px">
</a>

Claro, podemos ejecutar `git --word-diff` para ver mejor los cambios
concretos, pero eso nos hace pasar información por alto en ocasiones. En
este caso, por ejemplo, no muestra los espacios en blanco que se han
añadido delante de las etiquetas HTML.

<a href="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png" alt="" width="941" height="467" srcset="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png 941w, /wp-content/uploads/2020/08/git_word-diff_ejemplo-470x233.png 470w" sizes="(max-width: 941px) 100vw, 941px">
</a>

¿Y con otro programa diff? Veamos diff-highlight...

<a href="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png" alt="" width="859" height="705" srcset="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png 859w, /wp-content/uploads/2020/08/git-diff-highlight-ejemplo-429x352.png 429w" sizes="(max-width: 859px) 100vw, 859px">
</a>

Este último es mejor, ¿no? ¿Cómo se configura? Descubrí cómo hacerlo
en [una página web en la que solo entendía los
comandos](https://qiita.com/SakaiYuki/items/2814d417d0bec59046bb), lo
cual fue suficiente. Como ya sé cómo hacerlo, te explico.

Primero, hay que tener una versión de Git igual o superior a la 2.9...

    :::bash
    sudo apt update
    sudo apt install git
    git --version  # Debe ser igual o superior a la 2.9

Aunque ya tengas instalada la versión de Git necesaria, todavía hay que
configurar diff-highlight dándole permisos de ejecución y creando un
enlace simbólico en un directorio que esté en la variable de entorno
`$PATH`. En este caso, yo creo un enlace simbólico en
`/usr/local/bin/`...


    :::bash
    sudo chmod +x /usr/share/doc/git/contrib/diff-highlight/diff-highlight
    sudo ln -s /usr/share/doc/git/contrib/diff-highlight/diff-highlight /usr/local/bin/diff-highlight

Ahora podemos ejecutar `diff-highlight` cuando queramos, sin necesidad
de estar usando Git. Para que cada vez que ejecutemos `git diff` se use
este programa, debemos configurar el archivo `.gitconfig`, ya sea para
un proyecto concreto o para usarlo siempre. Como yo quiero usarlo
siempre, añado al archivo `~/.gitconfig` las siguientes líneas:

    :::config
    [core]
        pager = diff-highlight | less -r

Para mayor comodidad, paso la salida a less a través de una tubería.

¡Ya está! Si por alguna extraña razón tienes nostalgia por el viejo
diff, puedes volver a dejar el archivo `.gitconfig` como estaba.
