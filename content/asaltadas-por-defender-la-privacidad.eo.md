Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2018-07-04 19:36
Lang: eo
Slug: asaltadas-por-defender-la-privacidad
Tags: Germanio, ŝtato, organizacio, privateco, Zwiebelfreunde
Title: Registritaj pro defendi la privatecon

Anoj el la asocio [Zwiebelfreunde](https://www.zwiebelfreunde.de/),
pri la privateca defendo kaj edukado, estis viktimoj de policserĉado,
sen pravigebla kaŭzo. Estis serĉitaj la 20an de Junio la loko de la
asocio kaj la domoj de anoj de la plenumkomitato. Ankaŭ [serĉis la ejoj
de la Chaos Computer Club en
Aŭgsburgo](https://www.spiegel.de/netzwelt/web/hausdurchsuchungen-bei-netzaktivisten-chaos-computer-club-kritisiert-polizeivorgehen-a-1216463.html).

La pravigo prezentita de la ŝtata aŭtoritato estis retpaĝaro kiu incitis
al la agoj kontraŭe al la jara kunesto de la ekstremdekstra
partio [AfD](https://eo.wikipedia.org/wiki/Alternativo_por_Germanio).
Ĉi tio parto tute ne rilatas kun la asocio nek kun la serĉitaj personaj.
La supozita konekto estas la uzo de la retpoŝto-servilo de Riseup
(organizo nur ekonomike apogita de Zwiebelfreunde) [de la suspekta
retpaĝaro](https://augsburgfuerkrawalltouristen.noblogs.org/impressum/)
pri *estonta* kriminaleco.

La aktivuloj per la privateco estas ofte malrajte viktimoj de
invadoj al la privateco de la represia ŝtatuloj. La personaj
estis serĉitaj kiel atestantoj; traktitaj kiel krimuloj.
