Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-05-29 18:04
Lang: eo
Slug: acabemos-con-el-colonialismo-digital
Tags: cifereca koloniismo, malcentrigo, Usono, privateco, libera programaro, gvatado
Title: Ni ĉesigu la ciferecan koloniismon
Save_as: ni-ĉesigu-la-ciferecan-koloniismon/index.html
URL: ni-ĉesigu-la-ciferecan-koloniismon/
Image: <img src="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/05/absorbidos-por-el-ordenador.png 1920w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-960x540.png 960w, /wp-content/uploads/2023/05/absorbidos-por-el-ordenador-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

*Ni estas koloniitaj*.
La plej grandaj teknologiaj firmaoj, kun la kompliceco de Usono,
kontrolas la ciferecan mondon. La plejmulto de komputiloj (tiuj kiuj
uzas AMD- aŭ Intel-procesorojn) povas esti spionataj kaj fore
kontrolitaj danke al universala kaŝenirejo[^1], pere de la jura subteno
de la USA PATRIOT Act[^2]. Ni ankaŭ estas spionataj per poŝtelefonoj[^3],
gvatantaj filmiloj, per la banka sistemo, ktp.

## Ni estas la malamikoj

WikiLeaks[^4] kaj Edward Snowden[^5] montris al la mondo la gvatadon kaj
aliajn abomenaĵojn faritajn, precipe per la publikigo de konfidencaj
dokumentoj. La kreinto de WikiLeaks estas en malliberejo pro sia
raportado pri la situacio kaj Snowden estas ekzilita en Rusio. Sed vi ne
devas *krimi* por esti malliberigita aŭ persekutata, kiel bone scias la
programisto de liberaj programoj kaj defendanto de la privateco Ola
Bini[^6].

Se vi nun legas ĉi tiun artikolon, la (usona) Nacia Sekureca Agentejo
probable konsideras vin kiel ekstremisto, same kiel okazis al la
legintoj de <cite>Linux Journal</cite>[^7]. Bonŝance estas pli
GNU/Linuksaj ekstremistoj hodiaŭ. Firmaoj kiel Microsoft kutimis diri,
ke GNU/Linukso estis kancero[^8]; nun ili «amas Linukson»[^9]. Liberaj
programoj kaj privateco gajnis popularecon kaj adeptojn, sed la
plejmulto de la problemoj antaŭe menciitaj ankoraŭ ekzistas.

## Malkoloniigo

Kvankam la plejmulto de la homoj vivas koloniitaj fare de proprietaj kaj
spionadaj teknologioj, estas liberaj alternativoj, kiuj lasas al ni havi
pli da privatecon kaj kontrolon; estas projektoj de liberaj aparataroj
kaj liberaj programoj, kiuj donas al ni la liberecojn, kiujn la grandaj
firmaoj kaj registaroj rifuzas al ni.

Se ni volas akiri pli da privateco kaj libereco, ni ankaŭ devas [ĉesigi
firmaojn kiel Google](/eo/kiel-detrui-Google/), Meta, Apple kaj
Microsoft, kiuj neniel defendas la privatecon kaj liberajn programojn.
Por fari tion ni devas anstataŭigi iliajn spionajn kaj proprietajn
programojn per liberaj kaj malcentraj alternativoj. Ni ankaŭ devas
subteni projektoj de liberaj aparataroj.

Tamen la sistemo estas kontraŭ ni. En la plejmulto de la landoj oni uzas
la monon akiritan per impostoj por pagi permesilojn de spionaj programoj
kaj krei ciferecan kvatadan sistemon. Maniero batali kontraŭ tio estas
pagi kiel eble malplej impostoj (pagante per kontanta mono kaj uzante
anonimajn ĉifrovalutojn kiel Monero[^10], ekzemple) kaj denunci la
problemon.

La Roma Imperio ne disfalis en du tagoj. Nek la ĉinia nek la usona
cifereca imperio faros tion. Ni havas la povon krei kaj instigi
liberigajn elektojn.

[^1]: The Hated One (7-a de aprilo de 2019). <cite>How Intel wants to backdoor
    every computer in the world | Intel Management Engine
    explained</cite>. <https://inv.zzls.xyz/watch?v=Lr-9aCMUXzI>.
[^2]: Leiva A. (26-a de novembro de 2021). <cite>26 de octubre de 2001: George W. Bush firma la ley USA Patriot, o Patriot Act</cite>. <https://elordenmundial.com/hoy-en-la-historia/26-octubre/26-de-octubre-de-2001-george-w-bush-firma-la-ley-usa-patriot-o-patriot-act/>.
[^3]: Rosenzweig, A. (18-a de oktobro de 2017). <cite>Ne plu poŝtelefonoj</cite>.
    <https://freakspot.net/eo/sen-po%C5%9Dtelefonoj/>.
[^4]: Aŭtoroj de Vikipedio (15-a de majo de 2023). <cite>List of material
    published by WikiLeaks</cite>.
    <https://en.wikipedia.org/wiki/List_of_material_published_by_WikiLeaks>.
[^5]: BBC News Mundo (26-a de septembro de 2022). <cite>Snowden: Putin
    otorga la nacionalidad rusa al exagente estadounidense</cite>.
    <https://www.bbc.com/mundo/noticias-internacional-63039060>.
[^6]: Aŭtoroj de Vikipedio (1-a de februaro de 2023). <cite>Ola Bini</cite>. <https://es.wikipedia.org/wiki/Ola_Bini>.
[^7]: Rankin, K. (3-a de julio de 2014). <cite>NSA: Linux Journal is an
    "extremist forum" and its readers get flagged for extra
    surveillance</cite>.
    <https://www.linuxjournal.com/content/nsa-linux-journal-extremist-forum-and-its-readers-get-flagged-extra-surveillance>.
[^8]: Greene, T. C. (2-a de junio de 2001). <cite>Ballmer: 'Linux is a
    cancer'</cite>. <https://www.theregister.com/2001/06/02/ballmer_linux_is_a_cancer/>.
[^9]: Aŭtoroj de Vikipedio (7 de aprilo de 2023). <cite>Microsoft and open source</cite>.
    <https://en.wikipedia.org/wiki/Microsoft_and_open_source>.
[^10]: Maldonado Ventura, J. (20-a de februaro de 2023).
    <cite>Ĉifrovalutoj, anonimeco, sencentra ekonomio</cite>.
    <https://freakspot.net/eo/%C4%89ifrovalutoj-anonimeco-sencentra-ekonomio/>.
