Author: Jorge Maldonado Ventura
Category: Edición de imágenes
Date: 2016-07-11 04:57
Lang: es
Modified: 2017-02-25 21:23
Slug: como-agrego-bordes-a-un-texto-con-gimp
Status: published
Tags: bordes, bordes en texto, imagen, imágenes, GIMP, GIMP 2.8.10, GNU/Linux, Trisquel, Trisquel Belenos, Trisquel 7
Title: ¿Cómo Agrego bordes a un texto con GIMP?

1.  **Teniendo seleccionada una capa de texto, hacemos click derecho con
    el ratón y clicamos en _Ruta a partir del texto_**.<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-065305.png"><img class="aligncenter size-full wp-image-105" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-065305.png" alt="Captura de pantalla de 2016-07-11 06:53:05" width="762" height="578" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-065305.png 762w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-065305-300x228.png 300w" sizes="(max-width: 762px) 100vw, 762px" /></a>
2.  **Creamos una nueva capa transparente y la ponemos bajo la capa del
    texto**.<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-063703.png"><img class="aligncenter size-full wp-image-106" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-063703.png" alt="Captura de pantalla de 2016-07-11 06:37:03" width="197" height="88"></a>
3.  **Aumentamos el tamaño de la selección a partir de la ruta**. Para
    ello, clicamos en **Seleccionar&gt;A partir de ruta** y después en
    **Seleccionar&gt;Agrandar.** La agrandaremos lo que estimemos
    oportuno.<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540.png"><img class="aligncenter size-full wp-image-107" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540.png" alt="Captura de pantalla de 2016-07-11 06:45:40" width="1175" height="521" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540.png 1175w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540-300x133.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540-768x341.png 768w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064540-1024x454.png 1024w" sizes="(max-width: 1175px) 100vw, 1175px"></a>
4.  **Colorea la selección**. Coge la **herramienta de relleno**
    (**<kbd>Mayús</kbd>+<kbd>B</kbd>**), selecciona el color que quieras
    utilizar y clica en la selección de los bordes de las letras (no te
    olvides del borde
    interior de algunas letras).<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747.png"><img class="aligncenter size-full wp-image-108" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747.png" alt="Captura de pantalla de 2016-07-11 06:47:47" width="1918" height="1015" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747.png 1918w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747-300x159.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747-768x406.png 768w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-11-064747-1024x542.png 1024w" sizes="(max-width: 1918px) 100vw, 1918px" /></a>
