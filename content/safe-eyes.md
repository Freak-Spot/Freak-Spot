Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2019-11-10
Lang: es
Slug: descansa-los-ojos-de-vez-en-cuando-con-Safe-Eyes
Tags: ojos, programa, Safe Eyes, salud, Trisquel, Trisquel 8
Title: Descansa los ojos de vez en cuando con Safe Eyes
Image: <img src="/wp-content/uploads/2019/11/safeeyes.png" alt="">

Si pasas muchas horas seguidas delante del ordenador, puede que
experimentes los problemas del [síndrome visual informático](https://es.wikipedia.org/wiki/S%C3%ADndrome_visual_inform%C3%A1tico):

- Astenopía
- ojos secos, irritados o enrojecidos
- Poliopía (visión doble)
- dificultad para reorientar los ojos
- doleres de cabeza, espalda y cuello
- cansancio

Para evitar estos síntomas basta con tomar descansos a menudo. Sin
embargo, es complicado acordarse de cuánto tiempo has pasado sin
descansar cuando estás concentrado trabajando.  El programa Safe Eyes
nos ayuda a resolver el problema.

<a href="/wp-content/uploads/2019/11/ejercicio-de-safe-eyes.png">
<img src="/wp-content/uploads/2019/11/ejercicio-de-safe-eyes.png" alt="Safe Eyes pone la pantalla negra y dice «Cierra fuertemente tus ojos»">
</a>

<!-- more -->

Safe Eyes es un programa para gestionar los descansos frente al
ordenador. Cuenta con muchas funcionalidades que nos ayudan a adaptarlo
a nuestras necesidades.

Para instalar Safe Eyes, basta con ejecutar las siguientes instrucciones
en sistemas operativos basados en Ubuntu, como Trisquel:

    :::bash
    sudo add-apt-repository ppa:slgobinath/safeeyes
    sudo apt update
    sudo apt install safeeyes

Se puede instalar también con pip:

    :::bash
    sudo pip install safeeyes

Si hemos instalado Safe Eyes con nuestro gestor de paquetes, estará
habilitado por defecto y nos hará tomar descansos. Los descansos
muestran una pantalla negra con unas instrucciones que debemos seguir
para relajar la vista.

En el área de notificación  veremos el icono del programa y podremos
configurarlo a nuestro gusto desde el menú de **Ajustes**.

<figure>
<a href="/wp-content/uploads/2019/11/safe-eyes-área-de-notificación.png">
<img src="/wp-content/uploads/2019/11/safe-eyes-área-de-notificación.png" alt="Safe Eyes en el área de notificación">
</a>
    <figcaption class="wp-caption-text">Safe Eyes en el área de
    notificación de Trisquel 8</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2019/11/complementos-de-safe-eyes.png">
<img src="/wp-content/uploads/2019/11/complementos-de-safe-eyes.png" alt="">
</a>
    <figcaption class="wp-caption-text">Ajustes de complementos de Safe Eyes</figcaption>
</figure>
