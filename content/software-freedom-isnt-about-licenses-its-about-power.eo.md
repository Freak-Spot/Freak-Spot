Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2022-01-13
Modified: 2023-07-27 13:00
Lang: eo
Save_as: la-libereco-de-programaro-ne-temas-pri-permesiloj-temas-pri-povo/index.html
URL: la-libereco-de-programaro-ne-temas-pri-permesiloj-temas-pri-povo/
Slug: la-libertad-de-software
Tags: permesiloj, libera programaro, privateco
Title: La libero de programaro ne temas pri permesiloj &mdash; temas pri povo

Limiga licenca uzanta interkonsento estas maniero, per kiu kompanio
povas uzi ĝian povon super la uzanto. Kiam la movado de la libera
programaro estis fondita antaŭ 30 jaroj, ĉi tiuj limigaj permesiloj estis
la ĉefaj povrilatoj kontraŭaj al la uzanto, do la permesaj kaj rajtocedaj
permesiloj naskiĝis kiel sinonimoj de libereco de programaro. La licencado
gravas; oni perdas la uzantan aŭtonomecon per abonmodeloj, revokeblaj
permesiloj, programaro nur duuma kaj pezaj leĝaj klaŭzoj. Tamen ĉi tiuj
problemoj, kiuj trafas la labortablajn programojn estas nur eta parto de
la hodiaŭaj ciferecaj povrilatoj.

Nuntempe firmaoj uzas povon super siaj uzantoj per spurado, vendante
datumojn, per psikologian manipuladon, ĝenajn reklamojn, planitan rompiĝon kaj
malamikajn programojn de cifereca administrado de «rajtoj» ([angle] DRM).
Ĉi tiuj problemoj trafas ĉiun ciferecan uzanton, teknike aŭ ne teknike
kapablaj, kaj en la labortabloj kaj en aptelefonoj.

La movado de la libera programaro promesis korekti ĉi tiujn problemojn
per liberaj permesiloj en la fontkodo, argumentante adeptoj, ke la
permesiloj donas imunecon kontraŭ tiaj malicaj programoj, ĉar uzantoj
povis modifi la kodon. Bedaŭrinde la plejparto de la uzantoj malhavas la
rimedojn por fari tion. Dum la plej aĉaj malobservoj de la uzanta
libereco venas el firmaoj, kiuj eldonas proprietan programaron, ĉi tiuj
malbonoj restas nekorektitaj eĉ en malfermitkodaj programoj, kaj ne ĉiu
proprieta programaro montras ĉi tiujn problemojn. La moderna retumilo
estas nome libera programo, kiuj havas la triopon de telemetrio,
reklamoj kaj DRM; malnova videludo estas proprieta programo, sed
relative sendanĝera.

Pro tio ne sufiĉas rigardi la permesilon. Eĉ ne sufiĉas konsideri la
permesilon kaj fiksan aron de problemoj propraj de proprietaj programoj;
la kunteksto gravas. La programoj ne estas izolaj. Same kiel proprietaj
programoj kutimas integri aliajn proprietajn programojn, liberaj programoj
kutimas integri aliajn liberajn programojn. Programa libereco
*kuntekste* bezonas puŝeton al programoj direkte al la intereso de la
uzantoj, anstataŭ al firmaaj interesoj.

Tiam kiel ni devus koncepti la programan liberecon?

Konsideru la tri adeptojn al libera programaro kaj malfermitkoda
programaro: hobiuloj, firmaoj kaj aktivuloj. Individuaj aktivuloj zorgas
pri alĝustigi iliajn preferatajn programojn, substrekante libere
licencitan fontkodon. Tiuj aferoj ne trafas al kiu ne igas modifi
fontkodon ria ŝatokupo. Ne estas *io malbona* en tio, sed ĝi neniam
estos hejma problemo.

Siaflanke grandaj firmaoj diras ami «malfermitkodan programaron». Ne,
ili ne zorgas pri la socia movado, nur pri la kosta malpliigo atingita
utiligante permese licencitaj programoj. Tiu firmaa emfazo en licencado
estas kutime kontraŭ la programa libereco en la pli granda kunteksto.
Fakte estas ĉi tiu ironio, kiu motivas la programan liberecon preter la
permesiloj.

Estas la aktivula kies spirito devas apliki al ĉiu senrigarde al la
teknika kapablo aŭ financa stato. Ne estas manko de malfermitkodaj
programoj, kutime de firmaa deveno, sed ĉi tio ne sufiĉas &mdash; estas
la povrilato kontraŭ kiu ni devas batali.

Ni ne estas solaj. La programa libereco estas ligita al nuntempaj sociaj
problemoj, inkluzive de aŭtorrajta reformo, privateco, daŭripovo kaj
Interreta dependeco. Ĉiu problemo estiĝas kiel malbona povrilato inter
firmaa programa autoro kaj la uzanto, kun komplika interagoj kun
programa licencado. Sennodigi ĉiujn problemojn el permesiloj donas kadron
por trakti nuancitajn temojn de politika reformo en la cifereca epoko.

Aŭtorrajta reformo ĝeneraligas la manierojn de la movadoj de libera
programaro kaj de libera kulturo. Fakte la liberaj permesiloj povigas nin
libere uzi, adapti, remiksi kaj kunhavigi kaj verkojn kaj programojn.
Tamen proprietaj permesiloj troe administrante la kernon de homa komunumo
kaj kreemecon estas destinitaj al malsukceso. Proprietaj permesiloj havis
malgrandan sukceson eviti la dismultiĝon de la kreaj verkoj, kiujn ili
strebis «protekti», kaj la rajtoj adapti, remiksi verkojn estis longe
uzitaj de dediĉitaj fanoj de proprietaj verkoj, kreante multon da
fanfikcio kaj fanarto. La sama rimarko koncernas al programoj:
proprietaj licencaj akordoj por finaj uzantoj haltis nek
dosierinterŝanĝon nek retroprojektadon. Fakte unika kreema fanmondo
ĉirkaŭ proprieta programaro aperis en komunumoj de videluda modifado.
Senrigarde de la leĝaj problemoj, la homa imago kaj spirito de kunhavigi
restas. Pro tio ni ne devas taksi neniun laŭ proprietaj programoj kaj
verkoj en ria vivo; anstataŭe ni devas labori por aŭtorrajta reformo kaj
libera licencado por protekti ilin kontraŭ aŭtorrajtaj troatingoj.

Privatecaj zorgoj estas ankaŭ tradiciaj en la diskurso de libera
programaro. Veraj, sekuraj komunikadaj programoj neniam povas esti
proprietaj, pro la eblo de kaŝenirejoj kaj la neeblo de travideblaj
ekzamenoj. Bedaŭrinde la kontraŭo fiaskas: estas libere licencitaj
programoj, kiuj esence kompromitas uzantan privatecon. Konsideru
klientojn de ekstera liveranto por centra neĉifrita retbabila sistemo.
Kvankam du uzantoj de tia kliento private mesaĝante unu la alian uzas
nur libera programaro, se iliaj mesaĝoj estas datumanalizitaj, estas
ankoraŭ damaĝo. La bezono de kunteksto estas ree substrekita.

Daŭripovo estas aperanta problemo, kiu rilatas al la programa libereco
per la krizo de elektronika rubo. En la poŝtelefona mondo, kie
post kelkaj jaroj malaktualigitaj aptelefonoj estas la kutimo kaj litiaj
baterioj akumulas en rubejo senfine, ni vidas la paradokson de libere
licencita operaciumo kun malbonegaj sociaj antecedentoj. Kurioza sekvo
estas la bezono de liberaj peliloj. Kie proprietaj peliloj malaktualigas
aparatojn baldaŭ post la vendisto forlasas ilin pro nova produkto,
liberaj peliloj permesas longatempan prizorgadon. Kiel antaŭe, licencado
ne sufiĉas; la kodo ankaŭ devas esti prizorgita kaj akcepti kontribuojn.
Simple publikigi fontkodon estas malsufiĉa por solvi la problemon de
elektronika rubo, sed estas antaŭkondiĉo. En risko estas la rajto de la
posedanto de aparato daŭri uzi aparaton, kiun ri jam aĉetis, eĉ post la
produktanto ne plu volas subteni ĝin. Dezirita de la aktivuloj pri
klimato kaj la konsciaj pri la konsumo, ni ne povas permesi, ke la
programoj superregu ĉi tiun rajton.

Preter aŭtorrajta, privateca kaj daŭripova problemoj, neniu programo
povas vere esti «libera», se la teknologio mem alkatenas nin,
stultigante nin kaj kolerigante nin pro klakoj. Danke al televida
kulturo disvastiĝante al Interreto, la normala civitano devas pli timi
de ri mem ol de la registara subaŭskultado. Por ĉiu ĉifrita mesaĝo
malĉifrita de sekreta servo, miloj de mesaĝoj estas volante dissenditaj
al la publiko, serĉante tuja kontentiĝo. Kial devus firmao aŭ registaro
strebi spioni niajn privatajn vivojn, se ni montras ilin kiel donacoj.
Fakte popularaj malfermitkodaj realigoj de malvirta teknologio ne
signifas sukceson, problemo montrita de respondoj per liberaj programoj
al sociaj retejoj. Ne, eĉ sen proprieta programaro, centriĝo aŭ kruela
psikologia manipulado, la multiĝo de sociaj retejoj ankoraŭ endanĝerigas
la societon.

Entute koncentriĝi pri konkretaj problemoj de liberecoj de programoj
permesas nuancojn, anstataŭ la tradicia vidpunkto de du ebloj. Finaj
uzantoj povas fari pli informitajn decidojn, sciante la teknologiajn
kompromisojn preter la permesilo. Programistoj akiras ilon por kompreni
kiel iliaj programoj adaptas al la ĝenerala bildo, ĉar libera permesilo
estas necesa sed nesufiĉa por garantii programan liberecon hodiaŭ.
Aktivuloj povas dividi kaj konkeri.

Multaj ekster nia agokampo komprenas kaj zorgas pri tiuj aferoj;
longatempa sukceso bezonas ĉi tiujn aliancanojn. Asertoj de morala
supereco per permesiloj estas senbazaj kaj stultaj; ne estas sukceso perfidante niajn
amikojn. Anstataŭ nuanca metodo pligrandigas nian atingon. Kvankam
abstraktaj moralaj filosofioj povas esti intelekte validaj, ili estas
neatingeblaj por ĉiuj krom studentemuloj kaj la plej dediĉitaj
subtenantoj. Abstraktaĵoj estas eterne en la marĝena politiko, sed tiuj
konkretaj problemoj jam estas komprenitaj de la ĝenerala publiko. Krom,
ni ne povas limigi nin al teknikaj publiko; kompreni topologion de reto
ne povas esti antaŭkondiĉo de privataj konversacioj. Tro emfazi la rolon
de fontkodo kaj malpli ol necese emfazi la aktivajn povrilatojn estas
komdamnita strategio; dum jardekoj ni penis kaj fiaskis. En
post-Snowden-a mondo, estas multaj aferoj riskitaj por pli da fiaskoj.
Reformi la specifajn problemojn preparas la vojon al programa libereco.
Fine, socia ŝanĝo estas pli malfacile ol skribi kodon, sed per sinsekva
socia reformo, permesiloj iĝas la plej facila parto.

La nuanca analizo eĉ helpas individuaj aktivuloj pri programa libereco.
Puremaj klopodoj por tute rifuzi malliberan teknologion estas laŭdindaj,
sed ekster fermita komunumo, iri kontraŭ la plimulto igas la aktivulon
lacega. Tage, dungantoj kaj lernejoj ĉiam postulas proprietajn
programojn, iam uzitaj por faciligi gvatadon. Nokte, popularaj ŝatokupoj
kaj sociaj konektoj hodiaŭ estas peritaj de diskutebla programaro, el la
DRM en videludo al la gvatado de konversaciojn kun amikaro. Rompi
rilatojn kun amikoj kaj forlasi memzorgadon kiel *antaŭkondiĉo* por
batali fortajn organizojn ŝajnas nobla, sed estas vana. Eĉ sen
politikoj, restas teknikajn defiojn por uzi nur liberajn programojn.
Aldoni aliajn problemojn, aŭ eĉ vivi sen poŝtelefono, nur pligrandigas
riskon de laciĝi de programa libereco.

Praktike, ĉi tiu metodo por programa libereco igas videblaj diversajn
problemojn kun la moderna reto sonigante alarmon en la komunumo de
libera programaro. La tradicia problemo estas proprieta Ĝavoskripto,
licenca problemo, kvankam konsideri nur Ĝavoskriptan licencadon kreas
kaj malprecizajn kaj neĝustajn konkludojn pri retaj «programoj». Al pli
profundaj problemoj apartenas senbridaj reklamoj kaj spurado; Interreto
estas la plej granda spurada reto en la homa historio, plejparte pro
komercaj celoj. Parte ĉi tiuj problemoj estas malgrandigitaj de
skriptaj, reklamaj kaj sekvadaj blokiloj; tiuj ĉi povus esti
antaŭinstalitaj en retumilo por redukti damaĝon celante al pli afabla
Reto. Tamen la fatala eraro de la Reto estas eĉ pli fundamenta. Tiel
planita, kiam la uzanto iras al URL, ria retumilo plenumas *iun ajn*
kodon, kiu venas el la drato. Efekte la reto signifas aŭtomata
ĝisdatigo, sendepende de la permesilo de la kodo. Eĉ se la kodo estas
bona, ankoraŭ estas post ĉiu jaro pli kosta por plenumi, devigante
aparatara ĝisdatiga ciklo, kiu malaktualigas malnovajn aparatarojn, kiu
funkcius, se la Reto ne estus ŝvelita de firmaaj interesoj. Pli subtila
afero estas la «atenta ekonomio» ligita al la Reto. Kvankam estas
malfacile iĝi dependa de legi en nurteksta retumilo, manie vidi
DRM-televidon estas alia afero. Malmulte ambiciaj progresoj kiel la
«legilan vido» estas limigitaj de la ironia distribuo de dokumentoj en
apbazaro. En la Reto, diversaj problemoj de DRM, devigita ĝisdatigo,
privateco, daŭripovo kaj psikologiaj manipulaj desegnoj kuniĝas en unu
malplej bona situacio por programa libereco. La permesiloj estis nur la
komenco.

Tamen estas kialoj por optimismo. Bone celante, la batalo por la
libereco de programoj estas venkebla. Por batali por libereco de
programoj, batalu por privateco. Batalu por aŭtorrajta reformo. Batalu
por daŭripovo. Rezistu psikologiajn manipulajn desegnojn. En la kerno de
ĉiu estas batalo por programa libereco &mdash; batalu plu kaj ni povos
venki.

*Ĉi tiu artikolo estas traduko de la angla artikolo «[Software
freedom isn’t about licenses – it’s about
power.](https://rosenzweig.io/blog/software-freedom-isnt-about-licenses-its-about-power.html)»
publikigita de Alyssa Rosenzweig sub la <a
href="https://creativecommons.org/licenses/by-sa/4.0/deed.eo"><abbr
title="Creative Commons Atribuite-Samkondiĉe 4.0 International">CC
BY-SA 4.0</abbr></a>-permesilo*.
