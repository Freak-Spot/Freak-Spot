Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2017-03-19 00:32
Lang: es
Slug: instalar-y-actualizar-tor-browser-de-forma-sencilla
Tags: Bash, Deep web, interfaz de línea de órdenes, navegador, privacidad, Tor, Tor Browser
Title: Instalar y actualizar Tor Browser de forma sencilla

[Tor Browser](https://www.torproject.org/projects/torbrowser.html.en) es
un navegador que contiene todo lo necesario para navegar por Internet de
forma anónima.

Instalar Tor Browser es bastante sencillo: basta con descargarlo de la
[página oficial](https://www.torproject.org) y ejecutar el lanzador
`Tor Browser` (es el archivo que se encuentra dentro del directorio que
obtienes tras descargar y descomprimir Tor Browser). Sin embargo,
actualizarlo no lo es tanto, puesto que tienes que volver a descargarlo
de nuevo cada vez que aparece una nueva versión.

Para solucionar este problema, [Micah Lee](https://micahflee.com) creó
[Tor Browser Launcher](https://github.com/micahflee/torbrowser-launcher).
Gracias a este programa se puede instalar y actualizar Tor Browser con el
gestor de paquetes de tu distribución de GNU/Linux. Existen paquetes para
Ubuntu, Debian y Fedora, según dice el README del proyecto.

Si siempre quieres tener la última versión del paquete `torbrowser-launcher`
antes de que tu distribución la tenga, puedes ejecutar las siguientes
instrucciones:

    :::bash
    sudo add-apt-repository ppa:micahflee/ppa
    sudo apt-get update
    sudo apt-get install torbrowser-launcher

Con estas instrucciones se añade el [<abbr title="Pesonal Package
Archive">PPA</abbr>](https://es.wikipedia.org/wiki/Archivo_de_Paquete_Personal)
necesario, se actualiza la lista de repositorios y se instala el paquete
`torbrowser-launcher`.

Tras la instalación debe aparecer Tor Browser en el menú de inicio o lanzador
de aplicaciones. Desde ahí podrás ejecutar Tor Browser cómodamente. Si lo
prefieres, también puedes ejecutar `torbrowser-launcher` desde la terminal.
