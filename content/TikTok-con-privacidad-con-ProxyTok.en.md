Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2023-02-06 01:44
Lang: en
Slug: TikTok-con-privacidad-con-ProxiTok
Save_as: TikTok-privately-with-ProxiTok/index.html
URL: TikTok-privately-with-ProxiTok/
Tags: TikTok
Title: TikTok privately with ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

TikTok is a centralised social network and requires the use of
proprietary software. It is practically impossible to query TikTok with
the browser in a decent way without losing privacy or freedom... unless
you use another interface, such as
[ProxyTok](https://proxitok.pabloferreiro.es/), which I describe in this
article.

The ProxiTok interface is simple. You can go to trending content,
discover users and search by username, tag, TikTok URL, music identifier
and video identifier.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Search by username</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png">
<img src="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png" alt="" width="1917" height="942" srcset="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png 1917w, /wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok-958x471.png 958w, /wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok-479x235.png 479w" sizes="(max-width: 1917px) 100vw, 1917px">
</a>
    <figcaption class="wp-caption-text">Profile of <a href="https://proxitok.esmailelbob.xyz/@a_commoner">@a_commoner</a>. It can also be followed by RSS.</figcaption>
</figure>

The project was launched on 1 January 2022, so it is quite young and
more features are expected to be added. To automatically redirect to
ProxiTok you can install the [LibRedirect
extension](https://libredirect.github.io/), which also prevents other
privacy-harmful websites.

There are [several public
instances](https://github.com/pablouser1/ProxiTok/wiki/Public-instances),
and it is possible to install one on your own server.
