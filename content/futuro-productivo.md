Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2018-02-18 19:25
Lang: es
Slug: futuro-productivo
Tags: capitalismo, distopía, futuro, productividad, prosa, realidad virtual, software privativo, tecnología
Title: Futuro productivo

Hoy se levanta a las ocho de la mañana con el estridente despertador de
su *asistente virtual*. Desayuna la comida que le prepara y se desplaza
al trabajo en su *coche inteligente*.

<!-- more -->

No obstante, tiene que despertarse siempre más temprano de lo habitual,
pues los coches inteligentes suelen alterar un poco la ruta para mejorar
la experiencia de las usuarias. Esto tiene la ventaja de que te permiten
ver siempre los nuevos anuncios que se encuentran en la calle y comprar
algún producto antes de llegar al destino.

Esta vez ha ido a parar a una tienda, donde compra rosquillas.
«Últimamente, me cuesta más moverme», piensa mientras mastica la
rosquilla, «quizás debería cambiar mis preferencias y usar alguna
aplicación para hacer ejercicio».

Ella tiene el nivel 6, lo cual no está nada mal. Gracias a su rango
tiene un respetable trabajo de psicóloga, más relajado que otros. El
trabajo en sí es bastante agobiante, pero es respetado socialmente. Sin
duda es mucho mejor que los niveles menores. El peor de todos es el 1,
pues los del 1 se ven encerrados casi siempre hasta el final de sus
vidas en *centros de adaptación y reinserción social*.

Mientras mastica no despega ni un momento la vista del móvil. Antes de
empezar a comer hace una foto de la rosquilla, como de costumbre, para
compartirla en las redes sociales. Si eres activa, es más probable
conseguir seguidoras, y quizás subir así de rango. También consulta las
redes sociales para saber qué acontece en el mundo.

Las redes sociales, aunque diferentes entre sí, son guiadas por la misma
corporación que controla sus algoritmos. La gente sueña con conocer los
algoritmos para ganar influencia y ascender niveles. Cuanto más alto el
nivel, mayor es la influencia y el poder adquisitivo.

Termina el tentempié, comparte y etiqueta una última noticia, se dirige
en coche al trabajo. Durante el trayecto, suele visualizar alguna
película o entrar en algún mundo de realidad virtual, gracias a la
suscripción que les permiten a los del nivel 6. Sin embargo, son algo
molestos algunos de los cuatro o cinco anuncios que siempre le
interrumpen durante el trayecto, pues su suscripción es normal.
Necesitaría al menos el nivel 8 para tener una sin anuncios.

Al fin, el coche le comunica que ha llegado a su destino. Son las 09:00,
y se dispone a entrar a la clínica. Saluda como siempre nada más llegar
a su secretaria de nivel 5.

Su trabajo es algo monótono y aburrido, pero es fácil de llevar. Además,
desde que ascendió de nivel tiene una ayudante que le ayuda en su
labor. Los tiempos de consulta son estresantes y exigen celeridad. La
mayoría de las pacientes son muchachas antiautoritarias, desmotivadas,
violentas o que tienen algún otro problema para encajar en la sociedad.
Casi nunca tiene éxito con sus pacientes y se ve obligada a derivarlas a
una *autoridad educadora*, que tiene métodos más contundentes que los
típicos diagnósticos, planificaciones y las recomendaciones de sus
sesiones protocolarias.

Ella no sabe exactamente cuáles son los métodos utilizados por la
autoridad educadora. Sabe que reciben algún medicamento o algún «periodo
de aislamiento y reflexión» en un centro de adaptación y reinserción
social, que antes se conocía como «estancia en institución
penitenciaria». Muchos términos han cambiado para adaptarse a los nuevos
tiempos.

Termina las sesiones. Han ido como de costumbre, excepto cuando una
paciente le pidió que la dejarán libre y luego la insultó. Esta chica es
una salvaje de Rivero Verda, donde habían limpiado el terreno de árboles
y construido un nuevo núcleo urbano. Había desobedecido la ley
enfrentándose al Estado y la habían llevado encadenada a su clínica,
donde ante la falta de tiempo y de progreso la ha derivado a una
autoridad educadora. No le gusta nada tratar con gente salvaje y
violenta; casi nunca tienen solución. «Ojalá ascienda al nivel 7 para
que me permitan trabajar mi jornada completa telemáticamente», pensaba.

Sale del trabajo y sube al coche. Esta le vez le recomienda ir a un
nuevo restaurante de comida rápida, donde la gente come a la vez que
lee mensajes instantáneos y usa las redes sociales. El Internet ahí
es rapidísimo, así que hay muchas personas que han ido para que les
den de comer de forma intravenosa mientras juegan por Internet a
diversos juegos de realidad virtual, que requieren siempre mucho ancho
de banda.

De repente, mientras come en ese restaurante, se fija en un anuncio
recomendado por su red social favorita sobre una carrera de robots en el
estadio Labirinto. Por desgracia, no le llega con sus ingresos para
verla. Pero, por suerte, puede obtener el dinero que le falta haciendo
unas horas extra.

Vuelve a casa. Son las 14:00. Le pide los nuevos productos que necesita
a su asistente virtual. El asistente virtual funciona muy rápido y tiene
multitud de características, pero cada semana suele necesitar alguna
actualización o mejora, que no suelen ser baratas.

Más tarde realizará tres horas extra para obtener el dinero que necesita
para adquirir la entrada de la carrera de robots.

Tras jugar un rato a un juego de realidad virtual, vuelve a trabajar.
Esta vez puede hacerlo desde casa, pues por la tarde su empresa solo
ofrece sesiones por videoconferencia desde una aplicación para
*dispositivos inteligentes*.

Terminadas las horas extra, tiene suficiente dinero para comprar la
ansiada entrada. Pero está cansada y apenas tiene tiempo para escribir
algo en las redes sociales al final del día. Como consecuencia, ha
perdido tres seguidoras. No son muchas, pero ganar seguidoras de nuevo
requiere tiempo.

Llega la hora de dormir. Se agobia un poco pensando qué publicar o
compartir para volver a conseguir seguidoras; si pierde unas pocas más,
bajará de rango y tendrá un peor trabajo. «No quiero ni recordar mi
anterior trabajo de secretaria», piensa.

Al fin, descansa. Tiene un sueño desagradable en el que es degradada al
nivel 5. Se despierta a media noche, por el impacto de la pesadilla.
Toma una pastilla tranquilizante, vuelve a la cama. Se consuela en que
disfrutará este domingo viendo la carrera de robots y cierra los ojos.
