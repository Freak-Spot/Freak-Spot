Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-10-31 08:00
Lang: pt
Slug: consultar-twitter-con-privacidad-y-software-libre
Save_as: consultar-Twitter-com-software-livre-e-privacidade-com-Nitter/index.html
URL: consultar-Twitter-com-software-livre-e-privacidade-com-Nitter/
Tags: software livre, privacidade, programa, redes sociais, Twitter, página web
Title: Consultar o Twitter com <i lang="en">software</i> livre e privacidade
Image: <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">

O Twitter é uma rede social centralizada que exige o uso de <i lang="en">software</i>
privativo. É quase impossível consultar o Twitter com o navegador de
forma aceitável sem perder privacidade ou liberdade... a não ser que
usemos uma outra interface, como o [Nitter](https://nitter.net/), que eu
descrevo neste artigo.

Eu acho que o seu nome é acrónimo de <em><strong>n</strong>ot
tw<strong>itter</strong></em> (em português «não Twitter»). Tanto faz. O
facto é que funciona bem e a interface é leve, impede que o Twitter
obtenha o teu endereço IP, podes personalizar a sua aparência, tem os
seus próprios fontes RSS e é responsivo.

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png">
    <img src="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png" alt="Searching 'free software' with Nitter" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-cuentas-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-cuentas-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Podemos pesquisar por contas</figcaption>
</figure>

<figure>
  <a href="/wp-content/uploads/2019/11/Nitter-cuenta.png">
  <img src="/wp-content/uploads/2019/11/Nitter-cuenta.png" alt="Conta do Twitter vista com o Nitter" width="1438" height="840" srcset="/wp-content/uploads/2019/11/Nitter-cuenta.png 1438w, /wp-content/uploads/2019/11/Nitter-cuenta-719x420.png 719w" sizes="(max-width: 1438px) 100vw, 1438px">
  </a>
  <figcaption class="wp-caption-text">Podemos consultar contas</figcaption>
</figure>

<figure>
   <a href="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png">
     <img src="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png" alt="Aqui excluímos os retuítes dos resultados" width="1000" height="774" srcset="/wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre.png 1000w, /wp-content/uploads/2019/11/Nitter-filtrar-resultados-software-libre-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">E podemos filtrar Tweets</figcaption>
</figure>

Como é <i lang="pt">software</i> livre, podes instalar o Nitter no teu
servidor (se o tiveres) ou utilizar o [Nitter de outras pessoas](https://github.com/zedeus/nitter/wiki/Instances).

Achas desajeitado mudar os URLs do Twitter para os URLs do Nitter? A
extensão [Privacy Redirect](https://addons.mozilla.org/pt-PT/firefox/addon/privacy-redirect/?src=search) para o Firefox converte ligações do Twitter em
ligações para instâncias do Nitter. Também substitui as ligações do
 YouTube por ligações do [Invidious](/pt/YouTube-com-privacidade-com-o-Invidious/).
