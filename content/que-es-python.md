Author: Jorge Maldonado Ventura
Category: Python
Date: 2016-07-10 01:16
Lang: es
Slug: que-es-python
Status: published
Tags: .odp, alto nivel, características de Python, diapositivas, estilo Python, filosofía de Python, Guido Van Rossum, imperativo, lenguage de programación, multiparadigma, programación, Python, significado Python, ¿Qué es Python?
Title: ¿Qué es Python?

Python es el nombre de un lenguaje de programación muy popular. Te
recomiendo leer estas
[diapositivas](/wp-content/uploads/2016/07/Presentación-Python3.odp)
si quieres conocer las características de este lenguaje.

Si no tienes un programa capaz de leer el formato `.odp`, puedes ver las
diapositivas en formato `.pdf`
[aquí](/wp-content/uploads/2016/07/python.pdf).
