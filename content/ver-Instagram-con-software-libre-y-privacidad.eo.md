Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-11-28
Lang: eo
Slug: consultar-instagram-con-software-libre-y-privacidad
Save_as: Instagram-per-libera-programaro-kaj-privatece/index.html
URL: Instagram-per-libera-programaro-kaj-privatece/
Tags: Bibliogram, Instagram, privateco
Title: Instagram per libera programaro kaj privatece
Image: <img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">

Instagram estas centra socia reto, kiu bezonas la uzado de proprieta
programaro. Estas preskaŭ maleble uzi Instagramon sen forlasi vian
privatecon aŭ liberecon... krom se ni uzas alian fasadon, kiel
[Bibliogramon](https://bibliogram.art/), pri kiu mi skribas en ĉi tiu
artikolo.

Bibliogram lasas vidi uzantajn profilojn, afiŝojn kaj
[IGTV](https://en.wikipedia.org/wiki/IGTV)-videojn simple.
En tiu retejo estas serĉo-formularoj por afiŝoj kaj uzantoj.

<!-- more -->

<figure>
<a href="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png" alt="" width="1299" height="711" srcset="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png 1299w, /wp-content/uploads/2020/02/página-principal-de-Bibliogram-649x355.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Ĉefpâgo de Bibliogram</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Profilo en Bibliogram</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png" alt="" width="1920" height="1038" srcset="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png 1920w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-960x519.png 960w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-480x259.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Afiŝo en Bibliogram</figcaption>
</figure>

Ĉar ĝi estas libera programaro, vi povas instali Bibliogramon en via servilo
(se vi havas ĝin) aŭ uzi la [publikajn nodojn de aliuloj](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md).
