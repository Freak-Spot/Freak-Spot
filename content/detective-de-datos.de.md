Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2023-01-29 20:00
Lang: de
Slug: cómo-recoger-información-de-uso-de-usuarios-de-redes-sociales-ingeniería-social
Save_as: Wie-man-Menschen-über-soziale-Netzwerke-untersucht/index.html
URL: Wie-man-Menschen-über-soziale-Netzwerke-untersucht/
Tags: soziale Manipulation, Facebook, Instagram, soziale Netzwerke, TikTok, Twitter
Title: Wie man Menschen über soziale Netzwerke untersucht
Image: <img src="/wp-content/uploads/2023/01/secreto-mujer.png" alt="" width="1200" height="844" srcset="/wp-content/uploads/2023/01/secreto-mujer.png 1200w, /wp-content/uploads/2023/01/secreto-mujer-600x422.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Nur wenige Computerdetektive achten auf die Nutzungsmuster sozialer
Medien, die tiefe Wünsche, Stimmungen usw. offenbaren können. Der
Algorithmus der sozialen Netzwerke<!-- more -->[^1] tut dies. Wie können wir ihn also
bei der Untersuchung einer Person nutzen?

Wenn man eine Person in sozialen Netzwerken recherchiert, muss man auch
Informationen über ihre Nutzung sammeln, indem man ihr mit einem
isolierten Konto folgt. Bei dieser Methode empfiehlt der Algorithmus
Dinge, die mit den Interessen, der Stimmung usw. der Person
zusammenhängen, was er oder sie nicht einmal veröffentlicht hat.

[^1]: Auf der anderen Seite gibt es einige soziale Netzwerke wie
    [Mastodon](https://joinmastodon.org/de), die keinen Empfehlungsalgorithmus haben und keine großen
    Mengen an Nutzerdaten sammeln.


<img src="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg" alt="" width="6016" height="4000" srcset="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg 6016w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-3008x2000.jpg 3008w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-1504x1000.jpg 1504w" sizes="(max-width: 6016px) 100vw, 6016px">

Die IP-Adresse und die technischen Daten[^2] einer Person zu erhalten, ist
recht einfach: du musst sie nur zu einer URL auf einem deiner Server
leiten, und dazu musst du sie dazu bringen, auf einen Link zu klicken
oder ein Bild oder Video zu laden, das du zu diesem Zweck bereithältst,
was nicht schwer ist, wenn du ihre Interessen kennst. Wenn wir ihre
E-Mail haben, können wir ein [Zählpixel (d. h. ein unsichtbares Bild)
verwenden.

[^2]: Von wo aus er auf das Internet zugreift, welches Gerät
  er benutzt, welche Sprachen er eingestellt hat, welche Browser-
  und Betriebssystemversionen er verwendt usw.

Wenn wir mehr Informationen erhalten wollen, müssen wir die
HTTP-Anfragen der Person, gegen die ermittelt wird, einholen. Manchmal
können Daten von Internet-Providern oder aus dem [Versteckten Web](https://de.wikipedia.org/wiki/Deep_Web) gekauft
werden, manchmal haben staatliche Sicherheitskräfte oder Geheimdienste
Zugang zu diesen Daten. Auch [Social Engineering](https://de.wikipedia.org/wiki/Social_Engineering_(Sicherheit)) könnte in der realen
Welt eingesetzt werden, indem Daten von Websites gekauft werden, die die
Person nutzt, um ihre echte Telefonnummer zu erhalten usw.

Die einzige Möglichkeit, sich vor missbräuchlichen sozialen Netzwerken
und dem Zugriff von Hacker, Regierungen usw. auf deine Daten zu
schützen, besteht darin, sie nicht zu nutzen. Du kannst die Risiken
verringern, indem du sie nur eingeschränkt nutzt: ohne JavaScript (was
viele missbräuchliche soziale Netzwerke nicht zulassen); über [Tor](https://de.wikipedia.org/wiki/Tor_(Netzwerk)), indem
du nur postest oder nur liest; indem du nicht auf Beiträge klickst, die
deine Aufmerksamkeit erregen; indem du nicht sagst, was dir gefällt;
indem du immer die gleiche Zeit auf jedem Beitrag verbringst, um keine
Vorliebe für einen der Beiträge zu zeigen (was kompliziert ist) …

Kurz gesagt, es ist einfach, Menschen zu erforschen, die keine Nutzer
sind, sondern benutzt werden, da sie das Produkt von Unternehmen sind,
die ihr Leben, ihre Wünsche, ihre Stimmungen usw. kommerzialisieren. Die
technologische Souveränität kann erhöht werden, indem man eine eigene
Website betreibt, anstatt sich auf ein soziales Netzwerk zu verlassen,
oder indem man ein freies soziales Netzwerk nutzt, das die
Privatsphäre der Nutzer respektiert.
