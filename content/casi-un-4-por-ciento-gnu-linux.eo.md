Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2024-02-01 23:00
Lang: eo
Slug: casi-un-4-por-ciento-de-los-ordenadores-de-escritorio-usa-gnulinux
Save_as: preskaŭ-4-elcento-de-la-hejmaj-komputiloj-uzas-GNU-Linukson/index.html
URL: preskaŭ-4-elcento-de-la-hejmaj-komputiloj-uzas-GNU-Linukson/
Tags: statistiko, GNU/Linukso
Title: Preskaŭ 4 % de la hejmaj komputiloj uzas GNU/Linukson
Image: <img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png" alt="" width="1139" height="563" srcset="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png 1139w, /wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024-569x281.png 569w" sizes="(max-width: 1139px) 100vw, 1139px">

Laŭ datenoj de Statcounter GNU/Linukso estas uzata en 3,77&nbsp;% de la
hejmaj komputiloj (tekokomputiloj kaj tablaj komputiloj)<!-- more -->[^1].
En la grafiko ni povas vidi, ke en Januaro de 2021 la GNU/Linuksa
elcento estis 1,91&nbsp;%, kio signifas, ke en tri jaroj ĝia populareco
duobliĝis. Indas ankaŭ mencii, ke la elcento de Chrome OS (kiu uzas la
kernon Linukson) estas 1,78&nbsp;%.

<a href="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png">
<img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png" alt="">
</a>

Tamen tiu statistiko ne estas plene fidinda, ĉar la spurilo de
Statcounter estas instalita nur en 1,5 milionoj da retejoj[^2]. Krome,
kelkaj uzantoj de GNU/Linukso &mdash; tiuj, kiuj pli zorgas pri la
privateco &mdash; uzas ilojn, kiuj ŝanĝas la [`User-Agent`](https://en.wikipedia.org/wiki/User-Agent_header) (ekzemple Tor Browser, kiu ŝajnigas la uzadon de Windows por pli
bone kamufli sin).

Ĉiuokaze temas pri impresa kreskado, kiu verŝajne daŭros en la sekvaj
jaroj, ĉar GNU/Linukso estas pli kaj pli uzata en lernejoj kaj multaj
landoj klopodas pliigi sian teknologian suverenecon (GNU/Linukso estas
la plej malmultekosta kaj sekura maniero fari tion).

[^1]: <https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-200901-202401>
[^2]: «<i lang="en">Our tracking code is installed on more than 1.5 million sites
    globally</i>»: tio estas kio ili diras en <https://gs.statcounter.com/faq#methodology>.
