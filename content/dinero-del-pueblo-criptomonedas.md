Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2023-02-20 18:00
Modified: 2023-05-08 17:00
Lang: es
Slug: dinero-anónimo-digital-monero
Tags: dinero, Monero, privacidad, economía
Title: Criptomonedas, anonimato, economía descentralizada
Image: <img src="/wp-content/uploads/2023/02/no-compre-Monero-pegatina.png" alt="" width="827" height="1181" srcset="/wp-content/uploads/2023/02/no-compre-Monero-pegatina.png 827w, /wp-content/uploads/2023/02/no-compre-Monero-pegatina-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">

Mucha gente piensa que las criptomonedas son anónimas, pero no es así.
La mayoría de ellas son pseudoanónimas, pues las transacciones se
pueden rastrear y, así, descubrir quién posee la <!-- more -->cartera[^1].

Lo ideal sería que por defecto las criptomonedas funcionaran de forma
anónima para evitar el control de la población por parte del gobierno y
empresas; es decir, que fueran algo parecido al dinero en efectivo. ¿Por
qué es tan importante la privacidad de las transacciones?

Si las transacciones son públicas, cualquier empresa o persona puede
saber lo que has gastado y cómo lo has gastado; pueden descubrir dónde
vives, qué te gusta, etc. Además, el dinero que recibes o das puede
estar manchado (si ha sido anteriormente usado por
criminales).
Cualquiera con conocimientos técnicos puede rastrear las
transacciones[^2]. Hay empresas que han bloqueado transacciones por este
motivo[^3].

El dinero debe ser
[fungible](https://es.wikipedia.org/wiki/Bien_fungible), es decir, una
moneda de un euro debe valer lo mismo que otra moneda de un euro. Los
Bitcoin no son así, porque dejan un rastro y hay gente que preferiría no
aceptar dinero proveniente de actividades criminales, que valdría menos.

Las criptomonedas, sin embargo, han supuesto una revolución que ha
permitido realizar transacciones de forma descentralizada sin la
necesidad de un banco central y existen algunas que sí que ofrecen
anonimato de verdad.

## Monero: una criptomoneda anónima de verdad

<a href="/wp-content/uploads/2023/02/monero-symbol-480.png">
<img src="/wp-content/uploads/2023/02/monero-symbol-480.png" alt="">
</a>

[Monero](https://www.getmonero.org/es/index.html) es la criptomoneda más
popular que permite transacciones privadas, de forma descentralizada y
de bajo costo. A diferencia de la mayoría de las criptomonedas, Monero
no deja un rastro con el que se puedan identificar las transacciones,
a los receptores ni a quienes pagan: todo se registra en la cadena de
bloque de forma anónima.

Esto, unido a su gran comunidad de desarrolladores, criptógrafos,
traductores, etc., hace que Monero sea una de las criptomonedas más
utilizadas.

## Alto coste energético

Las criptomonedas como Monero y Bitcoin funcionan con un algoritmo
llamado prueba de trabajo, que garantiza la seguridad de la red, pero
que también supone un alto costo energético. Existe otro algoritmo
llamado [prueba de
participación](https://es.wikipedia.org/wiki/Prueba_de_apuesta), que
tiene un menor costo energético, pero que tiene otros problemas.

Quienes se oponen a las criptomonedas suelen criticar su alto coste
energético, pero no se suele hablar tanto del coste energético del
sistema bancario actual (equipos informáticos de bancos, cajeros,
oficinas, etc.).

## ¿La moneda del pueblo?

Hay quienes afirman que las criptomonedas son el dinero del pueblo, pues
no se depende de bancos centrales controlados por gobiernos, que suelen
imprimir dinero y así hacer que disminuya el valor de este. Al contrario,
las criptomonedas se crean con poder computacional validando
transacciones, es decir, no se crean de la nada y tienen una utilidad
práctica. Sin embargo, minar criptomonedas está fuera del alcance de
personas sin conocimientos técnicos o sin equipos informáticos
adecuados.

Las criptomonedas suelen comportarse de manera similar al oro o a la
plata, pues se emiten muy pocas criptomonedas nuevas, con lo que en la
práctica hace que muchas sean deflacionarias.

## ¿Por qué no se usan más?

Las criptomonedas se usan actualmente un montón: se realizan millones de
transacciones al día. Claro, no las usa todo el mundo, pues no son
fáciles de usar para mucha gente y no existe mucho incentivo para
gastarlas. Si tienes algo que va a aumentar o conservar su valor y algo
que va a perderlo, ¿qué prefieres gastar? Obviamente te quedas con lo
que va a aumentar o conservar su valor (oro, algunas criptomonedas) y
cambias lo que va a perder valor ([dinero por
decreto](https://es.wikipedia.org/wiki/Dinero_por_decreto), comúnmente
llamado dinero <i lang="lt">fiat</i>) antes de que lo pierda. Este fenómeno
es lo que se conoce como la [ley de Gresham](https://es.wikipedia.org/wiki/Ley_de_Gresham).

## ¿Cómo comprar criptomonedas de forma anónima?

Se debe elegir una plataforma descentralizada y libre como
[Bisq](https://bisq.network/es/) o realizar los intercambios en persona con
dinero en efectivo, sin dejar rastro con una transferencia bancaria.

Existen plataformas que facilitan mucho la compra de
criptomonedas. Sin embargo, algunas no te dejan enviar
las criptomonedas a tu propia cartera, lo que sería como tener dinero en
un banco que no te deja sacarlo y solo te deja intercambiarlo por dinero
por decreto. Ahí es adonde suelen ir los especuladores novatos. Si se
usa una plataforma centralizada &mdash;no recomendado para la
privacidad&mdash;, lo mejor es elegir una plataforma que te deje retirar
el dinero a una cartera, que está bajo tu control y no del de una
empresa.

## No pagar impuestos

Cuando comercias con dinero en efectivo puedes evitar pagar impuestos,
con criptomonedas anónimas como Monero igual.

<a href="/wp-content/uploads/2023/02/no-compre-Monero-pegatina.png">
<img src="/wp-content/uploads/2023/02/no-compre-Monero-pegatina.png" alt="" width="827" height="1181" srcset="/wp-content/uploads/2023/02/no-compre-Monero-pegatina.png 827w, /wp-content/uploads/2023/02/no-compre-Monero-pegatina-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">
</a>

[^1]: Aunque se pueden usar mezcladores de transacciones para blanquear
    criptomonedas y cubrir el rastro del dinero.
[^2]: Léase artículo
    [«¿Cómo los gobiernos rastrean las transacciones con cripto para combatir el crimen?»](https://es.beincrypto.com/como-gobiernos-rastrean-transacciones-cripto-para-combatir-crimen/).
[^3]: Léase artículo [«Bitcoin's Fungibility Graveyard»](https://sethforprivacy.com/posts/fungibility-graveyard/).
