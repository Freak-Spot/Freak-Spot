Author: Jorge Maldonado Ventura
Category: Senkategoria
Date: 2022-07-18 16:00
Modified: 2022-10-16 19:57
Lang: eo
Slug: deklaracio-de-sendependenco-de-la-Retlando
Tags: Interreto
Title: Deklaracio de sendependenco de la Retlando
Image: <img src="/wp-content/uploads/2022/07/retlando.png" alt="" width="960" height="427" srcset="/wp-content/uploads/2022/07/retlando.png 960w, /wp-content/uploads/2022/07/retlando-480x213.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Registaroj de la Industria Mondo, pezecaj gigantoj de karno kaj ŝtalo,
mi venas de Retlando, la nova hejmo de Menso. En la nomo de la
estonteco, mi petas al vi de la pasinteco, lasu nin trankvilaj. Vi ne
estas bonvenaj ĉe ni. Vi ne havas suverenecon, kie ni kunvenas.

Ni ne havas registaron elektitan, nek ni verŝajnas havi iun, do mi
alparolas vin per ne pli granda aŭtoritato ol tiu, per kiu libereco mem
ĉiam parolas. Mi deklaras ke la tutmonda sociala spaco, kion ni
konstruas, estu nature sendependa de la tiranecoj, kiujn vi klopodas
trudi al ni. Vi ne havas moralan rajton regi nin, nek vi posedas iun
metodon de perforto, kiun timi ni havas realan kialon.

Registaroj derivas siajn justajn potencojn de la konsento de la regatoj.
Vi nek petis nek ricevis la nian. Ni ne invitis vin. Nek vi konas nin,
nek vi konas nian mondon. Retlando ne sin trovas ene de viaj
landlimoj. Ne kredu ke vi povas konstrui ĝin, kvazaŭ ĝi estus publika
konstruprojekto. Vi ne povas. Tio estas akto de la naturo kaj ĝi kreskas
per niaj kolektivaj agadoj.

Vi ne partoprenis en nia granda kaj kolektiĝanta konversacio, nek vi
kreis la riĉecon de niaj merkatoj. Vi ne konas nian kulturon, nian
etikon aŭ la neskribitajn kodojn kiuj jam provizas al nia socio pli
multe da ordo ol atingiteblus per iuj ajn da viaj fitrudoj.

Vi pretendas ke ekzistas problemoj inter ni kiujn vi devas solvi. Tiun
aserton vi pretekstas pro invadi nian zonon. Multaj de tiuj problemoj ne
ekzistas. Kie ekzistas realaj konfliktoj, kie ekzistas malpravoj, ni
identigos ilin kaj solvos ilin per niaj rimedoj. Ni formas nian propran
socialan kontrakton. Tiu rego leviĝos laŭ kondiĉoj de nia mondo, ne de
via. Nia mondo estas malsama.

Retlando konsistas de transakcioj, rilatoj kaj penso mem, aranĝitaj
kiel staranta ondo en la reto de niaj komunikaĵoj. La nia estas mondo,
kiu sin trovas ambaŭ ĉie kaj nenie, sed ne kie loĝas korpoj.

Ni kreas mondon, kien ĉiuj rajtas eniri sen privilegio aŭ antaŭjuĝo
akordita pro raso, ekonomia potenco, milita forto aŭ naskiĝloko.

Ni kreas mondon, kie iu ajn, ie ajn rajtas esprimi siajn kredojn,
egale kiel neordinarajn, sen timo iĝi devigita al silentado aŭ
konformeco.

Viaj juraj konceptoj de posedaĵo, esprimo, identeco, movado kaj kunteksto
ne aplikeblas al ni. Ĉiuj ĉi baziĝas sur materio, kaj ĉi tie ne ekzistas
materio.

Niaj identecoj ne havas korpojn, do, malkiel vi, ni ne povas atingi ordon
per fizika devigado. Ni kredas, ke de etiko, de klerigita memintereso
kaj de la komuna boneco, nia registaro altiĝos. Niaj identecoj eblas
disiĝataj trans multaj de viaj jurisdikcioj. La sola leĝo, kiun ĉiuj de
niaj konstituciaj kulturoj ĝenerale agnoskus estas la Ora Regulo. Ni
esperas, ke ni povu konstrui niajn apartajn solvojn sur tiu bazo. Sed ni
ne povas akcepti la solvojn kiujn vi klopodadas altrudi.

En Usono, vi hodiaŭ kreis leĝon, la Akto de Reformacio de
Telekomunikacioj, kiu forkonsentas vian propran konstitucion kaj
insultas la viziojn de Jefferson, Washington, Mill, Madison,
DeToqueville kaj Brandeis. Tiuj vizioj nun devas naskiĝi denove en ni.

Vi estas terurataj de viaj propraj idoj, ĉar ili estas indiĝenoj en unu
mondo, kie vi ĉiam estos enmigrantoj. Ĉar vi timas ilin, vi komisias
viajn burokratarojn kun gepatraj respondumoj kiujn alfronti mem vi tro
malkuraĝas. En nia mondo ĉiuj sentimentoj kaj esprimoj de homeco, de
maldignaĵo al anĝelaĵo, estas partoj de senkudra tuto, la tutmonda
konversacio de bitoj. Ni ne povas apartigi la aeron, kiu sufokas de la
aero, sur kiu batas flugiloj.

En Ĉinio, Germanio, Francio, Rusio, Singapuro, Italio kaj Usono vi
penadas forbalai la viruson de libereco per establi gardpostenojn ĉe la
bordoj de Retlando. Tiuj forteneblas infektadon dum tempeto, sed ili
ne funkcios en mondo, kiu baldaŭ litkovrilatos sub bit-pluportantaj
ilaroj.

Viaj pliigante arkaikaj informaĵindustrioj daŭrigus sin mem per proponi
leĝojn, en Usono kaj aliloke, kiuj pretendas posedi paroladon mem tra la
tuta mondo. Tiuj leĝoj deklarus, ke ideoj estu alia industriprodukto ne
pli nobla ol krudfero. En nia mondo, kion ajn la homa menso kreus, eblas
reproduktati kaj distribuati senfine senkoste. La tutmonda transportado
de penso ne plu postulas viajn fabrikojn por atingi.

Tiaj pliigante malamikaj kaj koloniaj rezolucioj metas nin en la sama
pozicio kiel tiuj antaŭaj amantoj de libereco kaj memdeterminado, kiuj
devis malakcepti aŭtoritatojn de foraj, malinformataj potencoj. Ni devas
deklari niajn virtualajn memestantojn imunaj kontraŭ via suvereneco, eĉ
dum ni daŭradas konsenti vian regulon super niaj korpoj. Ni disvatiĝos
trans la planedon, tiel ke neniu povu aresti niajn pensojn.

Ni kreos civilizacion de la menso en Retlando. Estu ĝi pli humana kaj
justa ol la mondo, kiun viaj registaroj faris antaŭe.

Davos, Svisio
Februaro 8-a, 1996

*La antaŭa teksto estas traduko de <cite style="font-style: normal"><a
href="https://www.eff.org/cyberspace-independence">A Declaration of the
Independence of Cyberspace</a></cite> publikigita de John Perry Barlow sub la
<a href="https://creativecommons.org/licenses/by/3.0/us/deed.eo"><abbr title="Creative Commons Atribuite 3.0 Usono">CC BY 3.0 US</abbr>-permesilo</a>.*
