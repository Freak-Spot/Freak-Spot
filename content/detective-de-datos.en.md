Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2023-01-28 21:00
Lang: en
Slug: cómo-recoger-información-de-uso-de-usuarios-de-redes-sociales-ingeniería-social
Save_as: how-to-investigate-people-through-social-networks/index.html
URL: how-to-investigate-people-through-social-networks/
Tags: social engineering, Facebook, Instagram, social networks, TikTok, Twitter
Title: How to investigate people through social networks
Image: <img src="/wp-content/uploads/2023/01/secreto-mujer.png" alt="" width="1200" height="844" srcset="/wp-content/uploads/2023/01/secreto-mujer.png 1200w, /wp-content/uploads/2023/01/secreto-mujer-600x422.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Few cyberspace detectives pay attention to the usage patterns of social
networks, which can reveal deep desires, moods, etc. The social network
algorithm <!-- more -->[^1] does. But how to take advantage of it when investigating a person?

[^1]: On the other hand, there are some social networks such as
    [Mastodon](https://joinmastodon.org/) that do not have a
    recommendation algorithm and do not collect huge amounts of user
    data.

When investigating a person using social networks, it is necessary to
also collect usage information, and this can be done by following them
with an isolated account. With this method, the algorithm will recommend
things related to their interests, their mood, etc., things that the
person has not even published.

<img src="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg" alt="" width="6016" height="4000" srcset="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg 6016w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-3008x2000.jpg 3008w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-1504x1000.jpg 1504w" sizes="(max-width: 6016px) 100vw, 6016px">

Getting the IP address and technical data[^2] of the person is quite
easy: just guide them to a URL on a server that you have, and to do this
you have to make them click on a link or load an image or video that you
have for that purpose, which is not difficult if you know their
interests. If we have their email, we can use a [web
beacon](https://en.wikipedia.org/wiki/Web_beacon) (i.e. an invisible image).

[^2]: Where you access the Internet from, what device you use, what
  languages you have configured, what browser and operating system
  versions you use, etc.

If we want to get more information, we have to get the HTTP requests of
the person under investigation. Sometimes data can be bought from
Internet providers or the [deep web](https://en.wikipedia.org/wiki/Deep_web), sometimes government
security forces or intelligence agencies have access to this data. Also,
[social engineering](https://en.wikipedia.org/wiki/Social_engineering_(security))
could be used in the real world, buying data from sites the person uses
to get their real phone number, etc.

The only way to protect yourself from abusive social networking sites
and access to your information by hackers, governments, etc., is not to
use them. You could reduce the risks by using them in a limited way:
without JavaScript (which many abusive social networks do not allow),
through [Tor](https://en.wikipedia.org/wiki/Tor_(network)), only posting or only reading, not clicking on posts that
catch your attention, not saying what you like, always spending the same
amount of time on each post so as not to show a preference for any of
them (which is complicated), etc.

In short, it is easy to investigate people who are not users, but are used,
as they are the product of companies that commercialise their lives,
their desires, their moods, etc. Technological sovereignty can be
increased by having your own website, instead of relying on a social
network, or by using a free social network that respects users' privacy.
