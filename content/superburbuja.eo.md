Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2022-02-04 22:00
Lang: eo
Slug: superburbuja
Save_as: vezikego/index.html
URL: vezikego/
Tags: veziko, disfalo, malcentrigo, teknologio
Title: Vezikego: ĉu teknologia, ekonomia kaj socia ŝanĝo?
Image: <img src="/wp-content/uploads/2022/02/stock-crash.jpg" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/02/stock-crash.jpg 1024w, /wp-content/uploads/2022/02/stock-crash-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Estas vezikego eksplodota: la prezo de loĝejoj kreskis multe, la akcioj
estas trovalorigitaj, la manĝaĵoj estas pli multekostaj, la petrola prezo
multe altiĝis. Kio okazos, kiam la veziko eksplodu?

Iuj diras, ke la industria socio disfalas; aliaj pensas, ke baldaŭ
okazos [depresio](https://eo.wikipedia.org/wiki/Depresio_(ekonomiko)).
Gvidi la sistemon al senfina ekonomia kreskado dum ni vivas en planedo
kun limigitaj resursoj estas io absurda. Tamen la kapitalisma sistemo
dependas de ĉi tiu kreskado por supervivi. Iuj proponas kolonii la
kosmon por eviti la kapitalisman disfalon.

En mallonga periodo kreskos la malriĉeco. Tiuj kiuj ne havas
proprietaĵojn, kiuj kreas riĉecon (aktivoj), devos vendi ilian
laborforton kontraŭ pli malgranda prezo (se ili trovas laboron, kiam la
senlaboreco kresku), ŝteli aŭ dependi de almozoj. Tio estas pli facila
por tiuj kiuj havas kapitalon aŭ proprietaĵojn, kie kultivi manĝaĵon,
aliron al trinkebla akvo kaj loĝejon. Ĉu tio povas esti longtempe okazo
por instigi aliajn tipojn de valoroj?

La kreskiĝo de la temperaturoj jam kaŭzis fermitciklajn regilojn, kiuj
kaŭzas pli grandan altigon de temperaturoj (kio kreskigas la ekstremajn
meteorologiajn eventojn, la marnivelon, la dezertigadon, etc.).
Ekzemple, kiam la polusoj fandas, malpli da sunradiado reflektas, do
altiĝas la temperaturo; kiam la glacia tavolo degelas, metano liberiĝas
(forceja gaso) en la atmosfero; kiam la arbaroj kaj la koralaj rifoj
malaperas, malpli da CO<sub>2</sub> estas sorbita.

Estas teknologioj, kiuj favoras la akumuligon de kapitalo en malmultaj
manoj kaj signifas egan energian koston, kaj estas teknologioj
malcentraj, liberaj kaj efikaj. La problemoj, kiuj ni havas ja ne
estas nur teknologiaj, sed la teknologio pliigas ilin. Ĉu favoros la
eksplodo de vezikego teknologian, ekonomian kaj socian ŝanĝon?

Nenio restas eterne.

<small>[Bildo *Coit Tower fresco - the stock crash*](https://live.staticflickr.com/1/679924_59bcaf5217_b.jpg) de Zac Appleton.</small>
