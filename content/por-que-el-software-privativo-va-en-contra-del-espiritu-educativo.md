Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2016-08-29 00:53
Modified: 2020-02-01 12:04
Lang: es
Slug: por-que-el-software-privativo-va-en-contra-del-espiritu-educativo
Status: published
Tags: educación, Richard Stallman, software libre, software privativo
Title: ¿Por qué el <i lang="en">software</i> privativo va en contra del espíritu educativo?

El <i lang="en">software</i> privativo no forma parte del conocimiento de la humanidad,
porque ni siquiera se puede estudiar: permanece secreto y restringido.

La cooperación con el <i lang="en">software</i> privativo también está restringida. No
puedes compartir sin incumplir la ley. Al no poder estudiar el código
fuente, no se pueden realizar modificaciones ni averiguar cómo funciona.

La compañía de <i lang="en">software</i> o el particular es quien tiene el poder; no el
usuario. En cualquier momento pueden dejar de desarrollar la tecnología,
deshacerse de ella, subirle el precio, utilizarla para espiarte... En
resumen, dejas de tener el control: estás en manos de la buena voluntad
de una empresa.

Si nos centramos en el aprendizaje de programación, por ejemplo,
observamos que con <i lang="en">software</i> privativo es prácticamente imposible. Los
programas privativos impiden la colaboración de los usuarios y
programadores en su diseño y desarrollo. Por lo tanto, no puedes hacer
siquiera una pequeña modificación a un programa. No se aprende a
programar realizando ejercicios simples y sin sentido, sino modificando
<i lang="en">software</i> y colaborando con proyectos reales.

Las empresas de <i lang="en">software</i> privativo tienen mucho interés en hacer llegar
sus productos a las escuelas para inculcar una dependencia hacia ellos,
por eso muchas veces los dejan gratis o a un precio muy reducido. No
hacen un favor a los alumnos, al contrario. Cuando a un alumno le
inculcan una dependencia hacia el <i lang="en">software</i> privativo, le están haciendo
débil frente a corporaciones cuyo único fin es aumentar sus beneficios a
cualquier precio. Los alumnos que quieran aplicar lo poco que han
aprendido tendrán que gastar mucho dinero para poder utilizar productos
a los que dichas empresas den soporte; aún así, nunca sabrán cómo
funcionan.

La conclusión que podemos alcanzar es que el <i lang="en">software</i> privativo es
completamente incompatible con la buena educación y con la libertad de
los alumnos.

> También porque las escuelas deben enseñar el espíritu de
> buena voluntad, el hábito de ayudar a los demás a tu alrededor,
> cada clase debería tener esta regla: estudiantes, si traéis
> <i lang="en">software</i> a la clase no podéis guardároslo para vosotros.
> —Richard Stallman
