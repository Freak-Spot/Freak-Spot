Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2020-04-04
Lang: es
Slug: rastreo-exclusión-y-censura-con-la-excusa-del-covid19
Tags: brecha digital, COVID-19, España, Jami, Jitsi, Madrid, software libre, software privativo, teletrabajo, virus
Title: Rastreo, exclusión y censura con la excusa del COVID-19
Image: <img src="/wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses.jpg" alt="" width="1024" height="731" srcset="/wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses.jpg 1024w, /wp-content/uploads/2020/04/1024px-Electron_micrograph_of_two_coronaviruses-512x365.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

La excusa del [COVID-19](https://es.wikipedia.org/wiki/COVID-19) se está utilizando mucho para invadir la
privacidad y censurar los puntos de vista críticos mediante el uso de
tecnologías digitales de empresas que pretenden sacar el máximo
beneficio de la pandemia.

La cuarentena impuesta ha dejado a gente encerrada (sin empleo o con
teletrabajo) y a estudiantes sin clases, pues aunque dicen que hay clases
digitales, mucha gente no tiene Internet y los puntos de acceso públicos
están cerrados. Los estados no hacen nada para solucionar la brecha
digital.

Las empresas tecnológicas están utilizando la situación para blanquear
su imagen y obtener dinero público, como [han advertido ya muchos
expertos](https://www.eldiario.es/tecnologia/privacidad-gobierno-iniciativas-tecnologicas-coronavirus_1_1013127.html).
En España encontramos un caso de estos en el pago de dinero público a
empresas privadas para desarrollar una página web y aplicaciones móviles
en la Comunidad de Madrid cuyos objetivos son recopilar datos de sus
ciudadanos y mantenerlos en manos de empresas privadas. Estos recursos
ofrecen información que ya se encuentra por doquier en Internet, es
decir, no aportan nada útil.

Si su objetivo hubiera sido facilitar un autodiagnóstico por Internet e
informar al mayor número de personas, no habrían puesto como condición
previa introducir datos personales, la página funcionaría sin
JavaScript para ser más accesible (un ciego, por ejemplo, encontrará
dificultades para usar la página) y no habría elementos de rastreo de
Google en el código fuente. Los datos personales que recogen son el
nombre, apellidos, número de teléfono móvil, DNI, fecha de nacimiento,
correo electrónico, dirección completa, código postal, género y
geolocalización. Estos datos privados están ahora en las manos de
Google, Telefónica y Ferrovial, quienes aparecen en la
[política de
privacidad](https://web.archive.org/web/20200413131420/https://coronavirus.comunidad.madrid/proteccion-de-datos)
como «proveedores y colaboradores».

Las grandes empresas tecnológicas [se han puesto también de acuerdo para
censurar los puntos de vista
diferentes](https://www.20minutos.es/noticia/4133023/0/facebook-twitter-y-google-se-unen-a-la-lucha-contra-el-coronavirus-combatiendo-las-fake-news/),
diciendo que son noticias «falsas». Un ejemplo es la censura de la
canción de rap *El peor virus* de Pablo Hasél en que se critica al
Gobierno de España, aunque en ella no aparecía ninguna afirmación falsa.
Esta ha vuelto a aparecer en YouTube tras unos días.

Asimismo, gran parte de la población
está recomendando el uso de herramientas no libres para
videoconferencias, cuya seguridad deja mucho que desear. Por un lado,
[crece el uso del *malware* Zoom durante la
cuarentena](https://www.theguardian.com/technology/2020/apr/02/zoom-technology-security-coronavirus-video-conferencing)
y el de otros programas privativos. Por otro lado, también está
aumentando mucho el uso de programas libres como
[Jitsi](https://jitsi.org/) y [Jami](https://jami.net/).

La recesión económica que está produciéndose supone numerosas
dificultades para todos los sectores, incluido el tecnológico.
