Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2023-06-18 11:26
Lang: es
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Tags: Reddit, Lemmy
Title: Reddit pierde usuarios y dinero por ignorar el descontento de sus propios usuarios
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

Reddit está pagando el precio por ignorar la [protesta de sus usuarios
contra los últimos cambios de la empresa](/deja-de-usar-Reddit/).

Por un lado, mucha gente se ha mudado a plataformas como [Lemmy](https://join-lemmy.org/), que ha
triplicado en poco tiempo su número de usuarios:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

Por otro lado, el sitio web de Reddit dejó de funcionar por las
protestas en las que más de 7000 foros <!-- more -->dejaron de ser públicos[^1] y
cuatro millones de personas dejaron de usar la página[^2]. Para
intentar minimizar los daños, Reddit eliminó de los equipos de
moderación a varios moderadores que protestaban, sustituyéndolos con
moderadores complacientes[^3].

Sin embargo, hay foros que todavía siguen cerrados. Hay también usuarios
que están subiendo contenido irrelevante a la plataforma con el objetivo
de disminuir el valor de la empresa y de continuar la protesta. Un
ejemplo es el [foro r/pic](https://safereddit.com/r/pics), que ahora es
un «un lugar para imágenes en las que John Oliver sale sexy». Además,
hay quienes están llenando la plataforma de vídeos de ruido[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

Todo este descontento no ha pasado desapercibido para varias empresas
anunciantes, que han detenido sus campañas[^5].

Los moderadores trabajan gratis para la empresa; no han sido escuchados.
Los usuarios, que también son esenciales, tampoco han sido escuchados.
El resultado ha sido desastroso para Reddit: pérdida de usuarios,
pérdida de ingresos por anuncios y aumento de contenido basura.

[^1]: Peters, J. (12 de junio de 2023). <cite>Reddit crashed because of the growing subreddit blackout</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Bonifacic, I. (17 de junio de 2023). <cite>Reddit’s average daily traffic fell during blackout, according to third-party data</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (17 de junio de 2023). <cite>Mates, today without warning, the reddit royal navy attacked. I’ve been demoded by the admins.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (15 de junio de 2023). <cite>Since the blackout isn't
    working, everyone upload 1gb large videos of noise to reddit
    servers</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Perloff, C. (14 de junio de 2023). <cite>Ripples Through Reddit as Advertisers Weather Moderators Strike</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
