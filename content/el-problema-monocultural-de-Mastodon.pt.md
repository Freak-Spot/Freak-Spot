Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-05-08 00:12
Lang: pt
Slug: el-problema-monocultural-de-mastodon
Save_as: o-problema-monocultural-do-Mastodon/index.html
URL: o-problema-monocultural-do-Mastodon/
Tags: Fediverso, Mastodon, descentralização, redes sociais
Title: O problema monocultural do Mastodon

As recentes ações do Eugen Rochko (conhecido como `Gargron` no Fediverso),
o diretor executivo da
[associação sem fins lucrativos do Mastodon](https://joinmastodon.org/pt/about)
e principal desenvolvedor do [programa Mastodon](https://github.com/mastodon/mastodon)
fizeram com que algumas pessoas [ficassem
preocupadas](https://mastodon.social/@atomicpoet/110300833929868645) com
a [excessiva influência](https://mstdn.social/@feditips/110260432218416976)
que o Mastodon (o projeto de <i lang="en">software</i> e a organização
sem fins lucrativos) tem sobre o resto do Fediverso.

Bom. *Deveríamos* estar preocupados.

O programa Mastodon é utilizado, de longe, pela maioria das pessoas no
fediverso. A maior instância, `mastodon.social`, é o lar de mais de
200&nbsp;000 contas activas no momento em que escrevo. Isto é cerca de
1/10 de *[todo o Fediverso](https://the-federation.info/)*, numa *única
instância*. Pior ainda, o programa Mastodon é frequentemente identificado
como toda a rede social, obscurecendo o facto de que o Fediverse é [um
sistema muito mais amplo, composto por programas muito mais
diversificados](https://axbom.com/fediverse/).

Isto tem más consequências agora, e pode ter piores consequências mais
tarde. O que também me incomoda muito é o facto de já ter visto algumas
destas coisas antes.

## O que vi no OStatus-verso

Há alguns anos, eu tinha uma conta num precursor do Fediverso.
Baseava-se principalmente no programa StatusNet (desde então renomeado
como [GNU Social](https://pt.wikipedia.org/wiki/GNU_social)) e no
[protocolo OStatus](https://pt.wikipedia.org/wiki/OStatus). A maior
instância de longe era `identi.ca` &mdash; onde eu tinha a minha conta.
Havia também uma série de outras instâncias e outros projetos de
<i lang="en">software</i> que também implementaram o OStatus &mdash;
nomeadamente o [Friendica](https://friendi.ca/).

Para efeitos deste artigo, chamemos a essa rede social «OStatus-verso».

Comparado com o Fediverso atual, o OStatus-verso era minúsculo. Não
tenho números específicos, mas a minha estimativa aproximada
é de, digamos, ~100&nbsp;000 a ~200&nbsp;000 contas activas num dia
muito bom (se tiveres os números reais,
[diz-me](https://freakspot.net/pt/contato/) e eu atualizarei de bom
grado este artigo). Também não tenho os números exatos do `identi.ca`,
mas a minha estimativa aproximada é que tinha entre 10&nbsp;000 e
20&nbsp;000 contas ativas.

Ou seja, cerca de uma décima parte de toda a rede social.

OStatus-verso era pequeno mas animado. Havia debates, fios de
discussão e <i lang="en">hashtags</i>. Tinha grupos uma década antes de
o projeto de <i lang="en">software</i> Mastodon implementar grupos.
Tinha aplicações (para computadores) &mdash; ainda sinto falta da
usabilidade do [Choqok](https://choqok.kde.org/)! E, depois de um pouco de insistência, até
consegui convencer um ministério polaco a ter uma [presença oficial
lá](https://web.archive.org/web/20121122021505/http://identi.ca/maic).
Tanto quanto sei, este é o exemplo mais antigo de uma instituição
governamental com uma conta oficial numa rede social descentralizada e
gerida por <i lang="en">software</i> livre.

## Identipocalipsis

Certo dia Evan Prodromou, o administrador do `identi.ca` (e criador
original do <i lang="en">software</i> StatusNet), [decidiu reimplantá-lo
como um novo serviço, a executar `pump.io`](https://lwn.net/Articles/544347/).
Supostamente, o novo programa deveria ser [melhor e mais ligeiro](https://opensource.com/life/13/7/pump-io).
Foi criado um novo protocolo porque o OStatus tinhas limitações muito
reais.

Havia apenas um problema: esse novo protocolo era incompatível com o
resto do OStatus-verso. Isso arrancou as entranhas da rede social.

As pessoas com contas no `identi.ca` perderam as suas ligações em todas
as instâncias compatíveis com o OStatus. As pessoas com contas noutras
instâncias perderam o contato com pessoas no `identi.ca`, algumas das
quais era bastante populares no OStatus-verso (soa familiar?...).

Revelou-se que se uma instância é uma décima parte de toda a rede
social, muitas ligações sociais passam *por* ela. Apesar de existirem
outras instâncias, de repente uma grande parte dos utilizadores
*desapareceu*. Muitos grupos ficaram praticamente em silêncio. Mesmo tendo
uma conta numa instância diferente e contatos noutras instâncias, muitas
caras conhecidas simplesmente desapareceram. Deixei de o utilizar pouco
depois disso.

Do meu ponto de vista, esta única ação fez-nos recuar pelo menos cinco
anos, se não dez, no que diz respeito à promoção das redes sociais
descentralizadas. A reimplantação do `identi.ca` fraturou o
OStatus-verso não apenas no sentido das conexões sociais, mas também no
sentido do protocolo e da comunidade de desenvolvedores. Como disse
[`pettter`](https://mastodon.acc.umu.se/@pettter), um colega veterano do
OStatus-verso:

<blockquote>Penso que um pouco de nuance sobre a questão do grande golpe
é que não teve apenas impacto ao cortar as ligações sociais, mas também
na fragmentação de protocolos e na fragmentação dos esforços dos
desenvolvedores para reconstruir uma e outra vez os blocos básicos de
uma rede social federada. Talvez tenha sido necessário para que se
juntassem de novo na conceção do AP [ActivityPub], mas pessoalmente acho
que não.</blockquote>

Claro que o Evan tinha todo o direito de fazer isso. Era um serviço que
ele geria, <i lang="lt">pro bono</i>, nos seus próprios termos, com o
seu próprio dinheiro. Mas isso não muda o facto de ter prejudicado o
OStatus-verso.

Penso que temos de aprender com esta história. Quando o fizermos,
[*deveríamos* preocupar-nos com o grande tamanho do
`mastodon.social`](https://social.lol/@mcg/110302174281035077).
Deveríamos estar preocupados com a aparente monocultura do programa
Mastodon no Fediverso. E também deveríamos estar preocupados com a
identificação de todo o Fediverso com apenas «Mastodon».

## O custo de se tornar grande

Há custos reais e riscos reais relacionados com crescer tanto quanto o
`mastodon.social`. Esses custos e, sobretudo, esses riscos afetam tanto
a instância em si como o Fediverso em geral.

A moderação no Fediverso é em grande parte centrada em instâncias. Uma
única instância gigantesca é [difícil de moderar eficazmente](https://mastodon.social/@Gargron/109383947978442853),
especialmente se tiver registos abertos (como o `mastodon.social` tem
atualmente). Como a instância principal, promovida diretamente em
aplicações móveis oficiais, atrai muitos novos registos &mdash;
incluindo alguns problemáticos.

Ao mesmo tempo, isso também torna mais difícil para os administradores e
moderadores de *outras* instâncias tomarem decisões de moderação sobre o
`mastodon.social`.

Se um administrador de uma instância diferente decidir que a moderação
do `mastodon.social` é deficiente por qualquer razão, deverá silenciá-la
ou mesmo desfederar-se dela (como alguns já fizeram, aparentemente),
negando assim aos membros da sua instância o acesso a muitas pessoas
populares que têm contas lá? Ou deveriam manter esse acesso,
arriscando-se a expor a sua própria comunidade a ações potencialmente
prejudiciais?

O grande tamanho do `mastodon.social` torna qualquer decisão deste
género de outra instância imediatamente um grande negócio. Isto é uma
forma de poder: «claro, você pode desfederar de nós se não gostar nossa
moderação, mas seria uma *pena* se as pessoas da vossa instância perdessem
o acesso a uma décima parte de todo o Fediverso!». Como [diz o sítio web
do GoToSocial](https://gotosocial.org/).

<blockquote>Também não acreditamos que as instâncias principais com
milhares e milhares de utilizadores sejam muito boas para o Fediverso,
uma vez que tendem para a centralização e podem facilmente tornar-se
«demasiado grandes para serem bloqueadas».</blockquote>

Atenção, *não* estou a dizer que esta dinâmica de poder é consciente e
propositadamente explorada! Mas é inegável que existe.

O facto de ser uma instância principal gigantesca também significa que o
`mastodon.social` tem mais probabilidades de ser alvo de ações
maliciosas. Por exemplo, nos últimos meses, [foi atingido por ataques de
negação de serviço](https://mastodon.social/@Gargron/109781490892884305).
Algumas vezes [caiu por causa disso](https://status.mastodon.social/clf5ti6d818413wsobkvn2zruj).
A resiliência de um sistema federado baseia-se na eliminação de grandes
pontos de falha, e o `mastodon.social` é atualmente um deles.

A dimensão dessa instância e o facto de ser um alvo suculento também
significa que certas escolhas difíceis precisam serem feitas. Por
exemplo, devido a ser um alvo provável de ataques de negação de serviço,
ela está atrás do Fastly. [Isto é um problema](https://spheron.medium.com/the-fastly-internet-outage-shows-the-fallibility-of-leaving-so-much-power-in-the-hands-of-so-few-a234a1bd9fe1)
na perspetiva da privacidade e da centralização da infraestrutura da
Internet. É também um problema que instâncias mais pequenas evitam
completamente por serem simplesmente mais pequenas e, portanto, alvos
menos interessantes para alguém derrubar com um ataque de negação de
serviço.

## Aparente monocultura

Embora o Fediverso não seja exatamente uma monocultura, está demasiado
perto de ser uma. Mastodon, a organização sem fins lucrativos, tem uma
influência enorme sobre todo o Fediverso. Isto orna as coisas tensas
para as pessoas que usam a rede social, desenvolvedores do programa
Mastodon e doutros programas federados e administradores de instâncias.

O Mastodon não é o único projeto de <i lang="en">software</i> federado
no Fediverso, nem o primeiro. Por exemplo, o [Friendica](https://friendi.ca/)
existe há uma década e meia, muito antes de o programa Mastodon ter
recebido o seu primeiro <i lang="en">commit</i> do Git. Há instâncias do
Friendica (por exemplo, [`pirati.ca`](https://pirati.ca/)) funcionando
hoje dentro do Fediverso que fazia parte do OStatus-verso uma década
atrás!

Mas chamar todo o Fediverso de «Mastodon» faz parecer que apenas
o programa Mastodon existe no Fediverso. Isto leva as pessoas a
solicitar que funcionalidades sejam adicionadas ao Mastodon e a pedir
mudanças que em alguns casos já foram implementadas por outros programas
federados. O [Calckey](https://calckey.org) já tem
<a href="https://i.calckey.cloud/notes/994oo646qk"><i lang="en">toots</i>de citações</a>.
O Friendica tem [conversas com fios e formatação de texto](https://friendi.ca/about/features/).

Identificar o Mastodon com todo o Fediverso também é mau para os
programadores do programa Mastodon. Estes vêem-se pressionados a
implementar funcionalidades que podem não se enquadrar totalmente no
programa Mastodon. Ou encontram-se a lidar com dois grupos de
utilizadores que se fazem ouvir: um a solicitar uma determinada
funcionalidade, outro a insistir que não seja implementada por ser uma
mudança demasiado grande. Muitas dessas situações poderiam
provavelmente ser mais facilmente resolvidas estabelecendo claramente um
limite, e indicar às pessoas outros programas federados que possam ser
mais adequadas ao seu caso de utilização.

Finalmente, o Mastodon é atualmente (medido por utilizadores ativos e
pelo número de instâncias) de longe a implementação mais popular do protocolo
ActivityPub. Cada implementação tem suas peculiaridades. Com o tempo, e
com novas funcionalidades a serem implementadas, a implementação do
Mastodon pode ter que se afastar mais da especificação estrita. É
tentador, afinal: por que passar por um árduo processo de padronização
de qualquer protocolo se tu és a instância mais grande?

Se isso acontecer, terão de a seguir todas as outras implementações,
ficando à deriva com ela, mas sem uma agência efetiva sobre que
alterações à especificação <i lang="lt">de facto</i> são implementadas? Será que isso
criará mais tensões entre os desenvolvedores do programa Mastodon e os
desenvolvedores de outros projetos de <i lang="en">software</i>
federado?

A melhor solução para «Mastodon não tem a funcionalidade X» não é sempre
«Mastodon deve implementar a funcionalidade X». Muitas vezes pode ser
melhor um programa federado diferente, mais adequado para uma
determinada tarefa ou comunidade. Ou trabalhar numa extensão do
protocolo que permita que uma funcionalidade particularmente popular
seja implementada de forma fiável pelo maior número de instâncias
possível.

[Mas isso só pode funcionar se estiver claro para todos que o Mastodon é
apenas uma parte duma rede social maior: o Fediverso](https://mstdn.social/@rysiek/109543826226766974).
E que *já temos muitas opções* no que diz respeito a programas
federados, e no que diz respeito às instâncias individuais, e no que diz
respeito às aplicações móveis.

Infelizmente, isso parece ir contra as recentes decisões do Eugen, que
vão na direção de um modelo bastante hierárquico (não totalmente
integrado numa hierarquia de cima para baixo, mas que vai nessa direção)
de aplicações móveis oficiais do Mastodon que promovem a instância
principal `mastodon.social`. E isso é algo preocupante, na minha
opinião.

## Um caminho melhor

Quero deixar claro que não estou a defender aqui o congelamento do
desenvolvimento do Mastodon e nunca implementar quaisquer novas
funcionalidades. Também concordo que o processo de registo precisa de
ser melhor e mais simplificado do que antes, e que muitas mudanças da
interface do utilizador. Mas tudo isso pode e deve ser feito de uma
forma que melhore a resiliência do Fediverso, em vez de a minar.

## Grandes mudanças

A minha lista de grandes mudanças necessárias para o Mastodon e o
Fediverso seria:

<ol>
    <li><strong>Fechar o registos em <code>mastodon.social</code></strong>, agora.<br>Já
    é demasiado grande e representa um risco demasiado grande para o
    resto do Fediverso.</li>
    <li><strong>Tornar a migração de perfis ainda mas fácil, mesmo em diferentes tipos de instâncias</strong><br><a href="https://docs.joinmastodon.org/user/moving/">No Mastodon, a migração de perfil atualmente só move os seguidores</a>.
    Quem segues, favoritos, listas de bloqueio e silenciamento podem ser
    movidos manualmente. As publicações e listas não podem ser movidas
    &mdash;e isso é um grande problema para muitas pessoas, mantendo-as
    ligadas à primeira instância em que se inscreveram. Não é
    impossível (eu mudei o meu perfil duas vezes). Mas é demasiado difícil.
    Felizmente, alguns outros projetos de programas federados também estão a
    trabalhar para permitir também as migrações de publicações. Mas não
    vai ser uma solução rápida e fácil, pois o desenho do ActivityPub
    faz com que seja muito difícil mover publicações entre instâncias.</li>
    <li><strong>Por padrão, as aplicações oficiais deveriam oferecer às
    novas pessoas uma instância aleatória de uma pequena lista de
    instâncias verificadas</strong><br>Pelo menos algumas dessas instâncias
    promovidas não deveriam ser controladas pela organização sem fins
    lucrativos Mastodon. Idealmente, algumas instâncias deveriam
    executar <em>programas federados diferentes</em>, desde que usem uma
    API de cliente compatível.</li></ol>

## Que posso fazer individualmente?

Estas são algumas coisas que nós próprios podemos fazer, enquanto
utilizadores do Fediverso:

<ul>
    <li><strong>Considera a possibilidade de sair do
    <code>mastodon.social</code></strong> se tens uma conta lá. É, sem
    dúvida, um grande passo, mas é também algo que podes fazer e que
    ajuda diretamente a resolver a situação. Eu
    <a href="https://mastodon.social/@rysiek">migrei do
    <code>mastodon.social</code></a> há alguns anos, nunca mais olhei
    para trás.</li>
    <li><strong>Considera utilizar uma instância baseada num projeto de
    <i lang="en">software</i> diferente</strong><br>Quanto mais pessoas
    migrarem para instâncias que usem outro programa federado diferente
    ao Mastodon, mais equilibrado e resiliente será o Fediverso. Ouvi
    muitas opiniões positivas sobre o
    <a href="https://calckey.social/">Calckey</a>, por exemplo.
    <a href="https://gotosocial.org/">GoToSocial</a> também parece
    interessante.</li>
    <li><strong>Recorda que o Fediverso não é só Mastodon</strong><br>
    As palavras importam. Quando se fala do Fediverso, chamá-lo
    «Mastodon» só faz com que seja mais difícil lidar com os problemas
    que mencionei antes.</li>
    <li><strong>Se puderes, apoia outros projetos para além dos oficiais
    do Mastodon</strong><br>
    Neste momento, o projeto do programa Mastodon tem muitos
    colaboradores, uma equipa de desenvolvimento estável e suficiente
    financiamento sólido para continuar por um longo tempo. Isso é
    ótimo! Mas o mesmo não se pode dizer de outros projetos próximos ao
    Fediverso, incluindo aplicações móveis ou programas federados
    independentes. Para termos um Fediverso diversificado e resiliente,
    temos de garantir que esses projetos também sejam apoiados, inclusive
    financeiramente.</li>
</ul>

## Reflexões finais

Em primeiro lugar, o Fediverso é uma rede social muito mais resiliente,
mais viável, mais segura e mais democratizada do que qualquer rede
social fechada e centralizada. Mesmo com o problema monocultural do
Mastodon, ele ainda não é (e [não pode
ser](https://www.pcmag.com/opinions/whats-happening-to-twitter-could-never-happen-to-mastodon))
propriedade de uma única empresa ou pessoa nem estar sob seu controlo.
nem estar sob seu controlo. nem estar sob seu controlo. nem estar sob
seu controlo. Também acho que é uma escolha melhor e mais segura do que
as [redes sociais que apenas fingem ser descentralizadas e têm conversas
fiadas, como o BlueSky](https://rys.io/en/167.html).

De uma forma muito significativa, pode dizer-se que o OStatus-verso foi
uma versão inicial do Fediverso; como observado anteriormente, algumas
instâncias que fizeram parte dele na época ainda estão rodando e fazem
parte do Fediverso hoje. Em outras palavras, o Fediverso já existia há
uma década e meia e *sobreviveu* ao Identipocalipse, mesmo tendo sido
muito prejudicado por ele, enquanto presenciava [tanto o nascimento
quanto a morte prematura do Google+](https://rys.io/en/162.html).

Acredito que o Fediverso é hoje muito mais resiliente do que o
OStatus-verso era antes da reimplantação do `identi.ca`. É uma ordem de
grandeza (pelo menos) maior em termos de base de utilizadores. Existem
dezenas de projetos de programas federados diferentes e dezenas de
milhares de instâncias ativas. Existem também
[instituições](https://social.network.europa.eu/explore) importantes a
investir no seu futuro. Não devemos entrar em pânico com tudo o que
escrevi antes. Mas penso que devemos preocupar-nos.

Não atribuo malícia às recentes ações do Eugen (como fazer com que as
aplicações oficiais do Mastodon encaminhem novas pessoas para o
`mastodon.social`) nem às ações passadas do Evan (reimplantar o
`identi.ca` no `pump.io`). E não acho que ninguém deveria. Isso é
difícil, e todos nós estamos aprendendo à medida que avançamos, tentando
dar o melhor de nós com o tempo limitado que temos disponível e com
recursos restritos nas nossas mãos.

Evan passou a ser um dos principais criadores do
[ActivityPub](https://www.w3.org/TR/activitypub/), o protocolo com que o
Fediverso funciona. Eugen iniciou o projeto de <i lang="en">software</i>
Mastodon, que, eu acredito firmemente, permitiu que o Fediverso se
desenvolvesse até se tornar o que é hoje. Eu realmente aprecio muito o
trabalho deles e reconheço que é impossível fazer qualquer coisa no
mundo das redes sociais sem que alguém opine sobre isso.

Isso não significa, porém, que não possamos escrutinar essas decisões e
que não devamos ter essas opiniões.

<span style="text-decoration:underline">Este artigo é uma tradução do
artigo «[Mastodon monoculture problem](https://rys.io/en/168.html)»
publicado por Michal "rysiek" Woźniak sob a licença [CC BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.pt).</span>
