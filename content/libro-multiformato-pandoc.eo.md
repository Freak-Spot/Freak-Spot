Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2022-12-19 15:00
Lang: eo
Slug: generar-libro-en-varios-formatos-con-Pandoc
Save_as: krei-libron-kun-multaj-dosierformoj-per-pandoc/index.html
URL: krei-libron-kun-multaj-dosierformoj-per-pandoc/
Tags: libro, pandoc
Title: Krei libron kun multaj dosierformoj per Pandoc

Hodiaŭ libro povas esti disponebla en kelkaj dosierformoj: HTML, PDF, EPUB,
kiel presita libro, ktp. Plej bone, ke ĝi estu disponebla en kiel eble plej
multe da formoj por atingi kiel eble la plej grandan nombron da homoj.
Tamen ne komfortas redakti kelkajn dosierojn, kiam vi nur volas fari unu
ŝanĝon. Por eviti tiun tedan laboron vi povas uzi bazan dosierformon kaj
aliformigi ĉi tiun al aliaj formoj per Pandoc.

Vi povas skribi la libron per LibreOffice Writer aŭ per platteksta
tekstredaktilo en formoj kiel HTML, LaTeX, Markdown, ktp. Por aliformigi
tiun dosieron al alia formo sufiĉus plenumi komandon.

Por ilustri kiel ĉion fari mi montros al vi kiel mi kreas [ĉi tiun
libron](/eo/libro/la-kulturo-estas-libera/). Kiel versikontrola sistemo
mi decidis uzi Giton kaj krei [publikan deponejon](https://notabug.org/jorgesumle/la-kulturo-estas-libera-historio-de-kontrauproprieta-rezisto). Ĝin vi povas kloni plenumante la jenajn komandojn:

    :::bash
    sudo apt install git
    git clone https://notabug.org/jorgesumle/la-kulturo-estas-libera-historio-de-kontrauproprieta-rezisto

Post vi klonu ĝin vi vidos dosierujon nomitan `markdown`, tie estas kie
la libra teksto troviĝas en Markdown-formo. Instalu Pandoc-on kaj
Make-on plenumante `sudo apt install make pandoc`. Sufiĉas plenumi
`make html` en la radika deponeja dosierujo por krei la HTML-n; por la
EPUB-dosierformo oni devas plenumi `make epub`.

Estas malmultaj kodaj lineoj por krei la HTML- kaj EPUB-formojn. Tamen
por krei la PDF-n mi bezonas instali pli da dependoj kaj uzi pli da kodo
por krei la redakti LaTeX-dosieron, kiu bone aspektu, kiam ĝi estu
aliformigita al PDF. Por fari tion mi aldonis tajloritan LaTeX-kodon
(kiu troviĝas en la `latex_personalizado`-dosierujo) al la
LaTeX-dosiero, per kiu mi kreas la PDF-n. Ankaŭ eblas krei belan
PDF-dosieron per CSS, ĉar [eblas elekti PDF-kreilon](https://pandoc.org/MANUAL.html#option--pdf-engine) bazita sur HTML per la `--pdf-engine`-opcio.

La [dokumentaro de Pandoc](https://pandoc.org/MANUAL.html) estas bona
komenca loko por vidi kiel aldoni metadatumojn kaj krei elirdosierojn
kiel vi bezonu. Se, kiel mi, vi elektas uzi LaTeX-on por krei la PDF-n,
en la Interreto vi trovos dokumentarojn kaj homojn, kiuj proponas
solvojn al kutimaj problemoj.
