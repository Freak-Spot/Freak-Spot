Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-09-22 19:49
Lang: es
Modified: 2017-02-20 14:07
Slug: aliases-para-agilizar-tareas-en-bash
Status: published
Tags: .bashrc, .bash_aliases, alias, alias Bash, configuración de Bash
Title: Aliases para agilizar tareas en Bash

Los aliases, como su nombre indica, sirven para llamar a una orden por
otro nombre. La orden a la que se le aplica un alias va a funcionar como
si se le hubiese llamado directamente. Por ejemplo, si yo quiero ir al
directorio padre con la orden `..`, solo tengo que crear un alias desde
la terminal con la siguiente orden: `alias ..='cd ..'`.

Seguramente tengas ya varios aliases creados y no lo sepas. Si ejecutas
`alias`, podrás ver los aliases que tengas ya definidos. Estos aliases
se encuentran definidos en el archivo `.bashrc`, ahí puedes añadir los
tuyos propios (recuerda [recargar la configuración de
Bash](/recargar-la-configuracion-de-bash-bashrc/)
tras añadirlos para que puedas empezar a usarlos sin reiniciar el
ordenador). Pero si quieres añadir muchos y quieres distinguir cuáles
son los tuyos, es recomendable tenerlos en un archivo separado.

En el archivo `.bashrc` encontrarás probablemente estas líneas o algunas
parecidas:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

Esto quiere decir que cada vez que inicias Bash, se cargan los alias
encontrados en el archivo `~/.bash_aliases` en caso de que exista. Si no
tienes aún este archivo, créalo y añade algunos aliases que te ayuden en
tu día a día. A la larga te ahorrarán muchísimo tiempo.

A continuación, te muestro algunos aliases útiles. Tengo un repositorio
en <https://notabug.org/jorgesumle/bash_aliases> con todos mis aliases,
echále un vistazo y copia los que te sean útiles.

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'
