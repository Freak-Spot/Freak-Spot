Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2022-12-30 14:00
Lang: en
Slug: software-libre-y-política
Tags: anarchism, capitalism, communism, GNU/Linux, ideology, politics, free software
Save_as: free-software-and-politics/index.html
URL: free-software-and-politics/
Title: Free software and politics
Image: <img src="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg" alt="" width="1200" height="800" srcset="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg 1200w, /wp-content/uploads/2020/11/software-libre-e-ideología.-600x400.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Is free software anarchist or capitalist? Some call it communist, others
say it is capitalist, anarchist... Who is right? Who is right, and do
the following comments made by former Microsoft CEO Steve Ballmer make
sense?

> Linux is a tough competitor. There's no company called Linux, there's
> barely a Linux road map. Yet Linux sort of springs organically from the
> earth. And it had, you know, the characteristics of communism that
> people love so very, very much about it. That is, it's free.[^1]

## Capitalism

Proprietary software favours monopolies of companies that control almost
the entirety of a market. It is impossible to achieve a good market
position with proprietary software alone, so in order to compete many
companies must use free software. Nowadays it is difficult to find
technology companies that do not make considerable use of free software.

Of course, there are different capitalist currents. Free software, in
any case, has a place in this type of society as long as there is a
demand for it or its use provides a competitive advantage.

## Anarchism

Free software [ends the unjust power that programmers have over
users](https://www.gnu.org/philosophy/free-software-even-more-important.html).
Since anarchism is about ending the authority imposed on the individual,
the freedoms granted by free software mean liberation.

## Other political systems

Free software is used in a wide variety of political systems. What's the problem? North Korea, for example, developed a GNU/Linux distribution called [Red Star OS](https://en.wikipedia.org/wiki/Red_Star_OS).

## Conclusion

I consider it absurd to frame free software in a particular political
system. It is undoubtedly more efficient and secure than proprietary
software, and countless political models can benefit from its adoption.
[Proprietary software is like
alchemy](/en/free-software-is-better-than-alchemy/), while free
software is like science. No wonder [almost all
supercomputers](https://en.wikipedia.org/wiki/Supercomputer#Operating_systems)
and web servers run on free software.

[^1]: From the article from The Register [MS' Ballmer: Linux is communism](https://www.theregister.com/Print/2000/07/31/ms_ballmer_linux_is_communism/).
