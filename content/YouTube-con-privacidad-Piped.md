Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2022-02-13
Modified: 2022-10-31 13:10
Lang: es
Slug: youtube-con-privacidad-con-piped
Tags: Google, Piped, privacidad, YouTube
Title: YouTube con privacidad: con Piped
Image: <img src="/wp-content/uploads/2023/03/caratula_piped.png" alt="" width="1280" height="720" srcset="/wp-content/uploads/2023/03/caratula_piped.png 1280w, /wp-content/uploads/2023/03/caratula_piped-640x360.png 640w" sizes="(max-width: 1280px) 100vw, 1280px">

Al igual que [Invidious](/youtube-con-privacidad-con-invidious/),
[Piped](https://piped.kavin.rocks/) ofrece una interfaz para YouTube
libre y respetuosa con la privacidad.

La ventaja de Piped es que funciona con
[SponsorBlock](https://sponsor.ajay.app/), así que no pierdes tiempo
tragándote partes patrocinadas de vídeos. Solo he mencionado las funcionalidades que me
parecen más útiles; en la página del proyecto [hay una
lista](https://github.com/TeamPiped/Piped/#features) más detallada.

<figure>
    <a href="/wp-content/uploads/2022/02/canal-de-YouTube-en-Piped.png">
    <img src="/wp-content/uploads/2022/02/canal-de-YouTube-en-Piped.png" alt="" width="1863" height="991" srcset="/wp-content/uploads/2022/02/canal-de-YouTube-en-Piped.png 1863w, /wp-content/uploads/2022/02/canal-de-YouTube-en-Piped-931x495.png 931w, /wp-content/uploads/2022/02/canal-de-YouTube-en-Piped-465x247.png 465w" sizes="(max-width: 1863px) 100vw, 1863px">
    </a>
    <figcaption class="wp-caption-text">Canal de YouTube visto con Piped</figcaption>
</figure>

Algunas desventajas respecto a Invidious son que no permite ordenar los
vídeos de un canal según su antigüedad o popularidad, sino que
simplemente muestra los últimos vídeos del canal; no hay un botón
para descargar vídeos y audio; no se ve una miniatura del fotograma al
pasar el ratón por la línea temporal; no aparece la miniatura del vídeo
al compartir un enlace...

<figure>
    <a href="/wp-content/uploads/2022/02/vídeo-en-Piped-descripción-comentarios-en-bucle.png">
    <img src="/wp-content/uploads/2022/02/vídeo-en-Piped-descripción-comentarios-en-bucle.png" alt="" width="1866" height="1055" srcset="/wp-content/uploads/2022/02/vídeo-en-Piped-descripción-comentarios-en-bucle.png 1866w, /wp-content/uploads/2022/02/vídeo-en-Piped-descripción-comentarios-en-bucle-933x527.png 933w, /wp-content/uploads/2022/02/vídeo-en-Piped-descripción-comentarios-en-bucle-466x263.png 466w" sizes="(max-width: 1866px) 100vw, 1866px">
    </a>
    <figcaption class="wp-caption-text">Puedes poner vídeos en bucle,
    ver comentarios, leer descripciones de vídeos...</figcaption>
</figure>

<!-- more -->

A continuación muestro un vídeo cargado con Piped:

<iframe id='ivplayer' width='640' height='360' src='https://piped.kavin.rocks/embed/628hJVGJ49k' style='border:none;'></iframe>

Es genial que existan alternativas libres a programas libres. Estoy
satisfecho con el funcionamiento de Piped, pero también me gusta mucho
Invidious. Ambos son buenas opciones, desde luego mucho mejores que usar
YouTube directamente, pues ahorras tiempo evitando anuncios, proteges tu
privacidad y no le das ni un duro a YouTube.
