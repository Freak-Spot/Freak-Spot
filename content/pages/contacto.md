Author: Jorge Maldonado Ventura
Date: 2016-08-12 09:53
Lang: es
Modified: 2023-04-23 15:00
Slug: contacto
Status: published
Title: Contacto

Mi correo electrónico es
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net). Utiliza mi
clave pública <abbr title="GNU Privacy Guard">GPG</abbr>
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc))
para evitar que el mensaje pueda ser leído por otras personas.
