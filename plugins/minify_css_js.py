#!/usr/share/python3
#
# Copyright (C) 2020  Jorge Maldonado Ventura <jorgesumle@freakspot.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import subprocess

# Ajústalo según te convenga
OUTPUT_DIR = '../html/'

# Comprime archivos CSS
css_dirs = []
for directory in ['theme', 'css', 'de/css', 'en/css', 'eo/css']:
    css_dirs.append(OUTPUT_DIR + directory)

for root_dir in css_dirs:
    for directory, subdirectories, files in os.walk(root_dir):
        for _file in files:
            if os.path.splitext(_file)[1] == '.css':
                subprocess.run(['uglifycss', '--output',
                               os.path.join(directory, _file),
                               os.path.join(directory, _file)])


# Comprime archivos JavaScript
js_dirs = []
for directory in ['theme/js', 'js/', 'de/js/', 'en/js/', 'eo/js/']:
    js_dirs.append(OUTPUT_DIR + directory)


for root_dir in js_dirs:
    for directory, subdirectories, files in os.walk(root_dir):
        for _file in files:
            if os.path.splitext(_file)[1] == '.js' and os.path.splitext(_file)[0][-4:] != '.min':
                min_file = os.path.splitext(_file)[0] + '.min' + os.path.splitext(_file)[1]
                subprocess.run(['uglifyjs', '--output',
                               os.path.join(directory, min_file),
                               os.path.join(directory, _file)])
