Author: Jorge Maldonado Ventura
Date: 2017-10-28 17:46
Lang: en
Modified: 2018-07-03 03:17
Slug: apoyo-economico
Status: published
Title: Economic support

Since its birth, this website has used only free software and has been
free of ads. However, maintaining a website like this one requires both
time and money. Donations are a way
of covering the
[costs of the website](/en/pages/gastos-economicos.html) and
encourage its development and improvement.

You can choose between different donation methods.

## Monero

4AVrC3LT4Ad7DCK5L8MDfWfMeiTqDYJnWDAKGE1jJNbn7pBmFGay81C5sKcci9pwkXRRBexM5JKyVUTEPWvvZTAJ3WeAAex

<img alt="" src="/wp-content/uploads/2023/12/monero-qr.gif">

## Bank transfer

<dl>
  <dt>Bank</dt><dd>Cajamar Caja Rural</dd>
  <dt>Account holder</dt> <dd>Jorge Maldonado Ventura</dd>
  <dt>Account number</dt> <dd>3058 0990 2727 5316 9801</dd>
  <dt><abbr title="International Bank Account Number">IBAN</abbr></dt> <dd>ES80 3058 0990 2727 5316 9801</dd>
  <dt><abbr title="Society for Worldwide Interbank Financial Telecommunication">SWIFT</abbr></dt> <dd>CCRIES2AXXX</dd>
</dl>

## Liberapay

<a href="https://liberapay.com/Freak-Spot/donate" role="button"><img alt="Donate" src="/wp-content/uploads/2017/10/donate.svg"></a>
