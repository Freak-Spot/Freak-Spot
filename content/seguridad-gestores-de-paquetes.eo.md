Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-04-15
Lang: eo
Slug: otro-ataque-a-la-cadena-de-suministro-en-npm-eventsource-polyfill
Save_as: alia-provizoĉena-atako-en-npm:-EventSource-polyfill/index.html
URL: alia-provizoĉena-atako-en-npm:-EventSource-polyfill/
Tags: pako-administrilo, npm, Rusio, sekureco, Ukrainio
Title: Alia provizoĉena atako en npm: EventSource polyfill

npm jam pruvis esti malsekura pako-administrilo &mdash; lastatempe per la
[fiprogramaro de
node-ipc](/eo/reteja-milito-kontraŭ-senkulpaj-pri-la-fiprogramaro-enigita-en-nodeipc/)
&mdash;. Alia biblioteko tre uzata, elŝutita pli ol 600 000
fojoj semajne per npm, nomita [EventSource
polyfill](https://github.com/Yaffle/EventSource) [aldonis propagandajn
mesaĝojn skribitajn en la
rusa](https://github.com/Yaffle/EventSource/commit/de137927e13d8afac153d2485152ccec48948a7a)
favore al la ukraina reĝimo direktajn al tiuj, kiuj havas sian horzonon
agorditan kiel regionon de Rusio, eĉ malfermante fenestron en la
retumilo kun la URL de kolekto de subskriboj kontraŭ la milito.

Ĝi montras falsajn mensaĝojn kiel «91 % el la ukrainaj plene subtenas la
prezidanton Volodimir Zelenskij», sed la partopreno en la 2019aj elektoj
estis 49,84 % kaj la partio de tiu politikisto atingis entute 6 307 793
balotilojn (43,16 %). Ĝi ankaŭ rekomendas aliri al la ĵurnalo de la BCC
(brita ŝtata ĵurnalo) per Tor (ĉar ĝi estas cenzurita en Rusio) kiel
fidinda fondo de informo.
