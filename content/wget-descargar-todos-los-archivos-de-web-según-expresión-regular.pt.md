Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-04 22:22
Lang: pt
Slug: wget-descargar-todos-los-archivos-de-web-según-expresión-regular
Tags: baixar, expressão regular, wget
Title: Wget: baixar todos os arquivos de páginas web conforme expressão regular
Save_as: wget-baixar-todos-os-arquivos-de-páginas-web-conforme-expressão-regular/index.html
URL: wget-baixar-todos-os-arquivos-de-páginas-web-conforme-expressão-regular/

Digamos que encontrámos uma página web que oferece muitos arquivos no
formato PNG, MP3 ou que se chamam de uma certa maneira e queremos
baixá-los. Se há muitos arquivos, fazer isto manualmente não é
eficiente.

Com [GNU wget](https://www.gnu.org/software/wget/) podemos solucionar
este problema de forma muito simples: só temos de executar um comando
como o que segue.

<!-- more -->

    :::bash
    wget -c -A '*.mp3' -r -l 1 -nd https://www.esperantofre.com/kantoj/kantomr1.htm

As opções têm os seguintes significados:

- `-c`: continua a descarregar um arquivo parcialmente descarregado.
- `-A`: só aceita arquivos que coincidem com a expressão regular `*.mp3`.
- `-r`: recursivamente.
- `-l 1`: um nível de profundidade (ou seja, só os arquivos vinculados do
  sítio eletrônico fornecido).
- `-nd`: não cria estrutura de diretórios, só baixa todos os arquivos no
  diretório atual.
