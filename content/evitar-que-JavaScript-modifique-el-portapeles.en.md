Author: Jorge Maldonado Ventura
Category: Tricks
Date: 2023-06-25 00:56
Lang: en
Slug: evitar-que-javascript-modifique-el-portapeles
Save_as: stop-JavaScript-from-modifying-the-clipboard/index.html
URL: stop-JavaScript-from-modifying-the-clipboard/
Tags: Firefox, JavaScript, clipboard
Title: Stop JavaScript from modifying the clipboard in Firefox
Image: <img src="/wp-content/uploads/2023/06/JavaScript-modifying-the-clipboard.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/06/JavaScript-modifying-the-clipboard.png 1920w, /wp-content/uploads/2023/06/JavaScript-modifying-the-clipboard-960x540.png 960w, /wp-content/uploads/2023/06/JavaScript-modifying-the-clipboard-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Did it ever happen to you that you copied text from a webpage and when
you pasted the text it was modified, or nothing at all appeared? This
happens because the page in question executes JavaScript code that
modifies the clipboard.

To avoid that, you can [disable JavaScript](/en/disable-JavaScript-in-Firefox/)
or change your Firefox settings this way:

1. Type `about:config` in the address bar.
2. Press <kbd>Enter</kbd> and then the button **Accept the Risk and
   Continue**.
3. Type `dom.event.clipboardevents.enabled` in the search box.
4. Double-click on that preference, or press its **Toggle** button to
   change its value to `false`.

<a href="/wp-content/uploads/2023/06/toggle-preference-dom-event-clipboardevents-enable-Firefox.png">
<img src="/wp-content/uploads/2023/06/toggle-preference-dom-event-clipboardevents-enable-Firefox.png" alt="" width="1112" height="300" srcset="/wp-content/uploads/2023/06/toggle-preference-dom-event-clipboardevents-enable-Firefox.png 1112w, /wp-content/uploads/2023/06/toggle-preference-dom-event-clipboardevents-enable-Firefox-556x150.png 556w" sizes="(max-width: 1112px) 100vw, 1112px">
</a>

<figure>
<a href="/wp-content/uploads/2023/06/clipboard-events-disabled-Firefox.png">
<img src="/wp-content/uploads/2023/06/clipboard-events-disabled-Firefox.png" alt="" width="951" height="56" srcset="/wp-content/uploads/2023/06/clipboard-events-disabled-Firefox.png 951w, /wp-content/uploads/2023/06/clipboard-events-disabled-Firefox-475x28.png 475w" sizes="(max-width: 951px) 100vw, 951px">
</a>
    <figcaption class="wp-caption-text">It should look like this</figcaption>
</figure>

This way, the web pages you visit will not be able to modify your
clipboard. Note that some web applications that modify the clipboard
with JavaScript (such as
[Collabora](https://en.wikipedia.org/wiki/Collabora_Online)) will no
longer be able to do this, so their paste functionality will no longer
work properly.
