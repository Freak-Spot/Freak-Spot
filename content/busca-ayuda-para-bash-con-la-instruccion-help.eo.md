Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-04-17 02:14
Lang: eo
Slug: busca-ayuda-para-bash-con-la-instruccion-help
Status: published
Tags: Bash, help
Save_as: serĉi-helpon-por-Bash-per-la-komando-help/index.html
Title: Serĉi helpon por Bash per la komando help
URL: serĉi-helpon-por-Bash-per-la-komando-help/

La komando help utilas por trovi helpon pri komandoj de la ŝelo
interne difinitaj. Se ni plenumas la komandon `help`, ni povos vidi
resumon pri la internaj komandoj. Al la komando `help` ankaŭ ni povas
aldoni argumentojn. Per `help help` ni povas vidi ĝiajn utilojn.
