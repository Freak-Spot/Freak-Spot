Author: Jorge Maldonado Ventura
Category: Python
Date: 2016-09-06 02:42
Lang: es
Slug: barras-bajas-entre-numeros-grandes-en-python
Status: published
Tags: barras bajas entre números, mejora, PEP, Python, Python 3, Python 3.6, _
Title: Barras bajas entre números grandes en Python

A partir de la versión 3.6 de Python se podrán escribir barras bajas
entre números grandes. Está mejora
([<abbr title="Python Enhancement Proposal">PEP</abbr>
515](https://www.python.org/dev/peps/pep-0515/)) fue propuesta por Georg
Brandl y Serhiy Storchaka.

¿Para qué querría alguien escribir barras bajas entre números? Para
mejorar la legibilidad. Compara `print(1000000)` con `print(1_000_000)`.
Si lees el segundo *print*, podrás distinguir claramente que el número
es un millón; sin barras bajas es más difícil.
