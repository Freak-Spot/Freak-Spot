Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2024-01-10 17:00
Lang: en
Slug: internet-tiene-inquilinos-y-propietarios-¿qué-eres-tú
Save_as: the-Internet-has-tenants-and-landlords-what-are-you/index.html
URL: the-Internet-has-tenants-and-landlords-what-are-you/
Tags: Fediverso, Internet, Lemmy, Mastodon, PeerTube, Tor
Title: The Internet has tenants and landlords, which one are you?
Image: <img src="/wp-content/uploads/2024/01/propiedad-virtual.png" alt="" width="1169" height="945" srcset="/wp-content/uploads/2024/01/propiedad-virtual.png 1169w, /wp-content/uploads/2024/01/propiedad-virtual-584x472.png 584w" sizes="(max-width: 1169px) 100vw, 1169px">

Many complain that they are censored on the Internet, that they
can't speak openly about certain topics for fear of losing reach and
revenue, and so on. They depend on platforms over which they have no
control, they are like *tenants* that can be kicked out at any time. When
you post on platforms like X, Instagram, TikTok and YouTube, you are
using someone else's platform (someone else's *home*). The moment you
stop being profitable and the advertisers or the owners of the company
don't like you, they censor you, they kick you out.... And you can't do
anything about it. Or can you?

The cheapest way to protect yourself against the whims of one platform
is to use more than one. If one platform fails you, you still have the
others. In my opinion, it's best is to use a [free](https://freakspot.net/escribe-programas-libres/en/learn/) platform that is part
of a decentralized network. If you are not convinced by any platform,
you can set up your own. Is this expensive? No. Thanks to the
[ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) protocol, it is very cheap to create a node within the
Fediverse. The [Fediverse](https://en.wikipedia.org/wiki/Fediverse) is
a large decentralized network with which you can reach millions of
users. You can choose [PeerTube](https://joinpeertube.org/en_US) to upload videos, [Mastodon](https://joinmastodon.org/) to create and
share short written posts, [Lemmy](https://join-lemmy.org/?lang=en) to discuss with other people...

You can also buy a domain name for very little money and set up a website
on a server that you rent or set up at home. For further protection
against censorship you can also [set up a hidden Tor service](https://community.torproject.org/onion-services/setup/).

You can also create a mailing list or a group in a messaging
program. With this method, your publications reach your recipients
directly, without depending on arbitrary algorithms that can limit your
reach. If the server from which you send the emails belongs to
you, you will have more control, you will be the sovereign.

In short, the trick to avoid censorship and the whims of big owners is
to have your own platform or channel. If this is not possible, the best
thing to do is to have several accounts spread across different
platforms. This way, if you are censored on one, you still have the
others.
