Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2023-03-17 19:00
Lang: es
Slug: trovi-rompitajn-ligilojn-en-retejo
Save_as: encontrar-enlaces-rotos-en-sitio-web/index.html
URL: encontrar-enlaces-rotos-en-sitio-web/
Tags: HTML, software libre, enlaces, Perl, página web, W3C Link Checker
Title: Encontrar enlaces rotos en sitio web
CSS: asciinema-player.css
JS: asciinema-player.js (top)

«Página no encontrada», «error 404», etc.: es molesto. Debes buscar y
arreglar los enlaces rotos de vez en cuando.

## Encontrarlos

Existen muchas herramientas web, mi preferida es
[W3C Link Checker](https://validator.w3.org/checklink). También es
posible instalarla en tu ordenador, porque es libre.

Las instrucciones sobre su uso se encuentran en [su
repositorio](https://github.com/w3c/link-checker). Te muestro
rápidamente en el siguiente vídeo como instalarlo y usarlo en tu
ordenador.

<!-- more -->

<asciinema-player src="/asciicasts/link-checker.cast">
Perdona, asciinema-player no funciona sin JavaScript.
</asciinema-player>

Como se muestra en el vídeo, para usarlo, tenemos que pasarle la URL
base. Si nuestro sitio web es grande, debemos esperar algo de tiempo. La
salida del programa resume los problemas.

Es recomendable hacer esto de vez en cuando. Como mi sitio web no es
demasiado grande, pienso que arreglar los enlaces rotos cada tres meses
está bien. También está bien tener un correo electrónico en un sitio
fácilmente encontrable y un sistema de comentarios, porque así los
usuarios pueden avisarte si algo está roto.

## Arreglarlos

Si las páginas enlazadas simplemente se han movido, es fácil arreglarlo.
Sin embargo, si la página ya no existe, es posible que tengas que buscar
en un archivo web como [Internet Archive](https://es.wikipedia.org/wiki/Internet_Archive).

Si todavía no puedes encontrarlo, quizá debas reemplazar el enlace por
uno similar o, si hace falta, eliminarlo añadiendo una explicación.
