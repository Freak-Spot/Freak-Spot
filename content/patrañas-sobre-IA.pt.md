Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-11-16 16:36
Lang: pt
Slug: patrañas-sobre-IA
Save_as: mentiras-sobre-IA/index.html
URL: mentiras-sobre-IA/
Tags: bolha, capitalismo, dicas, AI
Title: Mentiras sobre IA

Existe uma bolha da aprendizagem automática, mas a tecnologia veio para
ficar. Quando a bolha rebentar, o mundo *será* alterado pela aprendizagem
automática. Mas provavelmente será pior, não melhor.

Contrariamente às expectativas sobre a IA dos mais pessimistas, o mundo
não se vai afundar mais rapidamente graças à IA. Os avanços
contemporâneos na aprendizagem automática não nos estão a aproximar da
inteligência artificial geral e, como Randall Monroe referiu em 2018:

<a href="/wp-content/uploads/2023/11/futuro-dos-robôs.png">
<img src="/wp-content/uploads/2023/11/futuro-dos-robôs.png" alt="Agora. A IA torna-se suficientemente avançada para controlar enxames imparáveis de robôs assassinos. A parte que me preocupa. A IA torna-se autoconsciente e rebela-se contra o controlo humano. A parte com que muitas pessoas parecem preocupar-se." width="1223" height="412" srcset="/wp-content/uploads/2023/11/futuro-dos-robôs.png 1223w, /wp-content/uploads/2023/11/futuro-dos-robôs-611x206.png 611w" sizes="(max-width: 1223px) 100vw, 1223px">
</a>

O que vai acontecer à IA é o velho e aborrecido capitalismo. A sua
capacidade de se manter no poder consistirá em substituir humanos competentes e
caros por robôs baratos e de má qualidade. Os grandes modelos de
linguagem são um bom avanço em relação às cadeias de Markov, e o Stable
Diffusion pode gerar imagens que só são um pouco estranhas com manipulação
suficiente dos comandos. Os programadores medíocres usarão o GitHub
Copilot para escrever código trivial e de base (código
trivial é tautologicamente desinteressante), e a aprendizagem automática provavelmente
continuará a ser útil para escrever cartas de apresentação. Os
carros autónomos podem aparecer A Qualquer Momento™, o que vai ser ótimo para
os entusiastas da ficção científica e para os tecnocratas, mas muito
pior em todos os aspectos do que, digamos, [construir mais comboios](https://yewtu.be/watch?v=0dKrUE_O0VE).

As maiores mudanças duradouras na aprendizagem automática serão, antes,
as seguintes:

- Redução da mão de obra para trabalhos criativos especializados.
- A eliminação completa de humanos em funções de apoio ao cliente.
- Conteúdos de <i lang="en">spam</i> e <i lang="en">phishing</i> mais
  convincentes, fraudes mais escaláveis.
- Conjuntos de servidores geradores de conteúdos optimizados para motores de busca a
  dominar os resultados de pesquisa.
- Conjuntos de servidores geradores de livros (tanto eletrónicos quanto em papel).
- Conteúdos gerados por IA a inundar as redes sociais.
- Propaganda e campanhas de ativistas fictícios, tanto na política como na publicidade

As empresas de IA continuarão a gerar resíduos e emissões de
CO<sub>2</sub> a uma escala enorme à medida que recolhem agressivamente
todo o conteúdo da Internet que conseguem encontrar, externalizando os
custos para a infraestrutura digital mundial, e alimentam os servidores
de GPU com esse tesouro para gerar os seus modelos. Os humanos talvez
tenham poder de decisão, ajudando a etiquetar os conteúdos, procurando os
mercados mais baratos com as leis laborais mais fracas com o objetivo de
construir fábricas que exploram os trabalhadores para alimentar o
monstro de dados da IA.

Nunca mais confiarás noutra avaliação de produto. Nunca mais voltarás a
falar com um ser humano do teu fornecedor de acesso à Internet. Os meios
de comunicação social insípidos e concisos encherão o mundo digital à
tua volta. A tecnologia criada para gerar engajamento em escala &mdash;
aqueles vídeos editados por IA com uma voz de máquina irritante que tens
visto nos teus canais ultimamente &mdash; será convertida numa marca
branca e utilizada para promover produtos e ideologias a uma escala
maciça com um custo mínimo a partir de contas de redes sociais que são
preenchidas com conteúdo de IA, geram uma audiência e são vendidas por
grosso e em ordem com o Algoritmo.

Todas estas coisas já estão a acontecer e vão continuar a piorar. O
futuro dos meios de comunicação social é uma regurgitação insípida e sem alma
de todos os média que vieram antes da época da IA, e o destino de todos
os novos meios de comunicação social criativos é serem subsumidos no
turbilhão matemático.

Isto será incrivelmente lucrativo para os patrões da IA e, para garantir
o seu investimento, estão a desenvolver uma imensa e dispendiosa
campanha de propaganda a nível mundial. Para o público, as capacidades
actuais e potenciais futuras da tecnologia são representadas em
promessas ridículas. Em reuniões à porta fechada, são
feitas promessas muito mais realistas de reduzir para metade os
orçamentos dos salários.

A propaganda também se apoia no cânone místico da ciência ficção
sobre a IA: a ameaça de computadores inteligentes com poder para acabar
com o mundo, o fascínio proibido de um novo Projeto Manhattan e todas as
suas consequências, a singularidade há muito profetizada. A tecnologia
não está nem perto deste nível, um facto bem conhecido dos especialistas
e dos próprios patrões, mas a ilusão é mantida no interesse de pressionar
os legisladores a ajudarem os barões a erguer um muro à volta da sua
nova indústria.

É claro que a IA representa uma ameaça de violência, mas, como refere
Randall, não é da própria IA, mas sim das pessoas que a empregam. O
exército dos EUA está a testar drones controlados por IA, que não serão
autoconscientes, mas que irão aumentar os erros humanos (ou a malícia
humana) até que pessoas inocentes sejam mortas. As ferramentas de IA já
estão a ser utilizadas para estabelecer condições de fiança e liberdade
condicional &mdash; podem colocar-te na prisão ou manter-te lá. A
polícia está a utilizar a IA para reconhecimento facial e «policiamento
preditivo». É claro que todos estes modelos acabam por discriminar as
minorias, privando-as da liberdade e, muitas vezes, levando-as à morte.

A IA é definida por um capitalismo agressivo. A exagerada bolha foi criada
por investidores e capitalistas que injectaram dinheiro nela, e os
rendimentos que esperam obter desse investimento vão sair do teu bolso.
A singularidade não está a chegar, mas as promessas mais realistas da IA
vão tornar o mundo pior. A revolução da IA chegou, e não me agrada.

<details style="margin-bottom: 1em; cursor: pointer;">
  <summary>Mensagem provocadora</summary>
<p>Tinha um artigo muito mais inflamado esboçado para este tópico com o
título «ChatGPT é o novo substituto tecnoateísta de Deus». Faz
algumas comparações bastante incisivas entre o culto às
criptomoedas e o culto à aprendizagem automática e a fé religiosa,
inabalável e em grande parte ignorante em ambas as tecnologias como
precursoras do progresso. Foi divertido de escrever, mas este é provavelmente o
melhor artigo.</p>
<p>Encontrei este comentário no Hacker News e citei-o no rascunho original:
«Provavelmente vale a pena falar com o GPT4 antes de procurar ajuda
profissional [para lidar com a depressão]».</p>
<p>
Caso precises de ouvir isto: <a href="https://pt.euronews.com/next/2023/04/01/conversa-com-inteligencia-artificial-leva-homem-ao-suicidio">não</a> procures (aviso: suicídio) os serviços da
OpenAI para te ajudar com a tua depressão. Encontrar e marcar uma
consulta com um terapeuta pode ser difícil para muitas pessoas &mdash; é
normal que sintas que é difícil. Fala com os teus amigos e pede-lhes que te
ajudem a encontrar os cuidados adequados às tuas necessidades.</p>
</details>

*Este artigo é uma tradução de «[AI crap](https://drewdevault.com/2023/08/29/2023-08-29-AI-crap.html)»
publicado pelo Drew Devault sub a licença [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.es)*.
