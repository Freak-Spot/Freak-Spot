Author: Jorge Maldonado Ventura
Category: Edición de imágenes
Date: 2020-03-09
Modified: 2023-04-06 17:00
Lang: es
Slug: diff-para-imagenes-en-git
Tags: Debian, diff, git, interfaz de línea de órdenes, Trisquel
Title: <code>diff</code> para imagenes en Git
Image: <img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="Comparación de dos imágenes lado a lado, con una en medio mostrando los cambios, de forma que se pueden apreciar visualmente los cambios" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">

El `diff` predeterminado de Git no muestra diferencias entre imágenes. Es
normal: no está pensado para eso. Sin embargo, estaría genial que Git
mostrase los cambios de imágenes como los de código, ¿no? Al menos algo
más bonito que...

    :::text
    $ git diff
    diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    Binary files a/es-ES/images/autobuilder.png and b/es-ES/images/autobuilder.png differ

Algo como esto...

<a href="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png">
<img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">
</a>

Eso lo he hecho usando un guion que usa la biblioteca ImageMagick para
comparar imágenes. Aquí te enseño cómo hacer lo mismo.

<!-- more -->

En distribuciones derivadas de Debian hay que instalar el paquete ejecutando...

    :::text
    sudo apt install imagemagick

A continuación, en un archivo con permisos de ejecución en cualquier
ruta accesible por la variable de entorno `$PATH` escribe el siguiente
código, que yo guardo en `~/bin/git-imgdiff` en este tutorial:

    :::bash
    #!/bin/sh
    compare $2 $1 png:- | montage -geometry +4+4 $2 - $1 png:- | display -title "$1" -

Ahora dile a Git las extensiones que quieres considerar como imágenes
mediante el archivo `.gitattributes`. Si no existe, créalo en el
directorio raíz de un proyecto Git o en tu directorio `$HOME` con las
siguientes líneas para los formatos GIF, PNG y JPG, o si el archivo ya
existe, añádelas:

    :::text
    *.gif diff=image
    *.jpg diff=image
    *.jpeg diff=image
    *.png diff=image

Para que la configuración de `.gitattributes` que has guardado en el
directorio `$HOME` se cargue para todos los proyectos Git, tienes que
ejecutar el siguiente comando:

    :::bash
    git config --global core.attributesFile ~/.gitattributes

Ahora configura así Git para que ejecute el guion creado anteriormente
cuando compare imágenes:

    :::text
    git config --global diff.image.command '~/bin/git-imgdiff'

Así de simple. Puedes personalizar el guion según tus necesidades.

Si te basta solo con saber qué metadatos han cambiado, puedes instalar `exiftool` para mostrar algo como esto:


    :::diff diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    --- a/es-ES/images/autobuilder.png
    +++ b/es-ES/images/autobuilder.png
    @@ -1,21 +1,21 @@
     ExifTool Version Number         : 10.10
    -File Name                       : vHB91h_autobuilder.png
    -Directory                       : /tmp
    -File Size                       : 44 kB
    -File Modification Date/Time     : 2020:03:09 02:12:08+01:00
    -File Access Date/Time           : 2020:03:09 02:12:08+01:00
    -File Inode Change Date/Time     : 2020:03:09 02:12:08+01:00
    -File Permissions                : rw-------
    +File Name                       : autobuilder.png
    +Directory                       : es-ES/images
    +File Size                       : 63 kB
    +File Modification Date/Time     : 2020:03:09 01:35:22+01:00
    +File Access Date/Time           : 2020:03:09 01:35:22+01:00
    +File Inode Change Date/Time     : 2020:03:09 01:35:22+01:00
    +File Permissions                : rw-rw-r--
     File Type                       : PNG
     File Type Extension             : png
     MIME Type                       : image/png
    -Image Width                     : 796
    -Image Height                    : 691
    +Image Width                     : 794
    +Image Height                    : 689
     Bit Depth                       : 8
     Color Type                      : RGB
     Compression                     : Deflate/Inflate
     Filter                          : Adaptive
     Interlace                       : Noninterlaced
    -Significant Bits                : 8 8 8
    -Image Size                      : 796x691
    -Megapixels                      : 0.550
    +Background Color                : 255 255 255
    +Image Size                      : 794x689
    +Megapixels                      : 0.547

Entonces sigue leyendo.

Instala `exiftool`. En distribuciones derivadas de Debian hay que ejecutar...

    :::text
    sudo apt install libimage-exiftool-perl

Luego añade al archivo `.gitattributes` lo siguiente:

    :::text
    *.png diff=exif
    *.jpg diff=exif
    *.gif diff=exif

Finalmente, ejecuta...

    :::text
    git config --global diff.exif.textconv exiftool`

Opcionalmente, en los dos métodos que he mostrado, puedes no añadir
`--global` para que la herramienta escogida solo se aplique al proyecto
Git donde te encuentres trabajando.

Espero que ahora te sea más sencillo revisar los cambios que has hecho a
a imágenes dentro de un proyecto.
