Author: Jorge Maldonado Ventura
Category: Kino
Date: 2023-01-04 20:20
Lang: de
Slug: crear-vídeos-de-ruido-con-ffmpeg
Save_as: Rauschvideos-mit-FFmpeg-erstellen/index.html
URL: Rauschvideos-mit-FFmpeg-erstellen/
Tags: FFmpeg, Rausch, Videos
Title: Rauschvideos mit FFmpeg erstellen

[FFmpeg](https://de.wikipedia.org/wiki/FFmpeg) hat Filter, die
zufällig Videorauschen erzeugen können. Der Filter
[`geq`](https://ffmpeg.org/ffmpeg-filters.html#geq) kann Videorauschen
erzeugen (mit `nullsrc` als weißem Hintergrund), während der Filter
[`aevalsrc`](https://ffmpeg.org/ffmpeg-filters.html#aevalsrc)
Audiorauschen erzeugen kann.

So können wir mit dem folgenden Befehl ein schwarz-weißes Video mit
Rausch mit `1280x720` Pixeln erstellen:

```
ffmpeg -f lavfi -i nullsrc=s=1280x720 -filter_complex \
"geq=random(1)*255:128:128;aevalsrc=-2+random(0)" \
-t 10 Rausch.mkv
```

<!-- more -->

<video controls="">
  <source src="/video/ruido.webm" type="video/webm">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser</p>
</video>

Mit dem folgenden Befehl erreichen wir dasselbe, allerdings mit Farbe:

```
ffmpeg -f rawvideo -video_size 1280x720 -pixel_format yuv420p -framerate 30 \
-i /dev/urandom -ar 48000 -ac 2 -f s16le -i /dev/urandom -codec:a copy \
-t 5 Rausch-mit-Farben.mkv
```

Hier verwenden wir `/dev/urandom`, aber wir können auch den zuvor
verwendeten `geq`-Filter anwenden.

<video controls="">
  <source src="/video/ruido-a-color.webm" type="video/webm">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser</p>
</video>

Mit diesen Befehlen kannst du Beispielvideos erstellen. Sie können auch
dazu verwendet werden, die Server von Websites, die Videos akzeptieren,
schnell mit Junk-Videos zu füllen. Die kosten auf lange Sicht Geld, wenn
sie nicht gelöscht werden (siehe
[„Wie man Google zerstört“](/de/wie-zerstört-man-Google/)), da diese Videos viel
Speicherplatz benötigen. Das letzte Video (5 Sekunden lang) nimmt 106
<abbr lang="en" title="megabytes">MB</abbr> in Anspruch.
