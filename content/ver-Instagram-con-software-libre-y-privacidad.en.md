Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2020-10-08
Lang: en
Slug: consultar-instagram-con-software-libre-y-privacidad
Save_as: check-Instagram-with-privacy-and-free-software/index.html
URL: check-Instagram-with-privacy-and-free-software/
Tags: Bibliogram, Instagram, privacy
Title: Check Instagram with privacy and free software
Image: <img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">


Instagram is a centralized social network that requires the use of
proprietary software. It's almost impossible to use Instagram without
giving up your privacy or freedom... unless we use another front-end,
such as [Bibliogram](https://bibliogram.art/), which I describe in this article.

Bibliogram allows consulting user profiles, posts and
[IGTV](https://en.wikipedia.org/wiki/IGTV) videos in a simple way. In
the home page, there are search forms for both posts and users.

<!-- more -->

<figure>
<a href="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png" alt="" width="1299" height="711" srcset="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png 1299w, /wp-content/uploads/2020/02/página-principal-de-Bibliogram-649x355.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Bibliogram home page</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Profile in Bibliogram</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png" alt="" width="1920" height="1038" srcset="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png 1920w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-960x519.png 960w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-480x259.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Post in Bibliogram</figcaption>
</figure>

Because it's free software, you can install Bibliogram in your server
(if you have one) or use the [public instances of other people](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md).
