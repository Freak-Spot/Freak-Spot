Author: Jorge Maldonado Ventura
Category: Bildbearbeitung
Date: 2020-08-30 14:00
Modified: 2023-04-06 17:00
Lang: de
Slug: diff-para-imagenes-en-git
Tags: Debian, diff, git, Kommandozeile, Trisquel
Save_as: diff-für-Bilder-in-Git/index.html
URL: diff-für-Bilder-in-Git/
Title: <code>diff</code> für Bilder in Git
Image: <img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="Comparación de dos imágenes lado a lado, con una en medio mostrando los cambios, de forma que se pueden apreciar visualmente los cambios" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">

Das Standard-<code>diff</code> von Git zeigt nicht die Unterschiede zwischen Bilder.
Das ist normal: es ist nicht dafür gedacht. Trotzdem wäre es super, wenn
Git Bildänderung wie Codeänderung zeigen würde, oder? Mindestens was
schöner als...

    :::text
    $ git diff
    diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    Binary files a/es-ES/images/autobuilder.png and b/es-ES/images/autobuilder.png differ

So etwas...

<a href="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png">
<img src="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png" alt="" width="1221" height="304" srcset="/wp-content/uploads/2020/03/diff-de-imágenes-con-Git.png 1221w, /wp-content/uploads/2020/03/diff-de-imágenes-con-Git-610x152.png 610w" sizes="(max-width: 1221px) 100vw, 1221px">
</a>

Das habe ich mit einem Script gemacht, das ImageMagick nutzt, um Bilder
zu vergleichen. Hier zeige ich, wie du das Gleiche machen kannst.

<!-- more -->

In auf Debian basierte Distributionen muss man für die Installierung
Folgendes ausführen:

    :::text
    sudo apt install imagemagick

Dann schreib in einer Datei mit Ausführungsberechtigungen in einer
Pfad, auf den über die Umgebungsvariable `$PATH` zugegriffen werden
kann, folgenden Code, den ich in diesem Tutorial in `~/bin/git-imgdiff`
behalte:

    :::bash
    #!/bin/sh
    compare $2 $1 png:- | montage -geometry +4+4 $2 - $1 png:- | display -title "$1" -

Dann sag Git mit dem Datei `.gitattributes` die Dateiendungen, die du
als Bilder betrachten willst. Wenn es existiert nicht, lege es im
Hauptverzeichnis eines Git Projekts oder in deinem `$HOME` Verzeichnis
mit folgenden Zeilen für GIF, PNG und JPG an; oder wenn die Datei schon
existiert, füge sie hinzu:

    :::text
    *.gif diff=image
    *.jpg diff=image
    *.jpeg diff=image
    *.png diff=image

Damit die `.gitattributes`-Konfiguration, die du im `$HOME`-Verzeichnis
gespeichert hast, für alle Git-Projekte geladen wird, musst du den
folgenden Befehl ausführen:

    :::bash
    git config --global core.attributesFile ~/.gitattributes


Nun konfiguriere Git so, damit es das Script zuvor erstellte ausführt,
wenn es Bilder vergleicht:

    :::text
    git config --global diff.image.command '~/bin/git-imgdiff'

So einfach. Du kannst das Script an deine Bedürfnisse anpassen.

Wenn es genügt dir nur, zu wissen, welche Metadaten geändert haben,
kannst du `exiftool` installieren, um so etwas zu zeigen:

    :::diff diff --git a/es-ES/images/autobuilder.png b/es-ES/images/autobuilder.png
    index 6f5f6eb..6f0dd78 100644
    --- a/es-ES/images/autobuilder.png
    +++ b/es-ES/images/autobuilder.png
    @@ -1,21 +1,21 @@
     ExifTool Version Number         : 10.10
    -File Name                       : vHB91h_autobuilder.png
    -Directory                       : /tmp
    -File Size                       : 44 kB
    -File Modification Date/Time     : 2020:03:09 02:12:08+01:00
    -File Access Date/Time           : 2020:03:09 02:12:08+01:00
    -File Inode Change Date/Time     : 2020:03:09 02:12:08+01:00
    -File Permissions                : rw-------
    +File Name                       : autobuilder.png
    +Directory                       : es-ES/images
    +File Size                       : 63 kB
    +File Modification Date/Time     : 2020:03:09 01:35:22+01:00
    +File Access Date/Time           : 2020:03:09 01:35:22+01:00
    +File Inode Change Date/Time     : 2020:03:09 01:35:22+01:00
    +File Permissions                : rw-rw-r--
     File Type                       : PNG
     File Type Extension             : png
     MIME Type                       : image/png
    -Image Width                     : 796
    -Image Height                    : 691
    +Image Width                     : 794
    +Image Height                    : 689
     Bit Depth                       : 8
     Color Type                      : RGB
     Compression                     : Deflate/Inflate
     Filter                          : Adaptive
     Interlace                       : Noninterlaced
    -Significant Bits                : 8 8 8
    -Image Size                      : 796x691
    -Megapixels                      : 0.550
    +Background Color                : 255 255 255
    +Image Size                      : 794x689
    +Megapixels                      : 0.547

Wenn das der Fall ist, lies weiter.

Installiere `exiftool`. In auf Debian basierte Distributionen ist das
auszuführen:

    :::text
    sudo apt install libimage-exiftool-perl

Dann füge Folgendes der Datei hinzu.

    :::text
    *.png diff=exif
    *.jpg diff=exif
    *.gif diff=exif

Endlich führ aus...

    :::text
    git config --global diff.exif.textconv exiftool`

In den zwei gezeigten Beispielen kannst du optional nicht `--global`
hinzufügen, damit das Tool wird nur für das Git-Projekt, wo du
gerade arbeitest, benutzt.

Ich hoffe, es ist jetzt einfacher für dich die Änderung von Bilder in
einem Projekt zu überprüfen.
