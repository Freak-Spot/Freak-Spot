Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2019-12-29 19:00
Image: <img src="/wp-content/uploads/2019/12/graffiti-camara-vigilancia.jpg" alt="Graffiti de una cámara de seguridad">
Lang: es
Slug: la-privacidad-es-un-asunto-colectivo
Tags: consejos, privacidad
Title: La privacidad es un asunto colectivo

Mucha gente da una explicación personal de por qué protegen o no su
privacidad. A quienes no les importa mucho se les escucha decir que no
tienen nada que ocultar. Quienes se preocupan lo hacen para protegerse
de empresas sin escrúpulos, de estados represivos, etc. En ambas
posiciones se suele asumir erróneamente que la privacidad es un asunto
personal, y no lo es.

La privacidad es un asunto tanto individual como público. Los datos
obtenidos por grandes empresas y gobiernos rara vez se usan de forma
individualizada. Podemos entender que la privacidad es un derecho del
individuo en relación con la comunidad, como dice [Edward
Snowden](https://es.wikipedia.org/wiki/Edward_Snowden):

> Argumentar que no te importa la privacidad porque no tienes nada que
> esconder no es diferente a decir que no te importa la libertad de
> expresión porque no tienes nada que decir.

Tus datos pueden ser usados para bien o para mal. Los datos recogidos de
forma innecesaria y sin permiso se suelen usar para mal.

Los estados y las grandes empresas tecnológicas violan flagrantemente
nuestra privacidad. Muchas personas dan su tácito beneplácito
argumentando que no es posible hacer nada para cambiarlo: las empresas
tienen demasiado poder y los gobiernos no van a hacer nada para cambiar
las cosas. Y ciertamente esa gente acostumbra a dar poder a empresas que
ganan dinero con sus datos y le está diciendo así a los estados que no
va a ser una piedra en el zapato cuando quieran implementar políticas de
vigilancia masiva. En el fondo, dañan la privacidad de quienes se
preocupan.

La acción colectiva empieza en el individuo. Cada persona debería
reflexionar si está dando datos propios que no debería, si está
favoreciendo el crecimiento de empresas antiprivacidad y, más importante
aún, si está comprometiendo la privacidad de sus allegados. **La mejor
forma de proteger la información privada es no darla**. Con una visión
consciente del problema pueden apoyarse proyectos en defensa de la
privacidad.

Los datos personales son muy valiosos &mdash;tanto que algunos los llaman el
«nuevo petróleo»&mdash; no solo porque pueden ser vendidos a terceros, sino
también porque dan poder a quién los tiene. Cuando se los damos a
gobiernos, estamos dándoles poder para que nos controlen. Cuando se los
damos a empresas, les estamos dando poder para que influyan en nuestro
comportamiento. En última instancia, la privacidad importa porque nos
ayuda a preservar el poder que tenemos sobre nuestras vidas, el que
tanto se empeñan en arrebatarnos. Yo no voy a regalar ni malvender mi
datos, ¿y tú?
