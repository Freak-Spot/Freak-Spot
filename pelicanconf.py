#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

import logging

from pelican import __version__ as PELICAN_VERSION

# Basic settings (http://docs.getpelican.com/en/stable/settings.html#basic-settings)
DEFAULT_CATEGORY = 'Sin categoría'
DELETE_OUTPUT_DIRECTORY = False
DISPLAY_CATEGORIES_ON_MENU = True
DISPLAY_PAGES_ON_MENU = True
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.footnotes': {'BACKLINK_TITLE': 'Volver a la nota %d en el texto'},
        'markdown.extensions.meta': {},
    },
    'output_format': 'html5',
}
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
LOG_FILTER = [(logging.WARN, 'Empty alt attribute for image %s in %s')]
PATH = 'content'
PLUGIN_PATHS = ['plugins']
PLUGINS = ['another_read_more_link', 'i18n_subsites', 'neighbors', 'pelican-css', 'pelican-js', 'sitemap', 'tag-cloud', 'tipue-search']
SITENAME = 'Freak Spot'
SITEURL = 'https://freakspot.net'

# URL settings (http://docs.getpelican.com/en/stable/settings.html#url-settings)
RELATIVE_URLS = True
ARTICLE_URL = '{slug}/'
ARTICLE_SAVE_AS = '{slug}/index.html'
CATEGORY_URL = 'categoría/{slug}/'
CATEGORY_SAVE_AS = 'categoría/{slug}/index.html'
CATEGORIES_URL = 'categorías/'
CATEGORIES_SAVE_AS = 'categorías/index.html'
TAG_URL      = 'etiqueta/{slug}/'
TAG_SAVE_AS  = 'etiqueta/{slug}/index.html'
TAGS_URL     = 'etiquetas/'
TAGS_SAVE_AS = 'etiquetas/index.html'
AUTHOR_URL      = 'autor/{slug}/'
AUTHOR_SAVE_AS  = 'autor/{slug}/index.html'
YEAR_ARCHIVE_URL = 'lista-de-artículos/{date:%Y}/'
YEAR_ARCHIVE_SAVE_AS = 'lista-de-artículos/{date:%Y}/index.html'
MONTH_ARCHIVE_URL = 'lista-de-artículos/{date:%Y}/{date:%m}/'
MONTH_ARCHIVE_SAVE_AS = 'lista-de-artículos/{date:%Y}/{date:%m}/index.html'
AUTHORS_URL     = 'páginas/créditos/'
AUTHORS_SAVE_AS = 'páginas/créditos/index.html'
ARCHIVES_URL     = 'lista-de-artículos/'
ARCHIVES_SAVE_AS = 'lista-de-artículos/index.html'
STATIC_PATHS = ['asciicasts', 'css', 'fonts', 'Jorge_jorgesumle@freakspot.net_(0x40486470)_pub.asc', 'js', 'wp-content']
PAGE_EXCLUDES = ['asciicasts', 'css', 'fonts', 'js', 'wp-content']
ARTICLE_EXCLUDES = ['asciicasts', 'css', 'fonts', 'js', 'wp-content']

# Time and date (http://docs.getpelican.com/en/stable/settings.html#time-and-date)
TIMEZONE = 'Europe/Madrid'
LOCALE = ('es_ES.UTF-8')

# Feed settings (http://docs.getpelican.com/en/stable/settings.html#feed-settings)
# feed generation is usually not desired when developing, set to true in publishconf.py
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Pagination (http://docs.getpelican.com/en/stable/settings.html#pagination)
DEFAULT_PAGINATION = 5
PAGINATION_PATTERNS = (
    (1, '{base_name}/', '{base_name}/index.html'),
    (2, '{base_name}/page/{number}/', '{base_name}/page/{number}/index.html'),
)
PAGINATOR_LIMIT = 4

# Translations (http://docs.getpelican.com/en/stable/settings.html#translations)
DEFAULT_LANG = 'es'
TRANSLATION_FEED_ATOM = None

# Themes (http://docs.getpelican.com/en/stable/settings.html#themes)
THEME = 'freak-theme/'
SHOW_RECENT_POSTS = 5  # the number of recent posts to show
USAGE_POLICY_URL = '/pages/política-de-uso.html'
SEARCH_URL = '/buscar.html'


# Plugins' configuration (not from Pelican core)
TAG_CLOUD_STEPS = 5
TAG_CLOUD_MAX_ITEMS = 53
TAG_CLOUD_SORTING = 'size'
TAG_CLOUD_BADGE = True

SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'yearly',
        'indexes': 'weekly',
        'pages': 'yearly'
    }
}

ANOTHER_READ_MORE_LINK = 'Continúa leyendo <span class="screen-reader-text">{title}</span>'
ANOTHER_READ_MORE_LINK_FORMAT = ' <a class="more-link" href="{url}#read_more_link">{text}</a>'

LANGUAGE_CODES = {
    'de': 'Deutsch',
    'en': 'English',
    'eo': 'Esperanto',
    'es': 'español',
    'pt': 'português',
    'ro': 'română',
    'ru': 'русский',
}
I18N_UNTRANSLATED_ARTICLES = 'remove'
I18N_UNTRANSLATED_PAGES = 'remove'
I18N_SUBSITES = {
    'de': {
        'ANOTHER_READ_MORE_LINK': 'Weiter lesen <span class="screen-reader-text">{title}</span>',
        'HASHOVER_LANG': 'de_DE',
        'LOCALE': ('de_DE.UTF-8'),
        'CATEGORY_URL': 'Kategorie/{slug}/',
        'CATEGORY_SAVE_AS': 'Kategorie/{slug}/index.html',
        'CATEGORIES_URL': 'Kategorien/',
        'CATEGORIES_SAVE_AS': 'Kategorien/index.html',
        'TAG_URL': 'Schlagwort/{slug}/',
        'TAG_SAVE_AS': 'Schlagwort/{slug}/index.html',
        'TAGS_URL': 'Schlagwörter/',
        'TAGS_SAVE_AS': 'Schlagwörter/index.html',
        'AUTHOR_URL': 'Autor/{slug}/',
        'AUTHOR_SAVE_AS': 'Autor/{slug}/index.html',
        'YEAR_ARCHIVE_URL': 'Artikelliste/{date:%Y}/',
        'YEAR_ARCHIVE_SAVE_AS': 'Artikelliste/{date:%Y}/index.html',
        'MONTH_ARCHIVE_URL': 'Artikelliste/{date:%Y}/{date:%m}/',
        'MONTH_ARCHIVE_SAVE_AS': 'Artikelliste/{date:%Y}/{date:%m}/index.html',
        'AUTHORS_URL': 'Seiten/Anerkennungen/',
        'AUTHORS_SAVE_AS': 'Seiten/Anerkennungen/index.html',
        'ARCHIVES_URL': 'Artikelliste/',
        'ARCHIVES_SAVE_AS': 'Artikelliste/index.html',
        'SEARCH_URL': '/de/suchen.html'
    },
    'en': {
        'ANOTHER_READ_MORE_LINK': 'Keep reading <span class="screen-reader-text">{title}</span>',
        'HASHOVER_LANG': 'en_US',
        'LOCALE': ('en_US.UTF-8'),
        'CATEGORY_URL': 'category/{slug}/',
        'CATEGORY_SAVE_AS': 'category/{slug}/index.html',
        'CATEGORIES_URL': 'categories/',
        'CATEGORIES_SAVE_AS': 'categories/index.html',
        'TAG_URL': 'tag/{slug}/',
        'TAG_SAVE_AS': 'tag/{slug}/index.html',
        'TAGS_URL': 'tags/',
        'TAGS_SAVE_AS': 'tags/index.html',
        'AUTHOR_URL': 'author/{slug}/',
        'AUTHOR_SAVE_AS': 'author/{slug}/index.html',
        'YEAR_ARCHIVE_URL': 'archives/{date:%Y}/',
        'YEAR_ARCHIVE_SAVE_AS': 'archives/{date:%Y}/index.html',
        'MONTH_ARCHIVE_URL': 'archives/{date:%Y}/{date:%m}/',
        'MONTH_ARCHIVE_SAVE_AS': 'archives/{date:%Y}/{date:%m}/index.html',
        'AUTHORS_URL': 'pages/credits/',
        'AUTHORS_SAVE_AS': 'pages/credits/index.html',
        'ARCHIVES_URL': 'archives/',
        'ARCHIVES_SAVE_AS': 'archives/index.html',
        'MARKDOWN': {
            'extension_configs': {
                'markdown.extensions.codehilite': {'css_class': 'highlight'},
                'markdown.extensions.extra': {},
                'markdown.extensions.footnotes': {'BACKLINK_TITLE': 'Jump back to footnote %d in the text'},
                'markdown.extensions.meta': {},
            },
            'output_format': 'html5',
        },
        'SEARCH_URL': '/en/search.html'
    },
    'eo': {
        'ANOTHER_READ_MORE_LINK': 'Legu plu <span class="screen-reader-text">el {title}</span>',
        'HASHOVER_LANG': 'eo',
        'LOCALE': ('eo.UTF-8'),
        'CATEGORY_URL': 'kategorio/{slug}/',
        'CATEGORY_SAVE_AS': 'kategorio/{slug}/index.html',
        'CATEGORIES_URL': 'kategorioj/',
        'CATEGORIES_SAVE_AS': 'kategorioj/index.html',
        'TAG_URL': 'etikedo/{slug}/',
        'TAG_SAVE_AS': 'etikedo/{slug}/index.html',
        'TAGS_URL': 'etikedoj/',
        'TAGS_SAVE_AS': 'etikedoj/index.html',
        'AUTHOR_URL': 'aŭtoro/{slug}/',
        'AUTHOR_SAVE_AS': 'aŭtoro/{slug}/index.html',
        'YEAR_ARCHIVE_URL': 'listo-de-artikoloj/{date:%Y}/',
        'YEAR_ARCHIVE_SAVE_AS': 'listo-de-artikoloj/{date:%Y}/index.html',
        'MONTH_ARCHIVE_URL': 'listo-de-artikoloj/{date:%Y}/{date:%m}/',
        'MONTH_ARCHIVE_SAVE_AS': 'listo-de-artikoloj/{date:%Y}/{date:%m}/index.html',
        'AUTHORS_URL': 'paĝoj/atribuoj/',
        'AUTHORS_SAVE_AS': 'paĝoj/atribuoj/index.html',
        'ARCHIVES_URL': 'listo-de-artikoloj/',
        'ARCHIVES_SAVE_AS': 'listo-de-artikoloj/index.html',
        'SEARCH_URL': '/eo/serĉi.html',
        'USAGE_POLICY_URL': '/eo/regularo-pri-uzado/'
    },
    'pt': {
        'HASHOVER_LANG': 'pt_BR',
        'LOCALE': ('pt_PT.UTF-8'),
        'CATEGORY_URL': 'categoria/{slug}/',
        'CATEGORY_SAVE_AS': 'categoria/{slug}/index.html',
        'CATEGORIES_URL': 'categorias/',
        'CATEGORIES_SAVE_AS': 'categorias/index.html',
        'YEAR_ARCHIVE_URL': 'lista-de-artigos/{date:%Y}/',
        'YEAR_ARCHIVE_SAVE_AS': 'lista-de-artigos/{date:%Y}/index.html',
        'MONTH_ARCHIVE_URL': 'lista-de-artigos/{date:%Y}/{date:%m}/',
        'MONTH_ARCHIVE_SAVE_AS': 'lista-de-artigos/{date:%Y}/{date:%m}/index.html',
        'ARCHIVES_URL': 'lista-de-artigos/',
        'ARCHIVES_SAVE_AS': 'lista-de-artigos/index.html',
        'SEARCH_URL': '/pt/procurar.html',
    },
    'ro': {
        'HASHOVER_LANG': 'ro_RO',
        'LOCALE': ('ro_RO.UTF-8'),
    },
    'ru': {
        'ANOTHER_READ_MORE_LINK': 'Читать дальше <span class="screen-reader-text">{title}</span>',
        'HASHOVER_LANG': 'ru_ru',
        'LOCALE': ('ru_RU.UTF-8'),
        'CATEGORY_URL': 'kategorija/{slug}/',
        'CATEGORY_SAVE_AS': 'kategorija/{slug}/index.html',
        'CATEGORIES_URL': 'kategorii/',
        'CATEGORIES_SAVE_AS': 'kategorii/index.html',
        'TAG_URL': 'tèg/{slug}/',
        'TAG_SAVE_AS': 'tèg/{slug}/index.html',
        'TAGS_URL': 'tègi/',
        'TAGS_SAVE_AS': 'tègi/index.html',
        'AUTHOR_URL': 'avtor/{slug}/',
        'AUTHOR_SAVE_AS': 'avtor/{slug}/index.html',
        'YEAR_ARCHIVE_URL': 'arhiv/{date:%Y}/',
        'YEAR_ARCHIVE_SAVE_AS': 'arhiv/{date:%Y}/index.html',
        'MONTH_ARCHIVE_URL': 'arhiv/{date:%Y}/{date:%m}/',
        'MONTH_ARCHIVE_SAVE_AS': 'arhiv/{date:%Y}/{date:%m}/index.html',
        'AUTHORS_URL': 'stranica/blagodarnosti/',
        'AUTHORS_SAVE_AS': 'stranica/blagodarnosti/index.html',
        'ARCHIVES_URL': 'arhiv/',
        'ARCHIVES_SAVE_AS': 'arhiv/index.html',
        'MARKDOWN': {
            'extension_configs': {
                'markdown.extensions.codehilite': {'css_class': 'highlight'},
                'markdown.extensions.extra': {},
                'markdown.extensions.footnotes': {'BACKLINK_TITLE': 'Вернуться к сноске %d в тексте'},
                'markdown.extensions.meta': {},
            },
            'output_format': 'html5',
        },
        'SEARCH_URL': '/ru/poisk.html'
    }
}
