Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-12-06 17:40
Lang: eo
Slug: cómo-hacer-llamadas-de-teléfono-anónimas
Tags: poŝtelefono, edukado, sekureco, Tor, gvidilo
Save_as: kiel-anonime-telefoni/index.html
URL: kiel-anonime-telefoni/
Title: Kiel anonime telefoni
Image: <img src="/wp-content/uploads/2023/11/mujer-al-móvil.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/11/mujer-al-móvil.png 1920w, /wp-content/uploads/2023/11/mujer-al-móvil-960x540.png 960w, /wp-content/uploads/2023/11/mujer-al-móvil-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

## 1-a metodo: pruntepreni poŝtelefonon

Simpla maniero telefoni estas pruntepreni poŝtelefonon de homo sur la
strato. Se vi ĝentile demandas, konvinkas rin pri tio, ke vi ne havas
vian poŝtelefonon kun vi kaj ke estas urĝaĵo, verŝajne vi ne havos
problemon. Se vi havas problemon, alia eblo estas oferi ion kontraŭ la
favoro.

<div style="background-color: #aa0909; color: #000007">
<p>Vi devas scii, ke <a href="/eo/sen-poŝtelefonoj/#privateco"><strong>la
telefona sistema ne privatecas</strong></a>. Interrete vi povas
ĉifre telefoni per <a href="https://eo.wikipedia.org/wiki/VPN">VPN</a>-oj kaj Tor por havi pli da privateco.</p>
</div>

Vokoj povas esti registritaj de la telofonaj firmaoj kaj spionaj
agentejoj, do se vi bezonas multege da privateco, vi povas distordi la
voĉon. Se vi timas, ke la persono, kiu lasis al vi la telefonon,
malkaŝos al iu informojn rilatajn al via identeco (vizaĝajn trajtojn,
altecon, ktp.), vi povas protekti vin ŝanĝante vian aspekton, kutimajn
vestaĵojn kaj altecon por la voko (uzante kolorajn kontaktlensojn,
platformajn ŝuojn aŭ altajn kalkanumojn, razante vian barbon aŭ lasante ĝin
kreski, ŝanĝante vian hararanĝon, ktp.) Se vi bezonas pli da privateco,
vi povas peti al alia persono fari la vokon por vi.

<a href="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png">
<img src="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png" alt="" width="1920" height="821" srcset="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png 1920w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-960x410.png 960w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-480x205.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

## 2-a metodo: anonime akiri SIM-karton

En multaj landoj oni bezonas doni siajn personajn informojn por aĉeti
SIM-karton. Eblas eviti tiun limigon aĉetante ĝin en la nigra merkato.
Alia maniero estas konvinki homon, kiu ne konas vi, aĉeti por vi karton
kontraŭ mono.

Enigante la karton en stulta telefono aŭ telefono kun sekura operaciumo,
vi povos telefoni, sen ke la vokoj estu ligitaj al via vera identeco.
Denove, se vi bezonas multege da privateco, estu singarda: ne forgesu
distordi la voĉon; telefoni en loko, kie vi ne ofte troviĝas; forigi la
vokon de la historio, ktp.<!-- more --> **Sciu, ke ĉi tiu metodo povus esti krima[^1]**.

[^1]: En Hispanio, ekzemple, identeca trompo kontraŭleĝas.
