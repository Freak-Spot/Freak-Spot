Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2022-11-19 14:00
Lang: pt
Slug: los-estúpidos-códigos-qr-en-restaurantes
Save_as: os-estúpidos-códigos-QR-en-restaurantes/index.html
URL: os-estúpidos-códigos-QR-en-restaurantes/
Tags: QR-kodo, restaurantes
Title: Os estúpidos códigos QR en restaurantes
Image: <img src="/wp-content/uploads/2022/11/hombre-chupando-móvil.png" alt="Homem chupando telemóvel" width="960" height="1200" srcset="/wp-content/uploads/2022/11/hombre-chupando-móvil.png 960w, /wp-content/uploads/2022/11/hombre-chupando-móvil-480x600.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Antes era comum ir a um restaurante, olhar para o menu e encomendar: era
tão simples como isso. Agora muitos lugares já nem sequer têm um menu
físico; assumem que o cliente tem um telefone «inteligente» e uma
ligação à Internet. Se for este o caso, espera-se que o cliente utilize
a câmara e digitalize o
[código QR](https://pt.wikipedia.org/wiki/C%C3%B3digo_QR), o que o leva
a uma página web que não respeita a privacidade, leva frequentemente
muito tempo a carregar e, em muitos casos, não é muito intuitiva.


## É ineficiente, polui mais

O carregamento de uma página web com imagens num servidor remoto,
para cada cliente, é poluente. Com um menu físico, não se desperdiça
electricidade, as pessoas podem reutilizar o menu indefinidamente... Se
não houver Internet ou se não tiveres bateria, como consultar o menu com
o QR?

## Sem privacidade

Quando visitamos um sítio web, deixamos uma pegada digital. Se
utilizarmos códigos QR para consultar o menu, há empresas, governos,
etc., que podem saber que num momento específico consultámos o menu de
um restaurante específico.

Os clientes também perdem a sua privacidade quando pagam com cartão em
vez de usarem dinheiro, mas isso é outra questão.

<a href="/wp-content/uploads/2022/11/no-obrigado-QR-restaurantes.png">
<img src="/wp-content/uploads/2022/11/no-obrigado-QR-restaurantes.png" alt="" width="1200" height="626" srcset="/wp-content/uploads/2022/11/no-obrigado-QR-restaurantes.png 1200w, /wp-content/uploads/2022/11/no-obrigado-QR-restaurantes-600x313.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## Melhor sem QR

Eu não tenho um telemóvel «inteligente» e não gosto de restaurantes. Se
eu comer num restaurante, peço o menu físico. Se não mo derem, têm de me
dizer, porque não tenho forma de ver o código QR. A maioria da comida
dos restaurantes não é saudável, os trabalhadores são frequentemente
explorados, muita comida é desperdiçada, há poucas opções veganas, etc.
A indústria da hospitalidade tem muitos problemas. A utilização do
código QR para menus é apenas mais um passo na direcção errada, mas
muito fácil de combater, recusando-se a utilizar um telefone
«inteligente» para digitalizar um estúpido código QR.
