Author: Jorge Maldonado Ventura
Category: Bash
Date: 2018-12-21 00:55
Modified: 2022-11-04 22:24
Lang: es
Slug: wget-descargar-todos-los-archivos-de-web-según-expresión-regular
Tags: descargar, expresión regular, wget
Title: Wget: descargar todos los archivos de página web según expresión regular

Digamos que hemos encontrado una página web que ofrece un montón de
archivos en formato PNG, MP3 o que se llaman de una determinada forma y
queremos descargarlos todos. Si hay muchos archivos, hacer esto
manualmente no es eficiente.

Con [GNU wget](https://www.gnu.org/software/wget/) podemos solucionar
este problema de forma muy sencilla. Simplemente podemos ejecutar una
instrucción como esta:

<!-- more -->

    :::bash
    wget -c -A '*.mp3' -r -l 1 -nd https://www.esperantofre.com/kantoj/kantomr1.htm

Las opciones tienen los siguientes significados:

- `-c`: continúa descargando un archivo descargado parcialmente
- `-A`: solo acepta archivos que coinciden con la expresión regular
  `*.mp3`
- `-r`: de forma recursiva
- `-l 1`: un nivel de profundidad (es decir, solo los archivos enlazados
  desde el sitio web proporcionado)
- `-nd`: no crea una estructura de directorio, solo descarga todos los
  archivos en el directorio actual
