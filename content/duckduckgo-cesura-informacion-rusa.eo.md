Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-03-13 16:00
Lang: eo
Slug: duckduckgo-censura-desinformacion-rusa
Save_as: DuckDuckGo-cenzuras-rusan-misinformon/index.html
URL: DuckDuckGo-cenzuras-rusan-misinformon/
Tags: cenzuro, informa libereco, politiko, privateco, Rusio, libera programaro
Title: DuckDuckGo cenzuras «rusan misinformon»
Image: <img src="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg" alt="" width="2083" height="1667" srcset="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg 2083w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-1041x833.jpg 1041w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-520x416.jpg 520w" sizes="(max-width: 2083px) 100vw, 2083px">

La ĉefo de DuckDuckGo [diris en Tvitero](https://nitter.snopyta.org/yegg/status/1501716484761997318):

> Kiel multaj aliaj, mi indignas pro la rusa invado de Ukranio kaj la
> ega homrajta krizo, kiun ĝi daŭre kreas. #StandWithUkraine️
>
> En DuckDuckGo ni disponigis serĉilajn ĝisdatigojn, kiuj malsuprigas la
> rangon de retejoj ligitaj al la rusa misinformado.

Ĉi tiu decido de DuckDuckGo estas problema por multaj uzantoj, kiuj
volas mem decidi, kio estas misinformo kaj kio ne. Pro tio multaj
homoj malkontentigitaj pro la decido de DuckDuckGo kaj kiuj defendas la
privatecon kaj la liberan programaron rekomendas nun uzi serĉilojn kiel
[Brave Seach](https://search.brave.com/) kaj [Searx](https://searx.github.io/searx/).
