Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2023-03-02 11:02
Lang: eo
Slug: obtener-las-palabras-más-usadas-de-un-texto-y-las-veces-que-se-repiten
Save_as: akiri-la-plej-uzitajn-vortojn-de-teksto-kaj-kiom-da-fojoj-ili-ripetiĝas-per-Python-kaj-Coreutils/index.html
URL: akiri-la-plej-uzitajn-vortojn-de-teksto-kaj-kiom-da-fojoj-ili-ripetiĝas-per-Python-kaj-Coreutils/
Tags: Bash, vortoj, Python
Title: Akiri la plej uzitajn vortojn de teksto kaj kiom da fojoj ili ripetiĝas: per Python kaj Coreutils

En ĉi tiu artikolo mi montras kiel simple akiri la plej uzitajn vortojn
de teksto. Ĉi-okaze mi uzos kiel montron la tekston de la oka libro de
la novelo <cite lang="grc">Τῶν περὶ Χαιρέαν καὶ Καλλιρρόην</cite>,
eltirita el
<https://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A2008.01.0668%3Abook%3D8>.
Mi forigis la notojn, kiun ĝi havis inter kvadrataj krampoj, per
`sed`:

    :::bash
    sed -i 's/\[[^]]*\]//g' oka_libro.txt

La programo, kiu montras al ni ĉiujn vortojn estas jen, mi nomis ĝin
`listo-de-vortoj.py` (mi klarigas sekve kiel ĝi funkcias):

    :::python
    archivo_texto = open('oka_libro.txt', 'r')
    texto = archivo_texto.read()
    archivo_texto.close()

    palabras = texto.split()

    for palabra in palabras:
        print(palabra.strip('‘’:;,.').lower())

En la teksta dosiero, kiun mi nomis `oka_libro.txt`
([elŝuti](/wp-content/uploads/2023/03/oka_libro.txt){:download}), mi
supozas, ke vorto estas apartigita per blanka spaceto, do mi uzas la
`split`-funkcion por akiri la liston de vortoj. Tamen kelkfoje estas
komoj, punktoj, citiloj, dupunktoj, punktokomoj antaŭ aŭ post la vortoj,
kaj kelkfoje ili komenciĝas per majuskloj. Por tiuj okazoj sufiĉas uzi
la `strip()`-funkcion per la signoj, kiujn ni volas forĵeti inter
citiloj, kaj `lower()` por minuskligi la vorton.

Nun mi volas, ke la vortoj, kiuj pleje aperas en la teksto, estu
montritaj en la ekrano, kun la nombro de fojoj, kiam ili aperas dekstre;
sed mi ne programos ĝin; mi uzos ilojn, kiuj ebligas fari tion en
GNU/Linukso: `uniq` kaj `sort`.

<!-- more -->

La kodo, kiun mi devas plenumi, estas jen (la antaŭa Python-programo
estas `listo-de-vortoj.py`):

    :::bash
    python3 listo-de-vortoj.py | sort | uniq -c | sort -n -r

La listo estas tre granda, do utilas vidi ĝin per `less`. Se mi volas
eltiri nur la liston de la 20 plej uzitaj vortoj, mi aldonas tubon:
`| head -n 20`:

    :::bash
    python3 listo-de-vortoj.py | sort | uniq -c | sort -n -r | head -n 20

Ĉi tio liveras la jenan rezulton:

    :::bash
    274 καὶ
    193 δὲ
    169 τὸν
    105 τῷ
     86 ἡ
     84 ἐν
     60 γὰρ
     56 τῆς
     52 εἰς
     41 μὲν
     40 τοῖς
     35 χαιρέας
     31 οὐ
     30 ὡς
     27 τοῦ
     27 ὅτι
     26 ἀπὸ
     26 οὖν
     24 πρὸς
     22 αὐτὸν

Kompreneble, la plejparto de la vortoj, kiuj pleje ripetiĝas, estas
konjunkcioj kaj prepozicioj ĉi-okaze, kaj en la malnova greka multaj
vortoj estas deklinaciitaj. Se ni volas solvi tiujn problemojn, ni devus
perfektigi la Python-programon.

Sed por via specifa problemo vi mem devos enprofundiĝi. Ĉi tie mi volas
simple montri, ke nur per listo de vortoj vi povas kalkuli la oftecon,
kiam ili aperas, kaj ordigi ilin per iloj de
[GNU Coreutils](https://www.gnu.org/software/coreutils/), kiuj jam estas
instalitaj en GNU/Linukso. Mi uzis nenion krom programoj, kiujn mi jam
havis instalitaj en mia operaciumo: ne necesas reinventi la radon nek
uzi eksterajn kodotekojn.
