Author: Jorge Maldonado Ventura
Category: Edición de imágenes
Date: 2016-08-10 10:17
Lang: es
Modified: 2017-03-26 14:38
Slug: convertir-un-video-a-formato-gif
Status: published
Tags: animate, cat animation, convert, ffmpeg, GIF, GNU/Linux, ImageMagick
Title: Convertir un vídeo a formato GIF

[![Animación de un gato hecha con
Pygame](/wp-content/uploads/2016/08/cat_animation.gif){.aligncenter .size-full .wp-image-235 width="1000" height="196"}](/wp-content/uploads/2016/08/cat_animation.gif)

Hay muchas formas y programas útiles para hacer esto. Aquí os cuento mi
método:

1.  **Si no tienes `ffmpeg`, instálalo** (en distribuciones de GNU/Linux
    basadas en Debian con `sudo apt-get install ffmpeg`).
2.  **Separa el vídeo en fotogramas**:
    `mkdir fotogramas && ffmpeg -i tuvideo fotogramas/fotograma%04d.png ` (los
    fotogramas se guardarán en la carpeta `fotogramas`).
3.  **Borra los fotogramas que no necesites** (paso opcional).
4.  **Une los fotogramas extraídos creando un gif con el programa
    adecuado**. Puedes usar `animate`
    (`animate fotograma*.png anim.gif`) para hacerlo rápido, pero si
    buscas controlar mejor el espacio de tiempo entre fotogramas utiliza
    `convert` (`convert -delay 0 -loop 0 fotograma*.png anim.gif`).
    `-delay num` indica el tiempo entre los fotogramas y `-loop 0`
    quiere decir que cuando acabe de reproducir las imagenes empezará
    de nuevo. Estas herramientas suelen estar ya instaladas en muchas
    distribuciones de GNU/Linux, forman parte de `ImageMagick`
    (<http://imagemagick.org/script/index.php>).
