Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-12-05
Modified: 2020-12-09 02:00
Lang: es
Slug: uso-avanzado-de-ublock-origin
Tags: Abrowser, Chromium, extensión, Firefox, Google Chrome, Microsoft Edge, Opera, tutorial, uBlock Origin
Title: Funciones avanzadas de uBlock Origin
Image: <img src="/wp-content/uploads/2020/12/parrilla-de-bloqueo-de-uBlock-Origin.png" alt="" width="1154" height="674" srcset="/wp-content/uploads/2020/12/parrilla-de-bloqueo-de-uBlock-Origin.png 1154w, /wp-content/uploads/2020/12/parrilla-de-bloqueo-de-uBlock-Origin-577x337.png 577w" sizes="(max-width: 1154px) 100vw, 1154px">

Anteriormente expliqué [cómo instalar uBlock
Origin](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/)
y cómo [eliminar elementos molestos (filtros
cosméticos)](/bloqueo-de-elementos-con-ublock-origin/). Este artículo es
para usuarios avanzados que desean tener un control enorme sobre lo que
su navegador carga. En el vídeo de abajo explico cómo bloquear peticiones
HTML estableciendo reglas de bloqueo, cómo configurar la extensión, cómo
restablecerla a su estado original, cómo entender las estadísticas de
uso y cómo añadir páginas a la lista blanca. El texto que sigue al vídeo
contiene una referencia de los componentes de la interfaz de uBlock
Origin.

<!-- more -->
<video controls poster="/wp-content/uploads/2020/12/parrilla-de-bloqueo-de-uBlock-Origin.png" src="/video/funciones-avanzadas-de-uBlock-Origin.webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

<figure>
    <img src="/wp-content/uploads/2020/12/parrilla-de-bloqueo-de-uBlock-Origin-numerada.png" alt="" usemap="#lista-de-elementos">
    <figcaption class="wp-caption-text">Pincha en cualquier número de la imagen para ver lo
    que hace.</figcaption>
</figure>

<map name="lista-de-elementos">
  <area shape="rect" coords="266,32,324,80" alt="Elemento 1" href="#elemento1">
  <area shape="rect" coords="330,34,386,81" alt="Elemento 2" href="#elemento2">
  <area shape="rect" coords="392,61,449,104" alt="Elemento 3" href="#elemento3">
  <area shape="rect" coords="571,59,628,156" alt="Elemento 4" href="#elemento4">
  <area shape="rect" coords="460,56,560,154" alt="Elemento 5" href="#elemento5">
  <area shape="rect" coords="391,108,450,156" alt="Elemento 6" href="#elemento6">
  <area shape="rect" coords="386,197,434,256" alt="Elemento 7" href="#elemento7">
  <area shape="rect" coords="434,197,486,256" alt="Elemento 8" href="#elemento8">
  <area shape="rect" coords="486,197,536,256" alt="Elemento 9" href="#elemento9">
  <area shape="rect" coords="536,197,588,256" alt="Elemento 10" href="#elemento10">
  <area shape="rect" coords="588,197,639,256" alt="Elemento 11" href="#elemento11">
  <area shape="rect" coords="388,396,448,447" alt="Elemento 12" href="#elemento12">
  <area shape="rect" coords="448,396,511,447" alt="Elemento 13" href="#elemento13">
  <area shape="rect" coords="511,396,575,447" alt="Elemento 14" href="#elemento14">
  <area shape="rect" coords="575,396,640,447" alt="Elemento 15" href="#elemento15">
</map>

<ol>
<li id="elemento1"><strong>Peticiones HTTP globales</strong>: Se aplican a todos los sitios web.</li>
<li id="elemento2"><strong>Peticiones HTTP locales</strong>: Se aplican solo al sitio web en el que te
encuentras actualmente.</li>
<li id="elemento3"><strong>Hace permanentes los cambios</strong>.</li>
<li id="elemento4"><strong>Recarga la página</strong>.</li>
<li id="elemento5"><strong>Botón de apagado grande</strong>: deshabilita uBlock Origin para la página
en que te encuentras.</li>
<li id="elemento6"><strong>Revierte los cambios</strong> de la página web en la que te encuentras.</li>
<li id="elemento7"><strong>Bloquea ventanas emergentes</strong>.</li>
<li id="elemento8"><strong>Bloquea elementos multimedia</strong> de más de 50 KB (se puede cambiar este
tamaño desde el menú de configuración).</li>
<li id="elemento9"><strong>Deshabilita el filtrado cosmético</strong>.</li>
<li id="elemento10"><strong>Bloquea fuentes tipográficas externas</strong>.</li>
<li id="elemento11"><strong>Bloquea guiones de JavaScript externos</strong>.</li>
<li id="elemento12">Entra al <strong>modo de eliminación de elementos</strong>.</li>
<li id="elemento13">Entra al <strong>modo de selección de elementos</strong>.</li>
<li id="elemento14">Va al <strong>registro de peticiones</strong>.</li>
<li id="elemento15">Abre el <strong>panel de control</strong>.</li>
</ol>

Podríamos agrupar estos elementos de la siguiente forma:

- **Cuadrícula de filtrado dinámico** (1, 2): requiere habilitar la opción **Soy usuario avanzado** en el menú de **configuración** (véase elemento 15 del diagrama).
- **Botones de edición** (3, 4, 6): Cuando editas algo aparecen estos botones.
- **Botón de apagado grande** (5).
- **Herramientas** (7-15): solo modifican el comportamiento de la página
  en la que te encuentras.

El panel de control nos permite adaptar uBlock Origin a nuestras
necesidades. Es necesario ser conscientes de que al modificar ajustes
podemos aumentar la posibilidad de que «se rompan» páginas web.

Hay minucias que me dejo en el tintero. Si quieres aprender más sobre
Ublock Origin, te recomiendo leer [su
documentación](https://github.com/gorhill/uBlock/wiki) (en inglés).
