Author: Jorge Maldonado Ventura
Category: Tricks
Date: 2023-01-07 15:00
Modified: 2023-03-08 09:00
Lang: en
Slug: saltar-muros-de-pago-de-periódicos
Save_as: how-to-bypass-newspaper-paywalls/index.html
URL: how-to-bypass-newspaper-paywalls/
Tags: paywall, trick
Title: How to bypass newspaper paywalls
Image: <img src="/wp-content/uploads/2022/05/saltando-muro.jpeg" alt="Hombre saltando un muro" width="2250" height="1500" srcset="/wp-content/uploads/2022/05/saltando-muro.jpeg 2250w, /wp-content/uploads/2022/05/saltando-muro.-1125x750.jpg 1125w, /wp-content/uploads/2022/05/saltando-muro.-562x375.jpg 562w" sizes="(max-width: 2250px) 100vw, 2250px">

Many newspapers display paywalls that prevent us from seeing the full
content of articles. There are, however, a few tricks to avoid them.

A useful browser extension that allows us to bypass those paywalls is
[Bypass Paywalls Clean](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean#installation). This extension works for popular websites, and
others can easily be added. How does it work? Basically, the extension
uses tricks such as disabling JavaScript, disabling cookies or changing
the [user agent](https://en.wikipedia.org/wiki/User_agent) to that of a known [web crawler](https://en.wikipedia.org/wiki/Web_crawler) (such as [Googlebot](https://en.wikipedia.org/wiki/Googlebot)).

There is no need to install the above extension if you don't want to.
Read on to find out in detail the tricks you can use to avoid most
paywalls.

<!-- more -->

## 1. Disable JavaScript

Sometimes, the newspaper puts up the paywall using JavaScript.
[Disabling JavaScript](/en/disable-JavaScript-in-Firefox/)
would be enough to remove the paywall.

## 2. Delete cookies

Sometimes newspapers display paywalls after viewing a few articles. This
happens because they don't want to scare off new visitors with a
paywall. To make them think you are a new visitor, simply [delete your
cookies](/elimina-las-cookies-en-navegadores-derivados-de-firefox/).

## 3. Change the user agent

Many newspapers want to restrict access to users, but not to web
crawlers, as this would affect their search engine ranking. There are
extensions to change the user agent in a simple way: one of them is
[User-Agent Switcher and Manager](https://addons.mozilla.org/en-US/firefox/addon/user-agent-string-switcher/).
You would have to impersonate web crawlers such as Googlebot or Bingbot.

## 4. Blocking elements

Sometimes it works to remove elements from web pages. We can do this by
opening the element inspector of the web browser and removing the
elements that belong to the paywall, but it is easier to
[do this with uBlock Origin](/bloqueo-de-elementos-con-ublock-origin/).

## 5. Pirate repositories

There are pirate repositories such as
[Sci-Hub](https://en.wikipedia.org/wiki/Sci-Hub) or
[Library Genesis](https://en.wikipedia.org/wiki/Library_Genesis). I
don't know if there is a specialised repository for Internet newspapers.
