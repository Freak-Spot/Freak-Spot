Author: Jesús E.
Category: Desarrollo web
Date: 2018-02-15 16:38
Modified: 2018-12-27 08:08
Lang: es
Slug: como-generar-freakspot
Status: published
Tags: educación, GNU/Linux, Pelican, Python, software libre, tutorial, video
Title: ¿Cómo generar Freak Spot?

Muchas veces alguno que otro usuario se ha preguntado alguna vez
cómo se genera este sitio web, la verdad es que es bastante sencillo
una vez explicado. Es por ello que en el siguiente vídeo se detallan
los pasos a seguir.

<!-- more -->

<video controls>
  <source src="https://archive.org/download/libreweb/freakspot.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por favor, cambia o actualiza tu navegador</p>
</video>

Durante el vídeo se explican los comandos para su generación, utilizando
un entorno virtual con python-virtualenv.

1. Clonación del repositorio e ingreso al directorio de trabajo

        $ git clone https://notabug.org/Freak-Spot/Freak-Spot

        $ cd Freak-Spot

2. Generar el entorno virtual y activarlo

        $ virtualenv env

        $ source env/bin/activate

3. Instalación de dependencias pip y npm

        $ pip install -U pelican beautifulsoup4 markdown babel

        $ npm install uglifycss uglify-js

4. Generar el sitio web

        $ cd freak-theme && make compile

        $ cd .. && make html
