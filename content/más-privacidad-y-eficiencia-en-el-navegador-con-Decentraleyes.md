Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-07-13 17:30
Lang: es
Slug: más-privacidad-y-eficiencia-en-firefox-con-Decentraleyes
Tags: Decentraleyes, Firefox, JavaScript, red de distribución de contenidos
Title: Más privacidad y eficiencia en Firefox con Decentraleyes
Image: <img src="/wp-content/uploads/2020/07/logo-de-decentraleyes.png" alt="">

JavaScript permite la modificación dinámica de documentos HTML. Muchas
de las bibliotecas de JavaScript requieren de mucho ancho de banda, y
los algunos desarrolladores web utilizan [redes de distribución de
contenidos](https://es.wikipedia.org/wiki/Red_de_distribuci%C3%B3n_de_contenidos)
para cargar el JavaScript desde estas. Entonces dependen de redes de
distribución de contenidos que, por supuesto, controlan por lo general
grandes empresas, porque es caro mantenerlas.

El problema técnico principal es que si la red de distribución de
contenidos falla, la página web no funcionará correctamente. Respecto a
la privacidad, quienes controlan el servidor pueden almacenar los
metadatos de los visitantes, como la dirección IP y la página visitada.
En cuanto a la seguridad, en cualquier momento podrían modificar el
código para introducir funcionalidades maliciosas.

No hay nada malo en usar redes de distribución de contenidos para
reducir el ancho de banda; el problema está en usar redes que no
controlas, como hacen tantos programadores incompetentes a día de hoy.

Como el problema está tan extendido, en este artículo os presento
[Decentraleyes](https://addons.mozilla.org/es/firefox/addon/decentraleyes/),
una extensión para Firefox que elimina este molesto intermediario «para
así proporcionar una entrega de archivos locales (empaquetados) a
velocidades exorbitantes y mejorar la privacidad en linea», como dice su
descripción. Las redes soportadas son las principales: «Google Hosted
Libraries, Microsoft Ajax CDN, CDNJS (Cloudflare), jQuery CDN (MaxCDN),
jsDelivr (MaxCDN), Yandex CDN, Baidu CDN, Sina Public Resources, y UpYun
Libraries».

<!-- more -->

Una vez clicado el enlace anterior e instalado el complemento, podemos
comprobar su correcto funcionamiento en [esta página de
prueba](https://decentraleyes.org/test/).

Podemos también configurar la extensión si queremos bloquear las
peticiones cuando no se encuentre el recurso localmente o tenemos otras
necesidades configurables.

<figure>
<a href="/wp-content/uploads/2020/07/configuración-Decentraleyes.png">
<img src="/wp-content/uploads/2020/07/configuración-Decentraleyes.png" alt="" width="924" height="351" srcset="/wp-content/uploads/2020/07/configuración-Decentraleyes.png 924w, /wp-content/uploads/2020/07/configuración-Decentraleyes-462x175.png 462w" sizes="(max-width: 924px) 100vw, 924px">
</a>
    <figcaption class="wp-caption-text">Preferencias de Decentraleyes</figcaption>
</figure>
