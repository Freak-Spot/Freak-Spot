Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-02-07 13:06
Lang: eo
Slug: es-importante-que-el-software-libre-use-infraestructuras-de-software-libre
Tags: GitHub, GitLab, liberaj programoj
Save_as: gravas-ke-liberaj-programoj-uzu-liberan-infrastrukturon/index.html
URL: gravas-ke-liberaj-programoj-uzu-liberan-infrastrukturon/
Title: Gravas, ke liberaj programoj uzu liberan infrastrukturon

<span style="text-decoration:underline">Ĉi tiu artikolo estas traduko de
la artikolo [«It is important for free software to use free software
infrastructure»](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html)
publikigita de Drew Devault sub la
<a href="https://creativecommons.org/licenses/by-sa/2.0/deed.eo"><abbr
title="Creative Commons Atribuite-Samkondiĉe 2.0 Ĝenerala">CC BY-SA
2.0</abbr></a>-permesilo.</span>

*Averto: mi fondis projekton kaj firmaon centritan sur libera
infrastrukturo. Mi elektas ne nomi ilin en ĉi tiu afiŝo kaj mi nur
rekomendos solvojn, pri kiuj mi ne havas personan intereson.*

La projektoj de liberaj programoj bezonas infrastrukturon: lokon por
gastigi la kodon, por faciligi aferojn kiel la kontroladon de kodo,
subtenon al la fina uzanto, sekvadon de cimoj, merkatiko, ktp. Kutima
ekzemplo de ĉi tio estas la «forĝeja» platformo: infrastrukturo, kiu
anoncas sin kiel universalan vendejon por multaj el la necesoj de
liberaj projektoj en unu loko, kiel la koda gastigado kaj kontrolado,
cimsekvado, diskutoj kaj tiel plu. Multaj projektoj ankaŭ uzos kromajn
platformojn por provizi aliajn specojn de infrastrukturo: babilejoj,
diskutejoj, sociaj retejoj, ktp.

Multaj el ĉi tiuj bezonoj havas <abbr title="Projektoj, kiuj ne uzas
permesilon kongruan kun la reguloj pri liberaj programoj">neliberajn</abbr>
solvojn disponeblajn. GitHub estas populara proprieta kodejo, kaj
GitLab, la plej granda konkuranto de GitHub, estas parte nelibera.
Kelkaj projektoj uzas Discord-on aŭ Slack-on por babilejoj, Reddit-on
kiel diskutejon aŭ Twitter-on kaj Facebook-on por merkatikado, atingo
kaj subteno; ĉiu ĉi tiuj estas neliberaj. Laŭ mia opinio, dependi de ĉi
tiuj platformoj por provizi infrastrukturon al via libera projekto estas
eraro.

Kiam via libera projekto elektas uzi neliberan platformon, vi donas al
ĝi oficialan konfidan voĉdonon nome de via projekto. Per aliaj vortoj,
vi pruntas iom de la kredindeco kaj legitimeco de via projekto al la
platformoj, kiujn vi elektas. Ĉi tiuj platformoj estas difinitaj per
retefikoj, kaj via elekto estas investo en tiu reto. Mi dubus pri ĉi tiu
investo mem kaj pri la saĝeco oferi al ĉi tiu platformoj vian konfidon
kaj legitimecon, sed ekzistas ankaŭ pli maltrankviliga sekvo de ĉi tiu
elekto: investo en nelibera platformo estas ankaŭ *manko de investo* en
liberaj alternativoj.

Denove, la retefikoj estas la ĉefa kialo de la sukceso de ĉi tiuj
platformoj. Grandaj komercaj platformoj havas multajn avantaĝojn
tiurilate: grandajn merkatikajn buĝetojn, multe da kapitalo de
investistoj kaj la avantaĝon de la posedo. Ju pli granda estu la
posedanta platformo, des pli malfacila iĝas la tasko konkuri kun ĝi.
Komparu ĉi tion kun liberaj platformoj, kiuj kutime ne havas la helpon de
grandaj kvantoj da investoj aŭ grandajn merkatikajn buĝetojn. Krome,
estas pli verŝajne, ke firmaoj fie agu por sekurigi sian pozicion ol
projektoj de liberaj programoj. Se viaj propraj liberaj projektoj
konkuras kun proprietaj komercaj opcioj, vi devas tre bone koni ĉi tiujn
defiojn.

La liberaj platformoj havas esencan malavantaĝon, kaj via fido, aŭ manko
de fido, en ili estas tre grava. GitHub ne maltrankviliĝos, se via
projekto elektas gastigi sian kodon aliloke, sed elekti [Codeberg](https://codeberg.org/)-on,
ekzemple, signifas multe por ili. Fakte via elekto malproporcie gravas
al la liberaj platformoj: elekti GitHub-on doloras Codeberg-on multe pli
ol elekti Codeberg-on doloras GitHub-on. Kaj kial devus projekto elekti
uzi vian oferon antaŭ la proprietaj alternativoj, se vi ne donis al ili
la saman ĝentilaĵon? Solidareco inter liberaj projektoj gravas por
altigi la ekosistemon kiel tuton.

<!-- more -->

Tamen, por kelkaj projektoj, tio kio gravas al ili havas nenian rilaton
kun la profito de la ekosistemo kiel tuto, sed anstataŭe nur konsideras
la eblecon de individua kresko kaj populariĝo de sia projekto. Multaj
projektoj elektas prioritatigi la aliron al establita publiko, kiun la
grandaj komercaj platformoj provizas, por maksimumigi iliajn ŝancojn
populariĝi kaj ĝui iujn el la flankaj efikoj de tiu populareco, kiel pli
da kontribuantoj[^1]. Tiuj projektoj preferus fie pliigi la problemon de
la retefikoj anstataŭ riski iom el sia socia kapitalo en malpli
populara platformo.

[^1]: Mi devas rimarkigi ĉi tie, ke mi nekritike prezentas la
    «popularecon» kiel bonan havindan aferon por projekto, kio
    adaptiĝas, mi pensas, al la mensaj procezoj de la projektoj, kiujn
    mi priskribas. Tamen la vero ne estas tiel. Eble temo por alia
    artikolo alitage.

Por mi ĉi tio estas tute egoista kaj maletika, kvankam vi povus havi
malsamajn etikajn normojn. Malbonŝance argumentoj kontraŭ la plejmulto
de la komercaj platformoj pro iu ajn etika normo abunde disponeblas, sed
ili kutimas esti facile venkita per biaso de konfirmo. Iu, kiu povus
laŭte kontraŭi la praktikojn de la usona agentejo Enmigrada kaj Dogana
Devigo, ekzemple, povas rapide trovi ian pravigon por daŭre uzi
GitHub-on malgraŭ ilia kunlaboro kun ili. Se ĉi tiu ekzemplo ne gustas
al vi, estas multaj ekzemploj por ĉiuj el la multaj platformoj.
Projektoj, kiuj ne volas moviĝi, kutime ignoras ilin[^2].

[^2]: Ekzemplo speciale konata estas la movado
  [Ethical Source](https://ethicalsource.dev/). Mi malkonsentas kun ili
  pro multaj kialoj, sed rilatas al ĉi tiu artikolo la fakto, ke ili
  publikigas (ne liberajn) programajn permesilojn, kiuj rekomendas
  kontraŭkapitalismajn sentojn kiel laboristajn rajtojn kaj etikajn
  juĝojn kiel neperforton, farante tion en... GitHub kaj Twitter,
  privataj profitcelaj platformoj kun miriado da publikigitaj etikaj
  malobservoj.

Sed, por esti klara, mi ne petas al vi, ke vi uzu malpli bonajn
platformojn pro filozofaj aŭ altruismaj kialoj. Ĉi tiuj estas nur unu el
la multaj faktoroj, kiuj devus kontribui al via decidiĝo, kaj la
kapableco estas alia konsiderinda faktoro. Cetere, multaj liberaj
platformoj estas, almenaŭ laŭ mia opinio, funkcie superaj al sia
proprieta konkurantaro. Ĉu iliaj diferencoj estas pli bonaj por la
unikaj bezonoj, estas io, kion mi lasas al vi, por ke vi mem esploru;
sed multaj projektoj tute ne ĝenas sin esplori. Restu trankvila: ĉi
tiuj projektoj ne estas getoj, kiuj loĝas sub la ombro de siaj grandaj
komercaj kontraŭantoj, sed ekscitantaj platformoj per siaj propraj
meritoj, kiuj oferas multajn unikajn avantaĝojn.

Krome, se vi bezonas, ke ili faru ion malsame, por ke ili pli bone
adaptiĝu al la bezonoj de via projekto, vi havas la povon plibonigi
ilin. Vi ne estas submetita al la kapricoj de la komerca organizo, kiu
respondecas pri la kodo, atendante ke ili donu prioritaton al la
problemon aŭ eĉ unue zorgu pri ĝi. Se problemo estas grava por vi, tio
sufiĉas al vi, por ke ĝi estu riparita en libera platformo. Vi eble
ne pensas, ke vi havas la tempon aŭ la scion por fari tion (kvankam eble
unu el viaj kunlaborantoj povas), sed pli grave, ĉi tio estigas
pensmanieron de kolektiva posedo kaj respondeco pri ĉiuj liberaj
programoj kiel tuto &mdash; popularigu ĉi tiun filozofion kaj facile
povus esti vi kiu simile ricevas kontribuon morgaŭ.

Resume elekti neliberajn platformojn estas individuisma mallongatempa
investo, kiu donas prioritaton al la ŝajna aliro de via projekto al la
populareco super la sukceso de la libera ekosistemo kiel tuto. Aliflanke
elekti liberajn platformojn estas kolektivisma investo en la longatempa
sukceso de la libera ekosistemo kiel tuto, kondukante al ĝia ĉiea
kreskado. Via elekto gravas. Vi povas helpi la liberan ekosistemon
elektante liberajn platformojn aŭ vi povas damaĝi la liberan ekosistemon
elektante neliberajn platformojn. Bonvolu elekti zorge.

Jen kelkaj rekomendoj pri liberaj iloj, kiuj faciligas kutimajn bezonojn
por projektoj de liberaj programoj:

- Kodejoj: [Codeberg](https://codeberg.org/),
  [Gitea](https://gitea.io/en-us)\*,
  [Gerrit](https://www.gerritcodereview.com/)\*, [GitLab](https://gitlab.com/)†.
- Tujmesaĝado: [Matrix](https://matrix.org/), [Libera
  Chat](https://libera.chat/)[^3].
- Publikigo: [Codeberg Pages](https://codeberg.page/), [Write.as](https://write.as/), [PeerTube](https://joinpeertube.org/).
- Sociaj retejoj: [Mastodon](https://joinmastodon.org/), [Lemmy](https://join-lemmy.org/)
- Dissendolistoj: [FreeLists](https://www.freelists.org/),
  [public-inbox](https://public-inbox.org/public-inbox-overview.html)\*,
  [Mailman](http://www.list.org/)\*.

\* Nur memgastigado.<br>
† Parte nelibera, nur rekomendita, se la aliaj opcioj ne estas taŭgaj.

[^3]: Mi multfoje prezentis la argumentojn de ĉi tiu afiŜo al la teamo
  de Libera, sed ili ankoraŭ dependas de GitHub, Twitter kaj Facebook.
  Ili estas unu el miaj kialoj por skribi ĉi tiun afiŝon. Mi esperas, ke
  ili iutage ŝanĝu sian opinion.
