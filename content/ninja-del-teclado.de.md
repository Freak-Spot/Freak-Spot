Author: Jorge Maldonado Ventura
Category: Textbearbeitung
Date: 2023-01-06 13:30
Lang: de
Slug: combinaciones-de-teclado-y-tecla-Componer-GNU/Linux
Save_as: schnelles-Schreiben-beliebiger-Zeichen-mit-der-Tasatur-in-GNULinux/index.html
URL: schnelles-Schreiben-beliebiger-Zeichen-mit-der-Tasatur-in-GNULinux/
Tags: Einstellungen, Compose, GNU/Linux, Cinammon, Tastatur, Debian, Debian 11, Unicode
Title: Schnelles Schreiben beliebiger Zeichen mit der Tastatur in GNU/Linux
Image: <img src="/wp-content/uploads/2020/04/ninja-del-teclado.jpg" alt="">

Wegen deiner Arbeit oder was auch immer musst du manchmal Zeichen
eingeben, die du auf der Tastatur nicht finden kannst (¹, «, &mdash;, ä,
ĉ, ß, ¢ usw.) oder du hast eine abgebrochene Taste. Was tust du? <!-- more -->

1. Das Zeichen im Internet suchen, es kopieren und einfügen.
2. Die Funktionen des Texteditors verwenden, um Sonderzeichen einzufügen.
3. Den Unicode-Code im Internet suchen und ihn über die
   Tastenkombination deines Betriebssystems einzugeben.[^1].
4. Lernen, wie man diese seltenen Zeichen mit leicht zu merkenden
   Tastenkombinationen tippt, und sie in Zukunft zu verwenden

Wenn deine Antwort 1, 2 oder 3 ist, solltest du weiter lesen; die beste
Lösung ist 4, wenn du schon mehrmals die Notwendigkeit hattest, fremde
Zeichen einzugeben.

Um dir eine Vorstellung zu geben, hier eine kleine Liste von Kombinationen, die
ich auf der spanischen Tastatur in GNU/Linux verwende[^2]:

- <kbd>Alt Gr</kbd> + <kbd>z</kbd> = «
- <kbd>Alt Gr</kbd> + <kbd>x</kbd> = »
- <kbd>Umschalttaste</kbd> + <kbd>^</kbd>, dann eine Nummer= ¹
- <kbd>Componer</kbd> + <kbd>_</kbd>, dann eine Nummer = ₁
- <kbd>Alt Gr</kbd> + <kbd>v</kbd> = “
- <kbd>Alt Gr</kbd> + <kbd>b</kbd> = ”
- <kbd>Alt Gr</kbd> + <kbd>Mayús</kbd> + <kbd>v</kbd> = ‘
- <kbd>Alt Gr</kbd> + <kbd>Mayús</kbd> + <kbd>b</kbd> = ’
- <kbd>Compose</kbd>, dann <kbd>-</kbd>, dann <kbd>-</kbd>, dann <kbd>-</kbd> = &mdash;

Wenn du wissen möchtest, wie du eine dieser Zeichen schreiben kannst,
hast du vielleicht schon das Problem gelöst. "Aber Moment!", vielleicht
fragst du noch...

- Woher kenne ich diese Kombinationen?
- Was ist die Compose-Taste?

## Die Tastenkombinationen sehen

Im Menü **Tastaturbelegungen** findest du, zumindest in der
Cinnamon-Desktop-Umgebung, eine Option zur Anzeige der Tastaturbelegung.
Wenn du ein wenig in den Tastatureinstellungen herumstöberst, findest
du die Tastaturbelegung. Es gibt auch den Befehl `xmodmap -pke`.

<figure>
    <a href="/wp-content/uploads/2023/01/Tasaturbelegungen-deutsch-Cinnamon.png">
    <img src="/wp-content/uploads/2023/01/Tasaturbelegungen-deutsch-Cinnamon.png" alt="" width="1706" height="777" srcset="/wp-content/uploads/2023/01/Tasaturbelegungen-deutsch-Cinnamon.png 1706w, /wp-content/uploads/2023/01/Tasaturbelegungen-deutsch-Cinnamon-853x388.png 853w, /wp-content/uploads/2023/01/Tasaturbelegungen-deutsch-Cinnamon-426x194.png 426w" sizes="(max-width: 1706px) 100vw, 1706px">
    </a>
    <figcaption class="wp-caption-text">Deutsche Tastaturbelegung in Cinnamon (Debian testing)</figcaption>
</figure>

In der Abbildung sehen wir, dass eine Taste bis zu vier Zeichen hat.
Auf der physischen Tastatur, die du vor dich hast, gibt es
wahrscheinlich nicht so viele gezeichnet. Schauen wir uns einen
bestimmten Taste an, nämlich die, die dem Buchstaben v entspricht.
Wie wir sehen, sind auf dieser Taste mehrere Zeichen zu sehen, die
jeweils so geschrieben sind:

- *Unten links (**v**)*. Einfach diese Taste tippen
- *Oben links (**V**)*. <kbd>Umschalttaste</kbd> + *Taste*
- *Unten rechts (**“**)*. <kbd>Alt Gr</kbd> + *Taste*
- *Oben rechts (**‘**)*. <kbd>Umschalttaste</kbd> + <kbd>Umschalttaste</kbd> + *Taste*

## Compose-Taste

Das Problem entsteht, wenn die Taste, die wir eingeben wollen, nicht auf
unserer Tastaturbelegung vorhanden ist, vielleicht ist es ein Symbol wie
&mdash;, → oder ein anderes.

In diesem Fall müssen wir die [Compose-Taste](https://de.wikipedia.org/wiki/Compose-Taste)
definieren, wenn unsere Tastatur nicht über sie verfügt oder sie nicht
zugewiesen ist. Um dies zu tun, können wir es in den
Tastatureinstellungen unserer Belegung konfigurieren oder die Datei
`/etc/default/keyboard` modifizieren, indem wir die Variable
`XKBOPTIONS` um die Taste erweitern, die wir verwenden wollen (`lalt`,
für linke Alt; `rwin`, rechte
[Super-taste](https://es.wikipedia.org/wiki/Windows_(Taste)); `lwin`,
linke Super-Taste...). Wenn ich die rechte Alt-Taste als Compose-Taste
verwenden wollte, würde ich diese Änderung in der Datei
`/etc/default/keyboard` vornehmen:

    :::diff
    - XKBOPTIONS=""
    + XKBOPTIONS="compose:ralt"

Versuch einmal eine Kombination wie <kbd>Compose</kbd> dann <kbd>-</kbd> dann <kbd>></kbd>
(sie sollte → eingeben). Wir können nach den vordefinierten Zeichen mit
ihren jeweiligen Kombinationen im Verzeichnis `/usr/share/X11/locale/`
suchen &mdash; mit einer rekursiven Suche mit grep (`grep -R symbol`)
ist es einfach. Das Definitionsformat kann in der [Compose(5)-Manualseite](https://man.archlinux.org/man/Compose.5)
gefunden werden. Wenn eine Kombination für das Zeichen, das wir
schreiben wollen, nicht existiert, können wir sie definieren, indem wir
eine `~/.XCompose`-Datei erstellen.

## Es gibt noch viel mehr...

Ich wollte nicht im Detail auf tote Tasten und andere Aspekte der
Tastaturen eingehen. Mit diesem Wissen wird es für Sie sicher einfacher
sein Es ist einfach, mehr Informationen zu erhalten, wenn das wenig für
dich war.

Ich hoffe, dass du jetzt mit dem, was ich dich in diesem Artikel
beigebracht habe, viele seltsame Zeichen mit der Gewandtheit eines Ninja
schreiben kannst. ☺.

[^1]: Auf dem GNU/Linux-Betriebssystem mit dem X-Fenstersystem, musst du
  <kbd>Ctrl</kbd>+<kbd>Shift</kbd> gedrückt halten und <kbd>u</kbd>
  drücken, dann <kbd>Ctrl</kbd>+<kbd>Shift</kbd> loslassen,
  den Hexadezimalcode eingeben und die Eingabetaste drücken.
[^2]: Das Zeichen + bedeutet, dass die Tasten gleichzeitig gedrückt
  werden müssen. Wenn ich sage «dann», müssen sie nacheinander gedrückt
  werden. Nachdem die = erscheint das Produkt der Tastenkombination.
