Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-04-02 15:00
Lang: es
Slug: es-importante-que-el-software-libre-use-infraestructuras-de-software-libre
Tags: GitHub, GitLab, software libre
Title: Es importante que el <i lang="en">software</i> libre use infraestructuras de <i lang="en">software</i> libre

<span style="text-decoration:underline">Este artículo es una traducción
del artículo [«It is important for free software to use free software
infrastructure»](https://drewdevault.com/2022/03/29/free-software-free-infrastructure.html)
publicado por Drew Devault bajo la licencia
<a href="https://creativecommons.org/licenses/by-sa/2.0/deed.es"><abbr
title="Creative Commons Atribución-CompartirIgual 2.0 Genérica">CC BY-SA
2.0</abbr></a>.</span>

*Aviso: he fundado un proyecto y una empresa centrada en infraestructura
de* <span lang="en">software</span> *libre. Decido no nombrarlos en
esta publicación y solo recomendaré soluciones en las que no tengo un
interés personal.*

Los proyectos de programas libres necesitan infraestructura; un lugar
para facilitar cosas como la revisión de código, apoyo al usuario final,
seguimiento de errores, mercadotecnia, etc. Un ejemplo común de esto es
la plataforma de «forja»: infraestructura que se anuncia como una tienda
de todo en uno para muchas de las necesidades de proyectos libres, como
alojamiento y revisión de código, seguimiento de errores, discusiones,
etc. Muchos proyectos también recurrirán a plataformas adicionales para
proporcionar otros tipos de infraestructura: salas de chat, foros, redes
sociales y demás.

Muchas de estas necesidades tienen a su disposición soluciones <abbr
title="Proyectos que no usan una licencia compatible con las directrices
del software libre">no libres</abbr>, privativas. GitHub es una popular
forja de código privativa, y GitLab, el mayor competidor de GitHub, es
parcialmente no libre. Algunos proyectos usan Discord o Slack para salas
de chat, Reddit como foro, o Twitter y Facebook para mercadotecnia,
divulgación y soporte; todos estos son no libres. En mi opinión,
depender de que estas plataformas proporcionen infraestructura para tus
proyectos libres es un error.

Cuando tu proyecto libre elige usar una plataforma no libre, le das un
voto de confianza oficial en nombre de tu proyecto. En otras palabras,
le prestas parte de la credibilidad y legitimidad de tu proyecto a las
plataformas que eliges. Estas plataformas son fruto de efectos de red,
y tu elección es una inversión en esa red. Yo cuestionaría esta
inversión por si sola, la conciencia de que estás brindando a estas
plataformas tu confianza y legitimidad; pero también hay una
consecuencia más preocupante de esta elección: una inversión en una
plataforma no libre también es una *no inversión* en las alternativas
libres.

Repito, los efectos de red son el principal motivo del éxito de estas
plataformas. Las grandes plataformas comerciales tienen un montón de
ventajas en este sentido: grandes presupuestos de mercadotecnia, mucho
capital de inversores y la ventaja de la titularidad. Cuanto más grande
sea la plataforma titular, mayor dificultad entraña la tarea de competir
con ella. Compara esto con las plataformas de <i lang="en">software</i>
libre, que generalmente no tienen el beneficio de grandes sumas de
inversiones o grandes presupuestos de mercadotecnia. Asimismo, las
empresas son más propensas a jugar sucio para asegurar su posición que
los proyectos de <i lang="en">software</i> libre. Si tus propios
proyectos compiten con opciones comerciales privativas, ya debes estar
muy familiarizado con estos desafíos.

Las plataformas libres están en una inherente desventaja, y tu fe, o
falta de fe, en ellas tiene mucho peso. A GitHub no le quitará el sueño
que tu proyecto decida alojar su código en otro lugar, pero elegir
[Codeberg](https://codeberg.org/), por ejemplo, significa mucho para
ellos. En efecto, tu elección les importa de manera desproporcionada a
las plataformas libres: elegir GitHub daña a Codeberg mucho más de lo
que elegir Codeberg daña a GitHub. ¿Y por qué debería un proyecto elegir
tu oferta en vez de las alternativas privativas si no le das la misma
cortesía? La solidaridad del <i lang="en">software</i> libre y de código
abierto es importante para elevar el ecosistema en conjunto.

<!-- more -->

Sin embargo, para algunos proyectos, lo que en última instancia es
importante para ellos tiene poco que ver con el beneficio al ecosistema
en su conjunto, sino que en su lugar considera solo el potencial para la
popularidad y el crecimiento individual del proyecto. Muchos proyectos
eligen dar prioridad al acceso al público consolidado que proporcionan
grandes plataformas comerciales, para maximizar sus posibilidades de
volverse populares y de disfrutar de algunos de los efectos secundarios
de esa popularidad, como más contribuciones[^1]. Tales proyectos
preferirían exacerbar el problema de los efectos de red en vez de
arriesgar algo de su capital social en una plataforma menos popular.

[^1]: Debo señalar aquí que estoy presentando de forma acrítica la
  «popularidad» como una cosa buena para un proyecto, que se ajusta,
  pienso yo, a los procesos mentales de los proyectos que estoy
  describiendo. Sin embargo, la verdad no es así. Quizá un tema para un
  artículo otro día.

Para mí esto es totalmente egoísta y antiético, aunque puede que tengas
diferentes estándares éticos. Desgraciadamente, argumentos contra la
mayoría de plataformas comerciales para cualquier estándar ético
razonable los hay en abundancia, pero suelen superarse fácilmente con un
sesgo de confirmación. Alguien que se opone fuertemente a las prácticas
del Servicio de Control de Inmigración y Aduanas de Estados Unidos, por
ejemplo, puede encontrar rápidamente alguna justificación para seguir
usando GitHub a pesar de su colaboración con ellos. Si este ejemplo no
es de tu agrado, hay muchos ejemplos para cada una de las muchas
plataformas. Para los proyectos que no se quieren mover, estos se barren
normalmente bajo la alfombra[^2].

[^2]: Un ejemplo particularmente notorio es el movimiento de [Ethical
  Source](https://ethicalsource.dev/). Discrepo con ellos por muchos
  motivos, pero adelantándose a este artículo está el hecho de que
  publican licencias (no libres) de <i lang="en">software</i> que
  abogan por sentimientos anticapitalistas como los derechos laborales y
  por juicios éticos como la no violencia, y lo hacen en... GitHub y
  Twitter, plataformas privadas comerciales con infinidad de violaciones
  éticas publicadas.

Pero, para ser claros, no estoy pidiendo que uses plataformas inferiores
por razones filosóficas o altruistas. Estos son solo uno de los muchos
factores que deberían contribuir a tu toma de decisiones, y la aptitud
es otro factor válido a considerar. Dicho eso, muchas plataformas libres
son, al menos en mi opinión, funcionalmente superiores a sus
competidoras privativas. Si sus diferencias son mejores para las
necesidades únicas de tu proyecto es algo que voy a dejar para que lo
investigues por tu cuenta, pero la mayoría de los proyectos no se
preocupan en absoluto por investigar. Estate seguro: estos proyectos no
son guetos que viven a la sombra de sus mayores equivalentes
comerciales, sino plataformas excitantes por sus propios méritos que
ofrecen muchas ventajas únicas.

Es más, si necesitas que hagan algo diferente para que se adapten mejor
a las necesidades de tu proyecto, tienes el poder de mejorarlas. No
estás supeditado a los caprichos de la entidad comercial responsable del
código, esperando a que den prioridad al problema o incluso a que, en
primer lugar, le importe. Si un problema es importante para ti, eso es
suficiente para que puedas conseguir que sea arreglado en una plataforma
libre. Puede que pienses que no tienes el tiempo o la habilidad para
hacerlo (aunque alguno de tus colaboradores quizá sí); pero, más
importante, esto crea una mentalidad de propiedad y responsabilidad
colectivas sobre todo el <i lang="en">software</i> libre en su conjunto
&mdash;vuelve popular esta filosofía y perfectamente puedes ser tú quien
reciba una contribución de forma similar mañana&mdash;.

En resumen, elegir plataformas no libres es una inversión individualista
a corto plazo que da prioridad al aparente acceso de tu proyecto a la
popularidad sobre el éxito del ecosistema libre en su conjunto. Por otro
lado, elegir plataformas libres es una inversión colectiva en el éxito a
largo plazo del ecosistema libre en su conjunto, conduciendo a su
crecimiento general. Tu elección importa. Puedes ayudar al ecosistema
libre eligiendo plataformas libres o puedes dañar el ecosistema libre
eligiendo plataformas no libres. Por favor, elige con cuidado.

Aquí hay algunas recomendaciones para herramientas de <i
lang="en">software</i> libre que simplifican necesidades comunes de
proyectos de <i lang="en">software</i> libre:

- Forjas de código: [Codeberg](https://codeberg.org/),
  [Gitea](https://gitea.io/en-us)\*,
  [Gerrit](https://www.gerritcodereview.com/)\*, [GitLab](https://gitlab.com/)†.
- Mensajería instantánea: [Matrix](https://matrix.org/), [Libera
  Chat](https://libera.chat/)[^3].
- Publicación: [Codeberg Pages](https://codeberg.page/), [Write.as](https://write.as/), [PeerTube](https://joinpeertube.org/).
- Redes sociales: [Mastodon](https://joinmastodon.org/), [Lemmy](https://join-lemmy.org/)
- Listas de correo: [FreeLists](https://www.freelists.org/),
  [public-inbox](https://public-inbox.org/public-inbox-overview.html)\*,
  [Mailman](http://www.list.org/)\*.

\* Solo autoalojado.<br>
† Parcialmente no libre, recomendado solo si otras opciones no son adecuadas.

[^3]: Le he presentado muchas veces los argumentos de esta publicación al
  equipo de Libera, pero todavía dependen de GitHub, Twitter y Facebook.
  Han sido una de mis motivaciones para escribir este artículo. Espero
  que algún día tengan un cambio de actitud.
