Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2018-06-27 20:20
Lang: es
Modified: 2018-09-15 23:58
Slug: la-sociedad-tras-el-colapso
Tags: civilización, colapso, cultura, futuro, guerra nuclear, recursos, prosa
Title: La sociedad tras el colapso

Habiendo colapsado la civilización, las formas de entender la vida
cambiaron radicalmente, tanto para bien como para mal.

Los estados se embarcaban en feroces guerras con el objetivo de
arrebatar los recursos a otras regiones, en un intento desesperado por
mantener su ostentoso nivel de vida. Sin embargo, las regiones y estados
más perjudicados oponían resistencia y les resultaba difícil lograr sus
objetivos, que solían contar con gran desaprobación por parte de la
sociedad, pues de todas formas la mayoría de recursos conquistados se la
repartían las clases dominantes. La esclavitud y el genocidio estuvieron
presentes en incontables países y regiones. Pero las tensiones nunca
cesaron.

En un solo mes, en el año 2050, se lanzaron 18 bombas nucleares en
varias partes del mundo, que extinguieron numerosas especies y mermaron
enormemente la población humana. Las bombas nucleares se lanzaban sin
piedad entre naciones poderosas. Quizá, muchas lo hacían por rabia y
como última muestra de su poderío, aunque fueran a desaparecer minutos
después de lanzar las bombas; siempre fieles a su nación, ejecutaban las
órdenes.

<!-- more -->

La destrucción fue casi total. Ninguna ciudad de gran tamaño quedó en
pie, y las que lo hacían se convertían en un infierno para sus
residentes. No era posible mantener el nivel de vida de una ciudad
cuando lo que la sustenta ha desaparecido; no había agua, alimentos,
electricidad...

Estaban en juego dos visiones: la libertaria y la autoritaria. Cuando se
iban acabando las reservas de agua y la gente no podía satisfacer sus
necesidades básicas, la tensión no podía ser mayor.

Como era de esperar, la lucha por los últimos recursos se desataba en
muchas ciudades, donde la gente, desesperada, aislada y antes
esclavizada solo había aprendido a seguir sus instintos básicos,
haciendo lo que fuera necesario para sobrevivir.

Ya no había estados ni ejércitos de robots ni corporaciones
monopolistas. La mayoría de personas que sobrevivieron cuando cayeron
las bombas, murieron poco después por enfermedades o heridas causadas
por la radiación. Casi nadie quería oír hablar del antiguo sistema, pero
aún así surgieron bastantes grupos autoritarios, que instauraban
pequeñas dictaduras y arrasaban con lo poco que quedaba vivo a su
alrededor, cavaban su propia tumba.

En lugares menos poblados y donde menos arraigo tuvo la ideología que
llevó al mundo al borde de su destrucción total, pequeños grupos
encontraron la ocasión de desarrollarse completamente sin la opresión y
los continuos ataques del sistema que ya no existía. Conocían las
dictaduras de primera mano; tenían claro que la única forma de
sobrevivir, ser felices y vivir en paz era trabajando cooperativamente,
sin jerarquías ni poder absoluto.

Las pequeñas comunidades pronto se reconstruyeron y aplicaron los
principios de solidaridad y apoyo mutuo. Las decisiones se tomaban en
asambleas, tratando de aplicar siempre el consenso. A diferencia de las
pequeñas dictaduras y otros grupos autoritarios, alrededor de las
pequeñas comunidades libertarias, la naturaleza volvía a surgir de las
cenizas.

Hicieron falta varios años más para asentar el modelo cooperativo. Las
pequeñas comunidades ahora estaban asociadas en federaciones, llegando a
tener la extensión de países. Colaboraban en la defensa frente a
eventuales agresiones de saqueadores de los pocos grupos autoritarios
que se atrevían a atacar algún pueblo. Estaban asentando un nuevo modelo
de sociedad, demostrando su validez en la práctica.

Estaban reconstruyendo la tecnología, la cultura y los valores propios
de su sociedad. Antes de la terrible guerra nuclear existieron personas
que compartían sus valores, pero que eran minoría o estaban oprimidas.
Recuperaron ese conocimiento almacenado en forma de libros,
herramientas, <i lang="en">software</i> libre, diseños abiertos, estándares abiertos...
Poco quedaba de lo demás. Quizá algunos dispositivos de almacenamiento
de algún servidor contenía algo, pero muchas veces estaba cifrado y no
era posible leerlo o utilizarlo sin una clave, esto paso sobre todo con
el contenido restringido usando DRM[^1] y <i lang="en">software</i> privativo.

La sociedad cambió un montón, y el individuo y la comunidad se
desarrollaban plenamente y sin ataduras. Esta vez respetando el medio
ambiente: desarrollando el veganismo, la agricultura ecológica y
permitiendo la liberación animal de la opresión del ser humano.

Poca gente de mi generación puede entender este nuevo proyecto que hoy
estoy redactando. Y es cierto que poca gente ha sobrevivido, y que hay
quien ha puesto los cimientos de esta nueva sociedad... Es solo la
impresión que tengo, al haber vivido estas dos etapas históricas
antagónicas. La clase opresora no se imaginaba lo que ocurriría,
pensaban en su estabilidad en el sistema, en el consumo de productos, en
disfrutar encerrados en sí mismos sin importarles lo que ocurría a su
alrededor; no les importaba que para vivir así tenían que destruir la
vida, tenían que trabajar más, tenían que administrar la muerte.

A veces me cuesta creer que todo lo que he visto y he vivido haya
sucedido de verdad. Ha caído un sistema que parecía eterno. Yo me alegro
de haber ayudado a acabar con él y de haber contribuido enormemente a
crear esta nueva sociedad. Espero que este relato que ahora os cuento no
caiga en el olvido y sirva para que no cometáis los errores pasados.

[^1]: DRM son las siglas en inglés de Digital Rights Management,
gestión de derechos digitales. Denotan las tecnologías que se
usaban para evitar que el conocimiento fuera accedido y compartido. Por
eso, algunas personas llamaban irónicamente al sistema Digital
Restrictions Management, gestión de restricciones digitales en
español.
