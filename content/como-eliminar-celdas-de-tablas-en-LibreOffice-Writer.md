Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2023-01-17 14:55
Lang: es
Slug: cómo-eliminar-celdas-de-tablas-en-libreoffice-writer
Tags: LibreOffice Writer
Title: Cómo eliminar celdas de tablas en LibreOffice Writer
Image: <img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">

Para las quitar celdas la única opción es borrar sus bordes. Aquí tengo
una tabla a la que quiero quitarle tres de las celdas superiores que
sobran:

<a href="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png" alt="" width="791" height="419" srcset="/wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer.png 791w, /wp-content/uploads/2023/01/tabla-de-ejemplo-LibreOffice-Writer-395x209.png 395w" sizes="(max-width: 791px) 100vw, 791px">
</a>

Para ello pulso el botón **Bordes** que aparece abajo cuando
seleccionamos las celdas de la tabla que queremos borrar y luego le doy
a **Ningún borde**, como hago en la siguiente imagen:

<a href="/wp-content/uploads/2023/01/quitando-bordes-de-tabla-en-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/quitando-bordes-de-tabla-en-LibreOffice-Writer.png" alt="" width="955" height="970" srcset="/wp-content/uploads/2023/01/quitando-bordes-de-tabla-en-LibreOffice-Writer.png 955w, /wp-content/uploads/2023/01/quitando-bordes-de-tabla-en-LibreOffice-Writer-477x485.png 477w" sizes="(max-width: 955px) 100vw, 955px">
</a>

Por si te pierdes, aquí tienes también un vídeo de demostración:

<video controls>
  <source src="/video/quitar-celdas-LibreOffice-Writer.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

El resultado es el siguiente:

<a href="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png" alt="" width="786" height="421" srcset="/wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer.png 786w, /wp-content/uploads/2023/01/tabla-sin-celdas-en-fila-LibreOffice-Writer-393x210.png 393w" sizes="(max-width: 786px) 100vw, 786px">
</a>
