Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2016-10-01 18:32
Image: <img src="/wp-content/uploads/2016/10/3004-800x510.png" width="800" height="510" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="La versión 1.6.10 de Bullet dodger ya está disponible">
Lang: es
Modified: 2017-05-14 03:36
Slug: la-version-1-6-10-de-bullet-dodger-ya-esta-disponible
Status: published
Tags: Bullet dodger, bullet_dodger, publicación de Bullet dodger, pygame, Python, Python 3, software libre, versión 1.6.10
Title: La versión 1.6.10 de <cite lang="en">Bullet dodger</cite> ya está disponible

En la última [publicación que hice sobre
Python](/creacion-de-un-videojuego-con-pygame/)
os enseñé a crear este juego. A dicho juego le he añadido algunas
mejoras y he hecho que se pueda instalar con el gestor de paquetes
[pip](https://pip.pypa.io/en/stable/) (leyendo la documentación sobre
paquetes de Python <https://packaging.python.org/distributing/> podéis
aprender a hacerlo).

La instrucción para instalar el juego es
<del>`sudo pip install --pre bullet_dodger` (la opción `--pre` no hará
falta cuando la versión 1.9.2 de pygame deje de estar en beta)</del>
<ins datetime="2016-12-15 17:03Z">`pip install bullet_dodger`</ins>. Esta
orden instala las dependencias del juego (por ahora solo pygame) y
también el juego. Una vez instalado, basta con ejecutar en la terminal
`bullet_dodger` para jugar.

He añadido también mi juego como entrada en las siguientes páginas web
para que más gente lo pueda encontrar:

-   <https://www.pygame.org/project/3004/>
-   <https://directory.fsf.org/wiki/Bullet_dodger>

