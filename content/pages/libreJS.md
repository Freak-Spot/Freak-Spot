Author: Jorge Maldonado Ventura
Date: 2017-02-16 19:02
Modified: 2020-11-06
Slug: librejs
Status: hidden
Title: LibreJS

<table id="jslicense-labels1">
  <tr>
    <td><a href="../js/asciinema-player.min.js">asciinema-player.min.js</a></td>
    <td><a href="http://www.apache.org/licenses/LICENSE-2.0">Apache-2.0</a></td>
    <td><a href="https://github.com/asciinema/asciinema-player/archive/v2.5.0.tar.gz">v2.5.0.tar.gz</a></td>
  </tr>
  <tr>
    <td><a href="../js/cliplibrejs.dev.min.js">cliplibrejs.dev.min.js</a></td>
    <td><a href="http://www.gnu.org/licenses/gpl-3.0.html">GPL-3.0+</a></td>
    <td><a href="../js/cliplibrejs.dev.js">cliplibrejs.dev.js</a></td>
  </tr>
  <tr>
    <td><a href="../hashover-next/api/count-link.php?unminified">count-link.php</a></td>
    <td><a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL-3.0</a></td>
    <td><a href="../hashover-next/api/count-link.php">count-link.php</a></td>
  </tr>
  <tr>
    <td><a href="../theme/js/functions.min.js">functions.min.js</a></td>
    <td><a href="http://www.gnu.org/licenses/gpl-2.0.html">GPL-2.0</a></td>
    <td><a href="../theme/js/functions.js">functions.js</a></td>
  </tr>
  <tr>
    <td><a href="../hashover-next/comments.php">hashover.js</a></td>
    <td><a href="http://www.gnu.org/licenses/agpl-3.0.html">AGPL-3.0</a></td>
    <td><a href="../hashover-next/comments.php">hashover.js</a></td>
  </tr>
  <tr>
    <td><a href="../theme/js/jquery-3.1.1.min.js">jquery-3.1.1.min.js</a></td>
    <td>
      <a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a>
      <br>
      <a href="http://www.gnu.org/licenses/gpl-2.0.html">GNU-GPL-2.0-or-later</a>
    </td>
    <td><a href="../theme/js/jquery-3.1.1.js">jquery-3.1.1.js</a></td>
  </tr>
  <tr>
    <td><a href="../js/tipuesearch/tipuesearch.min.js">tipuesearch.min.js</a></td>
    <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
    <td><a href="../js/tipuesearch/tipuesearch.js">tipuesearch.js</a></td>
  </tr>
  <tr>
    <td><a href="../js/tipuesearch/tipuesearch_set.min.js">tipuesearch_set.min.js</a></td>
    <td><a href="https://www.freebsd.org/copyright/freebsd-license.html">Expat</a></td>
    <td><a href="../js/tipuesearch/tipuesearch_set.js">tipuesearch_set.js</a></td>
  </tr>
</table>
