Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-01-03 00:35
Lang: pt
Slug: software-libre-y-política
Tags: anarquismo, capitalismo, comunismo, GNU/Linux, ideologia, política, software livre
Save_as: software-livre-e-política/index.html
URL: software-livre-e-política/
Title: <i lang="en">Software</i> livre e política
Image: <img src="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg" alt="" width="1200" height="800" srcset="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg 1200w, /wp-content/uploads/2020/11/software-libre-e-ideología.-600x400.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">


O <i lang="en">software</i> livre é anarquista ou capitalista? Alguns
dizem que é comunista, outros dizem que é capitalista, anarquista...
Quem tem razão? Fazem sentido comentários como os feitos pelo antigo
diretor executivo da Microsoft, Steve Ballmer?<!-- more -->

> O Linux é um concorrente forte. Não existe uma empresa chamada
> Linux; dificilmente existe um roteiro. Mas o Linux nasceu como se
> tivesse brotado do chão. E tinha, sabe, as características do
> comunismo que as pessoas tanto, tanto gostam. Ou seja,
> é grátis.[^1]

## Capitalismo

O <i lang="en">software</i> proprietário favorece os monopólios empresariais que
controlam a quase totalidade de um mercado. É impossível conseguir um
bom lugar no mercado apenas com <i lang="en">software</i> proprietário, pelo que para
competir muitas empresas têm de utilizar <i lang="en">software</i>
livre. Actualmente, é difícil encontrar empresas tecnológicas que não
façam uso considerável de <i lang="en">software</i> livre.

É claro que existem diferentes correntes capitalistas. O <i lang="en">software</i> livre,
em qualquer caso, tem lugar neste tipo de sociedade desde que haja uma
procura ou que a sua utilização proporcione uma vantagem competitiva.

## Anarquismo

O <i lang="en">software</i> livre [põe um fim ao poder injusto que os
programadores têm sobre os utilizadores](https://www.gnu.org/philosophy/free-software-even-more-important.pt-br.html). Uma vez que o anarquismo tem a
ver com o fim da autoridade imposta ao indivíduo, as liberdades
concedidas pelo <i lang="en">software</i> livre são a libertação.

## Outros sistemas políticos

O <i lang="en">software</i> livre é utilizado numa grande variedade de sistemas
políticos. Qual é o problema? A Coreia do Norte, por exemplo,
desenvolveu uma distribuição GNU/Linux chamada [Red Star OS](https://pt.wikipedia.org/wiki/Red_Star_OS).

## Conclusão

Penso que é absurdo enquadrar o <i lang="en">software</i> livre num determinado sistema
político. É sem dúvida mais eficiente e seguro que o <i lang="en">software</i>
proprietário, e inúmeros modelos políticos podem beneficiar da sua
adopção. [O <i lang="en">software</i> proprietário é como a alquimia](/pt/o-software-livre-é-melhor-do-que-a-alquimia/), enquanto que o
<i lang="en">software</i> livre é como a ciência. Não admira que [quase todos os
supercomputadores](https://es.wikipedia.org/wiki/Supercomputadora#Motivo_de_este_cambio,_qu%C3%A9_SO_predomina_actualmente_y_por_qu%C3%A9) e servidores web funcionem com <i lang="en">software</i> livre.

[^1]: Traduzido do artigo do The Register [MS' Ballmer: Linux é comunismo](https://www.theregister.com/Print/2000/07/31/ms_ballmer_linux_is_communism/).

