Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-12-04
Modified: 2020-12-05
Lang: es
Slug: bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin
Tags: Abrowser, Chromium, extensión, Firefox, Google Chrome, Microsoft Edge, Opera, tutorial, uBlock Origin
Title: Bloquear anuncios y rastreadores: uBlock Origin (tutorial para principiantes)
Image: <img src="/wp-content/uploads/2020/12/icono-uBlock-origin.png" alt="">

## Por qué mola bloquear anuncios

Si bloqueas anuncios y contenido de rastreo, ahorrarás mucho tiempo y
dinero: las páginas web tardarán menos en cargarse, y consumirás menos
datos y electricidad. Con uBlock Origin además contarás con más
privacidad y seguridad.

## Es muy fácil de instalar

<figure>
    <img alt="" src="/wp-content/uploads/2020/12/instalar-Ublock-origin.gif"/>
    <figcaption class="wp-caption-text">Instalación de uBlock origin en Firefox</figcaption>
</figure>

Basta con buscar la extensión para tu navegador e instalarla, nada más.
A partir de entonces no verás más anuncios.

Esta extensión funciona en los navegadores más populares (Chrome,
Microsoft Edge, Opera, Firefox, Chromium...). Aunque en este artículo
muestro la imagen de Abrowser (un navegador derivado de Firefox), el
funcionamiento es igual en todos los navegadores. Por cierto, si usas
Firefox y te marea el GIF anterior, puedes [hacer clic aquí para
instalar la extensión](https://addons.mozilla.org/firefox/downloads/file/4121906/ublock_origin-1.50.0.xpi).

## Hay una página que no funciona con el bloqueador o que no quiero bloquear

En esta situación poco común basta con pulsar el iconito de la extensión
y luego el botón de apagado ⏻.

<!-- more -->

<video controls src="/video/deshabilitar-anuncios-con-Ublock-origin.mp4">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

El botón ⏻ se puede pulsar cuando está apagado para volver a activar la
extensión. El anterior ajuste solo se aplica en el sitio web concreto
donde hacemos uso del botón y será recordado incluso tras cerrar el
navegador.

## Y mucho más

Este artículo es básicamente lo que buscan el 99% de las personas. Hay
quienes quieren bloquear también ventanas emergentes y otros elementos
incómodos, bloquear fuentes tipográficas, JavaScript, elegir y ajustar a
su gusto los filtros antianuncios, etc., en sitios web concretos.
Explico estas cosas en vídeo en los artículos [Bloquear elementos
molestos: uBlock Origin](/bloqueo-de-elementos-con-ublock-origin/)
(dificultad intermedia) y [Funciones avanzadas de uBlock
Origin](/uso-avanzado-de-ublock-origin/) (dificultad alta). La [documentación de Ublock
origin](https://github.com/gorhill/uBlock/wiki) también es muy completa,
aunque está en inglés.
