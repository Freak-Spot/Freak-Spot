Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-07-01 01:29
Lang: es
Slug: busca-ayuda-para-bash-con-la-instruccion-help
Status: published
Tags: Bash, help
Title: Busca ayuda para Bash con la instrucción help

La instrucción help sirve para encontrar ayuda sobre las órdenes del
shell definidas internamente. Si ejecutamos la instrucción `help`,
podremos ver un resumen breve de las órdenes internas. A la instrucción
`help` también podemos pasarle parámetros. Con `help help` podemos ver
su uso.
