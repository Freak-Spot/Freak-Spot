Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2020-10-07 14:20
Lang: eo
Slug: diff-highlight-un-mejor-diff-para-git
Save_as: diff-highlight-pli-bona-diff-por-Git/index.html
URL: diff-highlight-pli-bona-diff-por-Git/
Tags: diff, diff-highlight, Git, GNU/Linukso, komandlinia interfaco
Title: diff-highlight: pli bona diff por Git
Image: <img src="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de.png" alt="" width="1499" height="504" srcset="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de.png 1499w, /wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de-749x252.png 749w" sizes="(max-width: 1499px) 100vw, 1499px">

La defaŭlta [diff](https://de.wikipedia.org/wiki/Diff)-programo
de Git povus esti pli bona.

<!-- more -->

<a href="/wp-content/uploads/2020/08/git_diff-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_diff-ejemplo.png" alt="" width="838" height="705" srcset="/wp-content/uploads/2020/08/git_diff-ejemplo.png 838w, /wp-content/uploads/2020/08/git_diff-ejemplo-419x352.png 419w" sizes="(max-width: 838px) 100vw, 838px">
</a>

Ni povus ja plenumi je `git --word-diff` por pli bone vidi la ŝanĝojn,
sed per tio ni pretervidas informon okaze. Ĉi-kaze, ekzemple, ne montras
la spacetojn, kiujn estis aldonitaj antaŭ la HTML-etikedoj.

<a href="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png" alt="" width="941" height="467" srcset="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png 941w, /wp-content/uploads/2020/08/git_word-diff_ejemplo-470x233.png 470w" sizes="(max-width: 941px) 100vw, 941px">
</a>

Ĉu per alia diff-Programo? Vidu ni je diff-highlight...

<a href="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png" alt="" width="859" height="705" srcset="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png 859w, /wp-content/uploads/2020/08/git-diff-highlight-ejemplo-429x352.png 429w" sizes="(max-width: 859px) 100vw, 859px">
</a>

La antaŭa estas pli bona, ĉu ne? Kiel povas oni ĝin agordi? Mi malkovris
kiel [en retejo, kie mi nur komprenis la
komandojn](https://qiita.com/SakaiYuki/items/2814d417d0bec59046bb), kiu
sufiĉis.  Ĉar mi scias kiel tion fari, mi klarigas al vi ĝin.

Unue oni devas havi version de Git egala aŭ supra al la 2.9...

    :::bash
    sudo apt update
    sudo apt install git
    git --version  # Devas esti egala aŭ supra al la 2.9

Kvankam vi jam instalis la bezonatan Git-version, ankoraŭ vi devas
agordi je diff-highlight per permesoj de plenumado kaj kreante
simbolan ligilon en dosierujo, kiu estas en la medivariablo `$PATH`.
Ĉi-kaze mi kreas simbolan ligilon en
`/usr/local/bin`...

    :::bash
    sudo chmod +x /usr/share/doc/git/contrib/diff-highlight/diff-highlight
    sudo ln -s /usr/share/doc/git/contrib/diff-highlight/diff-highlight /usr/local/bin/diff-highlight

Nun povas ni plenumi je `diff-highlight` kiam ni volas, sen la necesa
uzado de Git. Por uzi ĉi tiun programon ĉiam, kiam ni plenumas `git
diff`, ni devas agordi la `.gitconfig`-dosieron kaj por konkreta
projekto kaj por uzi ĝin ĉiam. Ĉar mi volas uzi ĝin ĉiam, mi aldonas al
dosiero `~/.gitconfig` la jenajn liniojn:

    :::config
    [core]
        pager = diff-highlight | less -r

Pro pli da komforto, mi pasas la eligo de less tra dukto.

Preta! Se pro stranga kialo vi havas nostalgio de la malnova diff, vi
povas reagordi la `.gitconfig`-dosieron kiel ĝi estis.
