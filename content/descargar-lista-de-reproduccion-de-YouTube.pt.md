Author: Jorge Maldonado Ventura
Category: Truques
Date: 2023-04-15 19:00
Lang: pt
Slug: descargar-lista-de-reproduccion-de-youtube
Save_as: descarregar-lista-de-reprodução-do-YouTube/index.html
URL: descarregar-lista-de-reprodução-do-YouTube/
Tags: lista de reprodução, PeerTube, YouTube
Title: Descarregar lista de reprodução do YouTube
Image: <img src="/wp-content/uploads/2023/04/logo-yt-dlp.png" alt="">

Podemos utilizar o programa
[`yt-dlp`](https://github.com/yt-dlp/yt-dlp/). [Há várias maneiras de o
instalar](https://github.com/yt-dlp/yt-dlp/#installation), uma delas é
utilizando o gestor de pacotes da nossa distribuição:

    :::text
    sudo apt install yt-dlp

Para descarregar uma lista de reprodução, basta escrever `yt-dlp`
seguido do URL da lista de reprodução num terminal e premir
<kbd>Enter</kbd>. Por exemplo:

    :::text
    yt-dlp https://youtube.com/playlist?list=PLvvDxND6LkgB_5dssZod-3BET4_DFRmtU

Este programa não só descarrega vídeos do YouTube, mas também pode
descarregar vídeos de [muitos outros sítios web](https://ytdl-org.github.io/youtube-dl/supportedsites.html).
