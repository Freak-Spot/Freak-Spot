Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2024-01-12 18:55
Lang: eo
Slug: internet-tiene-inquilinos-y-propietarios-¿qué-eres-tú
Save_as: la-Interreto-havas-luantojn-kaj-proprietulojn-kio-vi-estas/index.html
URL: la-Interreto-havas-luantojn-kaj-proprietulojn-kio-vi-estas/
Tags: Fediverso, Interreto, Lemmy, Mastodon, PeerTube, Tor
Title: La Interreto havas luantojn kaj proprietulojn, kio vi estas?
Image: <img src="/wp-content/uploads/2024/01/propiedad-virtual.png" alt="" width="1169" height="945" srcset="/wp-content/uploads/2024/01/propiedad-virtual.png 1169w, /wp-content/uploads/2024/01/propiedad-virtual-584x472.png 584w" sizes="(max-width: 1169px) 100vw, 1169px">

Multaj plendas pri tio, ke ili estas cenzuritaj en la Interreto, ke ili
ne povas publike paroli pri specifaj temoj pro timo perdi publikon kaj
enspezon kaj tiel plu. Ili dependas de platformoj, kiujn ili neniel
regas, ili estas kvazaŭ *luantoj*, kiuj povas esti forpelitaj iam ajn.
Kiam vi afiŝas en platformoj kiel X, Instagram, TikTok kaj YouTube, vi
uzas la platformon de aliulo (aliulan *domon*). Kiam vi ne plu estas
profitdona kaj la reklamistoj aŭ la posedantoj de la firmao ne ŝatas
vin, ili cenzuras vin, ili forpelas vin... Kaj vi nenion povas fari
kontraŭ tio. Nu, eblas fari ion...

La plej simpla maniero ŝirmi vin de la arbitraj agoj de platformo estas
uzi pli ol unu. Se unu platformo fuŝus vian konton, vi ankoraŭ havus
aliajn. Mi opinias, ke plej bone estas uzi
[liberan](https://freakspot.net/escribe-programas-libres/eo/lerni/)
platformon, kiu apartenas al sencentra reto. Se neniu platformo
konvinkas vin, vi povas krei vian propran. Ĉu tio multekostas? Ne. Danke
al la ActivityPub-protokolo estas tre malmultekoste krei nodon en la
Fediverso. La [Fediverso](https://eo.wikipedia.org/wiki/Fediverso) estas
granda sencentra reto, per kiu vi povas atingi milionojn da uzantoj. Vi
povas elekti [PeerTube](https://joinpeertube.org/eo) por alŝuti videojn, [Mastodon](https://joinmastodon.org/eo) por krei kaj
kunhavigi mallongajn afiŝojn, [Lemmy](https://join-lemmy.org/?lang=eo) por diskuti kun aliaj homoj...

Vi ankaŭ povas aĉeti domajnon kontraŭ malmulte da mono kaj estigi
retejon en luata aŭ hejma servilo. Por pli da protekto kontraŭ cenzuro
vi ankaŭ povas [estigi kaŝitan Tor-servon](/eo/krei-ka%C5%9Ditan-servon-por-retejo-en-Nginx/)

Vi ankaŭ povas krei dissendoliston aŭ grupon en tujmesaĝilo. Per tiu
metodo viaj afiŝoj atingos rekte viajn ricevantojn, sen dependi de
arbitraj algoritmoj, kiuj povas limigi vian atingopovon. Se la
servilo, el kie vi sendas la retpoŝtojn apartenas al vi, vi havas pli da
aŭtonomeco.

Resume, lertaĵo por eviti la cenzuron kaj arbitrecon de grandaj
posedantoj estas havi propran platformon aŭ kanalon. Se tio ne eblas,
plej bone estas havi plurajn kontojn en kelkaj platformoj. Tiel, se vi
estas cenzurita en unu, vi ankoraŭ havos la aliajn.
