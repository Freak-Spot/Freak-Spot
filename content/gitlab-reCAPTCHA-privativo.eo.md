Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2018-07-16 03:33
Lang: eo
Save_as: gitlab-proprieta-captcha/index.html
Slug: gitlab-recaptcha-privativo
Tags: CAPTCHA, kodgastigo, Git, GitLab, Guglo, deponejo, libera programaro, privateco, proprieta programaro, reCAPTCHA
Title: GitLab uzas proprietajn CAPTCHAjn
Url: gitlab-proprieta-captcha/

GitLab enhavas proprietan programon el Guglo, kaj ne ŝaijnas, ke ili
formovos ĝin. Konkrete ĝi uzas la programon reCAPTCHA de Guglo,
[dokumentanta eĉ ĝian konfiguron](https://docs.gitlab.com/ee/integration/recaptcha.html).

Ĉi tio estas problema ne nur pro la
[temo de la programa libero](https://www.gnu.org/philosophy/shouldbefree.en.html),
sed ankaŭ pro aliaj postefikoj de reCAPTCHA (kiel la
[labora ekspluatado](/como-explota-Google-con-CAPTCHAs/)
kaj la
[vizaĝorekono por armeaj celoj](/al-completar-un-recaptcha-de-google-ayudas-a-matar/)).

La kodo ŝargas sin el la serviloj de Guglo, do ĝi povus bari la
apertigon de novaj problemoj kaj la registron de novaj kontoj, kiam la
Gugla servilo estu nealirebla.

Jam ekzistas malnove
[kelkaj](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/45684)
[problemoj](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/46548)
malfermaj en la problemo-administrilo de GitLab.
