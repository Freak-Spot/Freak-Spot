Author: Jorge Maldonado Ventura
Category: Video games
Date: 2021-12-03
Lang: en
Slug: videludo-Super-Bombinhas
Save_as: video-game-Super-Bombinhas/index.html
URL: video-game-Super-Bombinhas/
Tags: free culture, GNU/Linux, platform, video game, Windows
Title: Video game <cite lang="pt">Super Bombinhas</cite>
Image: <img src="/wp-content/uploads/2021/11/Super-Bombinhas.png" alt="">

The main characters of this platform game are living bombs. Bomba Azul
is the only bomb who wasn't captured by the evil Gaxlon. You must save
the other bombs and the king Aldan. Each bomb has a special ability:
Bomba Amarela can quickly run and jump higher than the others, Bomba
Verde can explode, the king Aldan can stop time, etc. After saving a
bomb, you can change to it during the adventure.

In the game there are 7 very different regions. Each of them has a final
boss. You must wisely use every bomb to advance.

The game also has a level editor to create new levels.

![Game editor](/wp-content/uploads/2021/11/Super-Bombinhas-level-editor.gif)

Because a picture is worth a thousand words, I show you a short video
below:

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2021/11/Super-Bombinhas.png" data-setup="{}">
  <source src="/video/super-bombinhas-en.webm" type="video/webm">
  <p>Sorry, your browser doesn't support HTML 5. Please change
  or update your browser</p>
</video>

You can install the game on GNU/Linux and Windows. To install it on
distributions based on Debian you must download the newest `.deb` file
from the [releases
page](https://github.com/victords/super-bombinhas/releases) and execute
it.
