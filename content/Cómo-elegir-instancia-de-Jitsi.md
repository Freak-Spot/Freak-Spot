Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-04-09 15:30
Lang: es
Slug: cómo-elegir-instancia-de-Jitsi
Tags: Jitsi, videoconferencias
Title: Cómo elegir instancia de Jitsi: Jitsimeter
Image: <img src="/wp-content/uploads/2020/04/Jitsimeter.png" alt="" width="1413" height="740" srcset="/wp-content/uploads/2020/04/Jitsimeter.png 1413w, /wp-content/uploads/2020/04/Jitsimeter-706x370.png 706w" sizes="(max-width: 1413px) 100vw, 1413px">

Por si no lo sabes, Jitsi es un programa para hacer videoconferencias.

Lo ideal es elegir la más rápida y con mayor privacidad. Con
[Jitsimeter](https://ladatano.partidopirata.com.ar/jitsimeter/) podemos
hacer una evaluación.
