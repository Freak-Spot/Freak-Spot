Author: Jorge Maldonado Ventura
Category: News
Date: 2024-02-01 23:53
Lang: en
Slug: casi-un-4-por-ciento-de-los-ordenadores-de-escritorio-usa-gnulinux
Save_as: almost-4-percent-of-desktop-computers-use-GNU-Linux/index.html
URL: almost-4-percent-of-desktop-computers-use-GNU-Linux/
Tags: stat, GNU/Linux
Title: Almost 4% of personal computers use GNU/Linux
Image: <img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png" alt="" width="1139" height="563" srcset="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png 1139w, /wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024-569x281.png 569w" sizes="(max-width: 1139px) 100vw, 1139px">

According to Statcounter, GNU/Linux is the operating system used on
3.77% of desktop computers (laptops and desktop computers)[^1]. In the
chart we can see that in January 2021 the share of GNU/Linux was 1.91%,
which means that in three years it has doubled its popularity. It's
also worth mentioning that the share of Chrome OS (which uses the Linux
kernel) is 1.78%.

<a href="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png">
<img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png" alt="">
</a>

However, this statistic is not fully representative, since Statcounter's
tracking code is installed on only 1.5 million websites[^2]. Also, some
GNU/Linux users &mdash; those who care more about privacy &mdash; use
tools that change the
[`User-Agent`](https://en.wikipedia.org/wiki/User-Agent_header) (an
example is Tor Browser, which always claims to use Windows to camouflage
itself better).

In any case, this is an impressive growth that will probably continue in
the coming years, as GNU/Linux is increasingly used in educational
institutions and many countries are trying to increase their
technological sovereignty (GNU/Linux is the cheapest and safest way to
do it).

[^1]: <https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-200901-202401>
[^2]: «<i lang="en">Our tracking code is installed on more than 1.5 million sites
    globally</i>»: that's what they say in <https://gs.statcounter.com/faq#methodology>.
