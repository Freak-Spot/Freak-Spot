Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-10-31 23:30
Lang: pt
Save_as: YouTube-com-privacidade-com-Piped/index.html
URL: YouTube-com-privacidade-com-Piped/
Slug: youtube-con-privacidad-con-piped
Tags: Google, Piped, privacidade, YouTube
Title: YouTube com privacidade com o Piped
Image: <img src="/wp-content/uploads/2022/10/vídeo-de-YouTube-no-Piped.png" alt="" width="1012" height="924" srcset="/wp-content/uploads/2022/10/vídeo-de-YouTube-no-Piped.png 1012w, /wp-content/uploads/2022/10/vídeo-de-YouTube-no-Piped-506x462.png 506w" sizes="(max-width: 1012px) 100vw, 1012px">

Tal como o [Invidious](/pt/YouTube-com-privacidade-com-o-Invidious/),
o [Piped](https://piped.kavin.rocks/) oferece uma interface livre e
privada para o YouTube.

A vantagem do Piped é que funciona com
[o SponsorBlock](https://sponsor.ajay.app/), de modo que não perdes
tempo vendo partes de vídeos patrocinadas. Apenas mencionei as
características que considero mais úteis; uma [lista mais
detalhada](https://github.com/TeamPiped/Piped/#features) está disponível
na página do projecto.

<figure>
<a href="/wp-content/uploads/2022/10/Canal-do-YouTube-no-Piped.png">
<img src="/wp-content/uploads/2022/10/Canal-do-YouTube-no-Piped.png" alt="" width="1182" height="949" srcset="/wp-content/uploads/2022/10/Canal-do-YouTube-no-Piped.png 1182w, /wp-content/uploads/2022/10/Canal-do-YouTube-no-Piped-591x474.png 591w" sizes="(max-width: 1182px) 100vw, 1182px">
</a>
    <figcaption class="wp-caption-text">Canal do YouTube visto com o Piped</figcaption>
</figure>

Algumas desvantagens em comparação com o Invidious são que não permite
classificar os vídeos de um canal de acordo com a antiguidade ou
popularidade, mas simplesmente mostra os últimos vídeos do canal; não há
botão para descarregar vídeos e áudio; não se vê uma miniatura do
quadro quando se passa o rato sobre a linha temporal; a miniatura do
vídeo não aparece quando se partilha uma ligação...

<figure>
<a href="/wp-content/uploads/2022/10/vídeo-no-Piped-descrição-comentários-repetir.png">
<img src="/wp-content/uploads/2022/10/vídeo-no-Piped-descrição-comentários-repetir.png" alt="" width="1226" height="949" srcset="/wp-content/uploads/2022/10/vídeo-no-Piped-descrição-comentários-repetir.png 1226w, /wp-content/uploads/2022/10/vídeo-no-Piped-descrição-comentários-repetir-613x474.png 613w" sizes="(max-width: 1226px) 100vw, 1226px">
</a>
    <figcaption class="wp-caption-text">Podes repetir vídeos, ver comentários, ler descrições de vídeos...</figcaption>
</figure>

<!-- more -->

Segue-se um vídeo carregado com o Piped:

<iframe id='ivplayer' width='640' height='360' src='https://piped.kavin.rocks/embed/NA7VVPzU34Q' style='border:none;'></iframe>

É óptimo que existam alternativas livres a programas livres. Estou
satisfeito com a forma como o Piped funciona, mas também gosto muito do
Invidious. Ambas são boas opções, certamente muito melhores do que usar
directamente o YouTube, porque poupas tempo ao evitar anúncios, proteges
a tua privacidade e não dás nem um cêntimo ao YouTube.
