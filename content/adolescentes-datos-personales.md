Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2020-07-13
Lang: es
Slug: adolescentes-en-tropel-a-nueva-red-social-donde-simplemente-introducen-datas-personales-en-formulario
Tags: redes sociales
Title: Adolescentes van en tropel a nueva red social donde simplemente introducen datos personales en formulario
Image: <img src="/wp-content/uploads/2020/07/adolescentes-redes-sociales-ordenador.jpg" alt="Adolescentes usando la nueva red social en el ordenador">

La red social Flock supera con creces sus predicciones de crecimiento en
los últimos seis meses. Los adolescentes están pasándose en bandada a
esta nueva aplicación en la que simplemente tienen que introducir su
información personal en un formulario. «La simplicidad de uso ha hecho
que más de 500 millones de usuarios activos en todo el mundo empiecen a
compartir su nombre, dirección y documento nacional de identidad»,
afirma el investigador de tendencias Casimiro Cortés. La aplicación se
distingue de otras centradas en el público joven en que permite a los
usuarios entregar información personal sensible con texto, imágenes e
incluso vídeos sin ofrecer ninguna funcionalidad especial. «También les
gusta», añade Casimiro, «el aspecto social: pueden etiquetar a sus
amigos para que no tengan que molestarse en introducir su información
personal».

El éxito de esta aplicación, que obvia las funcionalidades superfluas de
aplicaciones como Instagram para centrarse solo en enviar la información
personal a los encargados de mercadotecnia de la empresa, convertirá
con seguridad a Flock en el nuevo estándar de desarrollo de
aplicaciones. Flock anunció ayer un concurso con cuantiosos premios
para los usuarios que consigan introducir la mayor cantidad de
información personal en la aplicación, con lo que espera doblar su
popularidad.
