Author: Jorge Maldonado Ventura
Category: Notícias
Date: 2022-03-17
Lang: pt
Slug: duckduckgo-censura-desinformacion-rusa
Save_as: DuckDuckGo-censura-desinformaçao-russa/index.html
URL: DuckDuckGo-censura-desinformaçao-russa/
Tags: censura, liberdade de informação, política, privacidade, Rússia, software livre
Title: DuckDuckGo censura «desinformação russa»
Image: <img src="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg" alt="" width="2083" height="1667" srcset="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg 2083w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-1041x833.jpg 1041w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-520x416.jpg 520w" sizes="(max-width: 2083px) 100vw, 2083px">

O chefe de DuckDuckGo [disse en Twitter](https://nitter.snopyta.org/yegg/status/1501716484761997318):

> Como tantos outros, estou nauseado pela invasão russa da Ucrânia e
> pela gigantesca crise humanitária que continua a criar. #StandWithUkraine
>
> Na DuckDuckGo, temos vindo a lançar actualizações de pesquisa que estão
> associadas à desinformação russa.

Esta decisão da DuckDuckGo é problemática para muitos utilizadores que
querem decidir por si próprios o que é desinformação e o que não é. É
por isso que muitas pessoas descontentes com a decisão e defensoras da
privacidade e do <i lang="en">software</i> livre agora recomendam a
utilização de motores de busca como [Brave
Search](https://search.brave.com/) e [Searx](https://searx.github.io/searx/).
