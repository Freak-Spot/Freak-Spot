Author: Jorge Maldonado Ventura
Category: News
Date: 2018-03-17 23:55
Image: <img src="/wp-content/uploads/2018/03/recaptcha-usado-para-matar.png" alt="Completing a Google reCAPTCHA to train an artificial intelligence that will help US drones to kill">
Lang: en
Slug: al-completar-un-recaptcha-de-google-ayudas-a-matar
Save_as: by-completing-a-Google-reCAPTCHA-you-are-helping-to-kill/index.html
URL: by-completing-a-Google-reCAPTCHA-you-are-helping-to-kill/
Tags: United States of America, Google, The Pentagon, reCAPTCHA
Title: By completing a Google reCAPTCHA you are helping to kill

Google has recently partnered with [the
Pentagon](https://en.wikipedia.org/wiki/The_Pentagon) to help it develop
artificial intelligence. The project, called Maven, involves the
development of a system to identify objects through drone imagery.

This means that Google's power over people around the world will be used
by the US empire for its dark interests. Now completing a Google
reCAPTCHA not only [involves ethical
dangers](/en/como-explota-Google-con-CAPTCHAs/) related to economic
exploitation and the use of proprietary software, but also close
collaboration in the murder of human beings. Deaths that seem
dehumanised, but in the end it is unconscious (and conscious) people who
train machines to kill.
