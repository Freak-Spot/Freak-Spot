Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2017-05-05 14:20
Image: <img src="/wp-content/uploads/2017/05/reCAPTCHA-libros.png" alt="reCAPTCHA usado para digitalizar textos">
Lang: es
Modified: 2017-08-19 12:50
Slug: como-explota-Google-con-CAPTCHAs
Tags: CAPTCHA, explotación, Google, reCAPTCHA, spam
Title: Cómo explota Google con CAPTCHAs

Un <abbr title="Completely Automated Public Turing test to tell
Computers and Humans Apart">CAPTCHA</abbr> es una prueba que se realiza
para diferenciar ordenadores de humanos. Se usa principalmente para
evitar mensajes basura (en inglés llamados *spam*).

Un programa que realiza esta prueba es reCAPTCHA, que fue publicado el
27 de mayo de 2007 y adquirido por Google<!-- more --> en septiembre de
2009[^1]. Dicho programa es utilizado en páginas web de todo el mundo.

reCAPTCHA se ha utilizado mucho para digitalizar textos gracias al
trabajo gratuito de millones de usuarios, quienes son capaces de
identificar palabras incomprensibles por los ordenadores. La gran
mayoría no sabe que Google se está aprovechando de su trabajo; a otras
personas no les importa.

![reCAPTCHA usado para digitalizar textos](/wp-content/uploads/2017/05/reCAPTCHA-libros.png)

En el artículo [«Deciphering Old Texts, One Woozy, Curvy Word at a
Time»](https://www.nytimes.com/2011/03/29/science/29recaptcha.html)
publicado en el periódico The New York Times se informaba de que los
usuarios de Internet habían terminado de digitalizar usando reCAPTCHA el
archivo de dicho periódico, publicado desde 1851. Según decía el creador
de reCAPTCHA entonces, los usuarios, o mejor dicho «usados», descifraban
alrededor de 200 millones de <abbr title="Completely Automated Public
Turing test to tell Computers and Humans Apart">CAPTCHA</abbr>s por día,
tardando 10 segundos en resolver cada uno. Ese trabajo equivale a
500&nbsp;000 horas de trabajo al día[^2].

Un <abbr title="Completely Automated Public Turing test to tell
Computers and Humans Apart">CAPTCHA</abbr> no solo se puede usar con
texto difícil de entender, sino también con imágenes. Por ejemplo,
Google también hace uso de reCAPTCHA para identificar imágenes de
establecimientos, señales de tráfico, etc., realizadas para Google Maps.
Asimismo, los reCAPTCHAS de Google se utilizan para otros propósitos que
no conocen los usados.

![reCAPTCHA hecho para Google Maps](/wp-content/uploads/2017/05/reCAPTCHA-Google-Maps.png)

Solo Google sabe el beneficio económico que le reporta este sistema de
explotación. Es imposible auditar reCAPTCHA porque es <i lang="en">software</i>
privativo, y los usados no tienen ningún poder. Lo único que pueden
hacer es negarse a resolver <abbr title="Completely Automated Public
Turing test to tell Computers and Humans Apart">CAPTCHA</abbr>s como los
de Google para dejar de ser usados.

Ahora Google utiliza un método de identificación que les ahorra tiempo a
los usados y evita que tengan que descifrar textos e imágenes. Los
usados ahora solo deben pulsar un botón. Este mecanismo impone la
ejecución de código JavaScript desconocido por los usados, el cual
supone un gran peligro para la privacidad. Google puede obtener
muchísima información de los usados que usen este mecanismo, que
probablemente luego venda por cuantiosas sumas.

![Botón de reCAPTCHA](/wp-content/uploads/2017/05/Recaptcha_anchor@2x.gif)

Además, reCAPTCHA discrimina a los usuarios discapacitados y los
usuarios de <abbr title="The Onion Router">Tor</abbr>: por un lado, los
desafíos presentados a usuarios discapacitados son más largos y
difíciles de resolver; por otro lado, quienes navegan de forma privada
por Internet tienen que resolver un desafío más difícil y que requiere
más tiempo.

El
[artículo](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html)
escrito por el creador de reCAPTCHA[^3] cuando este fue adquirido por Google
decía:

> Mejorar la disponibilidad y accesibilidad de toda la información en
> Internet es realmente importante para nosotros, así que estamos
> deseando mejorar esta tecnología con el equipo de reCAPTCHA.

Sin embargo, el trabajo realizado usando reCAPTCHA no se encuentra
disponible ni accesible en muchos casos. Los datos son presentados de
forma que beneficia económicamente a Google y a otras empresas. Los
usados que ayudaron a digitalizar el archivo de The New York Times
tienen que pagar viendo anuncios cuando consultan el archivo que ellos
mismos ayudaron a digitalizar sin obtener nada a cambio.

[^1]:
    [reCAPTCHA](https://en.wikipedia.org/wiki/ReCAPTCHA). Wikipedia. Consultado el 5 de mayo de 2017.
[^2]:
    [Deciphering Old Texts, One Woozy, Curvy Word at a Time](http://www.nytimes.com/2011/03/29/science/29recaptcha.html) (28 de marzo de 2011). The New York Times. Consultado el 5 de mayo de 2017.
[^3]:
    [Teaching computers to read: Google acquires reCAPTCHA](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html) (16 de septiembre de 2009). Official Google Blog. Consultado el 5 de mayo de 2017.
