Author: Jorge Maldonado Ventura
Date: 2018-01-21 15:53
Lang: en
Modified: 2020-11-06
Slug: gastos-economicos
Status: hidden
Title: Economic costs

## Web hosting

The server costs monthly €9.

## Domain name

<dl>
<dt>2023-06-28/2024-06-28</dt>
<dd>€16,88 (<a href="/wp-content/uploads/2023/06/factura-dominio-2023-freak-spot.pdf">bill</a>)</dd>
<dt>2022-06-28/2023-06-28</dt>
<dd>€16,88 (<a href="/wp-content/uploads/2022/05/factura-dominio-2022-freak-spot.png">bill</a>)</dd>
<dt>2021-06-28/2022-06-28</dt>
<dd>€16,88  (<a href="/wp-content/uploads/2021/06/factura-dominio-2021-freak-spot.png">bill</a>)</dd>
<dt>2020-06-28/2021-06-28</dt>
<dd>€16.88 (<a href="/wp-content/uploads/2020/06/factura-dominio-2020-freak-spot.png">bill</a>)</dd>
<dt>2019-06-28/2020-06-28</dt>
<dd>€13.25 (<a href="/wp-content/uploads/2019/04/factura-dominio-2019-freak-spot.png">bill</a>)</dd>
<dt>2018-06-28/2019-06-28</dt>
<dd>€13.25 (<a href="/wp-content/uploads/2018/06/factura-dominio-2018-freak-spot.png">bill</a>)</dd>
<dt>2017-06-28/2018-06-28</dt>
<dd>€12.04 (<a href="/wp-content/uploads/2018/02/factura-dominio-y-alojamiento-2017-freak-spot.png">bill</a>)</dd>
<dt>2016-06-28/2017-06-28</dt>
<dd>€12.04 (<a href="/wp-content/uploads/2018/02/factura-dominio-2016-freak-spot.png">bill</a>)</dd>
</dl>
