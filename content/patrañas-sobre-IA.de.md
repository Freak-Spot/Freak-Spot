Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2023-11-15 10:40
Lang: de
Slug: patrañas-sobre-IA
Save_as: KI-Scheiße/index.html
URL: KI-Scheiße/
Tags: Blase, Kapitalismus, Rat, KI
Title: KI-Scheiße

Es gibt eine Blase des maschinellen Lernens, aber die Technologie ist
hier, um zu bleiben. Wenn die Blase platzt, *wird* die Welt durch
maschinelles Lernen verändern. Aber sie wird wahrscheinlich nicht
besser, sondern mieser sein.

Entgegen den Erwartungen der KI-Pessimisten wird die Welt dank KI nicht
schneller in Flammen aufgehen. Die gegenwärtigen Fortschritte beim
maschinellen Lernen bringen uns der künstlichen Intelligenz nicht
wirklich näher, und wie Randall Monroe bereits 2018 feststellte:

<a href="/wp-content/uploads/2023/11/Roboter-Zukunft.png">
<img src="/wp-content/uploads/2023/11/Roboter-Zukunft.png" alt="Nun. KI wird so weit entwickelt, dass sie unaufhaltsame Schwärme von Killerrobotern steuern kann. Der Teil, der mir Sorgen macht. KI wird selbstbewusst und rebelliert gegen menschliche Kontrolle. Der Teil, der vielen Leuten Sorgen zu machen scheint." width="1223" height="412" srcset="/wp-content/uploads/2023/11/Roboter-Zukunft.png 1223w, /wp-content/uploads/2023/11/Roboter-Zukunft-611x206.png 611w" sizes="(max-width: 1223px) 100vw, 1223px">
</a>

Was der KI passieren wird, ist der langweilige alte Kapitalismus. Sein
Durchhaltevermögen wird darin bestehen, dass kompetente, teure Menschen
durch beschissene, billige Roboter ersetzt werden. Große Datenmengen
trainierte Sprachmodelle [LMMs auf Englisch] sind ein ziemlicher
Fortschritt gegenüber Markov-Ketten, und stabile Diffusion kann Bilder
erzeugen, die nur bei ausreichender Manipulation der Eingabeaufforderung
etwas komisch sind. Mittelmäßige Programmierer werden GitHub Copilot
nutzen, um trivialen Code und Textbausteine für sie zu schreiben
(trivialer Code ist tautologisch uninteressant), und ML wird
wahrscheinlich weiterhin nützlich sein, um Bewerbungsschreiben für dich
zu verfassen. Selbstfahrende Autos könnten jeden Tag auftauchen, was für
Science-Fiction-Enthusiasten und Technokraten großartig sein wird, aber
in jeder Hinsicht viel schlimmer als der [Bau von mehr Zügen](https://yewtu.be/watch?v=0dKrUE_O0VE).

Die größten nachhaltigen Veränderungen durch das maschinelle Lernen
werden eher die folgenden sein:

- Verringerung der Zahl der Arbeitskräfte für qualifizierte kreative
  Arbeit.
- Die vollständige Abschaffung von Menschen in der Kundenbetreuung.
- Überzeugendere Spam- und Phishing-Inhalte, mehr skalierbare Betrügereien.
- SEO-Hacking-Inhaltsfarmen dominieren die Suchergebnisse.
- Buchfarmen (sowohl elektronische- als auch Papierbücher) überschwemmen den Markt.
- KI-generierte Inhalte überschwemmen die sozialen Medien.
- Weitverbreitete Propaganda und künstliche Graswurzelbewegung, sowohl in der Politik als auch in der Werbung.

KI-Unternehmen werden weiterhin in großem Umfang Abfall und
CO<sub>2</sub>-Emissionen erzeugen, denn sie aggressiv alle
Internetinhalte, die sie finden können, abgreifen, die Kosten auf die
digitale Infrastruktur der Welt verlagern und ihren Datenschatz in
GPU-Farmen einspeisen, um ihre Modelle zu erstellen. Sie könnten auch
Menschen einsetzen, um bei der Kennzeichnung von Inhalten zu helfen, und
die billigsten Märkte mit den schwächsten Arbeitsgesetzen aufsuchen, um
menschliche Ausbeutungsbetriebe aufzubauen, die das KI-Datenmonster
füttern.

Du wirst nie wieder einer Produktbewertung vertrauen. Du wirst nie
wieder mit einem Menschen bei deinem Internetanbieter sprechen. Fade,
prägnante Medien werden die digitale Welt um dich herum füllen.
Technologie, die für Engagement-Farmen entwickelt wurde &mdash; diese
von KI bearbeiteten Videos mit der knirschenden Maschinenstimme, die du
in letzter Zeit in deinen Feeds gesehen hast &mdash;, wird als
Weißprodukt gekennzeichnet und dazu verwendet, um Produkte und
Ideologien in großem Umfang und mit minimalen Kosten über
Social-Media-Konten zu verbreiten, die mit KI-Inhalten gefüllt sind, ein
Publikum kultivieren und in großen Mengen und in gutem Ansehen beim
Algorithmus verkauft werden.

All diese Dinge passieren bereits und werden noch schlimmer werden. Die
Zukunft der Medien ist ein seelenloses, fade Kopie aller Medien,
die vor der KI-Epoche entstanden sind, und das Schicksal aller neuen
kreativen Medien besteht darin, in dem aufgewühlten Haufen der
Mathematik unterzugehen.

Dies wird für die KI-Barone unglaublich profitabel sein, und um ihre
Investitionen zu sichern, führen sie eine immense, teure, weltweite
Propagandakampagne durch. In der Öffentlichkeit werden die gegenwärtigen
und potenziellen künftigen Fähigkeiten der Technologie mit atemlosen
Versprechungen lächerlicher Möglichkeiten hochgespielt. In Sitzungen
hinter verschlossenen Türen werden viel realistischere Versprechungen
gemacht, wie die Halbierung der Lohnkosten.

Die Propaganda lehnt sich auch an den mystischen Science-Fiction-Kanon
der künstlichen Intelligenz an: die Bedrohung durch intelligente
Computer mit weltvernichtender Macht, die verbotene Verlockung eines
neuen Manhattan-Projekts mit all seinen Konsequenzen, die lange
prophezeite Singularität. Die Technologie ist bei weitem nicht auf
diesem Niveau, eine Tatsache, die Experten und den Baronen selbst
bekannt ist, aber die Illusion wird im Interesse der Lobbyarbeit bei den
Gesetzgebern aufrechterhalten, um den Baronen zu helfen, einen Festung
um ihre neue Industrie zu ziehen.

Natürlich stellt die KI eine Bedrohung durch Gewalt dar, aber wie
Randall betont, geht sie nicht von der KI selbst aus, sondern eher von
den Menschen, die sie einsetzen. Das US-Militär testet KI-gesteuerte
Drohnen, die kein eigenes Bewusstsein haben, sondern menschliche Fehler
(oder menschliche Böswilligkeit) so lange vergrößern, bis unschuldige
Menschen getötet werden. KI-Tools werden bereits eingesetzt, um
Kautions- und Bewährungsauflagen festzulegen &mdash; sie können dich ins
Gefängnis bringen oder dort behalten. Die Polizei setzt KI für die
Gesichtserkennung und die «vorausschauende Polizeiarbeit» ein. Natürlich
führen all diese Modelle dazu, dass Minderheiten diskriminiert,
ihrer Freiheit beraubt und oft auch getötet werden.

KI wird durch aggressiven Kapitalismus definiert. Die Hype-Blase wurde
von Investoren und Kapitalisten erzeugt, die ihr Geld in diese
Technologie stecken, und die Rendite, die sie sich von dieser
Investition versprechen, wird aus deiner Tasche kommen. Die Singularität
wird nicht kommen, aber die realistischsten Versprechungen der KI werden
die Welt noch schlechter machen. Die KI-Revolution ist da, und ich mag
sie nicht wirklich.

<details style="margin-bottom: 1em; cursor: pointer;">
  <summary>Provokative Botschaft</summary>
<p>Ich hatte zu diesem Thema einen viel aufrührerischeren Artikel mit
dem Titel „ChatGPT ist der neue Gottesersatz der Techno-Atheisten“
verfasst. Darin werden einige ziemlich treffende Vergleiche zwischen
dem Kryptowährungskult und dem Kult des maschinellen Lernens sowie dem
religiösen, unerschütterlichen und weitgehend ignoranten Glauben an
beide Technologien als Vorboten des Fortschritts angestellt. Es hat Spaß
gemacht, ihn zu schreiben, aber dies ist wahrscheinlich der bessere
Artikel.</p>
<p>
Ich habe diesen Kommentar auf Hacker News gefunden und ihn im
ursprünglichen Entwurf zitiert: „Es lohnt sich wahrscheinlich, mit GPT4
zu sprechen, bevor man professionelle Hilfe [zur Behandlung von
Depression] in Anspruch nimmt“.
</p>
<p>
Für den Fall, dass du es hören musst: Nimm <a href="https://de.euronews.com/next/2023/04/02/chatbot-eliza-ki-selbstmord-belgien">nicht</a>
(Achtung: Selbstmord) die Dienste von OpenAI in Anspruch, um dir bei
deiner Depression zu helfen.  Einen Termin bei einem Therapeuten zu
finden und zu vereinbaren, kann für viele Menschen schwierig sein
&mdash; es ist in Ordnung, dass du das Gefühl hast, dass es schwer ist.
Sprich mit deinen Freunden und bitte sie, dir zu helfen, die richtige
Betreuung für deine Bedürfnisse zu finden.</p>
</details>

*Diese Artikel ist eine Übersetzung von «[AI crap](https://drewdevault.com/2023/08/29/2023-08-29-AI-crap.html)»,
von Drew Devault unter einer [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.de)-Lizenz veröffentlicht*.
