Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2020-04-17
Lang: es
Slug: instalación-digital-saboteada-en-berlín-alemania
Tags: acción directa, Alemania, Apple, Berlín, Google, privacidad
Title: Instalación digital saboteada en Berlín (Alemania)

*Este artículo fue publicado originalmente por Indymedia con el título*
[Shut down the power! Digitale Zurichtung sabotiert](https://de.indymedia.org/node/77193) *bajo la licencia
<a href="https://creativecommons.org/licenses/by-sa/3.0/de/deed.es"><abbr
title="Creative Commons Atribución-CompartirIgual 3.0 Alemania">CC BY-SA 3.0 DE</abbr></a>.*

## *Shut down the power!* Infraestructura digital saboteada

Le dimos rechazo a la llamada Corona-App y pasamos a la acción. Hoy
hemos prendido fuego a un túnel con cables de comunicación que dan
servicio al Heinrich-Herz-Institut, entre otros. Dicen que con nuestro
ataque han sido destruidos los cables de red de Colt, Telekom y  otros
proveedores. El Heinrich-Hertz-Institut (HHI) era el objetivo de nuestro
ataque. Identificamos el túnel abierto en el que se están instalando
nuevos cables como proveedores del HHI. La breve desconexión afectó
también a otras empresas locales, como concesionarios de los asesinos
climáticos VW [Volkswagen], Alfa Romeo, Jeep, Mercedes, Audi, Porsche,
etc. Hemos descartado poner en peligro la vida humana con nuestra
acción.

### Por qué hemos saboteado políticamente el uso de la aplicación

Los decretos contra la pandemia traen el toque de queda, la prohibición
de contactos y otras injerencias, cuya velocidad de implantación e
implementación no tienen precedentes en la República Federal. A estas
injerencias las acompaña una y otra vez un lenguaje de guerra. El modelo
para estas «reglas» es la gestión del Virus de China. China, una
dictadura patriarcal que vigila cada movimiento de la gente, controla y
castiga las violaciones de las «reglas» establecidas por la élite
comunista-capitalista. El aislamiento de metrópolis solo puede
realizarse en un sistema totalitario tan eficiente, una medida de lo que
es posible. China, con su toque de queda de 60 días (p. ej., en Wuhan) y
el control total de las personas se ha convertido en el modelo de la
(supuesta) contención de la pandemia para gobiernos de casi todo el
mundo. A principios de enero estas medidas eran todavía criticadas como
totalitarias y enemigas de los derechos humanos. Ahora son transferidas
de forma modificada a otras partes del globo.

Totalmente en este sentido ha sugerido varias veces Jens Spahn [ministro
de salud de la Republica Federal de Alemania] el rastreo del teléfono
móvil, el rastreo de datos de cada persona para poder encontrar y aislar
a los infectados y a los posibles nuevos infectados. Se ha mantenido a
al menos dos metros del criticismo de expertos en derecho constitucional
y defensores y defensoras de la privacidad. Ha copiado la
geolocalización absoluta del perfil de movimiento de cada persona de
China y Corea del Sur. El presidente del parlamento austriaco abogó
también por la introducción obligatoria de una aplicación similar. En
este país también se hicieron las mismas exigencias. Ya las discusiones
son calculadas formas de romper tabús con el resultado de que se facilita
la aceptación del creciente potencial de vigilancia de nuevas tecnologías
y, si es necesario, que sean controladas de forma autoritaria si no
funciona «voluntariamente».

En China la aplicación de Ant Financial se utiliza durante controles
policiales. El código QR personal decide sobre las compras en el
supermercado y sobre ir a pasear. Si el código QR es rojo o amarillo,
las instrucciones son dadas por las autoridades. Esta aplicación de pago
decide de forma opaca sobre la «carga del coronavirus». Corea del Sur
aún no ha impuesto un toque de queda. Todas las personas «pueden»
continuar trabajando hasta que el teléfono inteligente las identifique
como «infectadas» o «sospechosas» y se ordene el acceso estatal.
Actualmente el gobierno está obligando a la gente en Corea del Sur a
proporcionarle sus datos del móvil y su acceso. El rastreo de datos en
Corea del Sur se ha probado con el programa Total Information
Awareness, el cual la NSA siguió operando como Prism, como reveló Ed.
Snowden. En los EE. UU. Google y Apple quieren distribuir una aplicación
para el coronavirus de forma automática como un componente del sistema
operativo con una actualización.

El sistema de rastreo de datos se establecerá pronto en Alemania en la
forma de una aplicación instalada. La propaganda de esta aplicación ya
funciona a toda velocidad. Los políticos harán una publicidad masiva de
ella, porque en la opinión del público solo la aceptación general
promete los efectos deseados. Superficialmente, el uso de la aplicación
suena razonable. Durante la introducción se basa (inicialmente) en la
voluntariedad para evitar la flagrante anticonstitucionalidad; puesto
que los contactos (es decir, infectados y no infectados y su entorno)
pueden ser espiados. Pero del mismo modo que el envío de los perfiles de
movimiento (supuestamente) anonimizados mediante proveedores de
teléfono, en el que las personas afectadas ya ni tienen oportunidad de
dar su consentimiento o rechazo, se puede asumir que las opciones de
monitorización contenidas en la aplicación se convertirán rápidamente en
un estándar obligatorio una vez que hayan sido adoptadas de forma
«voluntaria» por una masa crítica: quien quiera ir a la biblioteca
debe tener la aplicación &mdash;visitar la biblioteca es
voluntario&mdash;. Puesto que el código fuente del programa no es
abierto, es imposible comprobar si la propaganda para el uso de la
aplicación corresponde con la realidad, o quién más puede usar los
datos; y si hay alguna posibilidad para el rastreo de datos. Una simple
actualización sería posible en cualquier momento. En esta aplicación
trabaja actualmente el Heinrich-Herz-Institut y la Oficina Federal para
la Seguridad de la Información y el ejército alemán, entre otros.

### Se regulará

Apenas se puede procesar la velocidad de los cambios diarios. No es por
nada que la lucha contra la pandemia se fundamenta en una retórica de
guerra deliberadamente elegida. Pues una guerra es siempre también un
ataque social hacia dentro para dirigir a la «comunidad nacional» o &mdash;y esto es nuevo en el caso del coronavirus&mdash; la comunidad global hacia los
intereses de la clase dominante. No hay una conspiración detrás de esto;
es la dinámica constante de la clase dominante, que durante siglos no ha
podido romperse mediante una revolución integral de la liberación de las
formas de toda dominación. El esquema no es nuevo: las crisis se usan
siempre como catalizadores de regulaciones represivas de la población, a
no ser que una fuerza revolucionaria siente otros precedentes. En vista
de la pandemia se pondrá en marcha una maquinaria de seguridad interna,
que viva de la participación de todos y todas. Para muchas personas los
propios algoritmos empresariales detrás de las aplicaciones ya regulan
sus rutinas diarias y son los acompañantes constantes. Ahora en tiempos
del coronavirus es, pues, el tiempo de limitarse, de distanciarse
socialmente, de observarse a sí mismo (y a otras personas) en los
contactos &mdash;y después en algún momento hacerlo fácil con la
aplicación&mdash;. Incluido el sentimiento positivo y de
responsabilidad, has hecho algo por la seguridad de todos.

### Aparecen nuevas palabras tabú

Con la «guerra contra el virus» también el lenguaje y el
pensamiento están cambiando. De repente hay gente «relevante para el
sistema», «grupos de riesgo» que se deberían aislar. La «distancia
social» es la nueva salvación para proteger a los «grupos de riesgo» y a
la gente «relevante para el sistema», los «héroes del día a día». Los
últimos, el personal de enfermería, los empleados y empleadas de
supermercados, conductores y conductores de camiones, etc., se
convierten en los héroes del «frente» en vez de pagarles decentemente
&mdash;mientras los jefes continúan recibiendo bonificaciones y
consiguen billones para sus empresas&mdash;. El concepto
militar-sanitario de «triaje» está avanzando a la esfera pública, la
ordenación sistemática de personas: a quién hay que salvar y para quién
ya no «merece la pena», quién debe ser abandonado en el «campo de
batalla del virus». No es el virus el que lleva a la crisis, sino un
sistema de sanidad privatizado y orientado al beneficio que lleva al
lamentable estado de emergencia en hospitales y geriátricos (en España,
Italia y quizá aquí también).

El hecho de que cada persona se enfrenta a la muerte sin protección,
especialmente cuando aparece como un virus invisible, y como nueva
pandemia no se puede evaluar nada, crea miedos. Estos miedos no son una
tontería. Ni hay que exagerar estos miedos, ni dejar que se vuelvan algo
extraordinario, pues todos moriremos un día. Sin embargo, esta pandemia
está instrumentalizando los miedos ancestrales a la muerte. «Se juega»
con estos miedos. No son las políticas de privatización en el sistema de
salud lo que se cuestiona, sino si **tú** no guardas una distancia
adecuada de cada persona; si **tú** sigues las normas. Estas normas
son monitorizadas (y en parte también castigadas). Y promueven una de
las virtudes más alemanas en todos lados: la tendencia a denunciar. En
los círculos intelectuales se lanza la acusación de que se uno o una no
tiene solidaridad si no sigue las reglas. Si **tú** no sigues estas
normas, **tú** tienes la culpa de que la gente muera. Con la referencia
a los «grupos de riesgo» se reprimen otras objeciones. Los «grupos de
riesgo» se vuelven un factor de chantaje moral para llevar a cabo las
reglas estatales y políticas entre amigos/as sin que te lo cuestiones.
La higiene médica se acompaña de una higiene social que apenas permite
el pensamiento y el debate sucio, resistente.

### Por ello es probable

Nuestra acción será calificada de insolidaria por aquellos que se
vuelven esbirros de nuevas técnicas de dominación estatal y un ataque
social tecnológico &mdash;quizá sin ni siquiera quererlo&mdash;. Nuestra
declaración será ignorada, sujeta a un bloqueo comunicativo o declarada
confusa.

### Permanecemos al margen solidariamente

No estamos tomando está arriesgada acción para ganar la aprobación
general, para esta las disputas son demasiado polarizadas para nuestra
desventaja en el sentido contrarrevolucionario. Sabemos de la aprobación
de una parte de la sociedad. Estamos del lado de quienes no están
preparados para la destrucción de una parte de la sociedad. Estamos del
lado de los refugiados en las fronteras y en los campamentos. Estamos
del lado de quienes reconocen o contraatacan la instrumentalización de
la pandemia y de los miedos. Estamos del lado de quienes se preocupan
por el rastreo masivo.

### Cómo sucede la transformación digital

Vemos la digitalización de nuestra vida diaria, la que se expande
inevitablemente por la prohibición de salida y de contacto y la que
parece desconocer alternativas analógicas, como una transformación
digital de la sociedad. A primera vista es la única oportunidad para las
personas aisladas de mantenerse en contacto entre sí. Pero el espacio en
el que se lleva a cabo no es neutral. Está controlado y monitorizado.
Los sujetos sociales, las personas, se convierten en figuras virtuales,
que los algoritmos descomponen en conjuntos de datos y juzgan según a
criterios secretos, dirigen la publicidad, señalan y denuncian la mala
conducta, y premian la sumisión. «Distanciamiento social» o
«distancia es decencia» son términos, como si hubieran sido prestados de
*Brave new world* de Huxley o *1984* de Orwell. Al descubierto son
términos de lucha que la inmersión en el mundo virtual nos asigna como
una acción social integral. Se pretende un «nosotros», y la Red se
ofrece al «nosotros» como el nuevo lugar de encuentro social y el mundo
del trabajo &mdash;por ende se consolidará más el aislamiento social ya
en curso&mdash;. Aquí es donde se forma la actual y futura
controlabilidad de sociedades enteras por la Red.

El comercio en línea, las lecciones escolares digitales, seminarios en
línea en universidades, conferencias de vídeo, oficina en casa, archivos
electrónicos de pacientes, Amazon, Zalando, Netflix, Lieferando, pagos
por tarjeta, portales de citas, emisiones en continuo, videojuegos,
etc., son prerrequisitos para esto. Aquí es donde se está
reestructurando la sociedad. Aquí tiene lugar la habituación, aquí
cambia la sociedad a un ritmo cuyo precio &mdash;la posibilidad la
manipulación y así la dominación total&mdash; se nos hará claro en todos
sus detalles en los años venideros. Se está construyendo actualmente un
nuevo, en concreto higiénico «nosotros» (nacional) para llevar a cabo
todo tipo de medidas contra las que había reservas y resistencia en el
pasado, como la digitalización en las escuelas, las tarjetas de seguro
sanitario de cristal y los archivos de pacientes, o los pagos en línea y
la desaparición del dinero en efectivo.

Deutsche Telekom proporciona, de forma totalmente desinteresada,
«servicios web de conferencias» basados en la nube para estudiantes y
profesores libre de coste. Ofertas similares también hay disponibles
para empresas y sus necesidades de la oficina en casa. Y para el tiempo
de ocio de los pequeños hay un nuevo servicio de emisiones en continuo
de Disney. Y además unos 10 gigabytes adicionales para navegar por
Internet en el móvil, gratis por el momento. Mientras que Telekom
difunde «Conectamos Alemania», el grito de batalla de Vodafone es
«Alemania permanece conectada». El abanico de servicios no difiere
significativamente. Pero Alemania y la red digital &mdash;eso crea
cohesión&mdash;. El coronavirus, un golpe de suerte para los operadores
de red: nueva demanda de más velocidad, amplitud, más. Con las ofertas
actuales, se ata a clientes futuros y se generan todavía más datos, a
los que las empresas y los servicios secretos acceden por igual.
Vodafone trabaja estrechamente con el servicio secreto británico, que a
su vez es el mayor aliado de la estadounidense NSA. Puesto que la gente
pasa más tiempo en la Red con contactos sociales, trabajo y
entretenimiento, es esto una celebración para los servicios y las
empresas. Un mayor acceso a la vida social no es posible. ¡Cuánto más en
términos de beneficios, cuánto más en términos de monitorización y
control de nuestro comportamiento adquisitivo, modos de vida deseados,
de detección temprana de revueltas puede derivarse de estos datos!

Por último desde las revelaciones de Snowden sobre la vigilancia masiva
del NSA de los estados y grupos hasta las afirmaciones digitales
individuales es conocido. Cualquier posibilidad técnica de rastreo
digital y control de comportamiento se usa también. En China, en los
Estados Unidos, en Rusia y también en Alemania. La aplicación del
coronavirus es la puerta de entrada. El escenario de que al menos el 60
por ciento de la población alemana debe ser «voluntariamente»
condicionada a una aplicación, a un estándar, a una intención, a un
cacheo «voluntario» de todos sus contactos públicos y privados
&mdash;esto requiere nuestro sabotaje directo&mdash;.

### Lo que aún queda por decir

Actualmente estamos experimentando un ejercicio de guerra civil mundial
para futuras crisis y situaciones de guerra. Las consecuencias de este
«ejercicio» cambiarán el mundo. La gravedad de la pandemia, su
propagación y la masa de personas que mueren son la matriz sobre la que
se nos introduce en una nueva era de crisis como un estado permanente.
En caso de duda, no cuentan ni los derechos básicos del país en cuestión
(que nunca se han aplicado a todo el mundo) ni los derechos humanos.
Mientras se imponen prohibiciones de contacto y toques de queda, se
mantiene la obligación de realizar trabajo remunerado y se deja a la
discreción de los empresarios si continúan trabajando como antes, si
obtienen un trabajo subvencionado de corta duración o si cambian la
producción a métodos más rentables. En otros lugares, al menos las
huelgas estallaron. En este país, el control de la pandemia termina en
las puertas de las fábricas. En la línea de montaje y en otros lugares,
donde no es posible la oficina en casa, la gente debe trabajar siempre
que sirva para maximizar los beneficios y luego volver rápidamente a los
panales de su familia, los sindicatos no escucharán nada más. Si bien
las mercancías deben seguir circulando libremente y los trabajadores y
trabajadoras migrantes de Europa oriental deben llegar a tiempo para que
la economía no se derrumbe, los fugitivos se mantienen en campamentos,
campamentos que garantizan la rápida propagación del virus y no
aseguran una atención sanitaria adecuada.

Una crisis no solo reemplaza a la siguiente, sino que hace que los
problemas desaparezcan. La crisis climática desaparece detrás del
coronavirus. Las guerras y sus consecuencias también desaparecen. Y las
razones de las guerras en cualquier caso. No está claro dónde terminaron
las 10&nbsp;000 personas que quedaron atrapadas en la frontera entre Turquía
y Grecia. La UE, que está convirtiendo cada vez más estas fronteras en
franjas de muerte, permanece impune. La preparación de pogromos en
Hungría contra los romaníes y los sintis por parte de Orban y la derecha
tampoco se observa. No hay reacción al uso del virus para establecer
gobiernos autoritarios en contra de la constitución, como en Polonia. O
la retención del poder por el corrupto presidente israelí. O para la
consolidación del poder de Putin. A más tardar ahora debería
reconocerse cuándo el gobierno y la economía confían en los expertos y
la ciencia y cuándo no. ¿Por qué una pandemia puede desencadenar un
programa de emergencia y medidas drásticas en todo el mundo, pero no el
colapso del clima que ya está ocurriendo? Esta pregunta es transferible
a todos los problemas globales.

En el caso de la destrucción del clima, que afecta a toda la humanidad
al menos tanto como la pandemia, las advertencias y propuestas de los
expertos han sido y serán en gran medida ignoradas. Porque una vacuna no
es suficiente para combatir las consecuencias de la alteración del
clima. El coronavirus es bastante diferente: no solo los expertos en
salud encuentran oídos abiertos, sino que su enfoque médico para
combatir la pandemia abre un nuevo campo de acción para los políticos.
Se está salvando una economía asesina, un sistema mundial bélico y una
orientación hacia el progreso y el crecimiento que tiene como objetivo
destruir la tierra y la base de toda la vida con billones de dólares y
euros, mientras que las protestas contra esto están siendo prohibidas
por las autoridades sanitarias. Es el principio colonial según el cual
se atribuyen diferentes valores a la vida humana. Cada año 100&#32;000
personas mueren de malaria. El cambio climático ya está matando: cientos
de millones de personas están muriendo de malnutrición o de hambre.
Miles de millones de personas no tienen acceso a agua potable.

En esta nueva época, las fuerzas que quieren un cambio fundamental deben
reorientarse y reposicionarse internacionalmente. Una agitación y
superación de los comportamientos patriarcales, coloniales y
capitalistas no es una cuestión de lujo, sino existencial.

**Nunca nos acostumbraremos a lo que deberíamos acostumbrarnos**

Volcano Group Shut Down the Power / Sabotage Digital Infrastructure

P.S.: Por un primero de mayo contra el colonialismo, el patriarcado y el
nacionalismo.
