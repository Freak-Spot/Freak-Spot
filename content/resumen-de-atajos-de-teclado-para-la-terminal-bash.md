Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-08-09 08:38
Lang: es
Slug: resumen-de-atajos-de-teclado-para-la-terminal-bash
Status: published
Tags: atajos de teclado, Bash, interfaz de línea de órdenes
Title: Resumen de atajos de teclado para la Terminal (Bash)

[Este
resumen](/wp-content/uploads/2016/08/atajos_terminal.html)
lo he elaborado a partir del [manual de referencia de
Bash](https://www.gnu.org/software/bash/manual/bash.html).

*C-b*
:   Se mueve atrás un carácter.

*C-f*
:   Se mueve adelante un carácter.

SUPR o Retroceso
:   Borra el carácter a la izquierda del cursor.

*C-d*
:   Borra el carácter debajo del cursor.

Caracteres imprimibles
:   Inserta un carácter en la posición del cursor.

*C-\_* o *C-u*
:   Deshace la última instrucción de edición. Puedes desahacer hasta
    dejar la línea vacía.

*C-a*
:   Se mueve al principio de la línea.

*C-e*
:   Se mueve al final de la línea.

*M-f*
:   Se mueve una palabra adelante, donde la palabra está compuesta de
    letras y números.

*M-b*
:   Se mueve atrás una palabra

*C-l*
:   Limpia la pantalla, volviendo a imprimir la línea actual arriba.

*C-k*
:   Mata (*matar* texto significa borrar texto de la línea, pero
    guardándolo para su uso posterior) el texto desde la posición actual
    del cursor hasta el final de la línea.

*M-d*
:   Mata desde la posición actual del cursor hasta el final de la línea

*M-d*
:   Mata hasta el final de una palabra.

*M-SUPR*
:   Mata desde el cursor hasta el final de la palabra actual, o, si
    entre palabras, hasta el principio de la palabra anterior. Los
    límites de las palabras son los mismos que usa *M-f*

*M-DEL*
:   Mata desde la posición del cursor hasta el inicio de la palabra
    actual, o, si entre palabras, hasta el principio de la
    palabra anterior. Los límites de las palabras son los mismos que los
    usados por *M-b*

*C-w*
:   Mata desde el cursor hasta el espacio en blanco anterior. Esto es
    diferente que *M-SUPR* porque los límites de las palabras difieren.

*C-y*
:   Pega el texto recientemente matado de nuevo al buffer del donde se
    encuentra el cursor.

*M-y*
:   Rota el *kill-ring*, y pega lo que está al tope. Solo puedes hacer
    esto si la instrucción anterior es *C-y* o *M-y*


