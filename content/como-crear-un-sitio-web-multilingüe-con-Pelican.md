Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2023-01-10 17:50
Lang: es
Modified: 2024-01-13 17:00
Slug: cómo-crear-un-sitio-web-multilingüe-con-pelican
Tags: i18n_subsites, Pelican, localización
Title: Cómo crear un sitio web multilingüe con Pelican
Image: <img src="/wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados.png" alt="" width="1920" height="1033" srcset="/wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados.png 1920w, /wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados-960x516.png 960w, /wp-content/uploads/2023/01/traducción-de-sitio-web-en-Pelican-dos-subsitios-comparados-480x258.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">


[Pelican](https://getpelican.com/) es un generador de páginas web escrito en Python con una gran
flexibilidad. Este sitio web está hecho con Pelican y está disponible en
varios idiomas<!-- more -->[^1]. Con Pelican hay dos opciones para traducir sitios
web:

1. [Traducir artículos individuales](#1).
2. [Crear un subsitio web entero](#2) (p. ej.,
   [freakspot.net/eo/](https://freakspot.net/eo/)) con plantillas
   traducidas.

<h2 id="1">Artículos individuales</h2>

<a href="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png">
<img src="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png" alt="" width="845" height="286" srcset="/wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo.png 845w, /wp-content/uploads/2023/01/traducción-de-artículo-en-Pelican-pie-de-artículo-422x143.png 422w" sizes="(max-width: 845px) 100vw, 845px">
</a>

Para la primera opción basta con instalar Pelican[^2] y crear dos
archivos con el mismo `Slug` en la carpeta `content/`. Por ejemplo, en
el archivo en español (`como-destruir-Google.md`) pondríamos algo así:

    :::text
    Author: Jorge Maldonado Ventura
    Category: Privacidad
    Date: 2022-11-06 19:00
    Lang: es
    Slug: Cómo-destruir-Google
    Tags: acción directa, anuncios, boicot, Google, privacidad
    Title: Cómo destruir Google

    El modelo de negocio de Google se basa en recoger datos personales de
    usuarios, venderlos a terceros y servir anuncios. La empresa también

Hay que especificar un idioma diferente (p. ej., `Lang: eo`) al del
archivo traducido en el archivo en el otro idioma
(en este ejemplo, `como-destruir-Google.eo.md`). Opcionalmente, se
pueden añadir también `Save_as` y `URL` para internacionalizar las URLs.

    :::text
    Author: Jorge Maldonado Ventura
    Category: Privateco
    Date: 2022-11-06 23:51
    Lang: eo
    Slug: Cómo-destruir-Google
    Save_as: kiel-detrui-Google/index.html
    URL: kiel-detrui-Google/
    Tags: rekta agado, reklamoj, bojkoto, Google, privateco
    Title: Kiel detrui Google

    La komerca modelo de Google baziĝas sur la kolekto de personaj datenoj de
    uzantoj, la vendado de ili al aliaj firmaoj kaj la montrado de reklamoj. La firmaoj ankaŭ

<h2 id="2">Subsitios web</h2>

Para crear un subsitio es necesario usar la extensión
[i18n_subsites](https://github.com/getpelican/pelican-plugins/tree/master/i18n_subsites). Para ello hay que hacer lo siguiente:

<ol>
    <li>Descargar los archivos de la extensión en el directorio usado para
    alojar las extensiones. En mi caso se encuentra en <code>plugins/i18n_subsites/</code>.</li>
    <li>Activar la extensión y configurar los subsitios que queramos generar.
    En el siguiente ejemplo configuro solo un subsitio en esperanto:</li>
</ol>

    :::python
    PLUGINS = ['i18n_subsites']

    I18N_SUBSITES = {
        'eo': {
            'SITENAME': 'Retejo pri libera kulturo',
            }
        }

Es recomendable traducir el tema del sitio web, es decir, los menús, el
pie de página, etc. Para ello es recomendable usar GNU gettext. La
extensión incluye una [guía sobre cómo localizar temas con
Jinja2](https://github.com/getpelican/pelican-plugins/blob/master/i18n_subsites/localizing_using_jinja2.rst) (en inglés) que puedes seguir.

Finalmente, usando las variables disponibles en la extensión se puede
añadir una lista desplegable para cambiar de idioma y otras
funcionalidades. Los metadatos del artículo deben ser como los que
hemos puesto [antes](#1) de ejemplo. Si te pierdes, puedes consultar el código fuente de
sitios web que usan `i18n_subsites`. Por ejemplo, este sitio web tiene
[su código fuente disponible](https://notabug.org/Freak-Spot/Freak-Spot).

[^1]: En [esperanto](/eo/), [portugués](/pt/), [alemán](/de/),
    [inglés](/en/), [español](/), [ruso](/ru/) y [rumano](/ro/).
[^2]: En Debian se ejecutaría ``sudo apt install pelican``. Para
    instalar Pelican con Pip hay que ejecutar ``sudo pip3 install
    pelican``.
