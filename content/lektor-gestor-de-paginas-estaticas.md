Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2017-08-12 17:26
Image: <img src="/wp-content/uploads/2017/08/Lektor-en-accion.png" alt="Lektor en acción" width="1621" height="985" srcset="/wp-content/uploads/2017/08/Lektor-en-accion.png 1621w, /wp-content/uploads/2017/08/Lektor-en-accion-810x492.png 810w, /wp-content/uploads/2017/08/Lektor-en-accion-405x246.png 405w" sizes="(max-width: 1621px) 100vw, 1621px">
Lang: es
Slug: lektor-el-gestor-de-paginas-estaticas
Tags: generador de páginas estáticas, Lektor, Python, Python3
Title: Lektor: el gestor de páginas estáticas

Los generadores de páginas estáticas permiten crear sitios web sencillos
sin depender de bases de datos y de un <i lang="en">software</i> complejo. Las ventajas
de los generadores de páginas estáticas son abrumadoras en cuanto a
sitios web sin mucho contenido interactivo, como blogs. Pero la mayoría
les resultan difíciles de usar o administrar a personas que no tienen
muchos conocimientos de informática.

Lektor pretende hacer más sencillo de usar un generador de páginas
estáticas. Realmente Lektor no es un generador de páginas estáticas al
uso, sino más bien un gestor de páginas estáticas. Permite administrar
los sitios web de forma muy sencilla mediante una interfaz gráfica que
recuerda a la de algunos
[sistemas de gestión de contenidos](https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_contenidos).

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2017/08/editando-un-articulo-con-Lektor.png">
    <img src="/wp-content/uploads/2017/08/editando-un-articulo-con-Lektor.png" alt="Editando un artículo con Lektor" width="1558" height="924" srcset="/wp-content/uploads/2017/08/editando-un-articulo-con-Lektor.png 1558w, /wp-content/uploads/2017/08/editando-un-articulo-con-Lektor-779x462.png 779w, /wp-content/uploads/2017/08/editando-un-articulo-con-Lektor-389x231.png 389w" sizes="(max-width: 1558px) 100vw, 1558px">
    </a>
    <figcaption class="wp-caption-text">Editando un artículo con Lektor</figcaption>
</figure>

Basta con iniciar Lektor e ir a la <abbr title="Uniform Resource Locator">URL</abbr>
local donde se ha abierto para empezar a administrar el sitio web. Los cambios
realizados se pueden controlar mediante sistemas de control de versiones (como
[Git](https://en.wikipedia.org/wiki/Git)) o simplemente haciendo copias
de seguridad de vez en cuando. Publicar el sitio web es tan sencillo
como pulsar un botón cuando hayamos comprobado en el navegador que los
cambios realizados son correctos.

La forma más sencilla de comprobar las ventajas de Lektor es descargándolo,
instalándolo y usándolo. [Su página web](https://www.getlektor.com/) contiene
instrucciones sobre cómo hacerlo.

Lo que no me gusta de Lektor es que la interfaz de administración requiere
JavaScript y que su documentación recomienda el uso de <i lang="en">software</i> privativo
(concretamente Disqus y GitHub en la fecha en la que escribo este artículo).
