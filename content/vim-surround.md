Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Edición de textos
Date: 2016-10-15 11:20
JS: asciinema-player.js (top)
Lang: es
Modified: 2017-03-03 00:59
Slug: vim-surround
Status: published
Tags: edición de texto sin formato, editores de texto, extensión, Neovim, PHP, plugin, Vim, vim-surround
Title: vim-surround

<!-- more -->

<asciinema-player src="../asciicasts/89325.json">Lo siento,
asciinema-player no funciona sin JavaScript.
</asciinema-player>

Cuando estoy programando a veces necesito cambiar caracteres o etiquetas
(en lenguajes de marcado) que rodean instrucciones. Por ejemplo, en
Python a veces tengo que cambiar comillas dobles por comillas simples;
lo cual se hace un poco incómodo, porque tienes que desplazarte a las
comillas de inicio y a las de fin y sustituirlas
individualmente.

Para este problema existe una solución: la extensión
[vim-surround](http://www.vim.org/scripts/script.php?script_id=1697)
para los editores Vim y Neovim. Con ella puedo reemplazar los caracteres
o etiquetas muy fácilmente. Para sustutuir `print("Hello, world!")` por
`print('Hello, world!')` basta con pulsar `cs"'` en modo normal dentro
de las comillas. También puedo sustituir comillas por etiquetas de
lenguajes de marcado como HTML y viceversa. Por ejemplo, puedo sustituir
`'Hello, world!'` por `<h1>Hello, world!</h1>` escribiendo `cs'<h1>`.

La orden para realizar sustituciones puede parecer difícil de recordar,
pero no lo es si sabes inglés y piensas que `cst"` quiere decir *change
surrounding tag \[with\] "* y `cs"'` quiere decir *change surrounding "
with '*.

Esta extensión además permite borrar alrededor de donde estamos
situados. Si, por ejemplo, queremos borrar las comillas en
`print('Hello, world!')`, solamente tenemos que escribir `ds'`.

Podemos añadir también etiquetas HTML alrededor de una palabra o varias
palabras. Escribir `ysiw<em>` en modo normal con el cursor en la palabra
hola, envuelve la palabra con la etiqueta `em`: `<em>hola</em>`. `yss)`
envuelve una línea donde está el cursor entre paréntesis. Para tener un
mayor control sobre el texto a envolver se puede utilizar el modo
visual. Cuando tengas algún texto seleccionado pulsa <kbd>S</kbd> y escribe el
carácter, etiqueta o caracteres para rodear el texto.

¿Pero que pasa si quieres añadir una etiqueta para PHP (su sintaxis
es `<?php código?>`)? Para hacer esto debemos añadir la siguiente línea a
nuestro archivo de configuración de Vim (`.vimrc`):
`autocmd FileType php let b:surround_45 = "<?php \r ?>`. Con esto
podemos rodear varias líneas de código con las etiquetas de PHP,
seleccionando un texto en modo visual y pulsando `S-` en caso de que
estemos trabajando con un un archivo escrito en PHP. Podemos rodear
también una línea en modo normal pulsando `yss-`.

A continuación, dejo más ejemplos de lo que podéis hacer con
vim-surround. El carácter marcado en la columna del texto original
representa la posición del cursor. Recordad que el cursor debe ubicarse
entre las comillas o etiquetas que queráis reemplazar. Podéis verlos en
acción en [el vídeo que os he mostrado al principio de este
artículo](https://asciinema.org/a/89325).

Texto original                          | Orden        | Resultado
----------------------------------------|--------------|--------------------------------------
"Look ma, I'm <mark>H</mark>TML!"       | cs"&lt;q&gt; | &lt;q&gt;Look ma, I'm HTML!&lt;/q&gt;
if <mark>x</mark>&gt;3 {                | ysW(         | if ( x&gt;3 ) {
my $str = <mark>w</mark>hee!;           | vllllS'      | my $str = 'whee!';
&lt;div&gt;Yo!<mark>&lt;</mark>/div&gt; | dst          | Yo!
Hello w<mark>o</mark>rld!               | yssB         | \{Hello world!\}
