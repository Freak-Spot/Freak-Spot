Author: Jorge Maldonado Ventura
Category: Nachrichten
Date: 2022-03-17
Lang: de
Slug: duckduckgo-censura-desinformacion-rusa
Save_as: DuckDuckGo-zensiert-russische-Fehlinformationen/index.html
URL: DuckDuckGo-zensiert-russische-Fehlinformationen/
Tags: Zensur, Informationsfreiheit, Politik, Privatsphäre, Russland, Freie Software
Title: DuckDuckGo zensiert „russische Fehlinformationen“
Image: <img src="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg" alt="" width="2083" height="1667" srcset="/wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.jpeg 2083w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-1041x833.jpg 1041w, /wp-content/uploads/2022/03/pato-de-DuckDuckGo-ahora-sin-desinformacion-rusa.-520x416.jpg 520w" sizes="(max-width: 2083px) 100vw, 2083px">

Der Geschäftsführer von DuckDuckGo [sagte auf Twitter](https://nitter.snopyta.org/yegg/status/1501716484761997318):

> Wie so viele andere bin ich auch angewidert von Russlands Einmarsch in
> der Ukraine und der gigantischen humanitären Krise, die dadurch
> ausgelöst wird. #StandWithUkraine️

> Bei DuckDuckGo haben wir Such-Updates eingeführt, die Websites, die
> mit russischen Desinformationen in Verbindung gebracht werden,
> herunterstufen.

Diese Entscheidung ist für viele Nutzer problematisch, weil sie selbst
entscheiden wollen, was eine Fehlinformation ist und was nicht. Darum
empfehlen viele Menschen, die mit der Entscheidung von DuckDuckGo
unzufrieden sind und Befürworter freier Software sind, die Verwendung
von Suchmaschinen wie [Brave Seach](https://search.brave.com/) und
[Searx](https://searx.github.io/searx/).
