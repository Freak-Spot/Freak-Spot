Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-06-11 10:00
Lang: eo
Slug: deja-de-usar-Reddit
Tags: Reddit, Lemmy, Postmill, Lobsters, /kbin, privateco, liberaj programoj
Save_as: ĉesu-uzi-Reddit/index.html
URL: ĉesu-uzi-Reddit/
Title: Ĉesu uzi Reddit
Image: <img src="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg" alt="" width="2048" height="1024" srcset="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg 2048w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-1024x512.jpg 1024w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-512x256.jpg 512w" sizes="(max-width: 2048px) 100vw, 2048px">

[Miloj da Reddit-komunumoj](https://safereddit.com/r/ModCoord/comments/1401qw5/incomplete_and_growing_list_of_participating/)
morgaŭ ĉesos esti alireblaj por protesti [kontraŭ la decido pagigi
milionojn da dolaroj al programoj, kiuj uzas la API](https://safereddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/).
Pro tiu politiko multaj programoj ne plu funkcios.

Tamen, kvankam iuj homoj rekomendas, ke oni ĉesu uzi Reddit, la protesto
daŭros nur du tagojn. La problemo de portempa bojkoto estas, ke ĝi
sendas jenan mesaĝon al la proprietuloj: multaj homoj koleras, sed post
du tagoj revenos kaj ni daŭre gajnos monon. Reddit [ĉesis esti libera
programo antaŭ jaroj](/eo/raddle-kiel-respondo-al-reddit/) kaj ne ĉesos
cenzuri informojn, kiujn ili ne ŝatas.

Mi subtenas la bojkoton al Reddit, sed mi pensas, ke ĝi ne devus daŭri
nur du tagojn; ĝi devus esti porĉiama. Ekzistas kelkaj programoj similaj
al Reddit, kiuj estas liberaj kaj kiuj respektas la privatecon de siaj
uzantoj: [Lemmy](https://join-lemmy.org/), [/kbin](https://kbin.pub/),
[Postmill](https://postmill.xyz/), [Lobsters](https://lobste.rs/),
[Tildes](https://tildes.net/)...

[Lemmy](https://join-lemmy.org/) kaj [/kbin](https://kbin.pub/), malkiel
Reddit, estas liberaj kaj federaciitaj, do la administrantoj de nodo ne
povas cenzuri informojn de nodoj, kiujn ili ne regas, aŭ trudi ion al
ili; ĉiu nodo havas sian politikon.
