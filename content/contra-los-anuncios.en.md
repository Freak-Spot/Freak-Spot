Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2023-09-13 16:30
Lang: en
Slug: no-pierdas-tu-tiempo-y-dinero-viendo-anuncios
Save_as: dont-waste-your-life-with-ads/index.html
URL: dont-waste-your-life-with-ads/
Tags: ads, advice
Title: Don't waste your life with ads
Image: <img src="/wp-content/uploads/2023/09/distopia-anuncios-editado.png" alt="" width="1387" height="898" srcset="/wp-content/uploads/2023/09/distopia-anuncios-editado.png 1387w, /wp-content/uploads/2023/09/distopia-anuncios-editado-693x449.png 693w" sizes="(max-width: 1387px) 100vw, 1387px">

You are watching a video you like. The video is interrupted.
<span style="color: #E8C244">An annoying ad</span>. Back to the video.
A sponsorship. Subscribe. Leave a comment...

## Slowness, pollution, consumerism

Digital ads make you buy useless things you don't need and waste
precious time. In addition, ads cause pages to load more slowly and
create huge CO<sub>2</sub> emissions (both directly and indirectly).

## Ads that kill

Advertisements have always been used to manipulate our perceptions using
psychology. Advertisers sold tobacco as a symbol of liberation,
unhealthy foods as if they were healthy... Then health problems and
death followed for people who were convinced by those ads.

## You pay for them with your money and time

If you buy a product advertised on the Internet, the part that
companies spent to produce and propagate the ad is paid by you: with
your time and money (we are talking about a billion-dollar industry).

## Censorship

You can't express unconventional political opinions, nudity is not
allowed... Advertisers don't like to take risks, everything has to be
under control. If they don't like you, your video will be demonetized,
it won't appear in the search results. Anything that doesn't contribute
to consumerism must be ignored, because it doesn't make them money.

## Zero privacy

With the rise of targeted advertising, many companies monitor
everything you do on Internet, creating psychological profiles that
allow advertisers optimise ways to manipulate you.

<h2><span style="color: #b5ffb5">A life without ads</span></h2>

<a href="/wp-content/uploads/2023/09/bosque-falso.png">
<img src="/wp-content/uploads/2023/09/bosque-falso.png" alt="" width="1024" height="989" srcset="/wp-content/uploads/2023/09/bosque-falso.png 1024w, /wp-content/uploads/2023/09/bosque-falso-512x494.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

For all these reasons, I block ads on websites, videos (including
sponsorships), etc. With the time I save, I can find the information I'm
looking for more efficiently and read articles or watch videos without
waiting. This has allowed me to learn more in less time and to spend
more time doing what I really like.

If I buy something, I try to buy it wisely:

- What are the ingredients?
- Are there higher quality options?
- Where was it produced?
- How much does it cost?
- ...

If I like something and I want to support it, I do it directly (without
intermediaries) so that they get more money
than they would if I gave away my time to abusive advertisers.

Of course, advertising on the Internet will continue to exist, because
there will always be sponsored articles, corporate-funded videos or
things like that. Luckily, it is possible to avoid many advertisements
using programs like [uBlock Origin](https://en.wikipedia.org/wiki/UBlock_Origin) and [Piped](/en/YouTube-privately-with-Piped/).
