Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-06-22 13:14
Lang: es
Slug: recortar-archivo-multimedia-con-FFmpeg
Tags: audio, ffmpeg, vídeo
Title: Recortar archivo multimedia con FFmpeg
Image: <img src="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg" alt="" width="1200" height="826" srcset="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg 1200w, /wp-content/uploads/2022/06/pinchadiscos-ffmpeg.-600x413.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Si solo quieres editar un archivo multimedia para recortar su
principio, su final o ambos, lo más rápido es usar
[FFmpeg](https://es.wikipedia.org/wiki/FFmpeg). FFmpeg se puede instalar
en distribuciones basadas en Debian con `sudo apt install ffmpeg`.

Si queremos quitar los 10 primeros segundos de un archivo multimedia,
basta con ejecutar FFmpeg así:

    :::text
    ffmpeg -i cancion.mp3 -ss 10 cancion2.mp3

Después de `-i` se especifica el archivo que queremos editar
(`cancion.mp3`); `-ss` seguido de `10` indica los segundos que
queremos quitar; finalmente, se especifica el nombre del nuevo archivo,
`cancion2.mp3`.

Si queremos quitar tanto el principio como el final, podemos añadir el
argumento  `-to`:

    :::text
    ffmpeg -i cancion.mp3 -ss 15 -to 04:10 cancion2.mp3

Después de `-to` debe haber una posición, en este caso el minuto 4 y el
segundo 10 (`04:10`). También existe la posibilidad de usar `-t`, que
para obtener el mismo resultado se usaría así:

    :::text
    ffmpeg -i cancion.mp3 -ss 15 -t 235 cancion2.mp3

`-t` indica que se grabará hasta pasados 235 segundos en el nuevo
archivo. En este caso, esos 235 nuevos segundos se grabarán después de
saltarse los 15 primeros.
