Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2022-06-18 12:00
Modified: 2023-11-30 19:00
Lang: es
Slug: odysee-con-privacidad-con-librarian
Tags: JavaScript, Odysee, privacidad, YouTube
Title: Odysee con privacidad con librarian
Image: <img src="/wp-content/uploads/2022/06/librarian.svg" alt="">

Odysee es un sitio web de vídeos que usa un protocolo libre llamado
[LBRY](https://es.wikipedia.org/wiki/LBRY) que usa una cadena de bloques
para compartir ficheros y remunerar a los creadores con su propia
criptomoneda. Mediante LBRY no se puede censurar un vídeo, como sucede
con muchas plataformas actuales.

El problema es que la plataforma Odysee no es muy respetuosa con la
privacidad y requiere JavaScript &mdash;aunque su [código es libre](https://github.com/OdyseeTeam/odysee-frontend)&mdash;. Como alternativa existe
[librarian](https://codeberg.org/librarian/librarian). Una instancia
recomendada es <https://lbry.projectsegfau.lt/>, pero también [existen otras](https://codeberg.org/librarian/librarian#instances).

