Author: Хорхе Мальдонадо Вентура
Category: Конфиденциальность
Date: 2022-08-18 00:00
Lang: ru
Slug: TikTok-con-privacidad-con-ProxiTok
Save_as: prosmotr-TikTok-v-privatnom-režime-s-pomoŝju-ProxiTok/index.html
URL: prosmotr-TikTok-v-privatnom-režime-s-pomoŝju-ProxiTok/
Tags: TikTok
Title: Просмотр TikTok в приватном режиме с помощью ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

TikTok - централизованная социальная сеть и требует использования
проприетарного программного обеспечения. Практически невозможно
посещать TikTok через веб-браузер, без ущерба для вашей конфиденциальности
или свободы... Конечно, если только вы не используете другой интерфейс,
например [ProxyTok](https://proxitok.pabloferreiro.es/), о котором я рассказываю в этой статье.

Интерфейс ProxyTok весьма прост. Вы можете сразу перейти к трендовому
контенту, выполнить поиск по имени пользователя или перейти в
конкретный профиль, выполнить поиск по тегу, URL-ссылке,
идентификатору музыки или видео.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Поиск по имени пользователя</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png">
<img src="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png" alt="" width="1917" height="942" srcset="/wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok.png 1917w, /wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok-958x471.png 958w, /wp-content/uploads/2023/02/profile-from-a-commoner-on-Proxitok-479x235.png 479w" sizes="(max-width: 1917px) 100vw, 1917px">
</a>
    <figcaption class="wp-caption-text">Профиль <a href="https://proxitok.pabloferreiro.es/@a_commoner">@a_commoner</a>. Все обновления можно отслеживать через RSS-ленты.</figcaption>
</figure>

Проект был запущен 1 января 2022 года, поэтому он достаточно молод,
и ожидается, что в него будут добавлены новые функции. Для автоматического
перенаправления на ProxiTok вы можете установить расширение [LibRedirect](https://libredirect.github.io/),
которое также предотвращает работу сторонних веб-сайтов, наносящих ущерб
вашей конфиденциальности.

Существует [несколько общедоступных экземпляров](https://github.com/pablouser1/ProxiTok/wiki/Public-instances), и есть возможность
установить один из них на свой собственный сервер.
