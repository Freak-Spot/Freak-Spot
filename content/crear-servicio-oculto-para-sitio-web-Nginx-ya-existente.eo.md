Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-02-15 00:10
Lang: eo
Slug: crear-servicio-oculto-para-sitio-web-en-nginx
Tags: Debiano, GNU/Linukso, Nginx, kaŝita servo, Tor
Save_as: krei-kaŝitan-servon-por-retejo-en-Nginx/index.html
URL: krei-kaŝitan-servon-por-retejo-en-Nginx/
Title: Krei kaŝitan servon por retejo en Nginx (<i lang="en">clearnet</i> kaj <i lang="en">darknet</i>)
Image: <img src="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png" alt="" width="1005" height="546" srcset="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png 1005w, /wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal-502x273.png 502w" sizes="(max-width: 1005px) 100vw, 1005px">

Ĉu vi volas krei retejon uzante Nginx-servilon kaj vi volas ankaŭ havi
tiun retejon kiel kaŝitan servon por Tor-uzantoj? Ĉi tie mi klarigas al
vi kiel tion fari en Debiano (GNU/Linukso).

Unue ni devas instali la jenajn pakojn:

    :::bash
    sudo apt install nginx tor

Poste oni devas malkomenti la sekvajn liniojn de la dosiero
`/etc/tor/torrc`:

    :::bash
    #HiddenServiceDir /var/lib/tor/hidden_service/
    #HiddenServicePort 80 127.0.0.1:80

Jen la rezulto:

    :::text
    HiddenServiceDir /var/lib/tor/hidden_service/
    HiddenServicePort 80 127.0.0.1:80

Sekve ni restartigas la Tor-servon:

    :::bash
    sudo systemctl restart tor

Dum la restartigo Tor kreas la `hidden_service/`-dosierujon kaj plenigas
ĝin per la URL de la kaŝita servo (dosiero `hostname`) kaj la publika
kaj privata ŝlosiloj.

Se kaj Nginx kaj Tor rulas kiel servoj kaj ni iras al la adreso, kiu
estas en `/var/lib/tor/hidden_service/hostname`, ni povos vidi la
bonvenan paĝon de Nginx.

<a href="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png">
<img src="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png" alt="" width="883" height="484" srcset="/wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome.png 883w, /wp-content/uploads/2023/02/tor_hidden_service_nginx-welcome-441x242.png 441w" sizes="(max-width: 883px) 100vw, 883px">
</a>

Implicite la retejo por Nginx devas troviĝi en la vojo
`/var/www/html/`. Do ni nur devas disvolvi la retejon ene de tiu
dosierujo. Ne gravas, ĉu oni uzu Tor-URL-n aŭ kutiman URL-n, la retejo
estas la sama. Atentu, ke por ke la ligiloj al aliaj paĝoj de via retejo
funkciu uzante `.onion`-URL-jn, oni bezonas uzi relativajn adresojn.

## Kromaĵo: aldoni paĝokapo <em>.onion disponebla</em>

<a href="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png">
<img src="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png" alt="" width="1005" height="546" srcset="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png 1005w, /wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal-502x273.png 502w" sizes="(max-width: 1005px) 100vw, 1005px">
</a>

<!-- more -->

Oni simple devas aldoni la sekvan linion `add_header` al la parto de la
agorda dosiero de Nginx[^1], kiu diras `location /`:

    :::nginx
    location / {
        # Aliaj aferoj, kiuj troviĝas en location /
        add_header Onion-Location http://[la_nomo_de_via_kaŝita_servo]/$request_uri;
    }

Vi devos anstataŭigi `[la_nomo_de_via_kaŝita_servo]` per la nomo, kiu
troviĝas en la dosiero  `/var/lib/tor/hidden_service/hostname`. La
rezulto devas esti io simila al tio ĉi[^2]:

    :::nginx
    location / {
        add_header Onion-Location http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/$request_uri;
    }

[^1]: Ĉi-okaze `/etc/nginx/sites-available/default`.
[^2]: La nomo de la kaŝita servo estas ekzemplo, ne kopiu kaj algluu ĝin tiel; metu la nomon de la kaŝita servo, kiun vi kreis.
