Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2016-09-14 07:47
Lang: es
Modified: 2017-05-18 22:56
Slug: desarrolladores-ingratos
Status: published
Tags: colaboración, desarrolladores, programación, programadores bordes, repositorios
Title: Desarrolladores ingratos

<a href="/wp-content/uploads/2016/09/cr1.png"><img class="aligncenter size-full wp-image-443" src="/wp-content/uploads/2016/09/cr1.png" alt="cr1" width="625" height="594" srcset="/wp-content/uploads/2016/09/cr1.png 625w, /wp-content/uploads/2016/09/cr1-300x285.png 300w" sizes="(max-width: 625px) 100vw, 625px" /></a> <a href="/wp-content/uploads/2016/09/cr2.png"><img class="aligncenter size-full wp-image-444" src="/wp-content/uploads/2016/09/cr2.png" alt="cr2" width="624" height="611" srcset="/wp-content/uploads/2016/09/cr2.png 624w, /wp-content/uploads/2016/09/cr2-300x294.png 300w" sizes="(max-width: 624px) 100vw, 624px"></a>

Nunca me había pasado que alguien despreciara los errores que le señalara ni
rechazara mis contribuciones a pesar de ser correctas. Anteriormente,
incluso cuando me equivoqué al hacer una corrección, siempre me
respondieron más amablemente.

En particular, a dicho personaje le señalé un error y pasó de él
excusándose diciendo que se trataba de un componente que usa el proyecto
pero no forma parte de él. Esto es una escusa pésima, ya que el error
afecta a su proyecto. En contraposición, los desarrollares del fork de
*gogs* para notabug.org y *Unknown Horizons* (por poner ejemplos que
conozco de primera mano) hacen lo correcto: han sido notificados varios
errores de los que no tenían la culpa directamente, pero hasta que no
los corrigieron colaborando con el otro proyecto o actualizaron a una
versión del <i lang="en">software</i> sin los errores, no cerraron los tiques (también
llamado *issues*). Es absurdo ignorar los problemas por pequeños que
sean, y más cuando te afectan, pues forman parte de las dependencias de
tu proyecto. Al idiota que os he mencionado le arreglé el error a dicho
proyecto (aunque el individuo cerró la incidencia antes de que lo
hiciera).

Al individuo, también le avisé en otro tique de que la licencia Creative
Commons no sirve para el código y le insté a que usara una que tuviera
validez para el <i lang="en">software</i>. La licencia Creative Commons sirve para el
contenido de un libro, sitio web, etc., no para el código fuente.

Lo más extraño fue que aún habiéndole dicho esto mostrándole las
explicaciones de los propios creadores de la licencia, volvió a ignorar
el tique irresponsablemente. En resumen, pasó de los dos errores que le
mostré y me contestó con desprecio. Exactamente lo contrario a lo que
estoy acostumbrado y lo que me ha pasado con otros equipos de
desarrollo.

En el equipo de desarrollo de *Unknown Horizons* del que formo parte
siempre nos agradamos de que alguien contribuya a nuestro proyecto y le
agradecemos su trabajo, aceptamos con gusto correcciones de nuestros
errores, les aclaramos cosas que no entienden y ayudamos a los
colaboradores a terminar las mejoras que han empezado. Siempre estamos
agradecidos y les respondemos amablemente porque son nuestros amigos, ya
que nos ayudan recibiendo poco o nada a cambio.

Si por el contrario, una persona que colabora con un repositorio se
encuentra con un equipo de desarrolladores que son incapaces de admitir
sus errores y que responden altivamente, siente que no tiene sentido
perder el tiempo en volver a ayudar a personas con el síndrome de
Estocolmo que van a despreciar e insultar su buena voluntad.
