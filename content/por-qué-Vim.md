Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2021-04-17 16:00
Lang: es
Slug: ¿por-qué-aprender-a-usar-vim
Tags: Vim
Title: ¿Por qué aprender a usar Vi(m)?
Image: <img src="/wp-content/uploads/2021/04/vim_header.gif" alt="Logotipo de Vim">

Aunque se trata de un editor sencillo, Vim puede ser algo difícil de
aprender. ¿Por qué perder el tiempo aprendiendo a usar un editor tan
extraño y viejo?

Hay dos versiones: Vi y Vim (<i>**Vi** i**m**proved</i>). Los dos
funcionan igual, pero Vim tiene más funcionalidades, aunque también
ocupa más espacio. Vi está instalado en la mayoría de ordenadores con
GNU/Linux. Si administras sistemas informáticos, es raro encontrar tu
editor favorito en un servidor aleatorio, pero Vi está casi siempre ahí.
Sabiendo cómo funciona puedes salir del paso y editar rápido archivos en
ordenadores sin interfaz gráfica.

Vim es un editor que te ahorra tiempo cuando lo sabes usar, pues no
tienes que despegar las manos del teclado para agarrar el ratón. Claro,
aprender a usar un editor modal como Vim —tiene varios modos: normal,
inserción...— no es sencillo, pero el esfuerzo a la larga vale la pena.
Yo diría que es de los editores más eficientes que hay disponibles, y
tienes la garantía de que no desaparecerá ni se le añadirán cambios muy
bruscos como pasa con otros editores modernos.

Para empezar a aprender puedes ejecutar `vimtutor` en la terminal si
tienes Vim instalado. Si no, se puede instalar en distribuciones de
GNU/Linux basadas en Debian con la siguiente instrucción:

    ::bash
    sudo apt install vim
