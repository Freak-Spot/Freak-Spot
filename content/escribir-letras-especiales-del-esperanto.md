Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2022-04-03 20:00
Modified: 2023-02-10 17:00
Lang: es
Slug: escribir-letras-especiales-del-esperanto-con-teclado-español-en-gnulinux
Tags: Debian, esperanto, gestor de ventanas X, GNU/Linux, teclado
Title: Escribir letras especiales del esperanto con teclado español en GNU/Linux
Image: <img src="/wp-content/uploads/2022/04/símbolo-deaniversario-del-esperanto-con-letras-con-acentos-breve-y-circunflejo.png" alt="" width="955" height="578" srcset="/wp-content/uploads/2022/04/símbolo-deaniversario-del-esperanto-con-letras-con-acentos-breve-y-circunflejo.png 955w, /wp-content/uploads/2022/04/símbolo-deaniversario-del-esperanto-con-letras-con-acentos-breve-y-circunflejo-477x289.png 477w" sizes="(max-width: 955px) 100vw, 955px">

En la distribución del teclado española por defecto no se puede escribir
la ŭ del esperanto, pero sí las ĉ, ĝ, ĥ, ĵ y ŝ, pulsando <kbd>^</kbd> y
la letra correspondiente (c, g, h, j o s).

Puedes seleccionar una distribución de teclado para el esperanto y
alternar entre esta y la distribución española para no tener que pulsar
varias teclas a la vez cuando escribes en esperanto. Sin embargo, si
solo escribes esporádicamente en esperanto, quizás te resulte más
sencillo escribir esos caracteres especiales con combinaciones de
teclas. Vale, ¿pero cómo escribes la ŭ?

En el algunos entornos de escritorio existe la opción de habilitar las
teclas especiales del esperanto. <!-- more -->En [Cinnamon](https://es.wikipedia.org/wiki/Cinnamon) hay que ir a la
configuración del teclado, ir a **Distribuciones**, pulsar en **Opciones...**, desplegar **Letras
en esperanto con superíndices** y seleccionar **A la tecla correspondiente
en un teclado QWERTY**[^1], como se muestra en la siguiente imagen:

<a href="/wp-content/uploads/2023/02/Configurar-letras-en-esperanto-en-distribución-de-teclado-en-español-GNU-Linux.png">
<img src="/wp-content/uploads/2023/02/Configurar-letras-en-esperanto-en-distribución-de-teclado-en-español-GNU-Linux.png" alt="" width="942" height="665" srcset="/wp-content/uploads/2023/02/Configurar-letras-en-esperanto-en-distribución-de-teclado-en-español-GNU-Linux.png 942w, /wp-content/uploads/2023/02/Configurar-letras-en-esperanto-en-distribución-de-teclado-en-español-GNU-Linux-471x332.png 471w" sizes="(max-width: 942px) 100vw, 942px">
</a>

Si no encuentras la opción y estás usando el gestor de ventanas X en
GNU/Linux, como es el caso de la mayoría de distribuciones de GNU/Linux,
puedes ejecutar la siguiente instrucción:

    ::bash
    setxkbmap -option esperanto:qwerty

Una vez hecho esto, para escribir la ŭ bastará con pulsar
<kbd>AltGr</kbd> y <kbd>u</kbd>. Para hacer esos cambios permanentes,
puedes añadir el comando anterior al archivo `.xprofile`[^2].

Otra opción es [usar la tecla Componer](/combinaciones-de-teclado-y-tecla-Componer-GNU/Linux/).

[^1]: Si usas la distribución de teclado Dvorak o Colekam, no deberás
    marcar opción de QWERTY, sino la opción con el nombre de la
    distribución de teclado que usas.
[^2]: Si el archivo no existe, deberás crearlo. Básicamente, los
    archivos `~/.xprofile` (para tu usuario) y `/etc/xprofile` (para
    todo el sistema) permiten ejecutar comandos al comienzo de la sesión
    de usuario del [servidor X](https://es.wikipedia.org/wiki/Sistema_de_ventanas_X).
