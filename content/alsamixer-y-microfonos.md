Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2016-07-15 00:32
Lang: es
Modified: 2017-02-25 21:36
Slug: alsamixer-y-microfonos
Status: published
Tags: alsamixer, configuración micrófono, GNU/Linux, micrófono, sonido, Trisquel, Trisquel Belenos, Trisquel 7
Title: Activando el micrófono en Trisquel

El otro día instalé Trisquel en un ordenador. Todo parecía funcionar
perfectamente excepto el micrófono. El ordenador es de sobremesa; yo
pretendía conectar el micrófono por la parte trasera de la torre.

El problema ocurría porque el volumen de *Rear Mic* (micrófono trasero)
en AlsaMixer estaba sin volumen. Para solucionarlo tuve que abrir
AlsaMixer y subir su volumen.

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-053931.png"><img class="aligncenter size-full wp-image-133" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-053931.png" alt="alsamixer sin volumen (microfono trasero)" width="577" height="413" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-053931.png 577w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-053931-300x215.png 300w" sizes="(max-width: 577px) 100vw, 577px" /></a>

Además, tuve que subir el volumen de entrada desde el menú de la configuración
del sonido (**Configuración del sistema&gt;sonido**).

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-054439.png"><img class="aligncenter size-full wp-image-136" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-054439.png" alt="Configuración de sonido (Trisquel)" width="861" height="543" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-054439.png 861w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-054439-300x189.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-054439-768x484.png 768w" sizes="(max-width: 861px) 100vw, 861px"></a>
