Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2022-08-18 01:20
Modified: 2022-10-29 14:00
Lang: eo
Slug: TikTok-con-privacidad-con-ProxiTok
Save_as: TikTok-privatece-per-ProxyTok/index.html
URL: TikTok-privatece-per-ProxyTok/
Tags: TikTok
Title: TikTok privatece per ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

TikTok estas centra socia reto, kiu bezonas la uzon de proprieta
programaro. Estas preskaŭ maleble uzi TikTok sen forlasi vian privatecon
aŭ liberecon... krom se ni uzas alian fasadon, kiel
[ProxyTok](https://proxitok.pabloferreiro.es/)-on, kiun mi priskribas en ĉi
tiu artikolo.

La fasado de ProxiTok estas simpla. Oni povas iri al la populara enhavo
(**<i lang="en">Trending</i>**), trovi uzantojn (**<i
lang="en">Discover</i>**) kaj serĉi laŭ uzantnomo, etikedo, URL de
TikTok, muzika identigilo kaj videa identigilo.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Serĉi laŭ uzantnomo</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Profilo de <a href="https://proxitok.herokuapp.com/@recetasveganas">@recetasveganas</a>. Ankaŭ sekveblas per RSS.</figcaption>
</figure>

La projekto komenciĝis la 1-an de januaro de 2022, do estas tre juna kaj
oni esperas, ke estu aldonitaj pli da funkcioj. Por aŭtomate alidirekti
al ProxiTok oni povas instali la aldonaĵon [LibRedirect](https://libredirect.github.io/),
kiu ankaŭ evitas aliajn retejojn malbonajn por la privateco.

Ekzistas [kelkaj publikaj
nodoj](https://github.com/pablouser1/ProxiTok/wiki/Public-instances),
eblante instali unu en via propra servilo.
