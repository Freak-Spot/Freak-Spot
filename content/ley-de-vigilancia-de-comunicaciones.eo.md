Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-03-27 16:00
Lang: eo
Slug: la-Union-Europea-quiere-analizar-las-comunicaciones-antes-de-que-sean-cifradas
Save_as: la-Eŭropa-Unio-volas-analizi-la-komunikadojn-antaŭ-ili-estu-ĉifritaj/index.html
URL: la-Eŭropa-Unio-volas-analizi-la-komunikadojn-antaŭ-ili-estu-ĉifritaj/
Tags: Eŭropa Unio, privateco, gvatado
Title: La Eŭropa Unio volas analizi la komunikadojn antaŭ ili estu ĉifritaj

La Eŭropa Komisiono povus analizigi la mesaĝojn de mesaĝservoj antaŭ ili
estu ĉifritaj kaj senditaj. Ĉi tiu leĝo, kies detaloj oni ankoraŭ ne
scias kaj kiu estos prezentita la 30an marton, intencas kontraŭi la
mistraktadon de infanoj. Praktike tio egalus al malfermi leterojn antaŭ
sendi ilin kaj ke ĉiuj estu konsideritaj suspektaj de mistraktado de
infanoj.

Estas kiuj pensas, ke la leĝo povas instigi al ega gvatado kaj al la
fino de la rajto al la sekreteco de komunikado.
