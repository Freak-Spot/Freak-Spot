PY?=python3
PELICAN?=pelican
PELICANOPTS=

BASEDIR=$(CURDIR)
INPUTDIR=$(BASEDIR)/content
OUTPUTDIR=/var/www/html
CONFFILE=$(BASEDIR)/pelicanconf.py
PUBLISHCONF=$(BASEDIR)/publishconf.py
MINIFIER=$(BASEDIR)/plugins/minify_css_js.py

FTP_HOST=********
FTP_USER=********
FTP_TARGET_DIR=********

SSH_HOST=localhost
SSH_PORT=22
SSH_USER=root
SSH_TARGET_DIR=/var/www

DEBUG ?= 0
ifeq ($(DEBUG), 1)
	PELICANOPTS += -D
endif

RELATIVE ?= 0
ifeq ($(RELATIVE), 1)
	PELICANOPTS += --relative-urls
endif

help:
	@echo 'Makefile for a pelican Web site                                                '
	@echo '                                                                               '
	@echo 'Usage:                                                                         '
	@echo '   make html                           (re)generate the web site               '
	@echo '   make clean                          remove the generated files              '
	@echo '   make regenerate                     regenerate files upon modification      '
	@echo '   make publish                        generate using production settings      '
	@echo '   make serve [PORT=8000]              serve site at http://localhost:8000     '
	@echo '   make serve-global [SERVER=0.0.0.0]  serve (as root) to $(SERVER):80         '
	@echo '   make devserver [PORT=8000]          serve and regenerate together           '
	@echo '   make ssh_upload                     upload the web site via SSH             '
	@echo '   make rsync_upload                   upload the web site via rsync+ssh       '
	@echo '   make ftp_upload                     upload the web site via FTP             '
	@echo '   make validate                       validate the web site via html5validator'
	@echo '                                                                               '
	@echo 'Set the DEBUG variable to 1 to enable debugging, e.g. make DEBUG=1 html        '
	@echo 'Set the RELATIVE variable to 1 to enable relative urls                         '
	@echo '                                                                               '

html:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

clean:
	[ ! -d $(OUTPUTDIR) ] || rm -rf $(OUTPUTDIR)

regenerate:
	$(PELICAN) -r $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)

serve:
ifdef PORT
	@echo 'Serving at http://localhost:$(PORT), server'
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	@echo 'Serving at http://localhost:8000, server'
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

serve-global:
ifdef SERVER
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b $(SERVER)
else
	$(PELICAN) -l $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT) -b 0.0.0.0
endif

devserver:
ifdef PORT
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS) -p $(PORT)
else
	$(PELICAN) -lr $(INPUTDIR) -o $(OUTPUTDIR) -s $(CONFFILE) $(PELICANOPTS)
endif

absolute_path_generate:
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)

publish: absolute_path_generate
	python3 $(MINIFIER)

ssh_upload: publish
	scp -P $(SSH_PORT) -r $(OUTPUTDIR)/* $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR)

rsync_upload: publish
	rsync -e "ssh -p $(SSH_PORT)" -P -rvzc --delete $(OUTPUTDIR)/ $(SSH_USER)@$(SSH_HOST):$(SSH_TARGET_DIR) --cvs-exclude

ftp_upload: publish
	lftp ftp://$(FTP_USER)@$(FTP_HOST) -e "mirror -R $(OUTPUTDIR) $(FTP_TARGET_DIR) ; quit"

validate: absolute_path_generate
	$(PELICAN) $(INPUTDIR) -o $(OUTPUTDIR) -s $(PUBLISHCONF) $(PELICANOPTS)
	html5validator --root $(OUTPUTDIR) || html5validator -l --root $(OUTPUTDIR)

broken_links:
	wget --spider -l 5 -nd -nv -o wget.log -r -p http://localhost/


.PHONY: absolute_path_generate broken_links html help clean regenerate serve serve-global devserver publish ssh_upload rsync_upload ftp_upload
