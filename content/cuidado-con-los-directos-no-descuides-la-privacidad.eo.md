Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-10-14
Lang: eo
Slug: ¡cuidado-con-los-directos-no-descuides-tu-privacidad
Save_as: vivaj-komunikoj-kun-privateco/index.html
URL: vivaj-komunikoj-kun-privateco/
Tags: uzantkonto, vivaj komunikado, retumilo, privateco, sekureco, reta laboro
Title: Estu atenta pri la vivaj komunikoj! Ne forgesu vian privatecon

Pro la pliiĝo de la vivaj komunikoj verŝajne via labortablo nun ne estas
tiel privata kiel antaŭe —kaj la korpa kaj la cifera—. Risko subtaskita
de multaj homoj.

Se ni pensas kiel superulo, al ri eble ne amuzas vidi, ke la firma
komputilo estas uzata por videludoj, pornografio aŭ ajn alio, kiun ri
traktas kiel nedeca; aŭ vidi en la adresbreto retejojn, kiuj havas
politikajn ideojn kontraŭajn al ria ekzemple.

Ideale oni uzas la komputilon de la firmo nur por aferoj de la firmo. Se
ĉi tiu komputilo estas la sama ol la persona aŭ estas kunuzita, oni
devas uzi specifan uzantan konton por la firmo, por ke al vidigi
labortablon ne estu videblaj niaj dosieroj, la programoj uzitaj en nia
libera tempo, nia reta historio, ktp.

Ĉar kvankam ni nur pensis montri nian diapozitivojn, ni senscie montras
informon, kiam io fiaskas kaj ni eliras el la diapozitivoj. Homo
antaŭvida estas pli valora: kiam io fiaskas, mi ne kompromitas mian
privatecon, kiam mi uzas uzantan konton specifan por la laboro.

Ne bezonas diri, ke la reala labortablo kaj ĝia ĉirkaŭaĵo, dum ni
troviĝas en videkunveno estas alia risko. Krome la homoj kun kiuj ni
loĝas povas kompromiti nian privatecon. Oni devas do trovi horarojn kun
malpli homoj hejme, elekti zorge la ejon atentante tion, kion la filmilo
povas vidi, kaj fari la ciferecan komunikon en la plej malbrua ejo.

Resume la retaj komunikoj per reta konferenco supozas multajn riskojn
por la privateco, pri kiu ni devas atenti. Tre necesas, koni la riskojn,
kiujn ĝi havas, por eviti ĝin, eĉ kiam okazas neatenditaj problemoj.
