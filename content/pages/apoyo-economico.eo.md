Author: Jorge Maldonado Ventura
Date: 2018-07-03 02:06
Lang: eo
Slug: apoyo-economico
Status: published
Title: Mona subteno

Ekde ĝia naskiĝo, ĉi tiu retpaĝaro uzas nur liberan programaron kaj
ne havas reklamojn. Tamen bonteni tian retpaĝaron bezonas kaj
tempon kaj monon. Donacoj estas
maniero pagi la
[kostojn de la paĝaro](/eo/pages/gastos-economicos.html)
kaj instigi ĝian ellaboradon kaj plibonigon.

## Monero

4AVrC3LT4Ad7DCK5L8MDfWfMeiTqDYJnWDAKGE1jJNbn7pBmFGay81C5sKcci9pwkXRRBexM5JKyVUTEPWvvZTAJ3WeAAex

<img alt="" src="/wp-content/uploads/2023/12/monero-qr.gif">

## Banktransigo

<dl>
  <dt>Banko</dt><dd>Cajamar Caja Rural</dd>
  <dt>Konto-propietulo</dt> <dd>Jorge Maldonado Ventura</dd>
  <dt>Konto-numero</dt> <dd>3058 0990 2727 5316 9801</dd>
  <dt><abbr title="International Bank Account Number">IBAN</abbr></dt> <dd>ES80 3058 0990 2727 5316 9801</dd>
  <dt><abbr title="Society for Worldwide Interbank Financial Telecommunication">SWIFT</abbr></dt> <dd>CCRIES2AXXX</dd>
</dl>

## Liberapay

<a href="https://liberapay.com/Freak-Spot/donate" role="button"><img alt="Donaci" src="/wp-content/uploads/2018/07/donaci.svg"></a>
