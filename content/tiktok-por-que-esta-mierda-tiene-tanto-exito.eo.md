Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-03-30 15:00
Lang: eo
Slug: tiktok-¿por-qué-esa-mierda-tiene-tanto-éxito
Save_as: tiktok-kial-tiu-merdo-estas-tiel-sukcesa/index.html
URL: tiktok-kial-tiu-merdo-estas-tiel-sukcesa/
Tags: Instagram, YouTube, TikTok, manio, sociaj retejoj
Title: TikTok: kial tiu merdo estas tiel sukcesa?
Image: <img src="/wp-content/uploads/2023/03/pastillas-adicción.jpeg" alt="" width="1880" height="1253" srcset="/wp-content/uploads/2023/03/pastillas-adicción.jpeg 1880w, /wp-content/uploads/2023/03/pastillas-adicción.-940x626.jpg 940w, /wp-content/uploads/2023/03/pastillas-adicción.-470x313.jpg 470w" sizes="(max-width: 1880px) 100vw, 1880px">

Imagu drogon, kiu pliigas vian streson, anksion kaj kiu mense lacegigas
vin; drogon, kiu ne nur dependigas vin, sed kiu ĉiutage lasas vin sen
energion. Kiu uzus tiun drogon? Preskaŭ ĉiuj. Tiu drogo nomiĝas TikTok.

Ĉu vi sentas vin sola? Ne zorgu, en la komunumo de tendencoj kaj defioj
vi havos «amikojn», kiuj ege volos vidi kiel vi agas kiel idioto aŭ
vi ridinde kaj hontinde dancas.

<a href="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp">
<img src="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp" alt="" width="1000" height="530" srcset="/wp-content/uploads/2023/03/tiktok-adictivo-para-niños.webp 1000w, /wp-content/uploads/2023/03/tiktok-adictivo-para-niños-500x265.webp 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

Adoleskantoj, infanoj kaj plenkreskuloj stulte agas pro socia aprobo.
Sed tio ne damaĝas iun, ĉu? Estas defioj kiel [metu lesivojn en la
okulojn por vidi, se ilia koloro
ŝanĝiĝas](https://www.iheartradio.ca/100-3-the-bear/trending/new-internet-challenge-involving-teens-and-bleach-great-1.9097980);
[sin ŝminki per oranĝa ŝimo](https://www.airedesantafe.com.ar/viral/nuevo-peligroso-desafio-tiktok-maquillarse-moho-naranja-n438593); faligi iun per la piedoj dum ri
saltas, kiel en la jena video:


<video controls poster="/wp-content/uploads/2023/03/desafío-rompecráneos-de-TikTok.jpg">
  <source src="/video/desafío-rompecráneos-de-TikTok.webm" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

[Homoj mortis farinte stultajn defiojn](https://www.muyinteresante.es/salud/21660.html).
Kiu kulpas? TikTok ne devigas al vi uzi la platformon, sed en mondo ege
stresa, kiu kaŭzas angoron kaj estas tro-dozo de dopamino, kun homoj
kiuj urĝe bezonas socian aprobon, ekzistas homoj, kiuj rifuĝas en toksa
medio. Estas homoj, kiuj sentas la neceson montri al aliuloj, ke ili
feliĉas, ke ili havas mirindan vivon... La realo estas tre malsama.

Pro familiaj konfliktoj, problemoj en la lernejo aŭ en la laborejo,
homoj rifuĝas en TikTok, kiu unuavide ne ŝajnas drogon, por forgesi
siajn problemojn. Tamen TikTok agas en la mezolimba vojo, aktivante la
sistemon de cerba rekompenco. Se vidi stultajn videojn igas, ke vi
forgesu viajn problemojn kaj ankaŭ donas al vi plezuron, vi vidos
stultajn videojn, kaj via cerbo volos fari tion denove, do vi vidas
alian videon kaj via cerbo produktas dopaminon. Iom post iom la
dopaminerga sistemo adaptiĝas: unue sufiĉos al vi vidi videon, sed poste
vi bezonos vidi du, tri, kvar... Por atingi la saman plezuran nivelon
kiel en la komenco vi devos vidi pli kaj pli da videoj.

Aliflanke la agoj, kiuj sane pliigas la dopaminon, kiel fari ekzercojn
kaj sane manĝi, estas tedaj. Oni ne povas kompari tiujn agojn kun tio,
kion la grandaj firmaoj ofertas, kio maksimume pliigas vian dopaminon,
sen nenia peno via, ĝis vi estas dika, elĉerpita...

Per sociaj retejoj kiel TikTok ne necesas, ke vi elektu tion, kion vi
volas vidi, ĉar la algoritmo elektas por vi la specon de merdo, kiun vi
ŝatas, donante ĝin al vi, sen ke vi devos movi unu fingron. Ĝi faras
tion senkoste, ĉar **vi estas la produkto**: ĝia negoco konsistas en
teni vin dependa antaŭ ekrano, por ke vi vidu la kiel eble plej grandan
nombron de reklamoj. Kial ili faras tion? Ŝanĝante iom post iom viajn
agojn kaj vian pensmanieron, alivorte igante vin dependa, sen ke vi
konsciu. Vi ne povas vidi kiel funkcias la algoritmo, kiu estis
hiperoptimumigita por dependigi vin, sed vi povas vidi la sekvan videon
kaj fari aliajn aferojn, kiuj igu, ke aliaj homoj restu pli da tempo en
la platformo, kiel komenti kaj ŝati. **Vi estas uzato, ne uzanto**.

Pro tio vi faris preskaŭ nenion dum tutaj tagoj kaj vi ĉiam estas tiel
laca, malgaja, enuigita, stresita, korpremita, senmotivigita, iritita,
malfortigita... **mania**.

<a href="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png">
<img src="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png" alt="" width="1200" height="579" srcset="/wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura.png 1200w, /wp-content/uploads/2023/03/sociedad-enferma-adicta-a-redes-sociales-y-a-comida-basura-600x289.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

Ĉu TikTok malsanigas homojn? TikTok estas reflekto de nia socio, socio
malsana, trompita per malvera feliĉo de la sociaj retejoj, laca de la
mondo. La sociaj retejoj scias multajn aferojn pri vi, sed vi nenion
scias pri ili, ĉar ili estas [malliberaj programoj](https://eo.wikipedia.org/wiki/Mallibera_programaro).
Ekzistas [liberaj](https://eo.wikipedia.org/wiki/Libera_programaro) sociaj retejoj,
sed [ili ankaŭ povas esti tre toksikaj](/eo/la-fediverso-povas-esti-tre-toksika/).

<a href="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp">
<img src="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp" alt="" width="1200" height="1200" srcset="/wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles.webp 1200w, /wp-content/uploads/2023/03/Prohibido-el-uso-de-telefonos-moviles-600x600.webp 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

Por liberigi vin de la dependeco vi povas klopodi la dopaminan
sentoksigon, tio estas, malpliigi la uzadon de sociaj retejoj kaj fari
aferojn, kiuj postulas penon kaj ne donas al vi tujan plezuron. Por ke
tio estu pli facila, oni povas fari tion iom post iom.

Kompreneble la sociaj retejoj ankaŭ povas esti utilaj, sed oni devas
modere uzi ilin. Plej bone estus, ke ili estu liberaj kaj
privatecrespektemaj. Ili ne devas instigi dependecon, tion kion TikTok,
Instagram, YouTube kaj aliaj sociaj retejoj faras.
