Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2017-11-14 22:38
Lang: es
Modified: 2017-11-15 01:11
Slug: desahabilitar-javascript-comodamente-firefox-y-derivados
Tags: Abrowser, Firefox, Iceweasel, JavaScript, navegador, privacidad, programación
Title: Deshabilitar JavaScript cómodamente en Firefox y derivados

Debido a [la «trampa» que supone la presencia de JavaScript en la
web](https://www.gnu.org/philosophy/javascript-trap.es.html),
podemos estar ejecutando <i lang="en">software</i> privativo sin darnos cuenta. Este
<i lang="en">software</i> suele comprometer nuestra privacidad o realizar tareas que no
deseamos. Puede ser que tampoco queramos ejecutar JavaScript porque
estemos probando cómo funciona una página sin JavaScript durante la
creación de un sitio web.

<!-- more -->

Para desactivar JavaScript en Firefox y en navegadores derivados de
Firefox, podemos introducir `about:config` en la barra de URL y cambiar
el valor de la preferencia `javascript:enabled` desde `true` a `false`.
Pero hacer esto cada vez que queramos activar o desactivar JavaScript
es bastante incómodo.

<a href="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png">
<img src="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png" alt="Cambiando el valor de la preferencia javascript.enabled en Tor Browser" width="1000" height="279" srcset="/wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox.png 1000w, /wp-content/uploads/2017/11/Desactivar-JavaScript-Firefox-500x139.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

Para activar y desactivar JavaScript más cómodamente podemos usar la
extensión JavaScript Toggle On and Off, que dispone de dos versiones:

- [JavaScript Toggle On and Off (WebExtension)](https://addons.mozilla.org/es/firefox/addon/javascript-toggler/). Para versiones de Firefox
  superiores o iguales a la 57.
- [JavaScript Toggle On and Off](https://addons.mozilla.org/es/firefox/addon/javascript-toggle-on-and-off/). Para versiones de Firefox más antiguas
  que la 57.

Una vez instalada la extensión aparecerá un botón que podemos pulsar
para activar o desactivar JavaScript. Hay muchas otras extensiones que
nos permiten controlar el código JavaScript que queremos ejecutar y el
que no, pero esta extensión es ideal si simplemente deseamos activar o
desactivar JavaScript.

Si queremos que cuando tengamos JavaScript desactivado con la extensión
se ejecute JavaScript en ciertos sitios web, podemos añadir las <abbr
title="Uniform Resource Locator">URL</abbr>s de esos sitios web a una
lista blanca desde el menú de configuración de la extensión.

<a href="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png">
<img src="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png" alt="Añadiendo sitios web a la lista blanca de JavaScript Toggle On and Off" width="1276" height="706" srcset="/wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off.png 1276w, /wp-content/uploads/2017/11/lista-blanca-JavaScript-Toggle-On-and-Off-638x353.png 638w" sizes="(max-width: 1276px) 100vw, 1276px">
</a>
