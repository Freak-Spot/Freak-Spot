Author: Jorge Maldonado Ventura
Category: Bash
Date: 2018-09-03 07:40
Lang: eo
Slug: xdg-open
Status: published
Tags: malfermi ajn dosieron el la komandlinio, xdg-open
Title: <code>xdg-open</code>

Tre utila komando estas `xdg-open`. Per ĝi ni povas plenumi iun ajn
programon aŭ URLon el la komandlinio. Se mi plenumus `xdg-open
https://freakspot.net`, malfermus sin la ĉefa paĝo el ĉi tiu retejo per
Abrowser (mia defaŭlta retilo) kaj mi povus plenumi alian komandon. Unu
malavantaĝo estas, ke ni nur povas aldoni unu parametron, do por
malfermi du retpaĝojn, ni devus plenumi `xdg-open` dufoje.
