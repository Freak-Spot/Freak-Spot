Author: Jorge Maldonado Ventura
Category: Trucos
Date: 2023-05-20 19:20
Lang: es
Slug: cómo-verificar-tu-cuenta-de-Mastodon
Tags: Mastodon, página web, verificación
Title: Cómo verificar tu cuenta de Mastodon
Image: <img src="/wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado.png" alt="" width="1167" height="944" srcset="/wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado.png 1167w, /wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado-583x472.png 583w" sizes="(max-width: 1167px) 100vw, 1167px">

Si quieres demostrar que una cuenta de Mastodon es tuya y no de otra
persona que se hace pasar por ti, puedes verificar tu sitio web. Para
ello tenemos que seguir los siguientes pasos: <!-- more -->

1. Darle a **Editar perfil** en la página de tu perfil o ir a
   **Preferencias** y después a **Perfil ⯈ Apariencia**.
<a href="/wp-content/uploads/2023/05/ajustes-para-verificar-sitio-web-en-Mastodon.png">
<img src="/wp-content/uploads/2023/05/ajustes-para-verificar-sitio-web-en-Mastodon.png" alt="" width="1275" height="736" srcset="/wp-content/uploads/2023/05/ajustes-para-verificar-sitio-web-en-Mastodon.png 1275w, /wp-content/uploads/2023/05/ajustes-para-verificar-sitio-web-en-Mastodon-637x368.png 637w" sizes="(max-width: 1275px) 100vw, 1275px">
</a>
2. En **Metadatos de perfil** añadir un sitio web. Puedes poner «Sitio
   web» en el campo de etiqueta y la URL de tu sitio web en el campo de
   contenido.
3. Copiar el código que aparece en **Verificación** (le puedes dar al
   botón **Copiar**) y pegarlo en el archivo `index.html`, `index.php` o
   equivalente de tu sitio web[^1].
4. Finalmente, pulsa **GUARDAR CAMBIOS**.

Cuando recargues tu perfil de Mastodon, tu sitio web aparecerá
verificado.

<a href="/wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado.png">
<img src="/wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado.png" alt="" width="1167" height="944" srcset="/wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado.png 1167w, /wp-content/uploads/2023/05/perfil-de-Mastodon-con-sitio-web-verificado-583x472.png 583w" sizes="(max-width: 1167px) 100vw, 1167px">
</a>

[^1]: Cuando se complete la verificación, te recomiendo borrar el código de verificación.
