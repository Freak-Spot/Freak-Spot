Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-04-25 00:37
Image: <img src="/wp-content/uploads/2018/04/brutalidad-policial-apoyada-por-Facebook.png" alt="Brutalidad policial apoyada por Facebook" width="864" height="742" srcset="/wp-content/uploads/2018/04/brutalidad-policial-apoyada-por-Facebook.png 864w, /wp-content/uploads/2018/04/brutalidad-policial-apoyada-por-Facebook-432x371.png 432w" sizes="(max-width: 864px) 100vw, 864px">
Lang: es
Slug: facebook-legitima-la-violencia-estatal
Tags: capitalismo, estado, Facebook, página web, represión, redes sociales, violencia
Title: Facebook legitima la violencia estatal

Facebook se ha sumado a
[Twitter](/twitter-legitima-la-violencia-del-estado/)
[legitimando la violencia estatal](https://about.fb.com/news/2018/04/keeping-terrorists-off-facebook/).
Pero ha ido un paso más allá: define como terroristas a quienes tratan
de acabar con los estados o la propiedad, como es el caso de personas
que comparten las ideologías anarquista y comunista.

Así definen el terrorismo:

> Definimos el terrorismo como: «Cualquier organización no gubernamental
> que se involucra en actos premeditados de violencia contra personas
> o propiedad para intimidar a una población civil, gobierno u
> organización internacional para lograr un objetivo político, religioso
> o ideológico».

Y así legitiman la violencia del estado:

> Nuestra política antiterrorista no se aplica a gobiernos. Esto refleja
un consenso académico general y legal de que los estados nacionales
pueden usar legítimamente la violencia bajo ciertas circunstancias. No
obstante, cierto contenido sobre violencia patrocinada por el estado
sería eliminado por nuestras otras políticas, como nuestra política de
violencia gráfica.
