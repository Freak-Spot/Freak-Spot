Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2023-02-13 20:00
Lang: es
Slug: mejores-buscadores-para-la-internet-profunda-de-tor-deep-web
Tags: buscador, Internet profunda, .onion, servicio oculto, Tor Browser
Title: Mejores buscadores para la Internet profunda de Tor (<i lang="en">deep web</i>)
Image: <img src="/wp-content/uploads/2023/02/cebollas-moradas.jpeg" alt="" width="1920" height="1280" srcset="/wp-content/uploads/2023/02/cebollas-moradas.jpeg 1920w, /wp-content/uploads/2023/02/cebollas-moradas.-960x640.jpg 960w, /wp-content/uploads/2023/02/cebollas-moradas.-480x320.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Encontrar servicios ocultos para Tor puede ser algo complicado sin la
ayuda de un buscador. Por suerte existen varios. Los enlaces a estos
buscadores funcionan solo para navegadores que permiten acceder a
servicios ocultos, como [Tor Browser](https://www.torproject.org/es/download/)
y Brave.

## 1. [Ahmia](http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/)

Este buscador es [libre](https://github.com/ahmia) y tiene más de
53&nbsp;000 servicios ocultos <!-- more --> indexados[^1]. A diferencia
de otros buscadores, no indexa pornografía infantil[^2].

<a href="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png">
<img src="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png" alt="" width="1000" height="950" srcset="/wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia.png 1000w, /wp-content/uploads/2023/02/resultados-de-búsqueda-en-el-buscador-Ahmia-500x475.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>

## 2. [Torch](http://torchdeedp3i2jigzjdmfpn5ttjhthh5wbmda2rr3jvqjg5p77c54dqd.onion/)

Este buscador afirma ser el buscador más antiguo y que más tiempo lleva
funcionando en la Internet profunda. Se financia con anuncios y no tiene
reparo en indexar pornografía infantil y otros tipos de contenidos
ilegales.

## 3. [Iceberg](http://iceberget6r64etudtzkyh5nanpdsqnkgav5fh72xtvry3jyu5u2r5qd.onion/)

Indexa contenido de todo tipo. Se financia con anuncios. Te permite
indexar páginas web y muestra una puntuación de la página basada en el
número de menciones y en la seguridad para el usuario.

<figure>
<a href="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png">
<img src="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png" alt="" width="1920" height="1040" srcset="/wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg.png 1920w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-960x520.png 960w, /wp-content/uploads/2023/02/puntuación-de-página-en-Iceberg-480x260.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption>Puntuación <a href="http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/">del servicio oculto de este sitio web</a> en Iceberg.</figcaption>
</figure>

## 4. [TorLanD](http://torlgu6zhhtwe73fdu76uiswgnkfvukqfujofxjfo7vzoht2rndyhxyd.onion/)

Indexa contenido de todo tipo. Se financia con anuncios. Te permite
indexar páginas web.

<figure>
<a href="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png">
<img src="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png" alt="" width="1000" height="579" srcset="/wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD.png 1000w, /wp-content/uploads/2023/02/añadiendo-servicio-oculto-en-TorLanD-500x289.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Indexando el servicio oculto de
    esta web en TorLanD</figcaption>
</figure>

## 5. [Haystak](http://haystak5njsmn2hqkewecpaxetahtwhsbsa64jom2k22z5afxhnpxfid.onion/)

No indexan contenido que consideran inmoral o ilegal, como abuso
infantil y tráfico de humanos. Afirman haber indexado más de 1,5 mil
millones de páginas de 260&nbsp;000 servicios ocultos. Tienen una
dirección para recibir donaciones y la opción de pagar por
funcionalidades adicionales.

<figure>
<a href="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png">
<img src="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png" alt="" width="1507" height="606" srcset="/wp-content/uploads/2023/02/servicios-de-pago-de-haystak.png 1507w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-753x303.png 753w, /wp-content/uploads/2023/02/servicios-de-pago-de-haystak-376x151.png 376w" sizes="(max-width: 1507px) 100vw, 1507px">
</a>
    <figcaption class="wp-caption-text">Funcionalidades de pago de
    Haystak</figcaption>
</figure>

## 6. [OnionLand Search](http://3bbad7fauom4d6sgppalyqddsqbf5u5p56b5k5uk2zxsy3d6ey2jobad.onion)

Tienen una página de términos y condiciones en la que condenan los usos
ilegales. Se financian ofreciendo alojamiento de servicios ocultos y con
anuncios.

## 7. [Ourrealm](http://orealmvxooetglfeguv2vp65a3rig2baq2ljc7jxxs4hsqsrcemkxcad.onion/)

Se financia con anuncios. Su lema es «busca más allá de la censura»[^3].

## 8. [Ondex](http://ondexcylxxxq6vcc62l3r2m6rypohvvymsvqeadhln5mjo73pf6ksjad.onion/)

Se financia con anuncios. Indexa todo tipo de contenido.

## 9. [Deep Search](http://search7tdrcvri22rieiwgi5g46qnwsesvnubqav2xakhezv4hjzkkad.onion/)

Muestra servicios ocultos en tendencia en la página principal. Indexa
todo tipo de contenido. Se financia con anuncios.

## 10. [Find Tor](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/)

Indexa todo tipo de contenido. Se financia con anuncios y [afirma estar a
la venta](http://findtorroveq5wdnipkaojfpqulxnkhblymc7aramjzajcvpptd4rjqd.onion/buy).

[^1]: La lista se puede consultar en <https://ahmia.fi/onions/> o
    <http://juhanurmihxlp77nkq76byazcldy2hlmovfu2epvl5ankdibsot4csyd.onion/onions/>.
[^2]: Tienen una
    [lista negra pública de páginas que no indexan](https://ahmia.fi/documentation/).
[^3]: <i lang="en">Search beyond censorship</i> en inglés.

## Otras opciones para encontrar contenido

Existen listas de servicios ocultos, foros, etc. Este artículo solo
presenta una lista de algunos de los buscadores más populares.
