Author: Jorge Maldonado Ventura
Category: Video games
Date: 2023-12-08 12:09
Lang: en
Slug: compras-videojuegos-yo-tengo-mas-de-6000-gratis
Tags: buying, emulation, GNU/Linux, free software
Save_as: do-you-buy-videogames-I-have-more-than-6000-for-free/index.html
URL: do-you-buy-videogames-I-have-more-than-6000-for-free/
Title: Do you buy video games? I've got more than 6000 for free
Image: <img src="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png" alt="" width="1441" height="835" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png 1441w, /wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando-720x417.png 720w" sizes="(max-width: 1441px) 100vw, 1441px">

The videogame industry is a multi-billion dollar industry. However,
there are thousands upon thousands of games that can be played for free.
I am not only talking about [free-as-in-freedom](https://freakspot.net/escribe-programas-libres/en/learn/) games, but also about old arcade games,
console games, etc.

If you use GNU/Linux, you can install many videogames using the package
manager of your distro. Other games are distributed in Flatpak,
Snap and AppImage formats or must be compiled. To find free games I
recommend [LibreGameWiki](https://libregamewiki.org/Main_Page).

However, we don't only have free games, but thousands of old arcade games,
which can be played with<!-- more --> <abbr title="Multiple Arcade Machine Emulator">MAME</abbr>[^1].
There are also emulators for consoles, for games only available on
Windows, etc. Many old games are very original and entertaining, so much
so that most modern games are mere imitations and are inspired by them.

Of course, there are those who prefer current videogames that are
innovative and can be played in high resolution. The problem is that
many of these games are proprietary, cost money and are usually only
available in major languages. Free-as-in-freedom games, on the other
hand, can always be improved and are often available in many languages.
I would rather give money to promote the development of free games than
spend money on something I can't modify.

<a href="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png">
<img src="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png" alt="" width="804" height="1080" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png 804w, /wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball-402x540.png 402w" sizes="(max-width: 804px) 100vw, 804px">
</a>

The best approach is to have a machine that allows you to play any game,
from any time, in any resolution... That machine is, in my case, a
computer with GNU/Linux.

[^1]: You can install it with `sudo apt install mame` on Debian-based
    distros. Games (ROMs) can be downloaded on
  <https://archive.org/download/mame-merged/mame-merged/>,
  <https://www.mamedev.org/roms/> and on many other websites. On Debian
  you must save the games in the `~/mame/roms` directory.
