Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-01-30 12:00
Image: <img src="/wp-content/uploads/2019/12/graffiti-camara-vigilancia.jpg" alt="Murskribaĉo de sekureckamerao">
Lang: eo
Slug: la-privacidad-es-un-asunto-colectivo
Save_as: Privateco-estas-kolektiva-afero/index.html
URL: Privateco-estas-kolektiva-afero/
Tags: konsiloj, privateco
Title: Privateco estas kolektiva afero

Multaj homoj persone klarigas kial ili protektas aŭ kial ne sian
privatecon. Oni aŭskultas de tiuj, al kiuj ne multe gravas, ke ili devas
kaŝi nenion. Kiuj zorgas, faras tion por protekti sin kontraŭ
senskrupulaj firmaoj, subpremaj ŝtatoj, ktp. Ambaŭ kutime supozas, ke
privateco estas persona afero, sed ne estas.

Privateco estas afero kaj individua kaj kolektiva. La datenoj akiritaj
de grandaj firmaoj kaj registaroj malofte estas individue uzitaj. Ni
povas kompreni, ke la privateco estas individua rajto rilate al
komunumo, kiel diras [Edward Snowden](https://eo.wikipedia.org/wiki/Edward_Snowden):

> Argumenti, ke la privateco ne gravas al vi ĉar vi ne devas kaŝi ion,
> ne estas malsama ol diri, ke la gazetarlibereco ne gravas al vi ĉar vi
> ne devas diri ion.

Viaj datenoj povas esti uzita por bono aŭ por malbono. La datenoj
senbezone kaj senpermese kolektitaj kutime estas uzitaj por malbono.

La ŝtatoj kaj la grandaj teknologiaj firmaoj ege malobservas nian
privatecon. Multaj homoj silente aprobas argumentante, ke ne eblas fari
ion por ŝanĝi tion: la firmaoj havas tro da povo kaj la registaroj faros
nenion por ŝanĝi la situacion. Kaj tiuj homoj ja kutimas doni povon al
firmaoj, kiuj gajnas monon per iliaj datenoj, kaj diras tiel al la
ŝtatoj, ke ili ne estos baro, kiam ili volu realigi politikojn de amasa
gvatado. Vere ili damaĝas la privatecon de tiuj, kiuj zorgas.

La kolektiva ago komencas en la individuo. Ĉiu homo devus pripensi, se
ri donas proprajn datenojn, kiujn ri ne devus doni, se ri favoras la
kreskadon de kontraŭprivatecaj firmaoj kaj, eĉ pli grave, se ri estas
rompante la privatecon de siaj proksimuloj. **La plej bona maniero
protekti la privatan informon estas ne doni ĝin**. Per konscia rigardo
al la problemo oni povas subteni protektojn, kiuj defendas la
privatecon.

La personaj datenoj estas tre valoraj &mdash; tiom, ke kelkaj nomas ilin
la «nova nafto» &mdash; ne nur ĉar ili povas esti venditaj al aliuloj,
sed ankaŭ ĉar ili donas povon al tiu, kiu havas ilin. Kiam ni donas ilin
al registarojn, ni donas al ili povon, por ke ili kontrolu nin. Kiam ni
donas ilin al firmaoj, ni donas al ili povon, por ke ili havu influon
sur nia konduto. Finfine la privateco gravas, ĉar ĝi helpas al ni
konservi la povon, kiun ni havas sur niaj vivoj, tiun povon, kiun tiom
pene ili klopodas forpreni de ni. Mi ne donacos nek fie vendos miajn
datenojn, ĉu vi?
