Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2020-01-04 19:55
Modified: 2020-01-05 01:30
Lang: es
Slug: obtener-las-palabras-más-usadas-de-un-texto-y-las-veces-que-se-repiten
Tags: Bash, palabras, Python
Title: Obtener las palabras más usadas de un texto y las veces que se repiten: con Python y GNU Coreutils

En este artículo muestro cómo obtener las palabras más utilizadas de un
texto de forma sencilla. En este caso, yo usaré como demostración el
texto del libro octavo de la novela *Τῶν περὶ Χαιρέαν καὶ Καλλιρρόην*,
extraído de
<https://www.perseus.tufts.edu/hopper/text?doc=Perseus%3Atext%3A2008.01.0668%3Abook%3D8>.
Le he quitado las anotaciones que tenía entre corchetes con `sed`...

    :::bash
    sed -i 's/\[[^]]*\]//g' libro_octavo.txt

El programa que nos muestra todas las palabras
es el siguiente, lo he llamado `lista-de-palabras.py` (explico cómo
funciona más abajo):


    :::python
    archivo_texto = open('libro_octavo.txt', 'r')
    texto = archivo_texto.read()
    archivo_texto.close()

    palabras = texto.split()

    for palabra in palabras:
        print(palabra.strip('‘’:;,.').lower())

En el archivo con el texto, que he llamado `libro_octavo.txt`
([descargar](/wp-content/uploads/2020/01/libro_octavo.txt){:download}),
asumo que una palabra está separada por un espacio en blanco, así que
uso la función `split` para obtener la lista de palabras. Sin embargo, a
veces hay comas, puntos, comillas, dos puntos y puntos y comas antes o
después de las palabras, y a veces empiezan por mayúscula. Para estos
casos basta usar la función `strip()`, con los caracteres que queremos
desechar entre comillas, y `lower()` para poner la palabra en minúscula.

Ahora quiero se muestren por pantalla las palabras que más veces
aparecen en el texto, con el número de veces que aparecen a su
izquierda; pero no voy a programarlo yo, sino que voy a utilizar
herramientas que permiten hacer eso en GNU/Linux: `uniq` y `sort`.

<!-- more -->

El código que debo ejecutar es el siguiente, siendo
`lista-de-palabras.py` el programa Python anterior...

    :::bash
    python3 lista-de-palabras.py | sort | uniq -c | sort -n -r

La lista es muy grande, así que es útil verla usando `less`. Si quiero
sacar solo la lista de las 20 palabras más usadas, añado una
tubería: `| head -n 20`...

    :::bash
    python3 lista-de-palabras.py | sort | uniq -c | sort -n -r | head -n 20

Esto devuelve el siguiente resultado:

    :::bash
    274 καὶ
    193 δὲ
    169 τὸν
    105 τῷ
     86 ἡ
     84 ἐν
     60 γὰρ
     56 τῆς
     52 εἰς
     41 μὲν
     40 τοῖς
     35 χαιρέας
     31 οὐ
     30 ὡς
     27 τοῦ
     27 ὅτι
     26 ἀπὸ
     26 οὖν
     24 πρὸς
     22 αὐτὸν

Claro, la mayoría de palabras que más se repiten son conjunciones y
preposiciones en este caso, y muchas palabras se declinan en griego
antiguo. Si queremos remediar esos problemas, habría que perfeccionar el
programa de Python.

Pero para tu caso concreto debes profundizar tú. Aquí básicamente quiero
mostrar que solo con la lista de palabras puedes contar la frecuencia
con que aparecen y ordenarlas usando herramientas de [GNU
Coreutils](https://www.gnu.org/software/coreutils/), que vienen ya
instaladas en GNU/Linux. No he usado nada aparte de programas que ya
tengo instalados en mi sistema: no hace falta reinventar la rueda ni
utilizar librerías externas.
