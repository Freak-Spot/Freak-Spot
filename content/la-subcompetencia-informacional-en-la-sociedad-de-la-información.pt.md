Author: Jorge Maldonado Ventura
Category: Sem categoria
Date: 2022-04-17 00:00
Lang: pt
Slug: la-competencia-informacional-en-la-sociedad-de-la-información-problemas-y-soluciones
Save_as: literacia-da-informação-na-sociedade-da-informação-problemas-e-soluções/index.html
URL: literacia-da-informação-na-sociedade-da-informação-problemas-e-soluções/
Tags: literacia digital, privacidade, software livre
Title: Literacia da informação na «sociedade da informação»: problemas e soluções

## 1. Introdução

A literacia da informação é de grande importância na sociedade actual,
onde a vida das pessoas é mediada por tecnologias de todos os tipos e
existe uma enorme quantidade de informação à sua disposição. O objetivo
deste ensaio é apresentar esta importante questão, analisar brevemente a
situação atual e determinar como melhorar as competências das pessoas na
nossa sociedade.

<!-- more -->

## 2. Literacia da informação <i lang="lt">versus</i> literacia digital

A sociedade do norte global de hoje não pode ser compreendida sem as
tecnologias de informação que se tornaram parte das nossas vidas. Já não
são apenas os jovens que utilizam tecnologias em grande escala; muitas
pessoas mais velhas já possuem um telefone inteligente, computadores,
<i lang="en">tablets</i>, leitores electrónicos, etc. Nos EUA, por exemplo, 61% da
população com mais de 65 anos de idade possui um telefone
inteligente[^1]. No entanto, é questionável se eles realmente fazem uso
eficiente da tecnologia para obter informação de qualidade.

[^1]: Pew Research Center: Internet, Science & Tech. <cite>Mobile Fact Sheet</cite>.
  23 de novembro de 2021 [acesso: 23 de fevereiro de 2022]. Disponível
  em: <https://www.pewresearch.org/internet/fact-sheet/mobile/>.

Com a adopção das tecnologias informáticas, surgiram termos como
*literacia digital*, *sociedade da informação* e *literacia da
informação*, entre outros. Começamos por descrever a sociedade da
informação, um conceito cunhado pelo sociólogo Yoneji Masuda no seu
trabalho <cite>Sociedade da Informação Como Sociedade
Pós-Industrial</cite> (publicado originalmente em japonês em 1980). O
termo refere-se à importância social atribuída à comunicação e à
informação na sociedade actual, onde as relações sociais, económicas e
culturais estão envolvidas[^2]. No entanto, é de notar que a sociedade
da informação não existe em todo o mundo, pelo menos não da mesma forma:
segundo dados da União Internacional das Telecomunicações, 2,9 mil
milhões de pessoas nunca utilizaram a Internet[^3].

[^2]: ALFONSO SÁNCHEZ, I. R. La Sociedad de la Información, Sociedad del
  Conocimiento y Sociedad del Aprendizaje. Referentes en torno a su
  formación. <cite>Bibliotecas. Anales de Investigación, 12</cite> (2),
  2016, p. 236. Disponível em:
  <https://dialnet.unirioja.es/servlet/articulo?codigo=5766698>.
[^3]: International Telecommunication Union. <cite>Statistics</cite>.
  2021 [acesso: 17 de abril de 2022]. Disponível em: <https://www.itu.int/en/ITU-D/Statistics/Pages/stat/default.aspx>.


Por outro lado, a literacia digital e a literacia da informação podem
ser compreendidas quase como sinónimo na sociedade da informação, uma
vez que o acesso à informação é mediado por tecnologias informáticas. Os
dois tipos de alfabetização são inseparáveis[^4]. É por isso que os
utilizarei como sinónimos neste ensaio. O termo literacia digital
enfatiza a utilização de tecnologias, enquanto que literacia da
informação é mais neutro. Uma definição de literacia da informação é
«a capacidade de uma pessoa saber quando e porque precisa de informação,
onde encontrá-la, e como avaliá-la, usá-la e comunicá-la
eticamente»[^5]. Pode-se ser mais ou menos literato em matéria de
informação.

[^4]: HOVIOUS, A. <cite>Digital vs. Information Literacy</cite>. Designer Librarian.
  24 de abril de 2014 [acesso: 3 de abril de 2022]. Disponível em:
  <https://designerlibrarian.wordpress.com/2014/04/24/digital-vs-information-literacy/>.
[^5]: Editores da Wikipédia. <cite>Alfabetización informacional</cite>. 2021 [acesso: 3 de março de 2022]. Disponível em:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

## 3. Os problemas presentes na «sociedade da informação»

### 3.1. Limitações técnicas

A infra-estrutura informática determina sem dúvida a capacidade de fazer
o melhor uso das competências digitais aplicadas à procura de
informação. Por exemplo, alguns computadores mais antigos não podem
utilizar determinados programas, algumas redes de informação são
inacessíveis a partir de certos países, alguns navegadores (normalmente
desactualizados ou obsoletos) não podem carregar correctamente certas
páginas web, etc.

A capacidade de ultrapassar estes obstáculos depende, entre outras
coisas, das condições socioeconómicas e do nosso local de residência.
Não importa a nossa competência, é impossível obter certas informações
quando não há acesso à electricidade ou à Internet onde vivemos. Se não
tivermos dinheiro para comprar um computador e não pudermos aceder a um
computador público, como é o caso em muitos países pobres, será
impossível fazer sequer uso de competências digitais. Se não tivermos
estas limitações, como é o caso nos países desenvolvidos, poderemos
fazer uso de tecnologias e podemos falar de literacia digital.

Algumas empresas têm comportamentos que exacerbam o problema. A
obsolescência programada, que reduz a vida útil dos produtos
tecnológicos, leva a um maior esforço financeiro a longo prazo, tornando
a tecnologia menos acessível. Existe legislação para resolver esta
situação, tal como a lei francesa 2015-992[^6], que pode levar a dois
anos de prisão ou multas pesadas.

[^6]: <cite>LOI n° 2015-992 du 17 août 2015 relative à la transition
  énergétique pour la croissance verte (1)</cite>. 17 de agosto de 2015
  [acesso: 17 de abril de 2022]. Disponível em:
  <https://www.legifrance.gouv.fr/loda/id/JORFTEXT000031044385/>.

### 3.2. Insuficiência de competências

No entanto, só porque temos os meios, não significa que tenhamos as
competências. Para desenvolver estas competências, devemos aprender a
utilizar as tecnologias e a utilizar estratégias que nos permitam lidar
com os obstáculos.

O actual sistema educativo está a concentrar-se fortemente na aquisição
destas competências, mas da forma errada, segundo muitos críticos que
alertam para a perda de privacidade de menores impondo plataformas como
o Google e a Microsoft[^7]. Plataformas amigas da privacidade,
descentralizadas, autogeridas e livres não foram promovidas, mas antes
o contrário: dependem principalmente de empresas americanas que
restringem o conhecimento, impedindo assim a aprendizagem[^8]. Por outro
lado, apenas uma em cada quatro empresas formou os seus empregados em
competências digitais em 2019[^9]; há ainda muitas pessoas que utilizam
tecnologias que os tornam «usados» em vez de «utilizadores»[^10],
sem as liberdades oferecidas pelo software livre[^11]; as notícias
falsas proliferam[^12]; o espaço digital está cheio de
monopólios[^13]; e assim por diante.

[^7]: MANUEL; RAQUEL. <cite>Competencias digitales con mirada crítica</cite>. 25 de
  junho de 2021 [acesso: 30 de março de 2022]. Disponível em:
  <https://commons.wikimedia.org/wiki/File:EsLibre_2021_P25_-_Manu,_Raquel_-_Competencias_digitales_con_mirada_cr%C3%ADtica.webm>.
[^8]: STALLMAN, R. M. <cite>Software libre y educación</cite>. Sítio web do sistema
  operativo GNU. 2021 [acesso: 30 de março de 2022]. Disponível em:
  <https://www.gnu.org/education/education.es.html>.
[^9]: RODELLA, F. <cite>Solo una de cada cuatro empresas forma a sus empleados
  en competencias digitales</cite>. El País. 2 de julho de 2019 [acesso: 30
  de março de 2020]. Disponível em:
  <https://elpais.com/retina/2019/07/02/tendencias/1562062119_351000.html>.
[^10]: STALLMAN, R. M. <cite>Glossary</cite>. Richard Stallman’s
  personal site. 2020 [acesso: 4 de março de 2022]. Disponível em:
  <https://stallman.org/glossary.html>.
[^11]: STALLMAN, R. M. <cite>¿Qué es el Software Libre?</cite> Sítio web
  do sistema operativo GNU. 2019 [acesso: 4 de março de 2022].
  Disponível em: <https://www.gnu.org/philosophy/free-sw.es.html>.
[^12]:CARVALHO, D. <cite>Por que as pessoas acreditam em fake news</cite>, segundo a
  psicologia social. Blog Política na Cabeça. 25 de junho de 2019
  [acesso: 2 de abril de 2022]. Disponível em:
  <https://www.blogs.unicamp.br/politicanacabeca/2019/06/25/fake-news-por-que-as-pessoas-acreditam-em-noticias-falsas-segundo-a-psicologia-social/>.
[^13]: DEL CASTILLO, CARLOS. <cite>Multinacionales digitales. Monopolio,
  control, poder: negocio</cite>. El Diario. 15 de agosto de 2020
  [acesso: 30
  de março de 2020]. Disponível em:
  <https://www.eldiario.es/tecnologia/multinacionales-digitales-monopolio-control-negocio_130_6137536.html>.

As notícias falsas proliferaram graças às redes sociais centralizadas
como o Facebook, que ganham dinheiro com a atenção e assim radicalizam
os utilizadores com notícias falsas, segundo as revelações de Frances
Haugen[^14]. A informação é partilhada muito rapidamente, e as pessoas
passam cada vez menos tempo a analisar a informação. O debate e a
análise crítica não são encorajados, mas sim o posicionamento (positivo
por partilhar, gostar, comentar positivamente; negativo por não gostar,
não partilhar, comentar negativamente) perante um facto chocante que nos
ofende, chama a nossa atenção, activa as nossas emoções, porque é isso
que fará com que as pessoas passem mais tempo em frente ao ecrã,
enchendo assim os bolsos das empresas, que podem mostrar mais anúncios e
recolher mais informação dos utilizadores. O resultado é uma sociedade
polarizada, mal informada e acrítica. Não existe uma rede social popular
baseada no debate respeitoso, fornecendo fontes, sem conteúdo
excessivamente chamativo, etc., precisamente porque não é rentável.

[^14]: MILMO, D. <cite>Facebook revelations: what is in cache of
  internal documents?</cite> The Guardian. 25 de outubro de 2021
  [acesso: 7 de abril de 2022]. Disponível em:
  <https://www.theguardian.com/technology/2021/oct/25/facebook-revelations-from-misinformation-to-mental-health>.

### 3.3. O mito dos nativos digitais

Pensky, que cunhou o termo «nativos digitais»[^15], não realizou uma
investigação extensiva sobre esta geração, mas sim baseou-se em
observações simples, tais como o número de horas que os estudantes
universitários passam em frente dos ecrãs (mas sem fornecer fontes) e o
facto de os jovens terem estado imersos na tecnologia digital ao longo
das suas vidas. Apenas só com estas observações não se pode afirmar que
os jovens compreenderam o que estavam a fazer com estes dispositivos, ou
que o estavam a fazer de forma eficaz e eficiente. Pelo contrário, a
investigação mostra-nos que estes «nativos digitais» nascidos depois de
1984 não têm um conhecimento profundo das tecnologias, mas apenas
competências básicas de escritório, de envio de mensagens de texto,
de utilização de redes sociais como o Facebook e navegação na Internet.
Rowlands <i lang="lt">et al.</i>[^17] concluíram que «muitas anotações
profissionais, escritos populares e apresentações em PowerPoint
sobrestimam o impacto das TICs na juventude, e que a presença
omnipresente da tecnologia nas suas vidas não resultou numa melhor
retenção de informação, pesquisa ou competências de avaliação».

[^15]: PRENSKY, M. Digital Native, Digital Immigrants. <cite>On the Horizon.
  2001, 9</cite> (5). Disponível em:
  <https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf>.
[^16]: BULLEN, M. <i lang="lt">et al.</i> <cite>The digital learner at BCIT and implications for
  an e-strategy</cite>. Octubre de 2008 [acesso: 17 de abril de 2022].
  Disponível em:
  <https://app.box.com/s/fxqyutottt>.
[^17]: ROWLANDS, I. <i lang="lt">et al.</i> The Google generation: The information
  behaviour of the researcher of the future. <cite>Aislib proceedings: New
  Information Perspectives</cite>. 2008, 60, p. 290-310. Disponível em:
  <http://dx.doi.org/10.1108/00012530810887953>.

As empresas tentam criar dependência dos estudantes de programas
proprietários, oferecendo-o gratuitamente. Estes programas recolhem
frequentemente dados pessoais para vender, lucram com os anúncios e têm
uma licença restritiva que deve ser paga[^8]. Os jovens, desconhecendo
as verdadeiras intenções das empresas, são presas fáceis. Eles não são
génios, são apenas aprendizes, e podem aprender mal. A intenção destas
empresas não é fazê-las aprender, mas torná-las dependentes dos seus
produtos.

O mito dos nativos digitais é algo pernicioso porque tenta fazer parecer
que o problema da sub-competência digital não existe ou será resolvido à
medida que as gerações mais novas substituírem as mais velhas.

### 3.4. Censura

Há factores políticos que influenciam a emergência de monopólios
tecnológicos e de informação, embora haja algumas tentativas por parte
de alguns sectores para remediar esta situação. Existe também uma forte
influência política sobre os resultados apresentados pelos motores de
busca. Por exemplo, a União Europeia obriga os motores de busca a
censurar resultados que infringem os direitos de autor, que não
respeitam o direito a ser esquecidos e, recentemente, os meios de
comunicação estatais russos têm sido censurados[^18]. Por outro lado, a
China também censura muitos resultados oferecidos pelos motores de busca
disponíveis no seu território[^19].

[^18]: <cite>União Europeia e gigantes da web censuram as agências RT e
  Sputnik</cite>. Pragmatismo Político. 3 de março de 2022 [acesso: 30 de
  março de 2022]. Disponível em:
  <https://www.pragmatismopolitico.com.br/2022/03/uniao-europeia-web-censuram-agencias-rt-sputnik.html>.
[^19]: Editores da Wikipédia. Censura de Internet en la República Popular
  China. 2022 [acesso: 30 de março de 2022]. Disponível em:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

Alguns motores de busca podem também decidir censurar todos os
resultados sobre um determinado tópico, tipo de conteúdo, etc.,
independentemente da localização geográfica dos utilizadores, caso
do DuckDuckGo, que já não enumera nos seus resultados sítios eletrónicos
relacionados com a «desinformação russa»[^20]. A maioria dos motores de
busca não admitem abertamente tais acções e não têm qualquer tipo de
transparência. O Google acumula dezenas de milhões em multas por alterar
os resultados em seu próprio benefício[^21]. Google é utilizado por
quase todos: 91,56% em março de 2022, segundo Statcounter[^22], seguido
de Bing com 3,1%. Existe um motor de busca gratuito e distribuído
Existe um motor de busca distribuído gratuitamente chamado YaCy, mas a
sua utilização é marginal e os seus resultados não são muito bons para
redes tão grandes como a Internet, pelo que é muito mais utilizado em
<i lang="en">intranets</i>.

[^20]: WEINBERG, G. Mensaje en la red social Twitter. 10 de março de
  2022 [acesso: 30 de março de 2022]. Disponível em:
  <https://nitter.snopyta.org/yegg/status/1501716484761997318>.
[^21]: RAYÓN, A. <cite>Los monopolios de Google</cite>. Deia.eus, 4 de
  abril de 2021 [acesso: 2 de abril de 2022]. Disponível em:
  <https://www.deia.eus/vivir-on/contando-historias/2021/04/04/monopolios-google/1110752.html>.
[^22]: <cite>Search Engine Market Share Worldwide</cite>. Statcounter. Março de 2022
  [acesso: 30 de março de 2020]. Disponível em:
  <https://gs.statcounter.com/search-engine-market-share>.

É muito dispendioso desenvolver um motor de busca para a Internet, tal
como é dispendioso mantê-lo. É por isso que quase todos os motores de
busca são propriedade de grandes empresas.

### 3.5. A informação desaparece

Por outro lado, muitas informações também desaparecem por razões
económicas, devido à falta de difusão do sítio eletrónico, etc.
Exactamente o mesmo aconteceu com os livros antigos: da Grécia e Roma
apenas algumas obras populares chegaram até nós, e muito poucas
impopulares. Arquivistas, bibliotecários, leitores, etc., preservaram
estas obras, e é por isso que hoje em dia podemos encontrar cópias em
bibliotecas e na Internet. Na Internet, o alojamento de um sítio eletrónico custa
dinheiro: tanto o servidor como o domínio, a menos que opte por utilizar
um protocolo alternativo, como o serviço oculto do Tor (onde só paga
pelo servidor), ou confie no alojamento fornecido por outro indivíduo,
organização ou empresa (geralmente em troca de anúncios, apoiado por
doações ou simplesmente como um acto típico de um mecenas). Muitos,
muitos sítios desapareceram porque os servidores não foram mantidos ou
porque o domínio não foi pago. Se as obras tivessem pouco interesse, as
cópias seriam provavelmente perdidas. Essa informação pode também ser
relevante para nós, e é possível que alguém a tenha arquivado. É por
isso que é bom saber que existem arquivos de sítios eletrónicos e arquivos
multimédia, tais como o Internet Archive.

Desde abril de 2022, o Internet Archive tem mais de 670 mil milhões
de páginas web; 34 milhões de livros e textos; quase oito milhões de
filmes, vídeos e programas de televisão; 800.000 programas informáticos,
14 milhões de ficheiros áudio, 4,3 milhões de imagens, entre outras
coisas, geralmente agrupadas em colecções, disponíveis em vários
formatos e com metadados[^23]. O Internet Archive é uma organização
sem fins lucrativos. Existem também outros projectos com o mesmo
objectivo (tais como [archive.today](https://archive.ph/)) e iniciativas
governamentais, tais como o projecto Memento[^24] (dos Estados Unidos).

[^23]: <cite>Internet Archive: Digital Library of Free & Borrowable Books,
  Movies, Music & Wayback Machine</cite>. 2022 [acesso: 6 de abril de 2022].
  Disponível em: <https://archive.org/>
[^24]: Editores da Wikipédia. <cite>Memento Project</cite>. 15 de novembro de 2021 [acesso: 17 de abril de 2022]. Disponível em:
  <https://en.wikipedia.org/wiki/Memento_Project>.

## 4. Como aumentar a literacia da informação

As competências na utilização de software e a capacidade de colocar
questões de pesquisa para resolver problemas são também muito importantes
na nossa sociedade da informação. As capacidades de pesquisa não se
referem apenas à pesquisa num motor de pesquisa, mas também à navegação
nos menus, navegação na documentação do programa, participação em
fóruns, etc. É também importante saber qual a versão do programa em
questão que estamos a utilizar, pois pode haver soluções na Internet
para uma versão que não utilizamos e que não é útil para nós.

Em suma, quanto mais elevado for o nível de literacia informática, mais
desenvolvidas são as nossas capacidades de procura de informação. Se
soubermos o que é um motor de busca web e como funciona, podemos
realizar melhores pesquisas; se soubermos como utilizar um servidor
<i lang="en">proxy</i>, podemos evitar restrições no acesso à informação com base na
localização geográfica (geralmente país) ou endereço IP; se conhecermos
os preconceitos e limitações dos motores de busca, podemos comparar
resultados de pesquisa e escolher o motor de busca mais apropriado para
cada consulta; se consultarmos arquivos web, podemos ver como a
informação mudou ao longo do tempo e páginas que já não estão
acessíveis; etc.

O desenvolvimento destas competências requer educação e prática. Tal
como não se pode aprender a tocar guitarra num dia, não se pode aprender
a utilizar um computador e aceder efectivamente à informação num dia. As
famílias, as empresas e a comunidade educativa têm um papel essencial a
desempenhar na promoção do desenvolvimento destas competências.

Quando precisamos de obter certas informações e encontramos obstáculos,
temos a oportunidade de aprender como evitá-los. Podemos investigar como
lidar com esses obstáculos, utilizar estratégias para obter informações
mais relevantes. Alguém que tenha ultrapassado esses obstáculos pode
ajudar-nos a fazer o mesmo. O papel da comunidade de utilizadores é
extremamente importante, assim como o papel dos professores.

Contudo, não faremos progressos nestas competências se o que sabemos for
suficiente ou se pensarmos que não há qualquer problema com a informação
que obtivemos. Uma pessoa presa na bolha dos meios de comunicação
social, consumindo informações falsas e comprando como resultado da
enorme indústria de publicidade na Internet, pode ignorar estas
competências e pensar que é especialista na utilização de tecnologias
informáticas. Ela pode ser vítima do efeito Dunning-Kruger, que David
Dunnung resumiu da seguinte forma: «Se é incompetente, não pode saber
que é incompetente... As competências de que precisa para produzir uma
resposta correcta são exactamente as competências de que precisa para
reconhecer o que é uma resposta correcta»[^25].

[^25]:MORRIS, E. <cite>The Anosognosic’s Dilemma: Something’s Wrong but You’ll p
  Never Know What It Is (Part 1)</cite>. Opinionator. The New York Times. 20 de
  junho de 2010 [acesso: 8 de abril de 2022]. Disponível em:
  <https://web.archive.org/web/20220331180557/https://opinionator.blogs.nytimes.com/2010/06/20/the-anosognosics-dilemma-1/>.

O melhor é ter competências que nos permitam adaptar-nos a tudo. Não
basta saber como utilizar um motor de busca específico, programa, etc.,
mas é necessário conhecer as suas características comuns. Desta forma,
se o programa for actualizado ou se nos encontrarmos num sistema
operativo diferente com um programa diferente, poderemos continuar a
utilizá-lo correctamente, sem grandes complicações ou contratempos.

## 5. Conclusão

A «sociedade da informação» em geral está bastante desinformada. Contudo,
as competências que fazem parte da alfabetização digital podem ser
adquiridas com a educação e a prática. A educação tem vindo a aumentar a
literacia digital ao longo dos anos, mas não o suficiente aos níveis
educacionais básicos. Aqueles que adquirirem níveis mais elevados de
literacia de informação estarão mais aptos a participar nesta sociedade.
