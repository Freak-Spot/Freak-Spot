Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-12-28 10:12
Lang: de
Slug: aliases-para-agilizar-tareas-en-bash
Save_as: Aliase-zur-Beschleunigung-von-Aufgaben-in-Bash/index.html
URL: Aliase-zur-Beschleunigung-von-Aufgaben-in-Bash/
Tags: .bashrc, .bash_aliases, alias, alias Bash, Alias, Bash-Einstellungen
Title: Aliase zur Beschleunigung von Aufgaben in Bash

Aliase werden, wie der Name schon sagt, verwendet, um einen Befehl unter
einem anderen Namen aufzurufen. Der Befehl, auf den ein Alias angewendet
wird, funktioniert so, als ob er direkt aufgerufen worden wäre. Wenn ich
zum Beispiel mit dem Befehl `..` in das übergeordnete Verzeichnis
wechseln möchte, muss ich nur einen Alias im Terminal mit folgendem
Befehl erstellen: `alias ..='cd ...'`.

Wahrscheinlich hast du bereits mehrere Aliasnamen angelegt und wisst
es nicht. Wenn du `alias` ausführst, siehst du die bereits definierten
Aliase. Diese Aliase sind in der Datei `.bashrc` definiert, wo du deine
eigenen Aliase hinzufügen kannst (denk daran, die [Bash-Konfiguration
nach dem Hinzufügen neu zu laden](/de/die-Konfiguration-von-Bash-neuladen/), damit du sie ohne Neustart des
Computers verwenden kannst). Wenn du jedoch viele von ihnen hinzufügen
möchtest und unterscheiden willst, welche von ihnen dir gehören, ist es
ratsam, sie in einer separaten Datei zu speichern.

In der `.bashrc`-Datei findest du wahrscheinlich diese oder ähnliche
Zeilen:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

Das bedeutet, dass jedes Mal, wenn du Bash startest, werden Aliase aus
der Datei `~/.bash_aliases` geladen, sofern sie existiert. Wenn
du diese Datei noch nicht hast, erstelle sie und füge einige
Aliasnamen hinzu, die dir bei deiner täglichen Arbeit helfen können. Sie
werden dir auf lange Sicht viel Zeit ersparen.

Hier sind einige nützliche Aliasnamen:

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'

Ich habe ein Repository auf
<https://notabug.org/jorgesumle/bash_aliases> mit all meinen Aliasen,
schaue es sich an und kopiere die, die du nützlich findest.
