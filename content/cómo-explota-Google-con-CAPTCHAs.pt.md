Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2017-10-28 03:03
Image: <img src="/wp-content/uploads/2017/05/reCAPTCHA-libros.png" alt="reCAPTCHA used to digitalize texts">
Lang: pt
Slug: como-explota-Google-con-CAPTCHAs
Save_as: Como-o-Google-explora-com-os-CAPTCHAs/index.html
URL: Como-o-Google-explora-com-os-CAPTCHAs/
Tags: CAPTCHA, exploração, Google, reCAPTCHA, spam
Title: Como o Google explora com os CAPTCHAs

Um <abbr title="Completely Automated Public Turing test to tell Computers and Humans Apart">CAPTCHA</abbr>
é uma prova que se realiza para diferenciar computadores de
humanos. Usa-se principalmente para evitar mensagens lixo (em inglês
chamados de <i lang="en">spam</i>).

Um programa que realiza este teste é o reCAPTCHA, que foi lançado a 27
de maio de 2007 e adquirido pelo Google em setembro de 2009[^1]. É
utilizado em sítios eletrónicos de todo o mundo.

O reCAPTCHA tem sido amplamente utilizado para digitalizar textos graças
ao trabalho gratuito de milhões de utilizadores, que são capazes de
identificar palavras incompreensíveis para os computadores. A grande
maioria desconhece que o Google está se aproveitando seu trabalho;
outros não se importam.

![reCAPTCHA usado para digitalizar texto](/wp-content/uploads/2017/05/reCAPTCHA-libros.png)

O artigo [«Deciphering Old Texts, One Woozy, Curvy Word at a
Time»](https://www.nytimes.com/2011/03/29/science/29recaptcha.html)
publicado no jornal The New York Times relatou que os utilizadores da
Internet tinham acabado de digitalizar o arquivo do jornal, publicado
desde 1851, utilizando o reCAPTCHA. De acordo com o criador do reCAPTCHA
na altura, os utilizadores, ou melhor «utilizados», decifravam cerca de
200 milhões de CAPTCHAs por dia, demorando 10 segundos para resolver
cada um deles. Isto equivale a 500 000 horas de trabalho por dia[^2].

Um CAPTCHA pode ser utilizado não só para textos de difícil compreensão,
mas também para imagens. Por exemplo, o Google também faz uso de
reCAPTCHAs para identificar imagens de lojas, sinalização rodoviária,
etc., feitas para o Google Maps. Os reCAPTCHAs do Google são também
utilizados para outros fins que não são conhecidos pelos utilizados.

![reCAPTCHA feito para o Google Maps](/wp-content/uploads/2017/05/reCAPTCHA-Google-Maps.png)

Só o Google sabe o lucro que obtém com este sistema de exploração. É
impossível auditar o reCAPTCHA porque é um programa proprietário, e
aqueles que o utilizam não têm qualquer poder. Tudo o que podem fazer é
recusar-se a resolver CAPTCHAs como os do Google para impedir serem
utilizados.

O Google utiliza agora um método de identificação que poupa tempo aos
utilizadores e evita que estes tenham de decifrar textos e imagens. Os
utilizadores agora só têm de clicar num botão. Este mecanismo impõe a
execução de código JavaScript desconhecido pelos «utilizados», o que
representa um grande perigo para a privacidade. O Google pode obter
muita informação dos «utilizados» que utilizam este mecanismo, que
provavelmente depois venderá por grandes somas de dinheiro.

![botão do reCAPTCHA](/wp-content/uploads/2017/05/Recaptcha_anchor@2x.gif)

Além disso, reCAPTCHA discrimina contra utilizadores deficientes e
utilizadores do Tor: por um lado, os desafios apresentados aos utilizadores
deficientes são mais longos e mais difíceis de resolver; por outro lado,
aqueles que navegam na Internet a título privado têm de resolver um
desafio mais difícil e demorado.

O
[artigo](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html)
escrito pelo criador do reCAPTCHA[^3] quando foi adquirido pelo Google
dizia:

> Melhorar a disponibilidade e acessibilidade de toda a informação na
> Internet é realmente importante para nós, por isso estamos ansiosos
> por fazer avançar esta tecnologia com a equipa da reCAPTCHA.

No entanto, o trabalho realizado utilizando o reCAPTCHA não está
disponível ou acessível em muitos casos. Os dados são apresentados de
uma forma que beneficia financeiramente o Google e outras empresas. Os
utilizadores que ajudaram a digitalizar o arquivo do The New York Times
têm de pagar vendo os anúncios quando consultam o arquivo que ajudaram a
digitalizar sem receber nada em troca.

[^1]:
    [reCAPTCHA](https://en.wikipedia.org/wiki/ReCAPTCHA). Wikipedia. Consultado o 5 de maio de 2017.
[^2]:
    [Deciphering Old Texts, One Woozy, Curvy Word at a Time](http://www.nytimes.com/2011/03/29/science/29recaptcha.html) (28 de março de 2011). The New York Times. Consultado o 5 de maio de 2017.
[^3]:
    [Teaching computers to read: Google acquires reCAPTCHA](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html) (2009-9-16). Official Google Blog. Consultado o 5 de maio de 2017.
