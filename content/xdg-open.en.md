Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-19 12:03
Lang: en
Slug: xdg-open
Status: published
Tags: xdg-open
Title: <code>xdg-open</code>

`xdg-open` is a very interesting command. With it we can open any
program or URL from the command line. If I were to run `xdg-open
https://freakspot.net`, it would open the home page of this website
with my default browser, and then I could execute another command.
One disadvantage is that we can only pass one argument to it, so to
open two web pages we would have to run `xdg-open` twice.
