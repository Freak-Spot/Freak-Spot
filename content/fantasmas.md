Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2020-05-09
Lang: es
Slug: fantasmas
Tags: civilización, colapso, control político, decadencia
Title: ¿Fantasmas?
Image: <img src="/wp-content/uploads/2020/05/fantasma.png" alt="">

<a href="/wp-content/uploads/2020/05/chocar-con-una-farola-por-mirar-el-celular.gif">
<img src="/wp-content/uploads/2020/05/chocar-con-una-farola-por-mirar-el-celular.gif" alt="">
</a>

El vulgo no entiende nada: es muy vago y no quiere pensar por sí mismo.
Entretenimiento en todo momento, comida... ¿qué más puedes pedir? Tiene
incluso orgullo de formar parte de este paraíso, que defenderá ondeando
telas de colores y articulando «su» meditada opinión. Hasta ahora le va
bien consumiendo su vida rodeado de *fantasmas* &mdash;como diría [Max
Stirner](https://es.wikipedia.org/wiki/Max_Stirner)&mdash;; ¿pero creerá
en fantasmas cuando no tenga nada que llevarse a la boca?, ¿cuándo no
pueda evadirse más?

Con la extinción masiva de especies, la brutal perdida de suelo, el
encarecimiento del petróleo y demás problemas creados, quien peor
parado puede salir es quien vive en otra realidad, aunque nadie se
libra.

<!-- more -->

¿Cuántos árboles, ríos, montañas, pájaros, insectos... de tu zona puedes
nombrar?; ¿cuántas marcas de móviles, coches, videojuegos, de otras
cosas? ¿Serías capaz de sobrevivir si de repente estuvieran vacías las
estanterías del supermercado, sin la ayuda del estado, de tu familia?

Del Imperio romano muchos ciudadanos consumían su vida
presenciando espectáculos, subsistiendo gracias al grano que el estado
les suministraba mientras podía: [*panes et circenses*](https://es.wikipedia.org/wiki/Panem_et_circenses).
Algún día dejaría de ser accesible el alimento, dejaría de protegerlos
la decadente Roma. Sin saber subsistir en el desamparo ni poder
encabezar sus vidas, llegarían mercenarios para cortarles las cabezas.

«El objetivo del estado es siempre el mismo: limitar al individuo,
domesticarlo, subordinarlo, subyugarlo». &mdash; Max Stirner
