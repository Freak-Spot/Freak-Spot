Author: Jorge Maldonado Ventura
Category: Text editing
Date: 2023-02-09 10:00
Lang: en
Slug: cómo-eliminar-celdas-de-tablas-en-libreoffice-writer
Save_as: how-to-delete-table-cells-in-LibreOffice-Writer/index.html
URL: how-to-delete-table-cells-in-LibreOffice-Writer/
Tags: LibreOffice Writer
Title: How to delete table cells in LibreOffice Writer
Image: <img src="/wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer.png" alt="" width="784" height="426" srcset="/wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer.png 784w, /wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer-392x213.png 392w" sizes="(max-width: 784px) 100vw, 784px">


To delete cells the only option is removing its borders. Here I have a
table from which I want to remove three of the top cells that are
leftover:

<a href="/wp-content/uploads/2023/02/example-table-in-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/02/example-table-in-LibreOffice-Writer.png" alt="" width="792" height="439" srcset="/wp-content/uploads/2023/02/example-table-in-LibreOffice-Writer.png 792w, /wp-content/uploads/2023/02/example-table-in-LibreOffice-Writer-396x219.png 396w" sizes="(max-width: 792px) 100vw, 792px">
</a>

To do this, click on the **Borders** button that appears below when we
select the table cells we want to delete and then click on **No
Borders**, as I do in the following image:

<a href="/wp-content/uploads/2023/02/no-cell-borders-in-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/02/no-cell-borders-in-LibreOffice-Writer.png" alt="" width="971" height="977" srcset="/wp-content/uploads/2023/02/no-cell-borders-in-LibreOffice-Writer.png 971w, /wp-content/uploads/2023/02/no-cell-borders-in-LibreOffice-Writer-485x488.png 485w" sizes="(max-width: 971px) 100vw, 971px">
</a>

In case you get lost, here is also a video demonstration:

<video controls>
  <source src="/video/eliminar-células-de-tabelas-no-LibreOffice-Writer.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Here is the result:

<a href="/wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer.png" alt="" width="784" height="426" srcset="/wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer.png 784w, /wp-content/uploads/2023/02/table-without-three-upper-cells-in-LibreOffice-Writer-392x213.png 392w" sizes="(max-width: 784px) 100vw, 784px">
</a>
