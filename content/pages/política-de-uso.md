Author: Jorge Maldonado Ventura
Date: 2018-03-05 16:30
Modified: 2021-11-02
Lang: es
Slug: política-de-uso
Status: hidden
Title: Política de uso

Se aceptan sugerencias y cambios a estas políticas enviados mediante el
[gestor de incidencias de Freak Spot](https://notabug.org/Freak-Spot/Freak-Spot/issues)
o usando
nuestro [correo de contacto](/pages/contacto.html).

## Condiciones de uso

No nos hacemos responsables sobre los posibles problemas derivados del
uso de nuestro sitio web. Aceptamos críticas y correcciones para mejorar
la página y subsanar los posibles errores que hayamos podido cometer.

Freak Spot respeta las opiniones, críticas o sugerencias expresadas en
comentarios. Nos reservamos el derecho de eliminar mensajes
publicitarios.

En la medida de lo posible, tratamos de hacer el sitio web accesible al
mayor número de personas posible: personas discapacitadas, a quien
navega con JavaScript desactivado, personas que usan navegadores de
texto, con poco ancho de banda, etc.

Donde no se indique lo contrario, las licencias de Freak Spot son la de
dominio público
[CC0](https://creativecommons.org/publicdomain/mark/1.0/), para el
<span title="textos, imágenes, vídeos, comentarios...">contenido</span>,
y la de <i lang="en">software</i> libre
<abbr title="Affero General Public License, version 3">AGPLv3</abbr>,
para el <i lang="en">software</i>. Freak Spot también usa <i lang="en">software</i>
producido por terceros que puede encontrar bajo otra licencia de
<i lang="en">software</i> libre, consulte [Información de
licencias](https://notabug.org/Freak-Spot/Freak-Spot#informaci%C3%B3n-de-licencias)
para más información sobre las licencias de <i lang="en">software</i>.

## Política de privacidad

Freak Spot utiliza un servidor
[Nginx](https://es.wikipedia.org/wiki/Nginx) que almacena en ficheros
datos de acceso: dirección <abbr title="Internet Protocol">IP</abbr>,
navegador utilizado, fecha de visita, etc. Esta información podría
revelar tu identidad. Para una máxima privacidad se puede acceder al
sitio web desde
<http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/>
con [Tor Browser](https://www.torproject.org/).

Sobre los artículos se pueden realizar comentarios anónimos, con nombres
falsos o con datos reales. En caso de que alguien desee eliminar o
rectificar un comentario, deberá
[contactarnos](/pages/contacto.html) demostrándonos
que escribió ese comentario, en cuyo caso indicaremos que el comentario
ha sido eliminado o modificado de la forma que estimemos oportuna,
siempre intentando ser lo más transparentes posibles con el resto de
participantes en la conversación y evitando perder el contexto de otros
comentarios.
