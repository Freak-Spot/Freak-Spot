Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-07-11 15:30
Lang: es
Slug: el-fediverso-puede-ser-bastante-tóxico
Tags: Fediverso, Mastodon, Pleroma
Title: El Fediverso puede ser bastante tóxico

Mastodon, inspirado en GNU social, junto con Pleroma, son los
componentes más populares de lo que hoy conocemos como «Fediverso».
Todos ellos son, en esencia, clones federados libres de Twitter,
interoperables entre sí mediante el protocolo ActivityPub.

En muchos aspectos, el Fediverso es una fuerza liberadora para el bien.
Su diseño federado distribuye la gobernanza y los costes entre muchas
entidades independientes, algo que considero una elección de diseño muy
potente. Sus herramientas de moderación también hacen un buen trabajo
para mantener a los neonazis fuera de publicaciones que ves y
proporcionar un espacio cómodo para expresarte, especialmente si tu
forma de expresión es denostada por la sociedad. Grandes grupos de
miembros del Fediverso han encontrado en ella un hogar para expresarse
que se les niega en otros lugares por su sexualidad, expresión de
género, política u otras características. Además, está totalmente libre
de propaganda comercial.

Pero sigue siendo un clon de Twitter, y muchos de los males sociales y
psicológicos que conlleva están presentes en el Fediverso. Es una fuente
de pensamientos aleatorios de otras personas, a menudo sin filtrar, que
se te presentan sin juicio de valor &mdash;incluso cuando un juicio de valor
puede ser sabio&mdash;. Funcionalidades como dar un impulso o dar a «me
gusta» en las publicaciones, ir tras el número de seguidores y
pequeños influentes, estas cosas refuerzan la dopamina como lo hace
cualquier otra red social. El aumento del límite de caracteres no ayuda
nada; la mayoría de las publicaciones son bastante cortas y nadie quiere
leer un ensayo agresivamente envuelto en palabras en una columna
estrecha.

El Fediverso es un entorno optimizado para las discusiones acaloradas.
Las discusiones en este medio se llevan a cabo bajo estas restricciones,
en público, con el público de los pocos seguidores de ambos lados
entrando y saliendo para reforzar su posición y atacar a los oponentes.
Los avances se miden en ganancias de territorio ideológico y en las
subidas y bajadas de los participantes que salpican sus comentarios a lo
largo de enormes hilos. No te limitas a argumentar tu posición, sino que
la interpretas ante tu audiencia y la de tu oponente.

Las redes sociales no son buenas para ti. El Fediverso sacó lo peor de
mí, y también puede sacar lo peor de ti. Los comportamientos que fomenta
se definen claramente como acoso, un comportamiento que no es exclusivo
de ninguna condición ideológica. La gente sale herida del Fediverso.
Tenlo en cuenta. Considera la posibilidad de mirarte al espejo y
preguntarte si tu relación con la plataforma es saludable para ti y para
la gente que te rodea.


*Este artículo es una traducción
del artículo «[The Fediverse can be pretty
toxic](https://drewdevault.com/2022/07/09/Fediverse-toxicity.html)»
publicado por Drew Devault bajo la licencia [CC BY-SA
2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.es).*
