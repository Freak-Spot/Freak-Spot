Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2016-08-17 04:19
Image: <img src="/wp-content/uploads/2016/08/soros_index-643x510.jpg" width="643" height="510" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="George Soros manipula países con sus fundaciones, según los documentos filtrados por DCLeaks">
Lang: es
Modified: 2017-02-25 17:33
Slug: george-soros-manipula-paises-con-sus-fundaciones-segun-los-documentos-filtrados-por-dc-leaks
Status: published
Tags: antirruso, control político, DCLeaks, especulador, filtración, hacktivismo, maganate, manipulación, Soros
Title: George Soros manipula países con sus fundaciones, según los documentos filtrados por DCLeaks

[DCLeaks](https://dcleaks.com/) ha publicado
[datos](https://fdik.org/soros.dcleaks.com/) (fechan desde 2008 al 2016) de
distintas fundaciones ligadas al magnate especulador George Soros.

Entre los documentos filtrados hay algunos muy curiosos, como una lista
de usuarios de Twitter españoles con presencia política que son
clasificados según sus opiniones hacia Rusia: [Ukraine and
Europe/spain/twitter
list.docx](https://fdik.org/soros.dcleaks.com/download/index.html%3Ff=%252FUkraine%2520and%2520Europe%252Fspain%252Ftwitter%2520list.docx&t=europe).
El documento acaba con una lista de posibles medios afines: El País, El
Diario.es, El Periódico, Diari Ara, La Directa y Radio Klara.

[Otro
documento](https://fdik.org/soros.dcleaks.com/download/index.html%3Ff=%252FUkraine%2520and%2520Europe%252Fspain%252Ftor%2520ukraine%2520debate%2520mapping%2520%2520spain%2520.docx&t=europe)
se centra en llevar el debate sobre la guerra de Ucrania a España. La
fundación del magnate defiende a los nazis ucranianos y proporciona una
guía sobre cómo llevar sus opiniones antirrusas a los medios españoles.

> «To date, there is no publicly available study or report that would
> map the different voices in the Spanish debate on Ukraine. <span
> lang="en-US">OSIFE would like to fill in the void by commissioning a
> short paper to inform its own programming».
> </span>

Los documentos no se limitan solo a Europa: hay una cantidad enorme de
documentos filtrados que afectan a todos los continentes.
