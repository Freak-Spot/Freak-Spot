Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2021-04-17 16:00
Lang: eo
Slug: ¿por-qué-aprender-a-usar-vim
Tags: GNU/Linukso, Vim
Save_as: kiel-lerni-uzi-Vim-n/index.html
URL: kiel-lerni-uzi-Vim-n?/
Title: Kiel lerni uzi Vi(m)-n?
Image: <img src="/wp-content/uploads/2021/04/vim_header.gif" alt="Bildeto de Vim">

Kvankam estas simpla redaktilo, Vim povas esti iom malfacile lernebla.
Kiel perdi tempon por lerni uzi redaktilo tiel stranga kaj malnova?

Estas du versioj: Vi kaj Vim (angle <i>**Vi** i**m**proved</i>). Ambaŭ
funkcias same, sed Vim havas pli trajtojn, kvankam ankaŭ bezonas pli da
diskspaco. Vi estas instalita en la plej granda parto de komputiloj kun
GNU/Linukso. Se vi administras komputajn sistemojn, estas strange trovi
vian plej ŝatatan redaktilon en hazarda servilo, sed Vi estas preskaŭ
ĉiam tie. Sciante kiel funkcias vi povas rapide solvi problemojn kaj
redakti rapide dosierojn en komputiloj sen grafika fasado.

Vim estas redaktilo, kiu ŝparas tempon, kiam vi scias kiel uzi ĝin, ĉar
oni ne devas disigi la manojn el la klavaro por preni la muson. Lerni
uzi reĝima redaktilo (havas kelkajn modojn: enmeto, normala...) kiel Vim
ja ne estas simple, sed valoras la penon longtempe. Mi dirus, ke estas
unu el la plej efikaj redaktiloj disponeblaj, kaj oni havas la garantio,
ke ne malaperos nek grandaj ŝanĝoj estos aldonitaj, kiel okazas kun
aliaj modernaj redaktiloj.

Por komenci lerni vi povas plenumi `vimtutor` en la terminalo, se vi
havas Vim-n instalitan. Se ne, oni povas instali ĝin en distribuoj de
GNU/Linukso bazitaj sur Debiano per la sekva komando:

    ::bash
    sudo apt install vim
