Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-12-03
Modified: 2022-10-29 21:00
Lang: eo
Slug: youtube-con-privacidad-con-invidious
Tags: kromaĵo, Firefox, Guglo, Invidious, privateco, Jutubo
Title: YouTube privatece per Invidious
Save_as: YouTube-privatece-per-Invidious/index.html
URL: YouTube-privatece-per-Invidious/
Image: <img src="/wp-content/uploads/2020/12/ĉefpaĝo-de-Invidious.png" alt="" width="1122" height="775" srcset="/wp-content/uploads/2020/12/ĉefpaĝo-de-Invidious.png 1122w, /wp-content/uploads/2020/12/ĉefpaĝo-de-Invidious-561x387.png 561w" sizes="(max-width: 1122px) 100vw, 1122px">

Kiel oni jam bone scias, Jutubo ne estas libera programaro kaj ne
respektas vian privatecon, sed bedaŭrinde kelkaj videoj nur troviĝas
tie. En ĉi tiu artikolo mi prezentas al vi simplan manieron vidi
Jutubajn videojn sen plenumi proprietan programaron el Guglo, per
Invidious.

Invidious estas libera kaj malpeza interfaco por Jutubo, kiu estas
farita atentante la programan liberecon. Jen kelkaj el ĝiaj trajtoj:

- Sen reklamoj
- Libera programaro, [fontokodo](https://github.com/iv-org/invidious) sub [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html)-permesilo
- Havas serĉilo
- Ne bezonas Guglan konton por konservi abonojn
- Subtenas apudtekstojn
- Tre agordebla
- Permesas enigi videojn el Invidious en via retpaĝo, kiel la jenan:

<!-- more -->

<iframe id='ivplayer' width='640' height='360' src='https://inv.zzls.xyz/embed/SzH2cSggbBA' style='border:none;'></iframe>

Ĉar ĝi estas libera programaro, ne estas nur unu versio. Tio signifas, ke vi
povas instali je Invidious en via servilo aŭ elekti inter la disponeblaj
[publikaj nodoj](https://api.invidious.io/).

Mi invitas al vi [testi kiel ĝi funkcias](https://invidious.snopyta.org/).
Jen mi montras bildojn, kiuj montras al vi tion, kion vi povas fari.

<figure>
<a href="/wp-content/uploads/2020/12/ensaluti-al-Invidious.png">
<img src="/wp-content/uploads/2020/12/ensaluti-al-Invidious.png" alt="" width="1231" height="775" srcset="/wp-content/uploads/2020/12/ensaluti-al-Invidious.png 1231w, /wp-content/uploads/2020/12/ensaluti-al-Invidious-615x387.png 615w" sizes="(max-width: 1231px) 100vw, 1231px">
</a>
    <figcaption class="wp-caption-text">Vi povas ensaluti sen Gugla
    konto.</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/12/abonoj-en-Invidious.png">
<img src="/wp-content/uploads/2020/12/abonoj-en-Invidious.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2020/12/abonoj-en-Invidious.png 1000w, /wp-content/uploads/2020/12/abonoj-en-Invidious-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">Vi povas vidi la plej novajn
    videojn de viaj abonoj.</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/12/ludante-videon-en-Invidious.png">
<img src="/wp-content/uploads/2020/12/ludante-videon-en-Invidious.png" alt="" width="1231" height="775" srcset="/wp-content/uploads/2020/12/ludante-videon-en-Invidious.png 1231w, /wp-content/uploads/2020/12/ludante-videon-en-Invidious-615x387.png 615w" sizes="(max-width: 1231px) 100vw, 1231px">
</a>
    <figcaption class="wp-caption-text">Ĝi havas malhelan etoson.</figcaption>
</figure>

Estas kromaĵoj de Firefox, kiuj aliigas ligilojn de Jutubo al ligiloj de
nodoj de Invidious.
[Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) ankaŭ funkcias por
[Nitter](/eo/konsulti-Tviteron-per-libera-programaro-kaj-privatece/)
kaj [Bibliogram](/eo/Instagram-per-libera-programaro-kaj-privatece/).
