Author: Jorge Maldonado Ventura
Category: Trucos
Date: 2022-05-22 14:00
Modified: 2023-03-08 09:00
Lang: es
Slug: saltar-muros-de-pago-de-periódicos
Tags: muro de pago, truco
Title: Saltar muros de pago de periódicos
Image: <img src="/wp-content/uploads/2022/05/saltando-muro.jpeg" alt="Hombre saltando un muro" width="2250" height="1500" srcset="/wp-content/uploads/2022/05/saltando-muro.jpeg 2250w, /wp-content/uploads/2022/05/saltando-muro.-1125x750.jpg 1125w, /wp-content/uploads/2022/05/saltando-muro.-562x375.jpg 562w" sizes="(max-width: 2250px) 100vw, 2250px">

Muchos periódicos muestran [muros de
pago](https://es.wikipedia.org/wiki/Muro_de_pago) que nos impiden ver el
contenido completo de los artículos. Existen, sin embargo, algunos
trucos para evitarlos.

Una extensión útil para el navegador que nos permite saltar esos muros de
pago es [Bypass Paywalls
Clean](https://gitlab.com/magnolia1234/bypass-paywalls-firefox-clean#installation). Esta
extensión funciona para sitios web populares, y pueden añadirse
fácilmente otros. ¿Cómo funciona? Básicamente, la
extensión usa trucos como desactivar el JavaScript, desactivar las <i
lang="en">cookies</i> o cambiar el [agente de
usuario](https://es.wikipedia.org/wiki/Agente_de_usuario) al de una
[araña web](https://es.wikipedia.org/wiki/Ara%C3%B1a_web) conocida (como
[Googlebot](https://es.wikipedia.org/wiki/Googlebot)).

No hace falta instalar la extensión anterior si no quieres. Sigue
leyendo para conocer al detalle los trucos que se pueden usar para
evitar gran parte de los muros de pago.
<!-- more -->

## 1. Desactivar JavaScript

A veces, el periódico pone el muro de pago usando JavaScript.
[Desactivar JavaScript](/desahabilitar-javascript-comodamente-firefox-y-derivados/)
bastaría para quitar el muro de pago.

## 2. Borrar <i lang="en">cookies</i>

A veces los periódicos muestran los muros de pago tras ver unos cuantos
artículos. Esto ocurre porque no quieren asustar a los nuevos visitantes
con un muro de pago. Para que piensen que eres un nuevo visitante basta
con [borrar las <i lang="en">cookies</i>](/elimina-las-cookies-en-navegadores-derivados-de-firefox/).

## 3. Cambiar el agente de usuario

Muchos periódicos quieren restringir el acceso a los usuarios, pero no a
las arañas web, pues eso afectaría a su posicionamiento en buscadores.
Existen extensiones para cambiar el agente de usuario de forma sencilla:
una de ellas es [User-Agent Switcher and
Manager](https://addons.mozilla.org/es/firefox/addon/user-agent-string-switcher/).
Habría que hacerse pasar por arañas web como Googlebot o Bingbot.

## 4. Bloquear elementos

Algunas veces funciona quitar elementos de páginas web. Podemos
hacer esto abriendo el inspector de elementos del navegador web y
eliminando los elementos que pertenecen al muro de pago, pero es más
fácil [hacerlo con uBlock Origin](/bloqueo-de-elementos-con-ublock-origin/).

## 5. Repositorios piratas

Existen repositorios piratas como
[Sci-Hub](https://es.wikipedia.org/wiki/Sci-Hub) o [Library
Genesis](https://es.wikipedia.org/wiki/Library_Genesis). Desconozco si
existe algún repositorio especializado para periódicos de Internet.
