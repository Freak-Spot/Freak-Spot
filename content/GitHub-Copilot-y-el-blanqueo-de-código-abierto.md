Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-07-12 21:00
Lang: es
Slug: github-copilot-y-el-blanqueo-de-código-abierto
Tags: aprendizaje automático, GitHub, Microsoft, software libre
Title: GitHub Copilot y el blanqueo de código abierto

<span style="text-decoration:underline">Este artículo es una traducción del artículo «[GitHub Copilot and open
source
laundering](https://drewdevault.com/2022/06/23/Copilot-GPL-washing.html)»
publicado por Drew Devault bajo la licencia [CC BY-SA
2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.es).</span>

*Aviso: soy el fundador de una empresa que compite con GitHub. También
soy un desarrollador de y defensor desde hace mucho tiempo del
<i style="font-style: normal" lang="en">software</i> libre, con un amplio conocimiento de las
licencias y la filosofía del <i style="font-style: normal" lang="en">software</i> libre. No voy a
nombrar a mi empresa en esta publicación para reducir el alcance de mi
conflicto de interés.*

Hemos visto una explosión del aprendizaje automático en la última
década, junto a la explosión en la popularidad del
<i lang="en">software</i> libre. Al mismo tiempo que el <i>software</i>
libre ha dominado el <i lang="en">software</i> y ha encontrado su lugar
en casi todos los nuevos productos de <i lang="en">software</i>, el
aprendizaje automático ha aumentado dramáticamente en sofisticación,
facilitando interacciones más naturales entre humanos y ordenadores. Sin
embargo, pese a su auge paralelo en la computación, estos dos dominios
permanecen filosóficamente distantes.

Aunque algunas empresas llamadas con nombres osados podrían sugerir lo
contrario, el área del aprendizaje automático no ha disfrutado de
casi ninguna de las libertades promovidas por el movimiento del
<i lang="en">software</i> libre y de código abierto. Gran parte del
código con relación con el aprendizaje natural está disponible
públicamente, y hay muchos artículos de acceso abierto disponibles para
que los lea cualquiera. <!-- more -->Sin embargo, la clave para el aprendizaje
automático es el acceso a conjuntos de datos de gran calidad y a
muchísimo poder computacional para procesar esos datos, y estos dos
recursos todavía están guardados bajo llave por casi todos los
participantes de este área[^1].


[^1]: Hay que dar las gracias a Mozilla Common Voice, una de las pocas
  excepciones a esta regla, que es un excelente proyecto que ha
  producido un conjunto de datos de muestras de voz de gran calidad y
  disponible gratuitamente y lo ha utilizado para desarrollar modelos y
  programas libres para la conversión de texto en voz y el
  reconocimiento del habla.

La barrera esencial de entrada para proyectos de aprendizaje automático
es superar estos dos problemas, que normalmente son muy difíciles de
conseguir. Para producir un conjunto de datos de alta calidad bien
catalogado se requieren generalmente miles de horas de trabajo[^2], una
tarea que puede llegar a costar millones de dólares. Por tanto,
cualquier enfoque que baje esta cifra es muy deseable, incluso si el
coste es hacer compromisos éticos. Con Amazon toma la forma de
explotación de la economía de los pequeños encargos. Con GitHub toma la
forma de incumplir los términos de las licencias de
<i lang="en">software</i> libre. En este proceso han creado una
herramienta que facilita el blanqueo a gran escala de
<i lang="en">software</i> libre como <i lang="en">software</i> no libre
para sus clientes, a quienes GitHub ofrece negación plausible mediante
un algoritmo inescrutable.

[^2]: Normalmente trabajo explotado de países poco desarrollados que la
  industria tecnológica suele pretender que no roza la esclavitud.

El <i lang="en">software</i> libre no es un regalo sin condiciones.
Existen condiciones para su uso y reutilización. Incluso las denominadas
licencias de <i lang="en">software</i> «liberales» imponen requisitos de
reutilización, como la atribución. Citando la licencia de MIT:

> Por la presente se concede el permiso [...] sujeto a las siguientes
> condiciones:

> El anterior aviso de copyright y este aviso de permiso se incluirán
> en todas las copias o partes sustanciales del Software.

O la igualmente «liberal» licencia de BSD:

> Se permite la redistribución y el uso en forma de código fuente y
> binario, con o sin modificación, siempre que se cumplan las siguientes
> condiciones:

> Las redistribuciones del código fuente deben conservar el aviso de
> derechos de autor anterior, esta lista de condiciones y el siguiente
> descargo de responsabilidad.

Al otro lado del espectro, las licencias <i lang="en">copyleft</i>, como
la Licencia Pública General de GNU o la Licencia Pública de Mozilla, van
más allá y no solo les exigen atribución a las obras derivadas, sino
que estas *también se publiquen* con la misma licencia. Citando la GPL:

> Puede transmitir un trabajo basado en el Programa, o las
> modificaciones para producirlo a partir del Programa, en forma de
> código fuente bajo los términos de la sección 4, siempre que también
> cumpla todas estas condiciones:

> [...]

> Debe licenciar el trabajo completo, en su totalidad, bajo esta
> Licencia a cualquier persona que entre en posesión de una copia.

Y la Licencia Pública de Mozilla:

> Toda distribución del Software Amparado en Forma de Código Fuente,
> incluyendo cualquier Modificación que Usted cree o a la que
> contribuya, debe realizarse bajo los términos de esta Licencia. Debe
> informar a los destinatarios de que la Forma de Código Fuente del
> Software Cubierto se rige por los términos de esta Licencia, y cómo
> pueden obtener una copia de la misma. Usted no puede intentar alterar
> o restringir los derechos de los destinatarios en la Forma de
> Código Fuente.

Las licencias de <i lang="en">software</i> libre imponen obligaciones a
los usuarios mediante los términos que cubren la atribución, el
sublicenciamiento, la distribución, patentes, marcas registradas y
relaciones con leyes como la Digital Millennium Copyright Act. La
comunidad del <i lang="en">software</i> libre no es desconocedora de las
dificultades para hacer cumplir estas obligaciones, que algunos grupos
ven demasiado onerosas. Pero por lo onerosas que le parezcan estas
obligaciones a alguno, uno está, no obstante, obligado a cumplirlas. Si
crees que la fuerza del derecho de autor debería proteger tu programa
privativo, entonces tienes que estar de acuerdo en que protege
igualmente las obras de código abierto, a pesar de la inconveniencia o
coste asociados con esta verdad.

El Copilot de GitHub está entrenado con programas que se rigen por estos
términos, y falla al respetarlos, y permite que clientes accidentalmente
fallen al respetar ellos mismos estos términos. Algunos argumentan sobre
los riesgos de una «sorpresa <i lang="en">copyleft</i>», en la que
alguien incorpora un trabajo licenciado por la GPL en su producto y se
sorprende al darse cuenta de que está obligado a publicar su producto
bajo los términos de la GPL también. Copilot institucionaliza este
riesgo, y cualquier usuario que desee usarlo para desarrollar
<i lang="en">software</i> no libre debería ser bien recomendado para no
hacerlo, o sino podría verse obligado a cumplir esos términos, quizás en
última instancia a liberar sus obras bajo los términos de una licencia
que no es deseable para sus objetivos.

En esencia, la discusión se reduce a si el modelo constituye o no una
obra derivada de sus entradas. Microsoft argumenta que no lo es. Sin
embargo, estas licencias no son específicas en cuanto a los medios de
derivación; el enfoque clásico de copiar y pegar de un proyecto a otro
no tiene por qué ser el único medio para que se apliquen estos términos.
El modelo existe como resultado de la aplicación de un algoritmo a estas
entradas, por lo que el propio modelo es una obra derivada de sus
entradas. El modelo, utilizado después para crear nuevos programas,
transmite sus obligaciones a esas obras.

Todo esto supone la mejor interpretación del argumento de Microsoft, con
una fuerte dependencia del hecho de que el modelo se convierte en un
programador de propósito general, habiendo aprendido significativamente
de sus entradas y aplicando este conocimiento para producir un trabajo
original. Si un programador humano adoptara el mismo enfoque, estudiando
<i lang="en">software</i> libre y aplicando esas lecciones, pero no el código en sí, a
proyectos originales, yo estaría de acuerdo en que su conocimiento
aplicado no está creando obras derivadas. Sin embargo, no es así como
funciona el aprendizaje automático. El aprendizaje automático es
esencialmente un motor glorificado de reconocimiento y reproducción de
patrones, y no representa una auténtica generalización del proceso de
aprendizaje. Quizás sea capaz de una cantidad limitada de originalidad,
pero también es capaz de degradarse al simple caso de copiar y pegar. He
aquí un ejemplo de Copilot reproduciendo, textualmente, una función que
se rige por la GPL, y que por tanto se regiría por sus términos:

<!-- more -->

<figure>
    <video src="/video/copilot-squareroot.mp4" muted autoplay controls></video>
    <figcaption class="wp-caption-text">Fuente: <a href="https://nitter.snopyta.org/mitsuhiko/status/1410886329924194309">Armin Ronacher</a> via Twitter</figcaption>
</figure>

La licencia reproducida por Copilot no es correcta, ni en su forma ni en
su funcionamiento. Este código no fue escrito por V. Petkov y la GPL
impone obligaciones mucho más fuertes que las sugeridas por el
comentario. Este pequeño ejemplo fue provocado deliberadamente con una
sugerencia de entrada (esta famosa función es conocida como la «[raíz
cuadrada inversa
rápida](https://es.wikipedia.org/wiki/Algoritmo_de_la_ra%C3%ADz_cuadrada_inversa_r%C3%A1pida)»),
y el «float Q_», pero no es exagerado suponer que
alguien puede hacer accidentalmente algo similar con cualquier
descripción en inglés particularmente desafortunada de su objetivo.

Por supuesto, el uso de una sugerencia para convencer a Copilot de que
imprima código con licencia GPL sugiere otro uso: blanquear
deliberadamente el código fuente libre. Si el argumento de Microsoft se
mantiene, lo único que se necesita para eludir legalmente una
licencia de <i lang="en">software</i> libre es enseñar a un algoritmo de aprendizaje
automático a regurgitar una función que se quiere utilizar.

Esto es un problema. Tengo dos sugerencias que ofrecer a dos audiencias:
una para GitHub y otra para los desarrolladores de <i lang="en">software</i> libre que
están preocupados por Copilot.

A GitHub: este es tu momento de Oracle contra Google. Habéis invertido
en la construcción de una plataforma sobre la que se construyó la
revolución del código abierto, y aprovechar esta plataforma para esta
jugada es una profunda traición a la confianza de la comunidad. La
ley se aplica a vosotros, y el hecho de que la comunidad de código abierto
descentralizada no sea capaz de montar un desafío legal eficaz a vuestra
caja de guerra de 7,5 mil millones de dólares de Microsoft no cambia
esto. La comunidad de código abierto está asombrada, y el asombro se
está convirtiendo lenta pero seguramente en rabia, ya que nuestras
preocupaciones caen en saco roto y seguís adelante con el
lanzamiento de Copilot. Espero que si la situación no cambia, os
encontréis con un grupo lo suficientemente motivado como para desafiar
esto. La legitimidad del entorno del <i lang="en">software</i> libre
puede depender de este problema, y hay muchas empresas que están
incentivadas económicamente para que esta legitimidad se mantenga.
Sin duda estoy dispuesto a unirme a una demanda colectiva como
mantenedor, o junto a otras empresas con intereses en el <i lang="en">software</i> libre
haciendo uso de nuestros recursos financieros para facilitar una
demanda.

La herramienta puede ser mejorada, probablemente todavía a tiempo para
evitar los efectos más perjudiciales (es decir, perjudiciales para
vuestro negocio) de Copilot. Ofrezco las siguientes sugerencias
concretas:

1. Permitir a los usuarios y repositorios de GitHub optar por no ser
   incorporados al modelo. Mejor, permitirles que opten por
   incorporarse. No vincular este centinela a proyectos no relacionados
   como Software Heritage e Internet Archive.
2. Hacer un seguimiento de las licencias de <i lang="en">software</i> que se incorporan al
   modelo e informar a los usuarios de sus obligaciones con respecto a
   esas licencias.
3. Eliminar por completo el código con <i lang="en">copyleft</i> del
   modelo, a menos que queráis que el modelo y su código de apoyo sean
   también <i lang="en">software</i> libre.
4. Considerar la posibilidad de compensar a los propietarios de los
   derechos de autor de los proyectos de <i lang="en">software</i> libre
   incorporados al modelo con un margen de las tarifas de uso de
   Copilot, a cambio de una licencia que permita este uso.

Probablemente haya que desechar el modelo actual. El código GPL
incorporado en él da derecho a cualquiera que lo utilice a recibir una
copia bajo GPL del modelo para su propio uso. Da derecho a estas
personas a un uso comercial, a construir un producto de la competencia
con él. Pero, presumiblemente, también incluye obras bajo licencias
incompatibles, como la CDDL, lo que es... problemático. Todo el asunto
es un lío legal.

No puedo hablar por el resto de la comunidad que se ha visto perjudicada
por este proyecto, pero por mi parte, estaría de acuerdo en no buscar
las respuestas a ninguna de estas preguntas con vosotros en los
tribunales si accedéis a resolver estos problemas ahora.

Y mi consejo para los mantenedores de <i lang="en">software</i> libre que están
cabreados porque sus licencias están siendo ignoradas. En primer lugar,
no uséis GitHub y vuestro código no entrará en el modelo (por ahora). [He
escrito antes](/es-importante-que-el-software-libre-use-infraestructuras-de-software-libre/) sobre por qué es normalmente importante para los
proyectos de <i lang="en">software</i> libre utilizar la infraestructura de <i lang="en">software</i>
libre, y esto solo refuerza ese hecho. Además, el viejo enfoque de «vota
con tu cartera» es una buena manera de mostrar vuestra disconformidad. Dicho
esto, si se os ocurre que en realidad *no* pagáis por GitHub, entonces
podéis tomaros un momento para considerar si los incentivos creados por
esa relación explican este desarrollo y pueden llevar a resultados más
desfavorables para ti en el futuro.

También podéis tener la tentación de resolver este problema cambiando
vuestras licencias de <i lang="en">software</i> para prohibir este comportamiento. Diré por
adelantado que, según la interpretación de Microsoft de la situación
(invocando el uso justo), no les importa qué licencia utilices:
utilizarán tu código a pesar de todo. De hecho, se ha descubierto que se
ha incorporado [código propietario](https://nitter.snopyta.org/ChrisGr93091552/status/1539731632931803137) en el modelo. Sin embargo, sigo
apoyando vuestros esfuerzos para abordar esto en vuestras licencias de <i lang="en">software</i>,
ya que proporciona una base legal aún más fuerte sobre la que podemos
rechazar Copilot.

Os advertiré de que la forma de enfocar esa cláusula de vuestra licencia
es importante. Siempre que escribáis o cambiéis una licencia de <i lang="en">software</i>
libre y de código abierto, debéis considerar si seguirá siendo libre o de
código abierto después de vuestros cambios. Para ser específicos, una
cláusula que prohíba rotundamente el uso de vuestro código para entrenar un
modelo de aprendizaje automático hará que vuestro <i lang="en">software</i> sea *no libre*, y no
recomiendo este enfoque. En su lugar, yo actualizaría vuestras licencias para
aclarar que la incorporación del código en un modelo de aprendizaje
automático se considera una forma de trabajo derivado, y que los
términos de su licencia se aplican al modelo y a cualquier trabajo
producido con ese modelo.

En resumen, creo que GitHub Copilot es una mala idea tal y como está
diseñado. Representa un flagrante desprecio de las licencias de <i lang="en">software</i>
libre en sí mismo, y permite un desprecio similar &mdash;deliberado o
no&mdash; entre sus usuarios. Espero que presten atención a mis
sugerencias, y espero que mis palabras a la comunidad del <i lang="en">software</i> libre
ofrezcan algunas formas concretas de avanzar respecto a este problema.
