Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2016-11-30 16:48
Image: <img src="/wp-content/uploads/2016/11/Screenshot-at-2016-11-30-154617-825x510.png" width="825" height="510" class="attachment-post-thumbnail size-post-thumbnail wp-post-image" alt="Impresiones sobre la versión alfa de Trisquel 8">
Lang: es
Modified: 2017-02-25 20:45
Slug: impresiones-sobre-la-version-alfa-de-trisquel-8-flidas
Status: published
Tags: Flidas, máquina virtual, Trisquel, Trisquel 8, Trisquel Flidas, virtualización
Title: Impresiones sobre la versión alfa de Trisquel 8

El desarrollador principal de Trisquel, Rubén Rodríguez, publicó ayer la
primera versión de prueba de Trisquel 8. En [el mensaje
publicado](https://listas.trisquel.info/pipermail/trisquel-devel/2016-November/001023.html)
en la lista de correo para el desarrollo de Trisquel
([trisquel-devel](https://listas.trisquel.info/mailman/listinfo/trisquel-devel)),
se puede encontrar un enlace hacia la página de descargas de las
versiones de prueba (no lo publico aquí porque seguramente esté durante
poco tiempo).

Sin dudar he decidido probarla. Este artículo lo estoy escribiendo desde
una máquina virtual con Trisquel 8.

El entorno de escritorio por defecto de Trisquel 8 será MATE. La
apariencia de esta nueva versión cambia un poco, pero no
demasiado.<!-- more -->

Por ahora solo puedo quejarme de que el escritorio no me muestra los
archivos que he creado y de que faltan algunos programas, como un
programa de edición de imágenes (suele ser GIMP), un programa de
mensajería, un cliente de correo electrónico, un reproductor de vídeo...

<figure style="width: 1024px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/11/Screenshot-at-2016-11-30-161017.png"><img class="size-full wp-image-568" src="/wp-content/uploads/2016/11/Screenshot-at-2016-11-30-161017.png" alt="Los archivos no aparecen en el escritorio" width="1024" height="768" srcset="/wp-content/uploads/2016/11/Screenshot-at-2016-11-30-161017.png 1024w, /wp-content/uploads/2016/11/Screenshot-at-2016-11-30-161017-300x225.png 300w, /wp-content/uploads/2016/11/Screenshot-at-2016-11-30-161017-768x576.png 768w" sizes="(max-width: 1024px) 100vw, 1024px" /></a><figcaption class="wp-caption-text">Los archivos no aparecen en el escritorio</figcaption></figure>

En este primer contacto con Trisquel 8 no he quedado decepcionado. Como
primera versión alfa, no está nada mal. Espero que la versión estable
sea aún mejor y solucione de la mejor manera posible las carencias y los
problemas de la versión 7 de Trisquel.
