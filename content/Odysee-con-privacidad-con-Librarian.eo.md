Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2022-06-18 12:00
Modified: 2023-11-30 19:00
Lang: eo
Slug: odysee-con-privacidad-con-librarian
Save_as: Odysee-privatece-per-librarian/index.html
URL: Odysee-privatece-per-librarian/
Tags: Ĝavoskripto, Odysee, privateco, YouTube
Title: Odysee privatece per librarian
Image: <img src="/wp-content/uploads/2022/06/librarian.svg" alt="">

Odysee estas retejo de videoj, kiu uzas liberan protokolon nomitan LBRY,
kiu uzas blokĉenon por kunhavigi dosierojn kaj rekompenci kreantojn per
sia propra cifereca valuto. Per LBRY videoj ne povas esti cenzuritaj,
kiel okazas en multaj nunaj retejoj.

La problemo estas, ke la retejo Odysee ne multe respektas la privatecon
kaj postulas Ĝavoskripton &mdash; kvankam ĝia kodo [estas
libera](https://github.com/OdyseeTeam/odysee-frontend) &mdash;. Kiel
alternativo estas [librarian](https://codeberg.org/librarian/librarian).
Rekomendinda nodo estas <https://lbry.projectsegfau.lt/>, sed ankaŭ [estas
aliaj](https://codeberg.org/librarian/librarian#instances).

