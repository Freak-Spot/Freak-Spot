Author: Хорхе Мальдонадо Вентура
Category: Кинематограф
Date: 2017-07-05 22:53
Image: <img src="/wp-content/uploads/2017/07/caracol.png" alt="" width="1366" height="768" srcset="/wp-content/uploads/2017/07/caracol.png 1366w, /wp-content/uploads/2017/07/caracol-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
Lang: ru
Slug: caracoles-cortometraje
Tags: Audacity, Blender, антиутопия, свободная культура, Garage Band, GIMP, Krita, короткометражный фильм, сюрреализм, Synfig
Save_as: ulitki-antiutopičeskij-korotkometražnyj-film/index.html
URL: ulitki-antiutopičeskij-korotkometražnyj-film/
Title: Улитки: антиутопический короткометражный фильм

<!-- more -->

<video controls poster="/wp-content/uploads/2017/07/caracol.png">
  <source src="/temporal/caracoles.mp4" type="video/mp4">
  <p>К сожалению, ваш браузер не поддерживает HTML 5. Пожалуйста,
  воспользуйтесь другим или обновите свой текущий браузер</p>
</video>

Пепе - молодой взрослый человек, который после работы, устав за день,
хочет отправиться домой, чтобы отдохнуть. В своем антиутопическом мире,
полном сюрреализма, они садятся на своеобразный автобус-улитку,
где неизбежно погружается в размышлениях.

Видео распространяется под лицензией <a href="https://creativecommons.org/licenses/by/3.0/"><abbr title="Creative Commons Attribution 3.0 International">CC BY 3.0</abbr></a> и опубликовано с
разрешения [Gobin Refuge](https://goblinrefuge.com/mediagoblin/u/guayabitoca/m/caracoles-snails/).
