Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-03-07 10:20
Lang: de
Slug: bloquear-sitios-web-etchosts
Save_as: Webseiten-blockieren-hosts-Datei/index.html
URL: Webseiten-blockieren-hosts-Datei/
Tags: Anzeigen, blockieren, GNU/Linux, Webseiten, Privatsphäre
Title: Webseiten blockieren: <code>hosts</code>-Datei
Image: <img src="/wp-content/uploads/2023/03/hosts-Datei-blockieren.png" alt="" width="960" height="780" srcset="/wp-content/uploads/2023/03/hosts-Datei-blockieren.png 960w, /wp-content/uploads/2023/03/hosts-Datei-blockieren-480x390.png 480w" sizes="(max-width: 960px) 100vw, 960px">


Um Websites zu blockieren, kannst du eine Browsererweiterung (z. B.
[Block Site](https://add0n.com/block-site.html)) oder einen [Proxyserver](https://de.wikipedia.org/wiki/Proxy_(Rechnernetz))
(z. B. [Squid](https://de.wikipedia.org/wiki/Squid)) verwenden. Es gibt aber
auch die Möglichkeit, die `hosts`-Datei zu bearbeiten, eine Methode, die
sehr wenig Arbeitsspeicher verbraucht und im Gegensatz zur
Browsererweiterung bei jedem Browser oder Programm funktioniert,
<!-- more -->das eine Verbindung zum Internet herstellt[^1].

## Wie funktioniert diese Methode?

Füg einfach Zeilen im folgendem Format in die `hosts`-Datei
(`/etc/hosts` in GNU/Linux) mit den Seiten ein, die du blockieren
möchtest:

    :::text
    0.0.0.0 beispiel.com

Diese Zeile führt dazu, dass jedes Mal, wenn du versuchst, dich mit der
Seite `beispiel.com` zu verbinden, du stattdessen mit der IP-Adresse
`0.0.0.0` verbunden wirst, die eine nicht routingfähige Adresse ist, die
verwendet wird, um ein unbekanntes ungültiges oder nicht anwendbares
Ziel zu bezeichnen. Das Ergebnis nach dem Neustart des Netzwerkdienstes
(`sudo systemctl restart NetworkManager` in Debian) und des Webbrowsers
ist wie folgt:

<a href="/wp-content/uploads/2023/03/hosts-Datei-blockieren.png">
<img src="/wp-content/uploads/2023/03/hosts-Datei-blockieren.png" alt="" width="960" height="780" srcset="/wp-content/uploads/2023/03/hosts-Datei-blockieren.png 960w, /wp-content/uploads/2023/03/hosts-Datei-blockieren-480x390.png 480w" sizes="(max-width: 960px) 100vw, 960px">
</a>

Es ist auch möglich, auf eine andere Website umzuleiten, obwohl die
meisten Browser die Website nicht anzeigen, da dies in der Vergangenheit
für [Phishing](https://de.wikipedia.org/wiki/Phishing) genutzt wurde.

Wenn du viele Websites sperren willst, ist es nicht sinnvoll, Hunderte
von Seiten manuell hinzuzufügen. Deshalb gibt es Menschen und Projekte,
die Listen von Websites zusammenstellen. Ein Beispiel ist die [von Steven
Black verwaltete Liste](https://github.com/StevenBlack/hosts/), die
Sperrlisten nach Themenbereichen wie Pornografie, Falschnachrichten und
Glücksspiel bereitstellt. Kopiere einfach die gewünschte Liste und füg
sie in Ihre Hosts-Datei ein.

[^1]: Diese Methode funktioniert nicht für Programme, die sich über Tor
    mit dem Internet verbinden, da der DNS innerhalb des Tor-Netzwerks
    aufgelöst wird.
