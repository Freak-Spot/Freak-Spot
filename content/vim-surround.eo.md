Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Tekstoprilaboro
Date: 2023-02-19 18:58
JS: asciinema-player.js (top)
Lang: eo
Slug: vim-surround
Tags: tekstoprilaboro sen aranĝo, redaktiloj, kromaĵo, Neovim, PHP, Vim, vim-surround
Title: <code>surround.vim</code>

<!-- more -->

<asciinema-player id="video" src="../../asciicasts/89325.json">Pardonu,
asciinema-player ne funkcias sen Ĝavoskripo.
</asciinema-player>

Kiam mi programas kelkfoje mi bezonas ŝanĝi signojn aŭ etikedojn (en
marklingvoj), kiuj ĉirkaŭas komandojn. Ekzemple en Python kelkfoje mi
devas anstataŭigi duoblajn citilojn per simplaj citiloj; kio estas iom
malkomforta, ĉar vi devas iri al la komencaj kaj al la finaj citiloj kaj
anstataŭigi ilin unu post la alia.

Por ĉi tiu problemo estas solvo: la kromaĵo
<a href="ttp://www.vim.org/scripts/script.php?script_id=1697"><code>surround.vim</code></a>
por la redaktiloj Vim kaj Neovim. Per ĝi mi povas tre facile anstataŭigi
la signojn aŭ etikedojn. Por anstataŭigi `print("Hello, world!")` per
`print('Hello, world!')` sufiĉas premi `cs"'` en la normala reĝimo ene
de la citiloj. Mi ankaŭ povas anstataŭigi la citilojn per etikedoj de
marklingvoj kiel HTML kaj inverse. Ekzemple mi povas anstataŭigi
`'Hello, world!'` per `<h1>Hello, world!</h1>` skribante `cs'<h1>`.

La komando por anstataŭigi povas ŝajni malfacile memorebla, sed ĝi ne
estas, se vi scias la anglan kaj pensas, ke `cst"` intencas diri
<i lang="en">change surrounding tag [with] "</i> kaj `cs"'` intencas
diri <i lang="es">change surrounding " with '</i>.

Krome ĉi tiu kromaĵo ebligas forigi ĉirkaŭ kie ni troviĝas. Ekzemple,
se vi volas forigi la citilojn en `print('Hello, world!')`, vi nur devas
skribi `ds'`.

Ni ankaŭ povas aldoni HTML-etikedojn ĉirkaŭ vorto aŭ vortoj. Skribi
`ysiw<em>` en normala reĝimo kun la kursoro ene de la vorto «saluton»
ĉirkaŭas la vortojn per la etikedo `em`: `<em>saluton</em>`. `yss)`
ĉirkaŭas per rondaj krampoj linion, kie la kursoro troviĝas. Por havi
pli da kontrolo pri la ĉirkaŭota teksto oni povas uzi la vidan reĝimon.
Kiam vi havu iun tekston elektitan, premu <kbd>S</kbd> kaj skribu la
signon, etikedon aŭ signojn por ĉirkaŭi la tekston.

Sed kion fari, se vi volas aldoni etikedon por PHP (ĝia sintakso estas
`<?php kodo?>`)?. Por fari tion ni devas aldoni la jenan linion al nia
agorda dosiero de Vim (`.vimrc`): `autocmd FileType php let
b:surround_45 = "<?php \r ?>` Per ĉi tio ni povas ĉirkaŭi kelkajn kodajn
liniojn per la etikedoj de PHP, elektante tekston en vida reĝimo kaj
premante `S-`, se ni prilaboras dosieron skribitan per PHP. Ni povas
ankaŭ cirkaŭi linion en normala reĝimo premante `yss-`.

Sekve estas skribitaj ekzemploj de tio, kion vi povas fari per
`surround.vim`. La signo markita en la kolumno de la origina teksto
reprezentas la kursoran pozicion. Memoru, ke la kursoro devas troviĝi
inter la citiloj aŭ etikedoj, kiujn vi volas anstataŭigi. Vi povas vidi
kiel ili funkcias [en la video, kiun mi montris al vi en la komenco de
ĉi tiu artikolo](#video).


Origina teksto                          | Komando      | Rezulto
----------------------------------------|--------------|--------------------------------------
"Look ma, I'm <mark>H</mark>TML!"       | cs"&lt;q&gt; | &lt;q&gt;Look ma, I'm HTML!&lt;/q&gt;
if <mark>x</mark>&gt;3 {                | ysW(         | if ( x&gt;3 ) {
my $str = <mark>w</mark>hee!;           | vllllS'      | my $str = 'whee!';
&lt;div&gt;Yo!<mark>&lt;</mark>/div&gt; | dst          | Yo!
Hello w<mark>o</mark>rld!               | yssB         | \{Hello world!\}
