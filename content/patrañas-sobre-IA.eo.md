Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2023-09-11 16:40
Lang: eo
Slug: patrañas-sobre-IA
Save_as: mensogoj-pri-AI/index.html
URL: mensogoj-pri-AI/
Tags: veziko, kapitalismo, konsiloj, AI
Title: Mensogoj pri AI

Estas veziko de aŭtomata lernado, sed tiu teknologio restos. Kiam la
veziko eksplodu, la mondo *estos* ŝanĝita per la aŭtomata lernado. Sed
verŝajne ĝi estos pli aĉa, ne pli bona.

Kontraŭe al la atendoj de pesimistegaj AI-antaŭdiristoj, la mondo ne
detruiĝos pli rapide danke al la AI. Nuntempaj progresoj en maŝinlernado
vere ne proksimigas nin al forta AI, kaj kiel Randall Monroe jam
rimarkis en 2018:

<a href="/wp-content/uploads/2023/09/robota-estonteco.png">
<img src="/wp-content/uploads/2023/09/robota-estonteco.png" alt="Nun. AI iĝas sufiĉe altnivela por kontroli nehaltigeblajn svarmojn da murdistaj robotoj. La Parto, pri kiu mi zorgas. AI iĝas memkonscia kaj ribelas kontraŭ homa kontrolo. La parto, pri kiu ŝajne multaj homoj zorgas" width="1223" height="412" srcset="/wp-content/uploads/2023/09/robota-estonteco.png 1223w, /wp-content/uploads/2023/09/robota-estonteco-611x206.png 611w" sizes="(max-width: 1223px) 100vw, 1223px">
</a>

Tio kio okazos al la AI estas la enuiga malnova kapitalismo. Ĝia kapablo
konservi la povon venos el la anstataŭigado de kompetentaj, multekostaj
homoj per aĉaj malmultekostaj robotoj. Grandegaj lingvaj modeloj estas
bonega progreso supera al Markov-ĉenoj, kaj Stable Diffusion povas
generi bildojn, kiuj estas nur iom strangaj per sufiĉa ŝanĝado de la
komandoj. Mezbonaj programistoj uzos GitHub Copilot por skribi bagatelan
kaj ripeteman kodon (bagatela kodo estas taŭtologie neinteresa), kaj la
maŝina lernado verŝajne restos utila por skribi kandidatigan leteron por
vi. La senŝoforaj aŭtoj povus alveni Iun Baldaŭan Tagon™, kiu bonegos por
sciencfikciaj admirantoj kaj teknokratoj, sed multe pli malbonos en ĉiuj
flankoj ol, ekzemple, [konstrui pli da trajnoj](https://yewtu.be/watch?v=0dKrUE_O0VE).

La plej grandaj daŭraj ŝanĝoj fare de maŝina lernado estos pli similaj
al tiuj ĉi:

- Malpliiĝo de la laborforto por spertaj kreivaj laboroj.
- La tuta forigo de homoj en klient-asistadaj roloj.
- Pli konvinka spamaj kaj trompaj enhavoj, pli skaleblaj fraŭdoj.
- Serĉil-optimimugitaj amasoj da enhavo, kiu superas en la serĉrezultoj.
- Amase generitaj libroj (kaj duumaj kaj paperaj), kiuj troos en la
  merkato.
- AI-generitaj enhavoj superŝutos la sociajn retejojn.
- Ĉieaj propagando kaj ŝajnigado, kaj en la politiko kaj reklamado.

La AI-firmaoj daŭre kaj amase produktos rubon kaj
CO<sub>2</sub>-emisiojn, ĉar ili agreseme eltiras ĉiun Interretan
enhavon, kiun ili povas trovi, eksterigante kostojn sur la mondan
ciferecan infrastrukturon, kaj nutras per tiu hamstrado la
servilaron de grafikaj procesoroj por generi siajn modelojn. Ili povus
lasi al homoj ian povon por helpi ordigi la enhavon, serĉante la plej
malmultekostaj merkatoj kun la plej malfortaj laborleĝoj por konstrui
hom-ekspluatantajn laborejojn por nutri la AI-datenmonstro.

Vi neniam fidos alian produktan recenzon. Vi neniam parolos denove al
homo en firmao, kiu provizas retkonekton. Sengustaj kaj koncizaj
enhavoj plenigos la ciferecan mondon ĉirkaŭ vi. Teknologio kreita por
engaĝigantaj amasoj da serviloj &mdash; tiuj videoj redaktitaj per AI
kun kraketa maŝina voĉo, kiun vi lastatempe vidis en viaj fluoj &mdash;
estos komercigata kvazaŭ blankan markon kaj uzata por amase promocii
produktojn kaj ideologiojn per malgrandega kosto el kontoj de sociaj
retejoj, kiuj estos plenigitaj per AI-enhavo, kulturas publikon kaj
konformos kun la algoritmo.

Ĉiuj tiuj aferoj jam okazas kaj pli malboniĝos. La estonteco de la
enhavo estas sengusta kaj senanima regurgitado de ĉiuj enhavoj, kiuj
aperis antaŭ la AI-epoko, kaj la destino de ĉiuj novaj kreemaj enhavoj
estas la submetado en la matematika malklariganta amaso.

Tio estos ege profitdona por la AI-magnatoj, kaj por sekurigi ilian
investon ili instigis grandegan kaj multekostan tutmondan
propagandkampanjon. Por la publiko, la nunaj kaj eblaj estontaj kapabloj
de la teknologio estas troigita per egaj promesoj ridinde neeblaj. En
fermitaj kunvenoj oni faras multe pli realigeblajn promesojn duone
malpliigi la buĝetojn.

La propagando ankaŭ apogas sin sur la mistika sciencfikcia kanono: la
minaco de inteligentaj komputiloj kun povo fini la mondon, la
malpermesita allogo de nova Projekto Manhattan kaj ĉiuj ĝiaj sekvoj, la
longe antaŭdirita singulareco. La teknologio eĉ ne proksimas al la ĉi
tiu nivelo, sed la iluzio estas konservita pro la intereso influi la
leĝfaristojn por helpi la magnatojn starigi muron ĉirkaŭ ilia nova
industrio.

Kompreneble AI prezentas perfortan minacon, sed kiel Randall rimarkas,
ne devenas de la AI mem, sed de la homoj, kiuj aplikas ĝin. La usona
armeo testas AI-kontrolitajn senpilotajn aviadilojn, kiuj ne estos
memkonsciaj, sed ege pliigos la homajn erarojn (aŭ la homan malicon) ĝis
senkulpaj homoj mortu. AI-iloj jam estas uzataj por starigi
kaŭciojn kaj kondiĉajn liberecojn &mdash; ĝi povas meti vin en
malliberejon aŭ reteni vin tie. La polico uzas AI-n por vizaĝrekonado
kaj «antaŭvida policado». Kompreneble ĉiuj tiuj modeloj fine
diskriminacias minoritatojn, senigante ilin je libereco kaj ofte
mortigante ilin.

Agresema kapitalismo karakterizas la AI-n. La propaganda veziko estis
kreita de investantoj kaj kapitalistoj, kiuj investas monon en ĝi, kaj
la rendimento, kiun ili atendas de tiu investo, eliros el via poŝo. La
singulareco ne venos, sed la plej realismaj promesoj de AI malbonigos la
mondon. La AI-revolucio jam venis, kaj mi malŝatas ĝin.

<details style="margin-bottom: 1em; cursor: pointer;">
  <summary>Provoka mesaĝo</summary>
<p>Mi redaktis multe pli flamigan skizon pri ĉi tiu temo kun la titolo
«ChatGPT estas la nova tekno-ateista anstataŭo de Dio». Ĝi faras kelkajn
sufiĉe akrajn komparojn inter la sekto de ĉifrovalutoj kaj la sekto de
la maŝinlernado, kaj inter la religia, neŝanĝebla kaj ege ignoranta
fido, ke ambaŭ teknologioj estas la heroldoj de la progreso. Estis amuze
skribi ĝin, sed ĉi tio estas probable la plej bona artikolo.</p>
<p>
Mi trovis ĉi tiun komenton el Hacker News kaj citis ĝin en la originala
skizo: «Verŝajne indas paroli kun GPT4 antaŭ serĉi profesian helpon [por
trakti depresion]».
</p>
<p>Okaze, ke vi bezonu aŭdi tion ĉi: <a
href="https://www.euronews.com/next/2023/03/31/man-ends-his-life-after-an-ai-chatbot-encouraged-him-to-sacrifice-himself-to-stop-climate-">ne</a>
(averto: memmortigo) uzu la servojn de OpenAI por helpi kuraci vian
depresion. Trovi kaj aranĝi rendevuon kun terapiisto povas malfacili por
multaj homoj &mdash; kutimas, ke tio ŝajnu malfacile. Parolu kun viaj
amikoj kaj petu al ili helpon por trovi la ĝustan flegon por viaj
bezonoj.</p>
</details>

*Ĉi tiu artikolo estas traduko de la artikolo «[AI crap](https://drewdevault.com/2023/08/29/2023-08-29-AI-crap.html)»
publikigita de Drew Devault sub la [CC BY-SA 2.0](https://creativecommons.org/licenses/by-sa/2.0/deed.eo)-permesilo*.
