Author: Jorge Maldonado Ventura
Date: 2017-10-28 18:01
Lang: en
Modified: 2023-04-23 15:00
Slug: contacto
Status: published
Title: Contact

My email is
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net). Use my
<abbr title="GNU Privacy Guard">GPG</abbr> public key
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc))
so that other people cannot read the message.
