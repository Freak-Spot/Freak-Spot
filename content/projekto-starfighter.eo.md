Author: Jorge Maldonado Ventura
Category: Videludoj
Date: 2021-06-09
Modified: 2023-12-11 16:30
Lang: eo
Slug: projekto-starfighter
Tags: pafludo, videludo
Title: Pafludo <cite lang="en">Project: Starfighter</cite>
Image: <img src="/wp-content/uploads/2023/12/mi-ludas-Project-Starfighter.png" alt="" width="800" height="600" srcset="/wp-content/uploads/2023/12/mi-ludas-Project-Starfighter.png 800w, /wp-content/uploads/2023/12/mi-ludas-Project-Starfighter-400x300.png 400w" sizes="(max-width: 800px) 100vw, 800px">


<cite lang="en">Project: Starfighter</cite> estas pafludo kun historio kaj multaj niveloj. Kris
Benfeldo estas la ĉefrolulo, kiu devas batali kontraŭ ARMKORP, arma
korporacio, kiu regas la konatan galaksion per AI-veturiloj.

Vi povas instali la programon en distribuaĵoj bazitaj sur Debiano per la
sekva komando:

    :::bash
    sudo apt install starfighter

<a href="/wp-content/uploads/2023/12/Project-Starfighter-komenca-ekrano.png">
<img src="/wp-content/uploads/2023/12/Project-Starfighter-komenca-ekrano.png" alt="" width="800" height="600" srcset="/wp-content/uploads/2023/12/Project-Starfighter-komenca-ekrano.png 800w, /wp-content/uploads/2023/12/Project-Starfighter-komenca-ekrano-400x300.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

Vi povas elekti la ordon de la misioj flugante per veturilo al la
planedo, al kiu vi volas iri.

<a href="/wp-content/uploads/2021/06/Project-Starfighter-sunsistemo-Spirito.png">
<img src="/wp-content/uploads/2021/06/Project-Starfighter-sunsistemo-Spirito.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2021/06/Project-Starfighter-sunsistemo-Spirito.png 1440w, /wp-content/uploads/2021/06/Project-Starfighter-sunsistemo-Spirito-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">
</a>

Eblas agordi la facilecon de la ludo. **Facilega** kaj **facila** estas
bonaj elektoj por komencantoj.

<a href="/wp-content/uploads/2023/12/Project-Starfighter-batalo.png">
<img src="/wp-content/uploads/2023/12/Project-Starfighter-batalo.png" alt="" width="800" height="600" srcset="/wp-content/uploads/2023/12/Project-Starfighter-batalo.png 800w, /wp-content/uploads/2023/12/Project-Starfighter-batalo-400x300.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

La ludo havas [oficialan retejon](https://pr-starfighter.github.io/).

<video class="floating" controls="">
    <source src="https://pr-starfighter.github.io/video/starfighter_trailer.webm" type="video/webm">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.webm" type="video/webm">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.ogv" type="video/ogg">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.mp4" type="video/mp4">
    <a href="https://archive.org/details/ProjectStarfighterTrailer">Antaŭfilmo</a>
</video>
