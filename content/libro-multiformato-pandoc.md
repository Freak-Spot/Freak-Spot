Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-07-26 12:00
Lang: es
Slug: generar-libro-en-varios-formatos-con-Pandoc
Tags: libro, pandoc
Title: Generar libro en varios formatos con Pandoc
Image: <img src="/wp-content/uploads/2022/07/multiformato-pandoc.jpg" alt="" width="960" height="1280" srcset="/wp-content/uploads/2022/07/multiformato-pandoc.jpg 960w, /wp-content/uploads/2022/07/multiformato-pandoc-480x640.jpg 480w" sizes="(max-width: 960px) 100vw, 960px">

Un libro hoy en día puede estar disponible en varios formatos: HTML,
PDF, EPUB, libro impreso, etc. Lo ideal es que esté disponible en la
mayoría de formatos posibles para llegar al mayor número de personas
posible. Sin embargo, no es cómodo editar varios archivos cuando solo
queremos realizar un cambio. Para evitar ese trabajo tedioso podemos
usar un formato base y convertir de este a otros formatos con Pandoc.

Podemos escribir el libro con LibreOffice Writer o con un editor de
texto plano en formatos como HTML, LaTeX, Markdown, etc. Para convertir
ese archivo a otro formato bastaría con ejecutar un comando.

Para ilustrar como hacerlo todo te enseñaré cómo genero [este
libro](/libro/la-cultura-es-libre/). Como sistema
de control de versiones decidí usar Git y crear un [repositorio
público](https://notabug.org/jorgesumle/la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad). Puedes clonar el repositorio ejecutando los siguientes comandos:

    :::bash
    sudo apt install git
    git clone https://notabug.org/jorgesumle/la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad

Una vez clonado verás una carpeta llamada `markdown`, allí es donde se
encuentra el texto del libro en formato Markdown. Instala Pandoc y Make
ejecutando `sudo apt install make pandoc`. Basta con ejecutar `make
html` en la carpeta raíz del repositorio para generar el HTML; con el
formato EPUB hay que ejecutar `make epub`.

Hay pocas líneas de código para generar los formatos HTML y EPUB. Sin
embargo, para generar el PDF necesito instalar más dependencias y usar
más código para generar y editar un archivo LaTeX que luzca bien cuando
se convierta a PDF. Para ello yo he añadido código LaTeX personalizado
(se encuentra en la carpeta `latex_personalizado`) al archivo LaTeX con
el que genero el PDF. También es posible generar un archivo PDF bonito
usando CSS, pues se puede [elegir un generador de
PDF](https://pandoc.org/MANUAL.html#option--pdf-engine) basado en HTML
con la opción `--pdf-engine`.

La [documentación de Pandoc](https://pandoc.org/MANUAL.html) es un buen
punto de partida para ver cómo añadir metadatos y generar archivos de
salida cómo necesites. Si, como yo, optas por usar LaTeX para generar el
PDF, en Internet encontrarás documentación y personas que proponen
soluciones a problemas comunes.
