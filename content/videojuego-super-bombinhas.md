Author: Jorge Maldonado Ventura
Category: Videludoj
Date: 2021-11-05
Modified: 2021-11-07 22:12
Lang: eo
Slug: videludo-Super-Bombinhas
Tags: libera kulturo, platforma, videludo
Title: Videludo <cite lang="pt">Super Bombinhas</cite>
Image: <img src="/wp-content/uploads/2021/11/Super-Bombinhas.png" alt="">

Bombuloj estas la ĉefroluloj de ĉi tiu platforma ludo. Bomba Azul estas
la nura bombulo, kiu ne estis kaptita de la malbona Gaxlon. Vi devas
savi la aliajn bombulojn kaj la reĝon Aldanon. Ĉiu bombulo havas
specialan kapablecon: Bomba Amerela povas rapide kuri kaj salti pli ol
la aliaj, Bomba Verde povas eksplodi, la reĝo Aldano povas haltigi la
tempon, ktp. Post savi bombulon, vi povas ŝanĝi al ri dum la
aventuro.

En la ludo estas 7 regionoj tre malsimilaj. Ĉiu el ili havas ĉefan
malamikon. Vi devos inteligente uzi ĉiun bombulon por antaŭeniri.

La ludo ankaŭ havas nivelan redaktilon por krei novajn nivelojn.

![Nivela redaktilo](/wp-content/uploads/2021/11/Super-Bombinhas-level-editor.gif)

Ĉar bildoj esprimas pli ol mil vortojn, mi montras malsupre al vi
mallongan videon:

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2021/11/Super-Bombinhas.png" data-setup="{}">
  <source src="/video/super-bombinhas.webm" type="video/webm">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Vi povas instali la ludon en GNU/Linukso kaj Vindovo. Por instali ĝin en
distribuoj bazitaj sur Debiano, vi devas elŝuti la plej novan `.deb`-an
dosieron el la [eldona
paĝo](https://github.com/victords/super-bombinhas/releases) kaj plenumi
ĝin.
