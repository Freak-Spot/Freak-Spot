Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2020-02-19
Lang: es
Modified: 2020-10-08 11:11
Slug: consultar-instagram-con-software-libre-y-privacidad
Tags: Instagram, privacidad
Title: Consultar Instagram con <i lang="en">software</i> libre y privacidad
Image: <img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">

Instagram es una red social centralizada y que requiere el uso de
<i lang="en">software</i> privativo. Consultar Instagram con el navegador de forma
decente sin perder privacidad o libertad es prácticamente imposible... a
no ser que usemos otra interfaz, como
[Bibliogram](https://bibliogram.art/), la que describo en este artículo.

Bibliogram permite consultar publicaciones, perfiles de usuarios y
vídeos de [IGTV](https://es.wikipedia.org/wiki/IGTV_(Instagram_TV)). En
la página de inicio hay buscadores para publicaciones y usuarios.

<!-- more -->

<figure>
<a href="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png" alt="" width="1299" height="711" srcset="/wp-content/uploads/2020/02/página-principal-de-Bibliogram.png 1299w, /wp-content/uploads/2020/02/página-principal-de-Bibliogram-649x355.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Página de inicio de Bibliogram</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png" alt="" width="1299" height="714" srcset="/wp-content/uploads/2020/02/perfil-en-Bibliogram.png 1299w, /wp-content/uploads/2020/02/perfil-en-Bibliogram-649x357.png 649w" sizes="(max-width: 1299px) 100vw, 1299px">
</a>
    <figcaption class="wp-caption-text">Perfil en Bibliogram</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png">
<img src="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png" alt="" width="1920" height="1038" srcset="/wp-content/uploads/2020/10/publicación-en-Bibliogram.png 1920w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-960x519.png 960w, /wp-content/uploads/2020/10/publicación-en-Bibliogram-480x259.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>
    <figcaption class="wp-caption-text">Publicación en Bibliogram</figcaption>
</figure>

Como es <i lang="en">software</i> libre, puedes instalar Bibliogram en tu servidor (si
tienes uno) o usar el [Bibliogram de otra gente](https://git.sr.ht/~cadence/bibliogram-docs/tree/master/docs/Instances.md).
