Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2022-11-07 13:55
Lang: en
Slug: Cómo-destruir-Google
Save_as: how-to-destroy-Google/index.html
URL: how-to-destroy-Google/
Tags: direct action, ads, boycott, Google, privacy
Title: How to destroy Google
Image: <img src="/wp-content/uploads/2022/11/aniquilar-a-Google.png" alt="" width="960" height="540" srcset="/wp-content/uploads/2022/11/aniquilar-a-Google.png 960w, /wp-content/uploads/2022/11/aniquilar-a-Google-480x270.png 480w" sizes="(max-width: 960px) 100vw, 960px">

The Google business model is based on collecting personal data from
users, selling it to third parties and serving ads. The company also
[enganges in surveillance programs](https://en.wikipedia.org/wiki/PRISM),
[develops artificial intelligence programs for military purposes](/en/by-completing-a-Google-reCAPTCHA-you-are-helping-to-kill/) and [exploits its users](/en/como-explota-Google-con-CAPTCHAs/), among other things.

It is one of the most powerful companies on the planet. However, Google
is a giant with feet of clay that can be annihilated.

## Finish off its ad revenue

Google makes money by serving personalised ads based on the information
it collects from its users. If people don't see ads, Google doesn't make
money. Blocking ads is a way to prevent tracking and make Google lose
money, but if you visit Google's pages, Google will still get
information it can sell to advertisers. So the easiest thing to do is to
[block ads](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/)
and avoid Google sites.

Another idea is to click on all ads with the
[AdNauseam](https://adnauseam.io/) extension, which also hides them from
us so that we don't find them annoying. This method means that Google
makes less money from ad clicks and that Google's servers have a little
more workload (minimal, but it does add to their costs).

## Filling Google's servers with crap

Google lets you upload almost anything to their servers (videos, files,
etc.). If the content uploaded to its servers takes up a lot of space
and is junk that scares people away from its services (videos with robot
voices speaking nonsense, hundreds of videos with noise that take up
gigabytes upon gigabytes), the cost of maintaining the servers increases
and the company's profit is reduced.

If this is a globally coordinated effort by multiple users, Google would
have to start restricting uploads, hiring people to find junk videos,
blocking people and IP addresses, etc., which would increase its losses
and reduce its profits.

For example, I can create 15-minute videos every hour and upload them to
YouTube automatically or semi-automatically. The videos should take up a
lot of space. The more resolution, the more colours, the more sound
variety, the more frames per second, the more money YouTube will spend
to keep those videos on its servers.

The video I show below was generated automatically with `ffmpeg`. It is
only two seconds long, but it takes up 136 MB. A similar 15-minute video
would take 61.2 GB.

<!-- more -->

<video controls>
  <source src="/video/basura.mp4" type="video/webm">
  <p>Sorry, your browser doesn't support HTML 5. Please change
  or update your browser</p>
</video>

It would be necessary to make variations of random videos, junk videos
of robots talking in strange languages... At the same time users should
be encouraged to switch to other platforms such as
[PeerTube](https://joinpeertube.org/),
[Odysee](https://en.wikipedia.org/wiki/LBRY), etc., which are more
privacy-friendly.

In the same way, we could fill Google Drive with junk files. We would
probably have to use several accounts and try not to attract too much
attention from Google. Minority languages, files that look legitimate,
etc., can be used.

For the attack on Google to have the greatest possible impact, several
different accounts should be used, with different IPs, and everything
should be coordinated between several people. There is no law against
uploading randomly generated videos and files, and even if there was,
it could be done anonymously to avoid being identified.

## Use their servers through a proxy without paying anything

You can use programs such as
[Piped](/en/YouTube-privately-with-Piped/),
[Invidious](/en/youtube-con-privacidad-con-invidious/) or
[NewPipe](https://newpipe.net/). YouTube spends money by sending video
files over the Internet. In this way, it does not collect personal data
from us while it loses money, and we get to enjoy the videos. 😆

## Boycott  its products

Do not buy anything that gives Google money. If you already have a
Google product, you can take steps to send as little data as possible to
Google. For example, you can
[change Androids captive portal settings](/no-digas-a-google-d%C3%B3nde-te-conectas-a-internet-en-android-portal-cautivo/).

If you need a phone, you can buy a dumbphone, a GNU/Linux phone or one
with a [free Android distribution](https://en.wikipedia.org/wiki/List_of_custom_Android_distributions).

## Propaganda against Google

This article is a case in point. ✊
