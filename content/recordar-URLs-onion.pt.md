Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-11-04 18:59
Lang: pt
Slug: recordar-urls-onion
Save_as: recordar-URLs-.onion/index.html
URL: recordar-URLs-.onion/
Tags: Abrowser, Firefox, Iceweasel, marcadores, .onion, privacidade, Tor, Tor Browser
Title: Recordar URLs <code>.onion</code>
Image: <img src="/wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço.png" alt="" width="1020" height="486" srcset="/wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço.png 1020w, /wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço-510x243.png 510w" sizes="(max-width: 1020px) 100vw, 1020px">

Recordar serviços ocultos é complicado, uma vez que têm muitos
caracteres aleatórios e são longos, como
<http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/>.
Como podemos lembrar-nos destes estranhos URLs?

<!-- more -->

O Tor Browser permite utilizar marcadores para guardar as páginas que
desejas recordar. Podes adicionar uma página à lista de marcadores usando
a interface gráfica ou o atalho de teclado <kbd>Ctrl</kbd>+<kbd>D</kbd>.
Depois de ter guardado um marcador, podes começar a escrever
na barra de endereço o título da página (que normalmente aparece no
separador da página, corresponde à etiqueta `<title>`), e deverás ver
os URLs dos marcadores cujos títulos correspondam ao que digitou. Depois
basta seleccionar a página onde queres ir.

<figure>
<a href="/wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço.png">
<img src="/wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço.png" alt="" width="1020" height="486" srcset="/wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço.png 1020w, /wp-content/uploads/2022/11/marcadores-do-Tor-Browser-na-barra-de-endereço-510x243.png 510w" sizes="(max-width: 1020px) 100vw, 1020px">
</a>
    <figcaption class="wp-caption-text">Marcadores na barra de endereço
    do Tor Browser.</figcaption>
</figure>

Uma barra de favoritos também pode ser adicionada como barra lateral ou
barra superior. Nesse caso, ícones e títulos de páginas aparecerão para
que possamos facilmente aceder-lhos clicando neles.

Se quisermos alterar o nome de um marcador, importar a lista de
marcadores, exportá-lo ou executar outras opções avançadas, temos de
entrar na **Barra de ferramentas de marcadores** na janela
**Biblioteca** (<kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>O</kbd>).

![como ir no Tor Browser à biblioteca](/wp-content/uploads/2022/11/biblioteca-de-marcadores-do-Tor-Browser.gif)

Podemos também anotar URLs de outras formas: uma lista de páginas em
formato HTML, uma folha num caderno, um gestor de senhas, etc.
