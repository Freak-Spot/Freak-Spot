Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-03-29 21:20
Lang: es
Slug: presentacion-libro-citela-cultura-es-librecite-en-jasyp-granada
Tags: cultura libre, Granada, libro, software libre
Title: Presentación del libro <cite>La cultura es libre</cite> en JASYP (Granada)
Image: <img src="/wp-content/uploads/2022/03/imagen-charla-de-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png" alt="" width="1710" height="2096" srcset="/wp-content/uploads/2022/03/imagen-charla-de-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad.png 1710w, /wp-content/uploads/2022/03/imagen-charla-de-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad-855x1048.png 855w, /wp-content/uploads/2022/03/imagen-charla-de-la-cultura-es-libre-una-historia-de-la-resistencia-antipropiedad-427x524.png 427w" sizes="(max-width: 1710px) 100vw, 1710px">

El viernes 22 de abril presentaré el libro <a href="https://freakspot.net/libro/la-cultura-es-libre/"><cite>La cultura es libre:
una historia de la resistencia antipropiedad</cite></a> en las [Jornadas de
Anonimato, Seguridad y Privacidad](https://jasyp.interferencias.tech/)
organizadas por el colectivo Interferencias en Granada.

No solo presentaré el libro impreso, que podréis también comprar, sino
que también mostraré cómo generar un libro en varios formatos (HTML,
EPUB, PDF). A día de hoy no están concretados los horarios exactos de
cada charla o taller, pero ya se pueden ver sus detalles, [incluidos los
de la mía](https://jasyp.interferencias.tech/programa#octava).
