Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2023-02-14 17:00
Lang: es
Slug: crear-servicio-oculto-para-sitio-web-en-nginx
Tags: Debian, GNU/Linux, Nginx, servicio oculto, Tor
Title: Crear servicio oculto para sitio web en Nginx (<i lang="en">clearnet</i> y <i lang="en">darknet</i>)
Image: <img src="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png" alt="" width="1005" height="546" srcset="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png 1005w, /wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal-502x273.png 502w" sizes="(max-width: 1005px) 100vw, 1005px">

¿Quieres crear un sitio web usando un servidor Nginx y quieres tener
también ese sitio web como servicio oculto para usuarios de Tor? Aquí te
explico cómo hacerlo en Debian GNU/Linux.

Primero instalamos los siguientes paquetes:

    :::bash
    sudo apt install nginx tor

Luego hay que descomentar las siguientes líneas del archivo
`/etc/tor/torrc`:

    :::bash
    #HiddenServiceDir /var/lib/tor/hidden_service/
    #HiddenServicePort 80 127.0.0.1:80

Yo he cambiado el nombre del directorio del servicio oculto a
`servicio_oculto`, quedando así:

    :::text
    HiddenServiceDir /var/lib/tor/servicio_oculto/
    HiddenServicePort 80 127.0.0.1:80

A continuación, reiniciamos el servicio de Tor:

    :::bash
    sudo systemctl restart tor

Al reiniciarse Tor crea el directorio `servicio_oculto/` y lo llena con
la URL del servicio oculto (archivo `hostname`) y las claves pública y
privada.

Si tenemos tanto Nginx y Tor ejecutándose como servicios y nos metemos
en la dirección que hay en `/var/lib/tor/servicio_oculto/hostname`,
podremos ver la página de bienvenida de Nginx.

<a href="/wp-content/uploads/2023/02/bienvenida-de-servidor-Nginx-en-servicio-oculto-de-Tor.png">
<img src="/wp-content/uploads/2023/02/bienvenida-de-servidor-Nginx-en-servicio-oculto-de-Tor.png" alt="" width="916" height="680" srcset="/wp-content/uploads/2023/02/bienvenida-de-servidor-Nginx-en-servicio-oculto-de-Tor.png 916w, /wp-content/uploads/2023/02/bienvenida-de-servidor-Nginx-en-servicio-oculto-de-Tor-458x340.png 458w" sizes="(max-width: 916px) 100vw, 916px">
</a>

Por defecto, el sitio web para Nginx se ha de ubicar en la ruta
`/var/www/html/`. Así pues, solo tenemos que desarrollar el sitio web en
esa ubicación. No importa que se use la URL de Tor o una URL
convencional, el sitio web es el mismo. Ten en cuenta de que para que
los enlaces a otras páginas de tu sitio web funcionen usando URLs
`.onion` es necesario usar URLs relativas.

## Extra: añadir cabecera de <em>.onion disponible</em>

<a href="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png">
<img src="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png" alt="" width="1005" height="546" srcset="/wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal.png 1005w, /wp-content/uploads/2023/02/servicio-oculto-con-nginx-para-sitio-web-de-Internet-normal-502x273.png 502w" sizes="(max-width: 1005px) 100vw, 1005px">
</a>

<!-- more -->

Simplemente hay que añadir la siguiente línea `add_header` a la parte
del archivo de configuración de Nginx[^1] que pone `location /`:

    :::nginx
    location / {
        # Otras cosas que hay en location /
        add_header Onion-Location http://[el_nombre_de_tu_servicio_oculto]/$request_uri;
    }

Deberás reemplazar `[el_nombre_de_tu_servicio_oculto]` por el nombre que
se encuentra en el archivo `/var/lib/tor/servicio_oculto/hostname`,
quedando algo así[^2]:

    :::nginx
    location / {
        add_header Onion-Location http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/$request_uri;
    }

[^1]: En este caso `/etc/nginx/sites-available/default`.
[^2]: El nombre del servicio oculto es un ejemplo, no lo copies y lo
    pegues así; pon el nombre del servicio oculto que has creado.
