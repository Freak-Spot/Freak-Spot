Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-01-03 08:00
Lang: pt
Save_as: linhas-de-execução-em-Python/index.html
Slug: subprocesos-en-python
Tags: Python 3, Python 3.10.9, aleatório, linhas de execução, threading, time
Title: Linhas de execução em Python
URL: linhas-de-execução-em-Python/

As linhas (ou encadeamento) de execução permitem-nos executar tarefas em
simultâneo. Em Python podemos usar o módulo [`threading`](https://docs.python.org/3.10/library/threading.html), embora
existam muitos outros.

Vamos criar várias linhas de execução (<i lang="en">threads</i>):

    :::python
    #!/usr/bin/env python
    # -*- coding: utf-8 -*-
    import threading
    import time
    import random

    def sleeper(name, s_time):
        print('{} começado às {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))

        time.sleep(s_time)

        print('{} terminado às {}.'.format(
            name, time.strftime('%H:%M:%S', time.gmtime())))


    for i in range(5):
        thread = threading.Thread(target=sleeper, args=(
            'Linha ' + str(i + 1), random.randint(1, 9)))

        thread.start()

    print('Já terminei, mas as outras linhas de execução não.')

Em primeiro lugar, importamos os módulos necessários: `time`, `random` e
`threading`. Para criar <i lang="en">threads</i> só precisamos do
último. Usamos `time` para simular uma tarefa e obter o seu tempo de
início e fim; `random`, para fazer com que o nossa linha tenha uma
duração aleatória.

A função `sleeper` «dorme» (não faz nada) durante o tempo que
especificamos, diz-nos quando começou a «dormir» e quando acabou de
«dormir». Como argumentos, passamos o nome que queremos dar à linha e o
tempo que a função vai «dormir».

Depois, criamos um <i lang="en">loop</i> que cria 5 fios que executam a
função `sleeper`. No `threading.Thread` devemos indicar a
função a executar (`target=sleeper`) e os argumentos que queremos
passar-lhe, `args=('Linha ' + str(i + 1), random.randint(1, 9)`.

O resultado da execução é aleatório: não sabemos qual o processo que irá
terminar primeiro:

    Linha 1 começado às 09:16:14.
    Linha 2 começado às 09:16:14.
    Linha 3 começado às 09:16:14.
    Linha 4 começado às 09:16:14.
    Linha 5 começado às 09:16:14.
    Já terminei, mas as outras linhas de execução não.
    Linha 2 terminado às 09:16:17.
    Linha 5 terminado às 09:16:20.
    Linha 3 terminado às 09:16:21.
    Linha 4 terminado às 09:16:21.
    Linha 1 terminado às 09:16:22.
