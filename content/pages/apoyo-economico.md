Author: Jorge Maldonado Ventura
Date: 2017-01-22 18:33
Lang: es
Modified: 2021-11-02
Slug: apoyo-economico
Status: published
Title: Apoyo económico

Desde su nacimiento, esta página ha usado solo <i lang="en">software</i> libre y ha
estado libre de publicidad. Sin embargo, para mantener un sitio web como
este se requiere tiempo y dinero. Las
donaciones son una forma de cubrir los
[costes del sitio web](/pages/gastos-economicos.html) y de
fomentar su desarrollo y mejora.

Puedes eligir entre diferentes formas de realizar donaciones.

## Monero

4AVrC3LT4Ad7DCK5L8MDfWfMeiTqDYJnWDAKGE1jJNbn7pBmFGay81C5sKcci9pwkXRRBexM5JKyVUTEPWvvZTAJ3WeAAex

<img alt="" src="/wp-content/uploads/2023/12/monero-qr.gif">

## Liberapay

<a href="https://liberapay.com/Freak-Spot/donate" role="button"><img alt="Dona" src="/wp-content/uploads/2017/04/donar.svg"></a>

## Transferencia bancaria

<dl>
  <dt>Banco</dt><dd>Cajamar Caja Rural</dd>
  <dt>Propietario de la cuenta</dt> <dd>Jorge Maldonado Ventura</dd>
  <dt>Número de cuenta</dt> <dd>3058 0990 2727 5316 9801</dd>
  <dt><abbr title="International Bank Account Number">IBAN</abbr></dt> <dd>ES80 3058 0990 2727 5316 9801</dd>
  <dt><abbr title="Society for Worldwide Interbank Financial Telecommunication">SWIFT</abbr></dt> <dd>CCRIES2AXXX</dd>
</dl>

