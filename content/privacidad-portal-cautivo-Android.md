Author: Jorge Maldonado Ventura
Category: Privacidad
CSS: asciinema-player.css
Date: 2020-01-02
Lang: es
JS: asciinema-player.js (top)
Slug: no-digas-a-google-dónde-te-conectas-a-internet-en-android-portal-cautivo
Tags: Android, Android Debug Bridge, Google, GNU/Linux, portal cautivo, privacidad, Trisquel
Title: No digas a Google dónde te conectas a Internet en Android: cambiando los ajustes del portal cautivo

Cada vez que te conectas a un Internet con tu móvil Android, el sistema
envía una petición HTTP a un servidor de Google, supuestamente solo para
comprobar si hay un [portal
cautivo](https://es.wikipedia.org/wiki/Portal_cautivo) en la red; pero
es poco probable que Google desaproveche esta valiosa información de,
entre otras cosas, los lugares y la hora a la que te conectas a
Internet.

En este tutorial te enseño a modificar la comprobación del portal
cautivo para no dar esos datos a Google y así proteger un poco más tu
privacidad. En cualquier caso, no recomiendo usar Android de Google
porque no es 100% <i lang="en">software</i> libre, sino que te recomiendo pasarte a una
distribución libre de Android como
[Replicant](https://www.replicant.us/). Si no te he hecho cambiar de
idea o no te resulta factible por el momento, sigue leyendo.

<!-- more -->

Necesitas instalar el Conector de Depuración en Android (también
conocido como *Android Debug Bridge*). En muchas distribuciones de
GNU/Linux hay disponibles paquetes con esta herramienta. En
distribuciones basadas en Debian, como Trisquel, se encuentra en el
paquete `android-tools-adb` y lo puedes instalar ejecutando...

    :::bash
    sudo apt install android-tools-adb

Si no puedes instalar el programa desde tu sistema operativo, tendrás
que descargarlo desde la [web de
Android](https://developer.android.com/studio/) (bajo el título *Command
line tools only*), descomprimirlo e instalarlo.

Ahora hace falta [activar el modo depuración USB en
Android](https://elandroidelibre.elespanol.com/2015/01/como-activar-el-modo-depuracion-usb-en-android.html).
Una vez hecho esto, abre una terminal y ejecuta los siguientes comandos:

    :::bash
    adb shell 'settings put global captive_portal_http_url "http://captiveportal.kuketz.de"'
    adb shell 'settings put global captive_portal_https_url "https://captiveportal.kuketz.de"'
    adb shell 'settings put global captive_portal_fallback_url "http://captiveportal.kuketz.de"'
    adb shell 'settings put global captive_portal_other_fallback_urls "http://captiveportal.kuketz.de"'

Recomiendo el [servicio que proporciona Kuketz-Blog](https://www.kuketz-blog.de/android-captive-portal-check-204-http-antwort-von-captiveportal-kuketz-de/) porque dice que
borra
todas las peticiones HTTP, pero puedes usar cualquier página web que
devuelva siempre un código HTTP 204.

Aquí dejo una demostración:

<asciinema-player src="/asciicasts/portal-cautivo-Android.cast">
Lo siento, asciinema-player no funciona sin JavaScript.
</asciinema-player>

Si quieres dejar el sistema cómo estaba antes, puedes ejecutar...

    :::bash
    adb shell settings delete global captive_portal_http_url
    adb shell settings delete global captive_portal_https_url
