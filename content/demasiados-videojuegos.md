Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2023-12-07 17:00
Lang: es
Slug: compras-videojuegos-yo-tengo-mas-de-6000-gratis
Tags: comprar, emulación, GNU/Linux, software libre
Title: ¿Compras videojuegos? Yo tengo más de 6000 gratis
Image: <img src="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png" alt="" width="1441" height="835" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png 1441w, /wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando-720x417.png 720w" sizes="(max-width: 1441px) 100vw, 1441px">

La industria de los videojuegos es multimillonaria. Sin embargo, existen
miles y miles de juegos que pueden ser jugados de forma gratuita.
No solo hablo de juegos <a href="https://freakspot.net/escribe-programas-libres/aprende/">libres</a>,
sino también de juegos antiguos de maquinas recreativas, de consolas, etc.

Si usas GNU/Linux, puedes instalar muchos videojuegos usando el gestor
de paquetes de tu distribución. Otros juegos se distribuyen en formato
Flatpak, Snap, AppImage o se deben compilar. Para encontrar
juegos libres recomiendo [LibreGameWiki](https://libregamewiki.org/Main_Page).

Sin embargo, no solo tenemos juegos libres, sino miles de
juegos antiguos de máquinas recreativas, que pueden ser jugados
con<!-- more --> el emulador MAME[^1]. También existen emuladores para consolas, para
juegos solo disponibles en Windows, etc. Muchos juegos antiguos son
muy originales y entretenidos, tanto que la mayoría de juegos modernos
son meras imitaciones e inspiraciones con mejores gráficos.

Claro, hay quien prefiere videojuegos actuales que traigan ideas
novedosas y puedan verse en alta definición. El problema es que muchos
de esos juegos son privativos, cuestan dinero y suelen estar disponibles
solo en idiomas mayoritarios. Los juegos libres, por otro lado, siempre
pueden mejorarse y suelen estar disponibles en muchos idiomas. Yo
prefiero dar dinero para promover el desarrollo de juegos libres a
gastar dinero en algo que no puedo modificar.

Por otro lado, no comprar videojuegos siempre será lo más ecológico y
sano. Las mayores empresas de la industria crean consolas cada pocos
años. De esta forma, fomentan el consumismo y generan desechos
electrónicos. Los juegos que has comprado dejan de funcionar en las
nuevas consolas. También existen juegos que venden objetos virtuales...
En fin, se trata de juegos diseñados para dejarte más pobre y volverte
adicto; hay que evitarlos.

Lo ideal es tener una máquina que permita jugar cualquier juego, de
cualquier época, en cualquier resolución...  Esa máquina es, en mi caso,
un ordenador con GNU/Linux.

<a href="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png">
<img src="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png" alt="" width="804" height="1080" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png 804w, /wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball-402x540.png 402w" sizes="(max-width: 804px) 100vw, 804px">
</a>

En cualquier caso, la mayoría de las veces lo mejor es salir a la calle
a hacer deporte. No es bueno pasarse horas y horas delante de una
pantalla, aunque los juegos sean libres.

[^1]: Se puede instalar con `sudo apt install mame` en distribuciones
  basadas en Debian. Los juegos (ROMs) pueden descargarse en
  <https://archive.org/download/mame-merged/mame-merged/>,
  <https://www.mamedev.org/roms/> y en muchas otras páginas. En Debian,
  los juegos deben guardarse en el directorio `~/mame/roms`.
