Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-06-20 17:37
Image: <img src="/wp-content/uploads/2018/06/Arab_keyboard.jpg" alt="Teclado árabe" width="800" height="531" srcset="/wp-content/uploads/2018/06/Arab_keyboard.jpg 800w, /wp-content/uploads/2018/06/Arab_keyboard-400x265.jpg 400w" sizes="(max-width: 800px) 100vw, 800px">
Lang: es
Slug: multiples-mapas-de-teclas
Tags: esperanto, GNU/Linux, idiomas, MATE, software libre, teclado, Trisquel, Trisquel 8
Title: Cambiar entre distribuciones de teclado rápidamente en MATE

A veces necesito escribir en idiomas diferentes, y no me es posible
insertar los caracteres cómodamente sin cambiar la distribución del
teclado. En este artículo os enseño cómo instalar una nueva distribución
de teclado y alternar entre distribuciones de teclado usando un atajo de
teclado en el entorno de escritorio MATE.

<!-- more -->

En el entorno de escritorio MATE es posible añadir una distribución de
teclado pulsando en **Sistema>Preferencias>Hardware>Teclado** desde el
menú de inicio.

<a href="/wp-content/uploads/2018/06/ajustes-teclado-MATE-GNU-Linux.png">
<img src="/wp-content/uploads/2018/06/ajustes-teclado-MATE-GNU-Linux.png" alt="Accediendo al menú de ajustes de teclado con el ratón en Trisquel 8 con el escritorio MATE." width="1010" height="468" srcset="/wp-content/uploads/2018/06/ajustes-teclado-MATE-GNU-Linux.png 1010w, /wp-content/uploads/2018/06/ajustes-teclado-MATE-GNU-Linux-505x234.png 505w" sizes="(max-width: 1010px) 100vw, 1010px">
</a>

Una vez en las preferencias del teclado, seleccionamos la pestaña
**Distribuciones**. Desde ahí podemos añadir, quitar y cambiar las
preferencias de nuestras distribuciones de teclado. En esta ocasión voy
a añadir la distribución de teclado para el idioma esperanto.

<a href="/wp-content/uploads/2018/06/Seleccionar-una-distribución-de-teclado-para-la-lista-MATE.png">
<img src="/wp-content/uploads/2018/06/Seleccionar-una-distribución-de-teclado-para-la-lista-MATE.png" alt="Pulsando el botón Añadir desde la pestaña Distribuciones.">
</a>

En la pestaña de selección de distribución, vamos a la pestaña **Por
idioma**, puesto que el esperanto no tiene país, y seleccionamos el
idioma esperanto. Existen algunas variantes; yo he dejado la
predeterminada. Ahora basta con pulsar en **Añadir**.

<a href="/wp-content/uploads/2018/06/seleccionando-la-distribución-de-teclado-esperanto-MATE.png">
<img src="/wp-content/uploads/2018/06/seleccionando-la-distribución-de-teclado-esperanto-MATE.png" alt="Seleccionando la distribución de teclado de esperanto desde la pestaña de idioma." width="1082" height="705" srcset="/wp-content/uploads/2018/06/seleccionando-la-distribución-de-teclado-esperanto-MATE.png 1082w, /wp-content/uploads/2018/06/seleccionando-la-distribución-de-teclado-esperanto-MATE-541x352.png 541w" sizes="(max-width: 1082px) 100vw, 1082px">
</a>

Una vez añadida la distribución de esperanto podemos usarla siempre
haciendo clic en Esperanto y dándole luego al botón **Subir**. Si
cambiamos las distribuciones de teclado muy a menudo, sería cómodo
añadir un atajo de teclado. Podemos hacer esto desde **Opciones...**.
Desplegamos ahí **Cambiar a otra distribución**.  Finalmente, elegimos
un atajo que nos resulte cómodo (por ejemplo, **Alt+Espacio**).

<a href="/wp-content/uploads/2018/06/establecer-atajo-para-cambiar-distribución-de-teclado-MATE.png">
<img src="/wp-content/uploads/2018/06/establecer-atajo-para-cambiar-distribución-de-teclado-MATE.png" alt="Seleccionando un atajo de teclado para cambiar a otra distribución de teclado." width="1096" height="517" srcset="/wp-content/uploads/2018/06/establecer-atajo-para-cambiar-distribución-de-teclado-MATE.png 1096w, /wp-content/uploads/2018/06/establecer-atajo-para-cambiar-distribución-de-teclado-MATE-548x258.png 548w" sizes="(max-width: 1096px) 100vw, 1096px">
</a>

Con esta opción puedo mantener el teclado de español como predeterminado
y alternar entre el teclado de esperanto pulsando el atajo seleccionado.

<small>La [imagen del teclado](/wp-content/uploads/2018/06/Arab_keyboard.jpg) fue realizada por [Marek](https://www.flickr.com/people/84921704@N00) y se encuentra bajo la licencia [Creative Commons Attribution-Share Alike 2.0 Generic](https://creativecommons.org/licenses/by-sa/2.0/deed.es).</small>
