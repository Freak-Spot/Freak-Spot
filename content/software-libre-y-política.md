Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2020-11-05
Lang: es
Modified: 2020-11-08 23:30
Slug: software-libre-y-política
Tags: anarquismo, capitalismo, comunismo, GNU/Linux, ideología, política, software libre
Title: <i lang="en">Software</i> libre y política
Image: <img src="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg" alt="" width="1200" height="800" srcset="/wp-content/uploads/2020/11/software-libre-e-ideología.jpeg 1200w, /wp-content/uploads/2020/11/software-libre-e-ideología.-600x400.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

¿El <i lang="en">software</i> libre es anarquista o capitalista?  Unos lo llaman
comunista, otros dicen que es capitalista, anarquista... ¿Quién tiene
razón? <!-- more -->¿Tienen sentido comentarios como los que hizo el ex director
ejecutivo de Microsoft Steve Ballmer?:

> Linux es un competidor duro. No hay ninguna empresa llamada Linux;
> apenas hay una hoja de ruta. Sin embargo, Linux nace como si brotara
> de la tierra. Y tenía, ya sabes, las características del comunismo que
> a la gente le gustan tanto, mucho. Es decir, es gratuito.[^1]

## Capitalismo

El <i lang="en">software</i> privativo favorece los monopolios de empresas que controlan
la práctica totalidad de un mercado. Es imposible lograr un buen lugar
en el mercado con <i lang="en">software</i> privativo solo, así que para competir muchas
empresas deben usar <i lang="en">software</i> libre. Actualmente es difícil encontrar
empresas tecnológicas que no hagan un uso considerable del <i lang="en">software</i>
libre.

Claro está que hay diferentes corrientes capitalistas. El <i lang="en">software</i>
libre, en cualquier caso, tiene lugar en este tipo de sociedades siempre
que haya demanda o que su uso suponga una ventaja competitiva.

## Anarquismo

El <i lang="en">software</i> libre [acaba con el poder injusto que tienen los
programadores sobre los
usuarios](https://www.gnu.org/philosophy/free-software-even-more-important.es.html).
Puesto que el anarquismo trata de acabar con la autoridad impuesta sobre
el individuo, las libertades que otorga el <i lang="en">software</i> libre suponen una
liberación.

## Otros sistemas políticos

El <i lang="en">software</i> libre se usa en sistemas políticos de lo más variopintos.
¿Qué problema hay? Corea del Norte, por ejemplo, desarrolló una
distribución de GNU/Linux llamada [Red Star
OS](https://es.wikipedia.org/wiki/Red_Star_OS).

## Conclusión

Considero absurdo encuadrar el <i lang="en">software</i> libre en un sistema político
determinado. Indudablemente es más eficiente y seguro que el <i lang="en">software</i>
privativo, e innumerables modelos políticos pueden aprovecharse de su
adopción. [El <i lang="en">software</i> privativo es como la
alquimia](/el-software-libre-es-mejor-que-la-alquimia/),
mientras que el <i lang="en">software</i> libre es como la ciencia. Por algo será que
[casi todos los
superordenadores](https://es.wikipedia.org/wiki/Supercomputadora#Motivo_de_este_cambio,_qu%C3%A9_SO_predomina_actualmente_y_por_qu%C3%A9)
y servidores web funcionan con <i lang="en">software</i> libre.

[^1]: Traducido del artículo de The Register [MS' Ballmer: Linux is communism](https://www.theregister.com/Print/2000/07/31/ms_ballmer_linux_is_communism/).
