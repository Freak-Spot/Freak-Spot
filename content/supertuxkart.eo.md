Author: Jorge Maldonado Ventura
Category: Videludoj
Date: 2022-07-10 15:45
Lang: eo
Save_as: veturila-videludo-SuperTuxKart/index.html
URL: veturila-videludo-SuperTuxKart/
Slug: videojuego-de-carreras-supertuxkart
Tags: veturiloj, GNU/Linukso, vetkuro, videludo, Vindozo
Title: Veturila videludo: <cite lang="en">SuperTuxKart</cite>
Image: <img src="/wp-content/uploads/2022/07/vetkuro-SuperTuxKart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/vetkuro-SuperTuxKart.png 1024w, /wp-content/uploads/2022/07/vetkuro-SuperTuxKart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">

SuperTuxKart estas veturila videludo altkvalita. La karakteroj estas
bestemblemoj de liberaj programoj, kiel Tux, Gnu kaj Wilber (la
bestemblemo de GIMP). Ĝi havas historian reĝimon, kelkajn malfacilecajn
nivelojn kaj eblas ludi rete.

<figure>
<a href="/wp-content/uploads/2022/07/elekti-gavroche-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/elekti-gavroche-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/elekti-gavroche-supertuxkart.png 1024w, /wp-content/uploads/2022/07/elekti-gavroche-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
<figcaption class="wp-caption-text">Elektado de karaktero</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/07/defio-historia-reĝimo-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/defio-historia-reĝimo-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/defio-historia-reĝimo-supertuxkart.png 1024w, /wp-content/uploads/2022/07/defio-historia-reĝimo-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
    <figcaption class="wp-caption-text">Animacio de la historia reĝimo</figcaption>
</figure>

Sed krom la klasikaj veturilaj vetkuroj kun aĵoj, estas aliaj ludaj
reĝimoj: piedpilko, sekvu la estron, tempa provo kaj batalo. El ĉi tiuj
la plej interesa por ludi rete estas piedpilko, ĉar permesas konkuri en
teamoj. Kutime estas reta piedpilka turniro ĉiumonate.

<figure>
<a href="/wp-content/uploads/2022/07/fútbol-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/fútbol-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/fútbol-supertuxkart.png 1024w, /wp-content/uploads/2022/07/fútbol-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
    <figcaption class="wp-caption-text">piedpilka ludo</figcaption>
</figure>

Oni povas instali la ludon en distribuo de GNU/Linukso bazitaj sur
Debiano plenumante `sudo apt install supertuxkart`.

Jen mi montras la antaŭfilmon de la plej nova versio, la 1.3:

<!-- more -->

<video controls="">
<source src="/video/trailer-supertuxkart.mp4" type="video/mp4">
Antaŭfilmo de la versio 1.3 de SuperTuxKart
</video>
