Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2019-11-29
Lang: es
Slug: el-gobierno-de-españa-ya-puede-intervenir-telecomunicaciones-sin-autorización-judicial
Tags: censura, control político, España, leyes, seguridad
Title: El Gobierno de España ya puede intervenir telecomunicaciones sin autorización judicial

Según el Real Decreto-ley 14/2019 de 31 de octubre aprobado el miércoles
27 de noviembre, el Gobierno podrá intervenir sin intervención judicial
telecomunicaciones a partir de ahora:

> El Gobierno, con carácter excepcional y transitorio, podrá acordar la
> asunción por la Administración General del Estado de la gestión
> directa o la intervención de las redes y servicios de comunicaciones
> electrónicas en determinados supuestos excepcionales que puedan
> afectar al orden público, la seguridad pública y la seguridad
> nacional. En concreto, esta facultad excepcional y transitoria de
> gestión directa o intervención podrá afectar a cualquier
> infraestructura, recurso asociado o elemento o nivel de la red o del
> servicio que resulte necesario para preservar o restablecer el orden
> público, la seguridad pública y la seguridad nacional.

Los conceptos orden público, seguridad pública y seguridad nacional son
tan ambiguos que dan pie a que haya una censura indiscriminada de
minorías y movimientos disidentes.
