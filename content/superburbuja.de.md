Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2022-02-04 22:00
Lang: de
Slug: superburbuja
Save_as: Superblase/index.html
URL: Superblase/
Tags: Blase, Untergang, Dezentralisierung, Technologie
Title: Superblase: technologischer, ekonomischer und sozialer Wandel?
Image: <img src="/wp-content/uploads/2022/02/stock-crash.jpg" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/02/stock-crash.jpg 1024w, /wp-content/uploads/2022/02/stock-crash-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Eine Blase wird gleich platzen: die
Immobilienpreise sind in die Höhe geschnellt, Aktien sind weit
überbewertet, Lebensmittel sind teurer geworden, der Ölpreis ist in die
Höhe geschossen. Was wird passieren, wenn die Blase platzt?

Die einen sagen, die Industriegesellschaft [zusammenbricht](https://de.wikipedia.org/wiki/Zivilisationskollaps); die anderen
meinen, es wird bald eine [Depression](https://de.wikipedia.org/wiki/Konjunktur#Depression_(Konjunkturtief)) geben. Es ist absurd, das System
auf endloses Wirtschaftswachstum auszurichten, wenn wir auf einem
Planeten mit begrenzten Ressourcen leben. Doch das kapitalistische
System ist auf dieses Wachstum angewiesen, um sich über Wasser zu
halten. Einige schlagen vor, den Weltraum zu besiedeln, um den
Zusammenbruch des Kapitalismus zu verhindern.

Kurzfristig wird die Armut zunehmen. Diejenigen, die kein Vermögen
haben, müssen ihre Arbeitskraft zu einem niedrigeren Preis verkaufen
(wenn sie bei weiter steigender Arbeitslosigkeit einen Job finden),
stehlen oder auf Almosen angewiesen sein. Diejenigen, die über Kapital
oder Grundbesitz verfügen, auf dem sie Lebensmittel anbauen können, und
Zugang zu sauberem Wasser und Wohnraum haben, haben es leichter. Könnte
dies auf lange Sicht eine Chance sein, andere Werte zu fördern?

Der Temperaturanstieg hat bereits Rückkopplungsschleifen ausgelöst, die
zu einem weiteren Temperaturanstieg führen (was zu einer Zunahme von
extremen Wetterereignissen, Meeresspiegeln, der Wüstenbildung usw. führt).
Wenn zum Beispiel die Pole schmelzen, wird weniger Sonnenstrahlung
reflektiert, so dass die Temperaturen steigen; wenn sich die Eiskappe
auflöst, wird Methan (ein Treibhausgas) in die Atmosphäre freigesetzt;
wenn Wälder und Korallenriffe verschwinden, wird weniger CO<sub>2</sub> absorbiert.

Es gibt Technologien, die die Akkumulation von Kapital in wenigen Händen
begünstigen und enorme Energiekosten verursachen, und es gibt
dezentrale, freie und effiziente Technologien. Die Probleme, die
wir haben, sind bei weitem nicht nur technologisch bedingt, aber die
Technologie trägt zu ihnen bei. Wird das Platzen der Superblase zu einem
technologischen, wirtschaftlichen und sozialen Wandel führen?

Nichts bleibt für immer.

<small>[Bild *Coit Tower fresco - the stock crash*](https://live.staticflickr.com/1/679924_59bcaf5217_b.jpg) von Zac Appleton.</small>
