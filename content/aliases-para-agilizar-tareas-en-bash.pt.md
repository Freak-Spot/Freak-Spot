Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-03 11:54
Lang: pt
Slug: aliases-para-agilizar-tareas-en-bash
Save_as: aliases-para-acelerar-tarefas-no-Bash/index.html
URL: aliases-para-acelerar-tarefas-no-Bash/
Tags: .bashrc, .bash_aliases, alias, alias Bash, configuração do Bash
Title: <i lang="en">Aliases</i> para acelerar tarefas no Bash

Os <i lang="en">aliases</i> servem para chamar um comando por outro nome.
O comando ao que se aplica um <i lang="en">alias</i> funcionará como se
tivesse sido chamado diretamente. Por exemplo, se eu quiser ir para o
diretório-pai com o comando `..`, só tenho que criar um
<i lang="en">alias</i> no terminal com a seguinte ordem:
`alias ..='cd ..'`.

Provavelmente já tens vários <i lang="en">aliases</i> criados e não o
sabes. Se queres executar <i lang="en">aliases</i>, podes ver os
<i lang="en">aliases</i> que já definiste. Esses
<i lang="en">aliases</i> são definidos no ficheiro `.bashrc`, lá podes
adicionar o teu próprio (lembra-te de [recarregar as configurações do
Bash](/pt/recarregar-a-configuração-do-Bash/) depois de adicioná-los
para que possas começar a usá-los sem reiniciar o computador). Mas se
quiseres adicionar muitos e quiseres distinguir o que é teu, é
aconselhável tê-los num ficheiro separado.

No ficheiro `.bashrc` provavelmente encontrarás as seguintes linhas ou
algumas semelhantes:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

Isso significa que cada vez que inicies o Bash, os <i lang="en">aliases</i>
encontrados no ficheiro `~/.bash_aliases` serão carregados se eles
existirem. Se ainda não tens esse ficheiro, cria e adiciona alguns
<i lang="en">aliases</i> que te ajudem no teu dia-a-dia. Vão poupar-te
muito tempo a longo prazo.

Seguem-se alguns <i lang="en">aliases</i> úteis. Tenho um repositório em
<https://notabug.org/jorgesumle/bash_aliases> com todos os meus
<i lang="en">aliases</i>, dá uma olhada e copia aqueles que te sejam
úteis.

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'
