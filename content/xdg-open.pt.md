Author: Jorge Maldonado Ventura
Category: Bash
Date: 2022-11-06 03:00
Lang: pt
Slug: xdg-open
Status: published
Tags: xdg-open
Title: <code>xdg-open</code>

`xdg-open` é um comando muito útil. Com ele podemos abrir qualquer
programa ou URL a partir da linha de comandos. Se eu executasse
`xdg-open https://freakspot.net/pt`, abriria a página principal desde
sítio eletrônico com o meu navegador padrão e logo poderia executar
outro comando. Uma desvantagem é que só lhe podemos passar um parâmetro,
pelo que para abrir duas páginas web teríamos de correr `xdg-open` duas
vezes.
