Author: Хорхе Мальдонадо Вентура
Category: Литература
Date: 2024-01-21 13:00
Lang: ru
Slug: luz-roja
Save_as: Krasnyj-svet/index.html
URL: Krasnyj-svet/
Tags: стихи
Title: Алый пурпур
JS: quiere-morir.js (bottom)

Черный дрозд пронзает взглядом,<br>
Иглами колют плоды каштанов.<br>
Прочь я рвусь из темной сини.

Туда, где льется лунный свет,<br>
где ночь мне шепчет твое имя,<br>
и мы сливаемся едино.<br>
Видишь, как пылают тени?<br>
Слышишь, как поют леса?

Алый пурпур разгорится в тебе.<br>
Темная синь пусть потухнет на веки.
