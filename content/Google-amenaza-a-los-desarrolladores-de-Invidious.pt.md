Author: Jorge Maldonado Ventura
Category: Notícias
Date: 2023-06-10 11:00
Lang: pt
Slug: google-amenaza-a-los-desarrolladores-de-invidious
Tags: Google, Invidious, privacidade, software livre, YouTube
Save_as: YouTube-ameaça-os-desenvolvedores-do-Invidious/index.html
URL: YouTube-ameaça-os-desenvolvedores-do-Invidious/
Title: YouTube ameaça os desenvolvedores do Invidious
Image: <img src="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png" alt="" width="1200" height="740" srcset="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png 1200w, /wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious-600x370.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Os desenvolvedores do Invidious receberam um correio electrónico do
YouTube a pedir-lhes que deixem de oferecer o programa. De acordo com a
equipa jurídica do YouTube, estão a violar os termos de utilização da
[API](https://pt.wikipedia.org/wiki/Interface_de_programa%C3%A7%C3%A3o_de_aplica%C3%A7%C3%B5es) do YouTube, o que é impossível, uma vez que o Invidious não utiliza
a API do YouTube.

Também afirmam que Invidious «está a ser oferecido em
[invidious.io](https://invidious.io/)», o que também não é verdade,
porque esse sítio não aloja qualquer versão de Invidious. Atualmente,
existem 40 sítios que alojam versões públicas do Invidious, sobre as
quais a equipa do Invidious não tem qualquer controlo, uma vez que o
Invidious utiliza a licença livre
[AGPL](https://www.gnu.org/licenses/agpl-3.0.html). Mesmo que o
Invidious fosse ilegal nos Estados Unidos, está alojado na [rede Tor](https://pt.wikipedia.org/wiki/Tor_(rede_de_anonimato)), na
[rede I2P](https://geti2p.net/pt/) e em muitos países, o que torna
praticamente impossível fazer o Invidious desaparecer. Para além disso,
o seu código encontra-se em várias plataformas de desenvolvimento e em
muitos computadores.

O Invidious não concordou nem com os termos de serviço da API do YouTube
nem com os termos de serviço do YouTube. O YouTube permite o acesso a
conteúdos alojados nos seus servidores através do protocolo HTTP, pelo
que a Invidious não está a cometer nenhum crime informático; está
simplesmente a salvaguardar o direito à privacidade e à liberdade.

A Google (a empresa que controla o YouTube), por outro lado, [não
respeita a privacidade](https://stallman.org/google.html#surveillance), [censura](https://stallman.org/google.html#censorship), [requer a utilização de programas
proprietários](https://stallman.org/google.html#nonfree-software),
[explora os seus
utilizadores](/pt/Como-o-Google-explora-com-os-CAPTCHAs/), [desenvolve
programas de inteligência artificial para fins militares](/pt/preencher-reCAPTCHA-do-Google-ajuda-a-matar/), [tem um enorme
impacto ecológico](https://time.com/5814276/google-data-centers-water/),
para citar apenas alguns exemplos. Por estas razões, há quem ache que [o
Google deve ser destruído](/pt/como-destruir-o-Google/).

Felizmente, mesmo que o Invidious desapareça, existem outros projectos
livres como o [Piped](/pt/YouTube-com-privacidade-com-Piped/), o
[NewPipe](https://newpipe.net/) e o
[`youtube-dl`](https://youtube-dl.org/). Será que a Google também vai
ameaçar os desenvolvedores destes projectos e os seus milhões de
utilizadores?
