Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2023-06-11 10:04
Lang: en
Slug: deja-de-usar-Reddit
Tags: Reddit, Lemmy, Postmill, Lobsters, /kbin, privacy, free software
Save_as: stop-using-Reddit/index.html
URL: stop-using-Reddit/
Title: Stop using Reddit
Image: <img src="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg" alt="" width="2048" height="1024" srcset="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg 2048w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-1024x512.jpg 1024w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-512x256.jpg 512w" sizes="(max-width: 2048px) 100vw, 2048px">

[Thousands of Reddit
communities](https://safereddit.com/r/ModCoord/comments/1401qw5/incomplete_and_growing_list_of_participating/)
will stop being accessible tomorrow in protest [against the decision to
charge millions of dollars to apps that use the API](https://safereddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/). As a result of the
policy, many apps will stop working.

However, while some people are recommending that people stop using
Reddit, the protest is limited to two days. The problem with doing a
temporary boycott is that it sends this message to the owners: a lot of
people are angry, but after two days they're going to come back and
we're going to keep making money. [Reddit stopped being free software
years ago](/en/raddle-como-respuesta-a-reddit/) and it's not going to
stop censoring information they don't like.

I support boycotting Reddit, but I think it shouldn't just last two
days; it should be permanent. There are several programs similar to
Reddit that are free and respect the privacy of their users: [Lemmy](https://join-lemmy.org/),
[/kbin](https://kbin.pub/), [Postmill](https://postmill.xyz/),
[Lobsters](https://lobste.rs/), [Tildes](https://tildes.net/)...

[Lemmy](https://join-lemmy.org/) and [/kbin](https://kbin.pub/), unlike
Reddit, are free and federated, so the administrators of a node cannot
censor information from nodes they don't control or impose anything on
them; each node has its own policy.
