Author: Jorge Maldonado Ventura
Category: Reta programado
Date: 2020-10-02
Lang: eo
Slug: validar-HTML-de-forma-efectiva
Save_as: efektive-validigi-HTML-n/index.html
URL: efektive-validigi-HTML-n/
Tags: GitLab, GitLab CI, HTML, html5validator, kontinua integriĝo, Java, Python, validado, WHATWG
Title: Efektive validigi HTML-n

La lingvo <abbr title="HyperText Markup Language">HTML</abbr> konformas
kun la [WHATWG](https://html.spec.whatwg.org/)-normo. Ĉar ĝi estas
[markolingvo](https://eo.wikipedia.org/wiki/Marklingvo), eraro en HTML
ne kaŭzas, ke la paĝaro ceŝu funkcii, sed la retumilo ĝin montras kiel
eble plej bone.

Havi erarojn en HTML estas problema, ĉar ĝi povas aperigi neatenditajn
kaj malfacile reprodukteblajn erarojn, ĉefe kiam ili nur aperas en
konkreta retumilo. Do estas necesega skribi validan HTML-n.

Tamen estas facile fari erarojn kaj ĝin pretervidi. Sekve oni rekomendas
validigi la HTML-kodon, tio estas, trovi la erarojn kaj korekti ilin.
Por tio ekzistas validiloj, kiuj generale simple montras erarojn. La
plej ĝisdatigita kaj rekomendinda estas [The Nu Html
Checker](https://validator.github.io/validator/). La W3C subtenas nodon
de ĉi tiu validilo, kiu permesas al ni validigi HTML-dokumentojn en la
retumilo, [enigante URL-n](https://validator.w3.org/nu/), [alŝutante
dosieron](https://validator.w3.org/nu/#file) kaj [skribante la kodon en
formularo](https://validator.w3.org/nu/#textarea). Ĉar ĉi tiu validilo
estas libera programaro, vi povas instali ĝin facile en via komputilo.

<figure>
    <a href="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png">
    <img src="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png" alt="" width="1364" height="712" srcset="/wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org.png 1364w, /wp-content/uploads/2020/08/validación-HTML-de-página-web-gnu.org-682x356.png 682w" sizes="(max-width: 1364px) 100vw, 1364px">
    </a>
    <figcaption class="wp-caption-text">Reta validado de la retejo de GNU <a href="https://gnu.org/">https://gnu.org/</a>.</figcaption>
</figure>

La reta validilo bone funkcias, se vi nur devas validigi kelkajn
retejojn okaze, sed ĝi ne utilas por validigi tutan retejon. Por tio mi
rekomendas uzi la version de The Nu Html Checker, kiun oni plenumas en
terminalo.  Ĝi troviĝas en la dosiero `vnu.jar` (Java estas bezona).

Mi persone uzas la
[html5validator](https://pypi.org/project/html5validator/)-pakon, ĉar mi
laboras kun Python kaj ĝi ne signifas kroma dependaĵo. Por instali ĉi
tiun pakon en GNU/Linukso-distribuo bazita sur Debiano oni nur devas
plenumi...

    :::bash
    sudo apt install default-jre
    sudo pip3 install html5validator

Post la instalo havas ni programon kun la nomo html5validator, kiun ni
povas plenumi en la terminalo:

    :::bash
    html5validator index.html

Utilega argumento estas `--root`, kiu permesas validigi ĉiujn dosierojn
en dosierujo, kaj en la dosierujo ene de la dosierujo..., tiel ĝis ĉiun
ĝi validigis. Mi uzas ĝin donante la kernan dosierujon de mia retejo,
validante tiel la tutan retejon en kelkaj sekundoj.

    :::bash
    html5validator --root retejo/

Estas inde  uzi ian [kontinuan
integriĝon](https://en.wikipedia.org/wiki/Continuous_integration) por
eviti plenumi mane la antaŭan komandon ĉiam, kiam vi ŝanĝas ion en la
retejo. Por tio mi uzas [GitLab
CI](https://docs.gitlab.com/ce/ci/yaml/README.html). Tiel mi prizorgas
ĉi tiun retejon kaj multajn aliajn sen HTML-eraroj kaj kiam mi rompas
ion, mi rimarkas baldaŭ.

<figure>
<a href="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png">
<img src="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png" alt="" width="1077" height="307" srcset="/wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI.png 1077w, /wp-content/uploads/2020/08/validación-HTML-con-GitLab-CI-538x153.png 538w" sizes="(max-width: 1077px) 100vw, 1077px">
</a>
    <figcaption class="wp-caption-text">Ĉi tiu testo de GitLab CI
    montras, ke la retejo estis kreita sukcese kaj sen HTML-eraroj.</figcaption>
</figure>
