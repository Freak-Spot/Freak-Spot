Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2022-02-04 22:00
Lang: pt
Slug: superburbuja
Save_as: superbolha/index.html
URL: superbolha/
Tags: bolha, colapso, descentralização, tecnologia
Title: Super-bolha: mudança tecnológica, económica e social?
Image: <img src="/wp-content/uploads/2022/02/stock-crash.jpg" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/02/stock-crash.jpg 1024w, /wp-content/uploads/2022/02/stock-crash-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Há uma super-bolha prestes a rebentar: os preços das habitações subiram
em flecha, as acções estão muito sobrevalorizadas, a comida é mais cara,
os preços do petróleo dispararam. O que acontecerá quando a bolha
rebente?

Alguns dizem que a sociedade industrial está a colapsar; outros pensam
que em breve haverá uma
[depressão](https://pt.wikipedia.org/wiki/Depress%C3%A3o_(economia)).
Orientar o sistema para um crescimento económico sem fim quando vivemos
num planeta com recursos limitados é um absurdo. No entanto, o sistema
capitalista depende desde crescimento para se manter à tona. Alguns
propõem a colonização do espaço para evitar o colapso do capitalismo.

A curto prazo, a pobreza irá aumentar. Aqueles que não possuem bens
geradores de riqueza (ativos) terão de vender a sua força de trabalho a
um preço mais baixo (se encontrarem emprego quando o desemprego aumentar
ainda mais), roubar ou depender de esmolas. Para aqueles que têm capital
ou propriedades onde cultivar alimentos, acesso a água potável e habitação
é mais fácil. Poderá esta ser uma oportunidade, a longo prazo, para
fomentar outros tipos de valores?

O aumento das temperaturas já desencadeou ciclos de retroalimentação que
levam a novos aumentos de temperatura (que estão a aumentar os eventos
climáticos extremos, o nível do mar, a desertificação, etc.). Por
exemplo, à medida que os polos derretem, menos radiação solar é
reflectida, pelo que as temperaturas sobem; à medida que a camada de
gelo se dissolve, o metano (um gás com efeito de estufa) é libertado
para a atmosfera; à medida que as florestas e os recifes de coral
desaparecem, menos CO<sub>2</sub> é absorvido.

Existem tecnologias que favorecem a acumulação de capital em poucas mãos
e implicam enormes custos energéticos, e existem tecnologias
[descentralizadas](https://pt.wikipedia.org/wiki/Descentraliza%C3%A7%C3%A3o_do_poder_da_informa%C3%A7%C3%A3o),
livres e eficientes. Os problemas que temos não são em absoluto apenas
tecnológicos, mas a tecnologia está a acrescentá-los. Será que o
rebentar da super-bolha conduzirá a mudanças tecnológicas, económicas e
sociais?

Nada dura para sempre.

<small>[Imagem *Coit Tower fresco - the stock crash*](https://live.staticflickr.com/1/679924_59bcaf5217_b.jpg) por Zac Appleton.</small>
