Author: Хорхе Мальдонадо Вентура
Category: Новости
Date: 2023-06-18 11:26
Lang: ru
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Save_as: Reddit-terjaet-dengi-i-polzovatelej-iz-za-ignorirovanija-žalob-sobstvennyh-polzovatelej/index.html
URL: Reddit-terjaet-dengi-i-polzovatelej-iz-za-ignorirovanija-žalob-sobstvennyh-polzovatelej/
Tags: Reddit, Lemmy
Title: Reddit теряет деньги и пользователей из-за игнорирования жалоб собственных пользователей
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

Reddit расплачивается за то, что игнорирует [протесты пользователей против
новых изменений, внесенных компанией](/en/stop-using-Reddit/).

С одной стороны, многие пользователи перешли на такие платформы,
как [Lemmy](https://join-lemmy.org/), которая за короткое время утроила число своих
новых пользователей:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

С другой стороны, веб-сайт Reddit был отключен от сети из-за протестов,
в результате которых более 7000 <!-- more -->форумов стали недоступны[^1],
и четыре миллиона пользователей перестали пользоваться веб-сайтом[^2].
Пытаясь минимизировать ущерб, Reddit удалил учетные записи нескольких
протестующих модераторов, заменив их на более послушных и
лояльных модераторов[^3].

Однако некоторые форумы по-прежнему недоступны. Есть и такие пользователи,
которые загружают на платформу нерелевантный контент, чтобы снизить
лояльность к компании и продолжить протест. Один из примеров - форум
[r/pic](https://safereddit.com/r/pics), который теперь является "Местом для изображений
Джона Оливера, в сексуальных нарядах". Кроме того, некоторые пользователи
загружают на платформу видеоролики с шумом[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

Все это недовольство не осталось незамеченным несколькими рекламными
компаниями, которые отозвали свои рекламные кампании[^5].

Модераторы работают бесплатно, и компания к ним не прислушивалась.
К пользователям, которые тоже очень важны для платформы, тоже не
прислушались. Результат оказался катастрофическим для Reddit: потеря
пользователей, потеря доходов от рекламы и увеличение количества
нежелательного контента.

[^1]: Питерс, Дж. (12 июня 2023 г.). <cite>Reddit потерпел крах из-за новой волны отключения субреддитов</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Бонифацич, И. (17 июня 2023 г.). <cite>Согласно сторонним данным - среднесуточный трафик Reddit упал во время блэкаута.</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (17 июня 2023 г.). <cite>Друзья, сегодня без предупреждения напал королевский флот Reddit. Меня заблокировали администраторы.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (15 июня 2023 г.). <cite>Поскольку блэкаут не работает,
    все начали загружать 1 Гб видеоролики с шумом
    на серверы Reddit</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Перлофф, К. (14 июня 2023 2023). <cite>На Reddit распространяется новая волна забастовок модераторов для снижения спроса со стороны рекламодателей</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
