Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Bash
Date: 2017-01-23 00:40
JS: asciinema-player.js (top)
Lang: es
Modified: 2017-02-24 11:10
Slug: creacion-de-archivos-de-autocompletado-en-bash
Status: published
Tags: /etc/bash_completion.d, asciinema, asciiplayer, autocompleción, autocompletado, Bash, bash_completion.d, compleción, completion, ducker, GNU/Linux, interactivo, interfaz de línea de órdenes, ls, parámetros, shell
Title: Creación de archivos de autocompletado en Bash

El autocompletado permite predecir lo que el usuario va a escribir.
Muchos programas soportan el autocompletado. Por ejemplo, si escribimos
`ls --` y pulsamos <kbd>Tabulador</kbd> en una terminal con Bash u otra
shell similar, nos aparecen todas las opciones disponibles que comienzan
con `--`. Si escribimos `ls --au` y pulsamos <kbd>Tabulador</kbd>, se
autocompleta el parámetro `--author` porque es el único que comienza por
`--au`. En el siguiente vídeo se puede ver en acción esta
funcionalidad.<!-- more -->

<asciinema-player src="../asciicasts/100288.json">Lo siento,
asciinema-player no funciona sin JavaScript.
</asciinema-player>

Sin embargo, no todos los programas permiten el autocompletado. Pero no
es un gran problema, ya que es muy sencillo de añadir este soporte.
Simplemente basta con crear un archivo con el nombre del programa al que
quieres añadirle soporte para autocompletado en el directorio
`/etc/bash_completion.d`. Ese archivo deberá contener la función de
autocompletado correspondiente a ese programa.

La función `complete` de Bash sirve para especificar cómo realizar el
autocompletado (ejecuta `help complete` para más información). El
parámetro `-F` sirve para indicar que queremos usar una función para el
autocompletado, después de `-F` debemos escribir el nombre de la función
que usaremos y el nombre del programa al que se le aplicarán las reglas
que hayamos definido en dicha función.

Como ejemplo voy a crear el autocompletado para la versión 0.5.3 de un
programa llamado [Ducker](http://freakspot.net/programas/ducker/).
Simplemente voy a completar los parámetros del programa, pero podríamos
crear reglas más complejas y específicas si el programa lo requiriera.

    :::bash
    _ducker()
    {
        # Aquí estableceremos las reglas de autocompletado
    }

    complete -F _ducker ducker

A continuación, necesitamos crear algunas variables:

-   `COMPREPLY`. Es una variable que tiene un significado particular
    para Bash. Dentro de la función de completado se usa para mostrar la
    salida del intento de autocompletado.
-   `cur`. La opción de completado actual.
-   `prev`. La opción de completado previa
-   `opts`. Todas las opciones de completado que tiene nuestro programa
    (en este caso, los parámetros disponibles del programa).

Después necesitamos establecer una condición para intentar realizar el
completado. En nuestro caso es suficiente si la opción actual comienza
por u guion (-), pues todos los parámetros de Ducker comienzan con un
guion. La función `compgen`, generará los valores que coinciden con el
texto introducido cuando se pulsa <kbd>Tabulador</kbd> y su resultado
será guardado en `COMPREPLY`. Posteriormente se saldrá de la función
devolviendo 0, lo cual indica que se ha ejecutado con éxito.

A continuación, se muestra el código completo.

    :::bash
    _ducker()
    {
        local cur prev opts
        COMPREPLY=()
        cur="${COMP_WORDS[COMP_CWORD]}"
        prev="${COMP_WORDS[COMP_CWORD-1]}"
        opts=" -h --help --version -m --multiple-search -w --website-search -H --no-javascript -l --lite -i --image-search"

        if [[ ${cur} == -* ]] ; then
            COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
            return 0
        fi
    }

    complete -F _ducker ducker
