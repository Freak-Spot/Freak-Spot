Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2023-01-27 14:00
Lang: es
Slug: cómo-recoger-información-de-uso-de-usuarios-de-redes-sociales-ingeniería-social
Tags: ingeniería social, Facebook, Instagram, redes sociales, TikTok, Twitter
Title: Cómo investigar a personas mediante redes sociales
Image: <img src="/wp-content/uploads/2023/01/secreto-mujer.png" alt="" width="1200" height="844" srcset="/wp-content/uploads/2023/01/secreto-mujer.png 1200w, /wp-content/uploads/2023/01/secreto-mujer-600x422.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Pocos detectives informáticos prestan atención a los patrones de uso de
las redes sociales, capaces de revelar deseos profundos, estados de
ánimo, etc. El algoritmo de las redes <!-- more -->sociales[^1] lo hace.
¿Cómo sacarle provecho a la hora de investigar a una persona?

A la hora de investigar a una persona usando las redes es necesario que
también recojamos información de uso, y eso se puede hacer siguiéndola
con una cuenta aislada. Con este método el algoritmo nos recomendará
cosas relacionadas con sus intereses, su estado de ánimo, etc., cosas
que la persona ni siquiera ha publicado.

<img src="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg" alt="" width="6016" height="4000" srcset="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg 6016w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-3008x2000.jpg 3008w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-1504x1000.jpg 1504w" sizes="(max-width: 6016px) 100vw, 6016px">

Conseguir la dirección IP y datos técnicos[^2] de la persona es bastante
fácil: basta con guiarla a una URL en un servidor que tengamos, y para
ello tenemos que hacer que clique en un enlace o cargue alguna imagen o
vídeo que tengamos para tal propósito, lo cual no es difícil sabiendo
sus intereses. Si tenemos su correo, se puede usar un
<a href="https://es.wikipedia.org/wiki/Web_bug"><i lang="en">web
bug</i></a> (es decir, una imagen invisible).

[^1]: Por otro lado, hay algunas redes sociales como
    [Mastodon](https://joinmastodon.org/es) que no tienen un algoritmo de
    recomendaciones ni recogen ingentes cantidades de datos de usuarios.
[^2]: Desde dónde accede a Internet, qué dispositivo utiliza, qué
    idiomas tiene configurados, qué versiones de navegador y de sistema
    operativo usa, etc.

Si queremos obtener más información, debemos conseguir las peticiones
HTTP de la persona investigada. Unas veces se pueden comprar datos a
proveedores de Internet o en la [Internet profunda](https://es.wikipedia.org/wiki/Internet_profunda), otras veces las
fuerzas de seguridad del gobierno o agencias de inteligencia tienen
acceso a esos datos. Asimismo, se podría usar [ingeniería
social](https://es.wikipedia.org/wiki/Ingenier%C3%ADa_social_(seguridad_inform%C3%A1tica))
en el mundo real, comprar datos de páginas que usa la persona para
obtener su número de teléfono real, etc.

La única forma de protegerte de redes sociales abusivas y el acceso a tu
información por parte de jáqueres, gobiernos, etc., es no usándolas. Se
podrían reducir los riesgos usándolas de forma limitada: sin JavaScript
(cosa que muchas redes sociales abusivas no permiten), a través de
[Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)),
solo publicar o solo leer, no hacer clics en publicaciones que te llaman
la atención, no decir qué es lo que te gusta, pasar siempre el mismo
tiempo en cada publicación para no mostrar preferencia por ninguna (algo
complicado), etc.

En definitiva, es fácil investigar a personas que no son usuarias, sino
que son usadas, pues son el producto de empresas que comercializan con
sus vidas, sus deseos, sus estados de ánimo, etc. Se puede aumentar la
soberanía tecnológica teniendo un sitio web propio, en vez de depender de
una red social, o usando una red social libre y que respete la privacidad
de los usuarios.
