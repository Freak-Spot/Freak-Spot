Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2023-02-26 19:59
Lang: eo
Save_as: forigi-la-kuketojn-en-retumiloj/index.html
URL: forigi-la-kuketojn-en-retumiloj/
Slug: elimina-las-cookies-en-navegadores-derivados-de-firefox
Tags: kuketoj, retumiloj, retejoj
Title: Forigi la kuketojn en retumiloj

La kuketoj estas precipe uzitaj kiel spurilo fare de firmaoj rilataj al
reklamoj en Interretoj aŭ al la amasa kolektado kaj analizado de
datenoj.

Por protekti la privatecon rekomendindas forigi la kuketojn post forlasi
retejojn. Kvankam ĉi tio signifas problemetojn, ĉar oni devas denove
enigi ensalutilojn, se estas retejo, en kiu vi ensalutas, tio estas bona
maniero protekti vin kontraŭ la sekvado. Ĉi tio estas kio Tor Browser
implicite faras.

Tamen, se ni konas konfidindajn retejojn, sensencas forigi tiujn
kuketojn. Plej bone estus implicite forigi ilin, krom en konfidindaj
retejoj. Aŭ eble por iuj homoj forigi ĉiujn kuketojn troas, ĉar por
tiuj homoj estus pli bona kompromiso forigi nur kuketojn de eksteraj
liverantoj.

Ĉiuokaze, ni povas estigi regulojn por forigi kuketojn laŭ nia bezono
per la kromaĵo por retumiloj
[Cookie AutoDelete](https://addons.mozilla.org/en-US/firefox/addon/cookie-autodelete/).

<!-- more -->

Implicite la kromaĵo uzas tre permeseman agordaron. Por aliri al la
opciojn sufiĉas premi la kromaĵan bildeton.

<a href="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png" alt="" width="799" height="444" srcset="/wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete.png 799w, /wp-content/uploads/2023/02/activar-limpieza-automática-galletas-informáticas-Cookie-AutoDelete-399x222.png 399w" sizes="(max-width: 799px) 100vw, 799px">
</a>

Laŭ mia gusto gravas estigi **Limpieza automática activada**. Tiel ne
necesas permane forigi ĉiujn kuketojn, premante la butonon **Limpiar**.
En la antaŭa bildo oni povis vidi du sugestojn de reguloj kaj du
butonojn: **Lista gris** kaj **Lista blanca**.

La reguloj estas kreitaj pere de simplaj regulaj esprimoj. La esprimo
`freakspot.net` forigas la kuketojn de <https://freakspot.net>, kaj
`*.freakspot.net` forigas la kuketojn de <https://freakspot.net> kaj
ĝia subdomajnoj. La *lista gris* igas, ke la kuketoj ne foriĝu ĝis la
retumila restartigo (ĉi tio utilas por retejoj, en kiuj oni volas resti
ensalutita kaj forigi la kuketojn antaŭ restartigi la retumilon). La
*lista blanca* malhelpas, ke la kuketoj estu forigitaj.

Por agordi aliajn opciojn, ni povas premi la
**Configuraciones**-butonon. Flanke de ĉiu opcio estas demandosigno, kiu
kondukas nin al pâgo de dokumentaro kun la signifo de tiu opcio. Je la
dato, kiam mi skribis ĉi tiun artikolon, tiu dokumentaro nur estas en
la angla.

<a href="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png" alt="" width="1375" height="780" srcset="/wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete.png 1375w, /wp-content/uploads/2023/02/configuración-de-Cookie-AutoDelete-687x390.png 687w" sizes="(max-width: 1375px) 100vw, 1375px">
</a>

En la flanka menuo, kiun ni vidas en la agordara paĝo, ni povas aliri al
la **Lista de expresiones**, por redakti ĉiujn la regulojn, kiujn ni
estigis, kaj ankaŭ por eksporti kaj importi la regulojn por reuzi ilin en
alia retumilo.

<a href="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png">
<img src="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png" alt="" width="1375" height="992" srcset="/wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete.png 1375w, /wp-content/uploads/2023/02/lista-de-expresiones-de-Cookie-AutoDelete-687x496.png 687w" sizes="(max-width: 1375px) 100vw, 1375px">
</a>
