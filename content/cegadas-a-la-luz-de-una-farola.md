Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2017-12-21 01:46
Lang: es
Slug: cegadas-a-la-luz-de-una-farola
Tags: ciudad, poesía, verso
Title: Cegadas a la luz de una farola

<p>¿Qué es esta jaula en mi derredor?<br>
Es la ciudad; sucio es su motor<br>
¿Dónde está el verde en su esplendor?<br>
Donde hubo tierra asfalto hay sin color.</p>

<p>Si mirar a las estrellas a mí me impide,<br>
si andar libre sus coches me niegan,<br>
si incluso el aire libre puro ha corrompido,<br>
no puedo más que odiar esta ciudad muerta.</p>

<p>Mientras, contemplan luces de monitores.<br>
Pequeños, omnipresentes seguidores.<br>
¿Qué habremos de pensar pues ahora?<br>
Cegadas a la luz de una farola.</p>
