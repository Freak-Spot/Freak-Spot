Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2017-04-08 13:48
Image: <img src="/wp-content/uploads/2017/04/ReTux-enemigo-de-nieve.png" alt="Imagen de Tux en un castillo contra un enemigo final" class="attachment-post-thumbnail wp-post-image" width="800" height="447" srcset="/wp-content/uploads/2017/04/ReTux-enemigo-de-nieve.png 800w, /wp-content/uploads/2017/04/ReTux-enemigo-de-nieve-400x223.png 400w" sizes="(max-width: 800px) 100vw, 800px">
Lang: es
Modified: 2024-02-10 18:00
Slug: retux-una-aventura-contra-el-impuesto-de-pescado
Tags: 2D, arcade, cultura libre, GNU/Linux, plataformas, ReTux, Tux, videojuego, Windows
Title: <cite>ReTux</cite>: una aventura contra el impuesto de pescado

En el videojuego ReTux, Tux es el progragonista de una aventura en la que
deberá derrotar al Rey de la Nieve, quien pretende imponer un impuesto
sobre el pescado. A ningún pingüino le gusta que le quiten su pescado.

<!-- more -->

ReTux es un videojuego de plataformas <i lang="en">software</i> libre y de [cultura
libre](http://freedomdefined.org/Definition/Es) para ordenador. Se puede
descargar de forma gratuita en [su página web
oficial](https://retux-game.github.io/).

El juego comparte algunos rasgos con
[SuperTux](https://www.supertux.org/), así como varios sonidos,
imágenes y música. Pero también tiene características propias. Una de
las que más me gusta es que Tux puede coger, lanzar y soltar objetos.
Si necesitas romper un bloque de madera que está fuera del alcance de
Tux, puedes coger y lanzar un bloque de hielo hacia arriba para
romperlo. Las posibilidades son enormes, y se puede ser bastante
creativo.

<a href="/wp-content/uploads/2017/04/ReTux-pingüino-oculto.png">
<img src="/wp-content/uploads/2017/04/ReTux-pingüino-oculto.png" alt="Un pingüino oculto en ReTux" class="size-fill" width="800" height="449" srcset="/wp-content/uploads/2017/04/ReTux-pingüino-oculto.png 800w, /wp-content/uploads/2017/04/ReTux-pingüino-oculto-400x224.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

Una vez derrotas al Rey de la Nieve, puedes intentar encontrar todos los
pingüinos ocultos. En cada nivel hay un pingüino oculto. Te advierto que
algunos son bastante difíciles de encontrar. En algunos niveles
encontrarás pistas que te ayudarán a encontrarlos.

<a href="/wp-content/uploads/2017/04/ReTux-pista.png">
<img src="/wp-content/uploads/2017/04/ReTux-pista.png" alt="Una pista para encontrar pingüinos ocultos en ReTux" class="size-fill" width="800" height="448" srcset="/wp-content/uploads/2017/04/ReTux-pista.png 800w, /wp-content/uploads/2017/04/ReTux-pista-400x224.png 400w" sizes="(max-width: 800px) 100vw, 800px">
</a>

He disfrutado mucho el juego y se lo recomiendo a personas de todas la
edades, en especial a los aficionados a videojuegos de plataformas.
ReTux es un juego sencillo y divertido al que siempre podrás dedicar
algunos minutos.

Como un vídeo dice más que mil palabras, echadle un vistazo al tráiler y
jugad.

<video controls preload="none" poster="/wp-content/uploads/2017/04/ReTux-pingüino-oculto.png" data-setup="{}">
  <source src="https://video.hardlimit.com/download/streaming-playlists/hls/videos/25ec4cc9-d1ca-4d48-8bbd-8eb739675bd6-480-fragmented.mp4" type="video/mp4">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>
