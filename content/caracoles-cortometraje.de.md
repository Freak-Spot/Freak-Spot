Author: Jorge Maldonado Ventura
Category: Kino
Date: 2019-08-27
Image: <img src="/wp-content/uploads/2017/07/caracol.png" alt="" width="1366" height="768" srcset="/wp-content/uploads/2017/07/caracol.png 1366w, /wp-content/uploads/2017/07/caracol-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
Lang: de
Save_as: Schnecken-Kurzfilm/index.html
Slug: caracoles-cortometraje
Tags: Audacity, Blender, Dystopie, Freie Kultur, Garage Band, GIMP, Krita, Kurzfilm, surrealism, Synfig
Title: Schnecken: ein dystopischer Kurzfilm
URL: Schnecken-Kurzfilm/

<!-- more -->
<video controls poster="/wp-content/uploads/2017/07/caracol.png">
  <source src="/temporal/caracoles.mp4" type="video/mp4">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser</p>
</video>

Pepe ist ein Junge, der müde nach seinem Arbeitstag nach Hause will, um
zu entspannen. In seiner dystopischen Welt voll von Surrealismus,
steigt er in einem Art von Schnecke-Bus, wo er unweigerlich
gedankenverloren wird.

Das Video ist unter <a href="https://creativecommons.org/licenses/by/3.0/deed.de"><abbr title="Creative Commons Namensnennung 3.0 International">CC BY 3.0</abbr></a> und wurde von
[Goblin Refuge](https://goblinrefuge.com/mediagoblin/u/guayabitoca/m/caracoles-snails/)
erhalten.
