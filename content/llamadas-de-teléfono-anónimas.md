Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2023-11-25 09:28
Lang: es
Slug: cómo-hacer-llamadas-de-teléfono-anónimas
Tags: celular, educación, seguridad, teléfonos móviles, Tor, tutorial
Title: Cómo hacer llamadas de teléfono anónimas
Image: <img src="/wp-content/uploads/2023/11/mujer-al-móvil.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/11/mujer-al-móvil.png 1920w, /wp-content/uploads/2023/11/mujer-al-móvil-960x540.png 960w, /wp-content/uploads/2023/11/mujer-al-móvil-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

## Método 1: tomar prestado un móvil

Una forma sencilla de realizar una llamada anónima es tomar prestado un
móvil a una persona de la calle. Si pides el favor de forma amable, le
convences de que no llevas el móvil encima y de que es una emergencia,
lo más probable es que no tengas problema. Si tienes problema, otra
opción es ofrecer algo a cambio por el favor.

<div style="background-color: #aa0909; color: #000007">
<p>Debes saber que <a href="/no-más-celulares-a-partir-de-ahora/#privacidad"><strong>el
sistema telefónico no proporciona privacidad</strong></a>. Por Internet
puedes realizar llamadas cifradas y a través de <a href="https://es.wikipedia.org/wiki/Red_privada_virtual">redes privadas virtuales</a>
o Tor para una mayor privacidad.</p>
</div>

Las llamadas pueden ser grabadas por las operadoras de telefonía y
agencias de inteligencia, así que si necesitas un nivel alto de
privacidad, puedes distorsionar la voz. Si temes que la persona que te
ha dejado el teléfono le revele a alguien información relacionada con tu
identidad (rasgos faciales, estatura, etc.), puedes protegerte cambiando
tu apariencia, vestimenta habitual y estatura para la operación (usando
lentillas de colores, zapatos de plataforma o tacones, afeitándote o
dejándote
barba, cambiando el peinado, etc.). En casos en los que necesites más
privacidad aún, puedes encargarle a otra persona que realice la llamada
en tu lugar.

<a href="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png">
<img src="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png" alt="" width="1920" height="821" srcset="/wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo.png 1920w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-960x410.png 960w, /wp-content/uploads/2023/11/chica-maquillándose-frente-a-espejo-480x205.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
</a>

## Método 2: conseguir una tarjeta SIM de forma anónima

En muchos países es necesario proporcionar tu información personal para
comprar una tarjeta SIM. Es posible evitar esta restricción comprándola
en el mercado negro. Otro forma es convenciendo a una persona que no te
conozca para que te compre una tarjeta a cambio de dinero.

Introduciendo la tarjeta en un móvil de teclas o que tenga un sistema
operativo seguro, podrás hacer llamadas que no estén asociadas a tu
identidad real. De nuevo, si necesitas una máxima privacidad, ten
extrema precaución: no olvides distorsionar la voz, realizar la llamada
desde un lugar que no suelas frecuentar, borrar la llamada del historial,
etc.<!-- more --> **Ten en cuenta que con este método
podrías estar cometiendo un delito[^1]**.

[^1]: En España, por ejemplo, es ilegal la suplantación de identidad.
