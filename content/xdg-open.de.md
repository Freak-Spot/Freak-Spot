Author: Jorge Maldonado Ventura
Category: Bash
Date: 2019-08-26
Lang: de
Slug: xdg-open
Status: published
Tags: irgendwelches Programm von der Kommandozeile zu öffnen, xdg-open
Title: <code>xdg-open</code>

`xdg-open` ist ein ganz nützlicher Befehl. Mit dem können wir
irgendwelches Programm oder URL von der Kommandozeile öffnen. Wenn ich
`xdg-open https://freakspot.net` ausführe, dann öffnet sich die
Startseite dieser Webseite in Abrowser (mein voreingestellte Browser).
Gleich könnte ich anderen Befehl ausführen. Es hat den Nachteil, dass
wir nur einen Parameter nutzen können; folglich muss man zweimal
`xdg-open` ausführen, um zwei Webseiten zu öffnen.
