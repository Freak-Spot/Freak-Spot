Author: Jorge Maldonado Ventura
Category: Textbearbeitung
Date: 2023-02-25 00:05
Image: <img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="">
Lang: de
Save_as: Code-Änderung-in-Vim-mit-vim-gitgutter-anzeigen/index.html
URL: Code-Änderung-in-Vim-mit-vim-gitgutter-anzeigen/
Slug: viendo-las-modificaciones-de-codigo-en-vim-con-vimgitgutter
Tags: Erweiterung, diff, Klartext, Git, Änderungen, Neovim, Vim, vim-gitgutter
Title: Code-Änderung in Vim mit <code>vim-gitgutter</code> anzeigen

Manchmal, wenn du Text oder Quellcode bearbeitest, ist es nützlich, die
Änderungen zu sehen, die du im Vergleich zur vorherigen Version gemacht
hast. Mit [`vim-gitgutter`](https://github.com/airblade/vim-gitgutter)
ist es möglich, dies automatisch zu tun, ohne den Editor zu verlassen.
`vim-gitgutter` ist eine Erweiterung für Vim, die die letzten Änderungen
an einer Datei in einem Git-Repository anzeigt.

<!-- more -->

Die Erweiterung zeigt in der Spalte links von der Zeile die Art der
Bearbeitung, die durchgeführt wurde, an. Standardmäßig werden drei
Symbole mit verschiedenen Farben verwendet:

<a href="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png">
<img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="Bild mit den drei vim-gutter-Symbolen">
</a>

- <span style="color: red">–</span> zeigt an, dass der Inhalt zwischen
  den Zeilen, in denen dieses Symbol steht, entfernt wurde.
- <span style="color: green">+</span> bedeutet, dass die Zeile neu ist.
- <span style="color: yellow">~</span> bedeutet, dass die Zeile geändert
  wurde.

Um die Erweiterung zu installieren, müssen wir Vim mit
Symbolunterstützung haben. Du kannst das überprüfen, indem du `:echo
has('signs')` ausführst. Wenn der letzte Befehl 1 zurückgibt, kannst
du `vim-gitgutter` wie jede andere Erweiterung installieren (fuhr
`:h plugin` in Vim für weitere Informationen über Plugins aus). Du
musst auch Git installiert haben.

Um die Erweiterung zu testen, ändere einfach eine Datei in einem
Git-Repository. Die Symbole werden erscheinen, wenn du Änderungen an
der Datei vornimmst. Bei einem neuen Commit verschwinden die
Symbole, bis du erneut Änderungen vornimmst.

Die Erweiterung verfügt über eine Vielzahl von Optionen und kann ganz
nach deinen Bedürfnissen konfiguriert werden. Weitere Informationen
findest du in der Dokumentation der Erweiterung, indem du `:h
gitgutter` ausführst.
