Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2024-01-29 22:00
Lang: en
Slug: dinero-anónimo-digital-monero
Save_as: cryptocurrencies-anonymity-decentralized-economy/index.html
URL: cryptocurrencies-anonymity-decentralized-economy/
Tags: money, Monero, privacy, economy
Title: Cryptocurrencies, anonymity, decentralized economy
Image: <img src="/wp-content/uploads/2024/01/monero-sticker.png" alt="" width="827" height="1181" srcset="/wp-content/uploads/2024/01/monero-sticker.png 827w, /wp-content/uploads/2024/01/monero-sticker-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">

Many people think that cryptocurrencies are anonymous, but that's not
the case. Most of them are pseudo-anonymous, as transactions can be
traced and thus reveal who owns the <!-- more -->wallet[^1].

Ideally, cryptocurrencies should operate anonymously by default to avoid
government and corporate control of the population; in other words, they
should be something like cash. Why is transaction privacy so important?

If transactions are public, any company or person can know what you have
spent and how you have spent it; they can find out where you live, what
you like, etc. In addition, the money you receive or give may be tainted
(if it has been previously used by criminals). Anyone with technical
knowledge can trace transactions[^2]. There are companies that have
blocked transactions for this reason[^3].

Money should be fungible, that is, a one euro coin should be worth the
same as another one euro coin. Bitcoins are not like that, because they
leave a trace and there are people who would prefer not to accept money
from criminal activities, which is worth less.

Cryptocurrencies, however, have brought about a revolution that has
allowed decentralized transactions without the need for a central bank,
and there are some that do offer true anonymity.

## Monero: a truly anonymous cryptocurrency

<a href="/wp-content/uploads/2023/02/monero-symbol-480.png">
<img src="/wp-content/uploads/2023/02/monero-symbol-480.png" alt="">
</a>

[Monero](https://www.getmonero.org/) is the most popular cryptocurrency
that allows private, decentralized and low-cost transactions. Unlike
most cryptocurrencies, Monero leaves no trace by which transactions,
recipients or payers can be identified: everything is recorded on the
blockchain anonymously.

This, together with its large community of developers, cryptographers,
translator, etc., makes Monero one of the most widely used
cryptocurrencies.

## High energy cost

Cryptocurrencies such as Monero and Bitcoin work with an algorithm
called [proof of work](https://en.wikipedia.org/wiki/Proof_of_work), which guarantees the security of the network, but
also involves a high energy cost. There is another algorithm called
[proof of stake](https://en.wikipedia.org/wiki/Proof_of_stake), which has a lower energy cost, but it presents other issues.

Opponents of cryptocurrencies often criticize their high energy cost,
but the energy cost of the current banking system (bank IT equipment,
ATMs, offices, etc.) is not often discussed as much.

## People's money?

There are those who claim that cryptocurrencies are the people's money,
since they do not depend on central banks controlled by governments,
which usually print money and thus cause its value to decrease. On the
contrary, cryptocurrencies are created with computational power
validating transactions, that is, they are not created out of thin air
and have a practical utility. However, mining cryptocurrencies is beyond
the reach of people without technical knowledge or adequate computer
equipment.

Cryptocurrencies tend to behave similarly to gold or silver, as very few
new cryptocurrencies are issued, which in practice makes many of them
deflationary.

## Why aren't they more widely used?

Cryptocurrencies are currently used a lot: millions of transactions are
made every day. Of course, not everyone uses them, as they are not easy
to use for many people and there's not much incentive to spend them. If
you have something that's going to increase or retain value and
something that's going to lose value, which would you rather spend?
Obviously you keep what's going to increase or retain its value (gold,
some cryptocurrencies) and trade what's going to lose value ([fiat
money](https://en.wikipedia.org/wiki/Fiat_money)) before it loses it. This
phenomenon is what is known as [Gresham's law](https://en.wikipedia.org/wiki/Gresham's_law).

## How to buy cryptocurrencies anonymously?

You should choose a decentralized and free platform such as
[Bisq](https://bisq.network/) or make exchanges in person with cash,
leaving no trace with a bank transfer.

There are platforms that make it very easy to buy cryptocurrencies.
However, some don't let you send cryptocurrencies to your own wallet,
which would be like having money in a bank that doesn't let you take it
out and only lets you exchange it for cash by fiat. That's where novice
speculators tend to go. If using a centralized platform (not
recommended for privacy), it's best to choose a platform that lets you
withdraw money to a wallet, which is under your control &mdash; not
under the control of a company.

## Not paying taxes

When you trade with cash you can avoid paying taxes, same thing with
anonymous cryptocurrencies like Monero.

<a href="/wp-content/uploads/2024/01/monero-sticker.png">
<img src="/wp-content/uploads/2024/01/monero-sticker.png" alt="" width="827" height="1181" srcset="/wp-content/uploads/2024/01/monero-sticker.png 827w, /wp-content/uploads/2024/01/monero-sticker-413x590.png 413w" sizes="(max-width: 827px) 100vw, 827px">
</a>

[^1]: Although transaction mixers can be used to launder cryptocurrencies and cover the money trail.
[^2]: See article [Dirty Crypto Takedown: How Gov’ts Deanonymize Crypto Transactions to Fight Crime](https://beincrypto.com/dirty-crypto-takedown-how-govts-deanonymize-crypto-transactions-to-fight-crime/).
[^3]: See article [Bitcoin's Fungibility Graveyard](https://sethforprivacy.com/posts/fungibility-graveyard/).
