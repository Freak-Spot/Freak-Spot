Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2017-11-01 17:31
Modified: 2020-12-05
Image: <img src="/wp-content/uploads/2017/11/libregamenight.png" alt="" width="1735" height="989" srcset="/wp-content/uploads/2017/11/libregamenight.png 1735w, /wp-content/uploads/2017/11/libregamenight-867x494.png 867w, /wp-content/uploads/2017/11/libregamenight-433x247.png 433w" sizes="(max-width: 1735px) 100vw, 1735px">
Lang: es
Slug: noche-de-videojuegos-libres
Tags: comunidad, cultura libre, en línea, IRC, software libre
Title: Noche de videojuegos libres

La Noche de Juegos Libres (*Libre Game Night*) es una ocasión para
relajarse, jugar y reír en compañía. Se trata de una comunidad que juega
a un videojuego libre en línea cada sábado.

<!-- more -->

Los videojuegos privativos, generalmente dirigidos a las masas a través
de las corporaciones, no respetan la libertad de los usuarios para
modificarlos, estudiarlos, distribuirlos o ejecutarlos. Muchas veces
dichos juegos solo funcionan en una consola cuyo funcionamiento solo
conoce la empresa. Estos factores no solo aíslan y dividen a personas,
sino que también encarecen los juegos.

En contraposición, surgen con un espíritu de autogestión, inclusividad y
libertad comunidades como La Noche de Juegos Libres, que se reúne cada
sábado en el canal <abbr title="Internet Relay Chat">IRC</abbr>
`#libregamenight` de la red [Freenode](https://es.wikipedia.org/wiki/Freenode).

La comunidad tiene también una [página web](https://lgn.xwx.moe/).

