Author: Jorge Maldonado Ventura
Category: Textbearbeitung
Date: 2022-12-02 12:25
Lang: de
Slug: Duden-Wörterbuch-schnell-Terminal
Tags: duden, Wörterbuch
Title: Duden-Wörterbuch schnell über das Terminal
Image: <img src="/wp-content/uploads/2022/12/duden-Hallo-Wort-Terminal.png" alt="" width="736" height="298" srcset="/wp-content/uploads/2022/12/duden-Hallo-Wort-Terminal.png 736w, /wp-content/uploads/2022/12/duden-Hallo-Wort-Terminal-368x149.png 368w" sizes="(max-width: 736px) 100vw, 736px">

Das Duden-Wörterbuch ist eines der besten. Leider ist die Webseite sehr
werbebehaftet und die Benutzeroberfläche macht es sehr langsam, Wörter
nachzuschlagen. Zum Glück gibt es ein [Python-Programm namens
Duden](https://github.com/radomirbosak/duden), das uns erlaubt von dem
Terminal Wörter vom Wörterbuch nachschlagen.

Um das Programm zu installieren, führ einfach die folgenden Befehle aus:

    :::bash
    sudo apt install python3-pip
    sudo pip3 install duden

Dann kannst du `duden Wort` ausführen, um Wörter nachzuschlagen.
