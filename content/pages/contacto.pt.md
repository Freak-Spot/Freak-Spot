Author: Jorge Maldonado Ventura
Date: 2017-10-28 18:01
Modified: 2023-04-23 15:00
Lang: pt
Slug: contacto
Save_as: contato/index.html
URL: contato/
Status: published
Title: Contato

Meu correio eletrônico é
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net).
Usa o meu chave pública <abbr title="GNU Privacy Guard">GPG</abbr>
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc))
para evitar que a mensagem seja lida por outros.
