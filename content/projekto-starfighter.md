Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2021-08-26 13:00
Lang: es
Slug: projekto-starfighter
Save_as: Proyecto-Starfighter/index.html
URL: Proyecto-Starfighter/
Tags: videojuego de tiros, videojuego
Title: Videojuego de disparos <cite lang="en">Project: Starfighter</cite>
Image: <img src="/wp-content/uploads/2021/08/Project-Starfighter.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2021/08/Project-Starfighter.png 1440w, /wp-content/uploads/2021/08/Project-Starfighter-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">

<cite lang="en">Project: Starfighter</cite> es un juego de tiros con una historia y muchos
niveles. Cris Bainfield es el protagonista, que debe luchar contra
WEAPCO, una empresa de armas que domina el universo conocido con naves
de inteligencia artificial.

Puedes instalar el programa en distribuciones basadas en Debian con el
siguiente comando:

    :::bash
    sudo apt install starfighter

<a href="/wp-content/uploads/2021/08/Project-Starfighter-pantalla-de-título.png">
<img src="/wp-content/uploads/2021/08/Project-Starfighter-pantalla-de-título.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2021/08/Project-Starfighter-pantalla-de-título.png 1440w, /wp-content/uploads/2021/08/Project-Starfighter-pantalla-de-título-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">
</a>

Se puede elegir el orden de las misiones volando con la nave al planeta
al que quieres ir.

<a href="/wp-content/uploads/2021/08/Project-Starfighter-sistema-solar-Eyananth.png">
<img src="/wp-content/uploads/2021/08/Project-Starfighter-sistema-solar-Eyananth.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2021/08/Project-Starfighter-sistema-solar-Eyananth.png 1440w, /wp-content/uploads/2021/08/Project-Starfighter-sistema-solar-Eyananth-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">
</a>

La dificultad del juego se configura al iniciar la partida. **Fácil** y
**Súper-Fácil** son buenas opciones para principiantes.

<figure>
<a href="/wp-content/uploads/2021/08/Project-Starfighter-cañón-láser.png">
<img src="/wp-content/uploads/2021/08/Project-Starfighter-cañón-láser.png" alt="" width="1440" height="900" srcset="/wp-content/uploads/2021/08/Project-Starfighter-cañón-láser.png 1440w, /wp-content/uploads/2021/08/Project-Starfighter-cañón-láser-720x450.png 720w" sizes="(max-width: 1440px) 100vw, 1440px">
</a>
    <figcaption class="wp-caption-text">Cañón láser</figcaption>
</figure>

El juego tiene un [sitio web oficial](https://pr-starfighter.github.io/).

<video class="floating" controls="">
    <source src="https://pr-starfighter.github.io/video/starfighter_trailer.webm" type="video/webm">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.webm" type="video/webm">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.ogv" type="video/ogg">
    <source src="https://archive.org/download/ProjectStarfighterTrailer/Project:%20Starfighter%20Trailer.mp4" type="video/mp4">
    <a href="https://archive.org/details/ProjectStarfighterTrailer">Tráiler</a>
</video>
