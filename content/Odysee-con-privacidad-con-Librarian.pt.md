Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2022-10-29 19:00
Modified: 2023-11-30 19:00
Lang: pt
Slug: odysee-con-privacidad-con-librarian
Save_as: Odysee-com-privacidade-com-o-librarian/index.html
URL: Odysee-com-privacidade-com-o-librarian/
Tags: JavaScript, Odysee, privacidade, YouTube
Title: Odysee com privacidade com o librarian
Image: <img src="/wp-content/uploads/2022/06/librarian.svg" alt="">

Odysee é um sítio eletrónico de vídeos que usa um protocolo livre
chamado LBRY que usa uma cadeia de blocos para partilhar ficheiros e
remunerar os criadores com a sua própria criptomoeda. Mediante o LBRY
não é possível censurar um vídeo, como acontece com muitas plataformas
atuais.

O problema é que a plataforma Odysee não é muito respeitosa com a
privacidade e requer JavaScript &mdash; embora o seu código [seja
livre](https://github.com/OdyseeTeam/odysee-frontend). Como alternativa
existe [librarian](https://codeberg.org/librarian/librarian).
<https://lbry.projectsegfau.lt/> é uma instância recomendada, mas
também [existem outras](https://codeberg.org/librarian/librarian#instances).
