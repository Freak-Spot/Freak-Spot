Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2022-10-24 04:00
Lang: es
Slug: modificar-espacio-entre-párrafos-en-LibreOffice-Writer
Tags: LibreOffice Writer
Title: Modificar espacio entre párrafos en LibreOffice Writer
Image: <img src="/wp-content/uploads/2022/10/espacio-entre-párrafos-seleccionado.png" alt="">

Mucha gente que usa LibreOffice considera que la separación entre
párrafos por defecto es muy pequeña y optan por pulsar dos veces la
[tecla Entrar](https://es.wikipedia.org/wiki/Entrar_(tecla)) para separar párrafos.
Pero existe una solución mejor: aumentar el espacio predeterminado entre
párrafos.

Para ello debemos modificar el **Estilo de párrafo predeterminado** en
la sección de estilos de la barra lateral. Esta se puede abrir, si no
está abierta, pulsando F11 o pulsando **Ver ▸ Estilos**.

<a href="/wp-content/uploads/2022/10/ver-estilos-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2022/10/ver-estilos-LibreOffice-Writer.png" alt="">
</a>

<!-- more -->

Para cambiar el estilo de párrafo predeterminado hay que hacer clic
derecho en **Estilo de párrafo predeterminado**[^1] y pulsar
**Modificar...**.

<a href="/wp-content/uploads/2022/10/modificar-estilo-de-párrafo-predeterminado-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2022/10/modificar-estilo-de-párrafo-predeterminado-LibreOffice-Writer.png" alt="">
</a>

A continuación, se nos abrirá un menú. Ahora debemos ir a la pestaña
**Sangrías y espaciado** y modificar el espacio **Bajo el párrafo**
añadiendo los centímetros que consideremos. A mí, personalmente, me gusta
que haya una separación de 0,20 cm entre párrafos. Finalmente, debemos
aplicar los cambios.

<a href="/wp-content/uploads/2022/10/aumentar-separación-entre-párrafos-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2022/10/aumentar-separación-entre-párrafos-LibreOffice-Writer.png" alt="" width="1582" height="734" srcset="/wp-content/uploads/2022/10/aumentar-separación-entre-párrafos-LibreOffice-Writer.png 1582w, /wp-content/uploads/2022/10/aumentar-separación-entre-párrafos-LibreOffice-Writer-791x367.png 791w, /wp-content/uploads/2022/10/aumentar-separación-entre-párrafos-LibreOffice-Writer-395x183.png 395w" sizes="(max-width: 1582px) 100vw, 1582px">
</a>

Estos cambios solo tienen efecto en el documento actual. Si queremos que
este sea el estilo predeterminado de todos nuestros documentos debemos
modificar la plantilla predeterminada o crear una nueva con esta
configuración. La forma más sencilla y recomendada de hacerlo es abrir
un documento nuevo, modificar el espacio **Bajo el párrafo** como
expliqué antes, guardar el documento como plantilla pulsando **Archivo ▸
Plantillas ▸ Guardar como plantilla...**, darle un nombre a nuestra
nueva plantilla, marcar **Establecer como plantilla predeterminada** y
pulsar **Guardar**.

<a href="/wp-content/uploads/2022/10/guardar-como-plantilla-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2022/10/guardar-como-plantilla-LibreOffice-Writer.png" alt="">
</a>

<a href="/wp-content/uploads/2022/10/guardar-como-plantilla-predeterminada-LibreOffice-Writer.png">
<img src="/wp-content/uploads/2022/10/guardar-como-plantilla-predeterminada-LibreOffice-Writer.png" alt="">
</a>

[^1]: Si no ves lo mismo que en mis imágenes, puede que tengas que
  seleccionar los **Estilos de párrafo** (el primer botón) del menú de estilos.
  <a href="/wp-content/uploads/2022/10/seleccionar-estilos-de-párrafo-LibreOffice-Writer.png">
  <img src="/wp-content/uploads/2022/10/seleccionar-estilos-de-párrafo-LibreOffice-Writer.png" alt="">
  </a>
