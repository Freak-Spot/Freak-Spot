Author: Jorge Maldonado Ventura
Category: Reta programado
Date: 2022-11-01 22:22
Image: <img src="/wp-content/uploads/2017/06/tree-html-css.png" alt="Eliro de la tree-programo en HTML kaj CSS">
Lang: eo
Slug: estructura-de-árbol-con-css-html
Save_as: arba-strukturo-per-CSS-kaj-HTML/index.html
URL: arba-strukturo-per-CSS-kaj-HTML/
Tags: CSS, HTML, retejoj
Title: Arba strukturo per CSS kaj HTML

Kelkfoje utilas prezenti datumojn per arba strukturo kiel tiu, kiun
kreas la <code>tree</code>-programo. La <code>tree</code>-programo kreas eligon de arbo de
dosierujoj kiel tiu ĉi:

<pre><samp>
✔ /var/www/html/Repos/Freak-Spot/freak-theme [master|✔] $ tree
.
├── static
│   ├── css
│   │   └── style.css
│   ├── genericons
│   │   ├── COPYING.txt
│   │   ├── genericons.css
│   │   ├── Genericons.eot
│   │   ├── Genericons.svg
│   │   ├── Genericons.ttf
│   │   ├── Genericons.woff
│   │   ├── LICENSE.txt
│   │   └── README.md
│   ├── images
│   │   ├── creativecommons_public-domain_80x15.png
│   │   ├── gnu-head-mini.png
│   │   └── questioncopyright-favicon.png
│   └── js
│       ├── functions.js
│       └── jquery-3.1.1.js
└── templates
    ├── archives.html
    ├── article.html
    ├── article_info.html
    ├── author.html
    ├── authors.html
    ├── base.html
    ├── category.html
    ├── index.html
    ├── page.html
    ├── pagination.html
    ├── period_archives.html
    ├── tag.html
    ├── taglist.html
    └── tags.html

6 directories, 28 files
</samp></pre>

Por prezenti la komandon tiel, kiel ĝi aperas en terminalo, mi uzis la
HTML-etikedojn `<samp>` kaj `<pre>` (`<pre><samp>eliro de
tree</samp></pre>`). Sed kio okazas, se mi volas inkludi ligilon aŭ uzi
aliajn HTML-elementojn, aŭ CSS? Tiuokaze ni devas uzi CSS por montri la
branĉan aspekton.

<!-- more -->

Ni uzu kiel ekzemplon la antaŭan eliron. Tiun ni povas esprimi tiel en
HTML:

    :::html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="UTF-8">
        <title>Arba listo</title>
      </head>
      <body>
        <div class="arba-ujo">
          <ul>
            <li>static
              <ul>
                <li>css
                  <ul>
                    <li>style.css</li>
                  </ul>
                </li>
                 <li>genericons
                   <ul>
                     <li>COPYING.txt</li>
                     <li>genericons.css</li>
                     <li>Genericons.eot</li>
                     <li>Genericons.svg</li>
                     <li>Genericons.ttf</li>
                     <li>Genericons.woff</li>
                     <li>LICENSE.txt</li>
                     <li>README.md</li>
                   </ul>
                 </li>
                <li>images
                  <ul>
                    <li>creativecommons_public-domain_80x15.png</li>
                    <li>gnu-head-mini.png</li>
                    <li>questioncopyright-favicon.png</li>
                  </ul>
                </li>
                <li>js
                  <ul>
                    <li>functions.js</li>
                    <li>jquery-3.1.1.js</li>
                  </ul>
                  </li>
              </ul>
            </li>
            <li>templates
              <ul>
                <li>archives.html</li>
                <li>article.html</li>
                <li>article_info.html</li>
                <li>author.html</li>
                <li>authors.html</li>
                <li>base.html</li>
                <li>category.html</li>
                <li>index.html</li>
                <li>page.html</li>
                <li>pagination.html</li>
                <li>period_archives.html</li>
                <li>tag.html</li>
                <li>taglist.html</li>
                <li>tags.html</li>
              </ul>
            </li>
          </ul>
        </div>
      </body>
    </html>

Unue ni devas krei regulojn, por ke la listo kaj ĝia ujo ne
enmiksiĝu en la desegno, kiun ni poste faros per la CSS-regulo `border`.

    :::css
    .arba-ujo, .arba-ujo ul, .arba-ujo li {
        position: relative;
    }

    .arba-ujo ul {
        list-style: none;
    }

Sekve ni ŝanĝu la pozicion de la pseŭdoelementoj por havi pli da spaco
por la arbaj linioj.

    :::css
    .arba-ujo li::before, .arba-ujo li::after {
        content: "";
        position: absolute;
        left: -12px;
    }

Poste, pere de pseŭdoelementoj, ni pentru la liniojn horizontalajn
(`::before`) kaj vertikalajn (`::after`).

    :::css
    .arba-ujo li::before {
        border-top: 1px solid green;
        top: 9px;
        width: 8px;
        height: 0;
    }

    .arba-ujo li::after {
        border-left: 1px solid brown;
        height: 100%;
        width: 0px;
        top: 2px;
    }

La lasta retuŝo estas fari, ke la branĉo ne kresku troe en la lasta
elemento.

    :::css
    .clt ul > li:last-child::after {
        height: 8px;
    }

Jen la rezulto:

<iframe allowfullscreen src="/wp-content/uploads/2022/11/arba-strukturo-per-CSS-HTML.html" style="width: 100%; background-color: white"></iframe>

Sube estas la tuta kodo. Evidente oni povas modifi ĝin kaj ŝanĝi multajn
dimensiojn por atingi aspekton kaj funkciaron pli adaptitan.

    :::html
    <!DOCTYPE html>
    <html>
      <head>
        <meta charset="UTF-8">
        <title>Arba listo</title>
        <style>
          .arba-ujo .arba-ujo ul, .arba-ujo li {
              position: relative;
          }

          .arba-ujo ul {
              list-style: none;
          }

          .arba-ujo li::before, .arba-ujo li::after {
              content: "";
              position: absolute;
              left: -12px;
          }

          .arba-ujo li::before {
              border-top: 2px solid green;
              top: 9px;
              width: 8px;
              height: 0;
          }

          .arba-ujo li::after {
              border-left: 2px solid brown;
              height: 100%;
              width: 0px;
              top: 2px;
          }

          .arba-ujo ul > li:last-child::after {
              height: 8px;
          }
        </style>
      </head>
      <body>
        <div class="arba-ujo">
          <ul>
            <li>static
              <ul>
                <li>css
                  <ul>
                    <li>style.css</li>
                  </ul>
                </li>
                 <li>genericons
                   <ul>
                     <li>COPYING.txt</li>
                     <li>genericons.css</li>
                     <li>Genericons.eot</li>
                     <li>Genericons.svg</li>
                     <li>Genericons.ttf</li>
                     <li>Genericons.woff</li>
                     <li>LICENSE.txt</li>
                     <li>README.md</li>
                   </ul>
                 </li>
                <li>images
                  <ul>
                    <li>creativecommons_public-domain_80x15.png</li>
                    <li>gnu-head-mini.png</li>
                    <li>questioncopyright-favicon.png</li>
                  </ul>
                </li>
                <li>js
                  <ul>
                    <li>functions.js</li>
                    <li>jquery-3.1.1.js</li>
                  </ul>
                  </li>
              </ul>
            </li>
            <li>templates
              <ul>
                <li>archives.html</li>
                <li>article.html</li>
                <li>article_info.html</li>
                <li>author.html</li>
                <li>authors.html</li>
                <li>base.html</li>
                <li>category.html</li>
                <li>index.html</li>
                <li>page.html</li>
                <li>pagination.html</li>
                <li>period_archives.html</li>
                <li>tag.html</li>
                <li>taglist.html</li>
                <li>tags.html</li>
              </ul>
            </li>
          </ul>
        </div>
      </body>
    </html>
