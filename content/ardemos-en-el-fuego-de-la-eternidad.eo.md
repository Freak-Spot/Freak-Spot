Author: Jorge Maldonado Ventura
Category: Literaturo
Date: 2022-12-31 11:41
Lang: eo
Slug: arderemos-en-el-fuego-de-la-eternidad
Save_as: ni-brulos-en-la-fajro-de-la-eterneco/index.html
URL: ni-brulos-en-la-fajro-de-la-eterneco/
Tags: poezio, verso
Title: Ni brulos en la fajro de la eterneco
Image: <img src="/wp-content/uploads/2022/12/fuego.webp" alt="" width="1200" height="800" srcset="/wp-content/uploads/2022/12/fuego.webp 1200w, /wp-content/uploads/2022/12/fuego.-600x400. 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Vi kaj mi estas la arbaro,<br>
kun siaj branĉoj, kun siaj ombroj,<br>
kun siaj lumoj, kun la vento,<br>
kiu naskas uraganon.

Vi kaj mi, ĉiame brakumitaj...<br>
brulos en la fajro de la eterneco.<br>

`git commit -m "Mi amas vin"`

<small>Ĉar ni estos memoritaj per niaj agoj. 🔥</small>
