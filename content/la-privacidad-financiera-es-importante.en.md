Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2024-01-16 02:15
Lang: en
Slug: jamás-pagues-con-tarjeta
Save_as: why-you-should-never-pay-by-card/index.html
URL: why-you-should-never-pay-by-card/
Tags: finances
Title: Why you should never pay by card
Image: <img src="/wp-content/uploads/2024/01/privacidad-financiera.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2024/01/privacidad-financiera.png 1920w, /wp-content/uploads/2024/01/privacidad-financiera-960x540.png 960w, /wp-content/uploads/2024/01/privacidad-financiera-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Banks love to sell you cards. Don't fall into the trap:
paying by card comes with a lot of dangers and extra costs.

<!-- more -->

## You will spend more

Numerous studies[^1] show that people who pay by card spend more. When you
pay with cash, you see the money you are going to spend with your own
eyes, and thus you consume less impulsively. With a card, on the other hand, the
amount you spend is in the background; you don't have to count bills or
think about how much money you have left in your wallet, you just swipe
the card without thinking.

On top of that, you have to add the fees that banks charge for using
cards and the commissions when you pay on credit.


<h2 id="falta-de-privacidad">Lack of privacy</h2>

Banks sell your financial information to other companies. Even if they
don't, many bankers and computer companies can see your financial
activity. When you buy by card, purchase data such as the time of
payment, the product or the amount are no longer private.

This data can be used to know what percentage of your income you spend
on food, leisure, drugs, etc. If you have a company, this data can be
bought or hacked, getting into the hands of your competitors, who can
use it to beat you in a [price war](https://en.wikipedia.org/wiki/Price_war), for example.

## They can pull the rug from under your feet

Unlike cash, which cannot be controlled remotely, cards can be
deactivated or blocked by banks at any time. Also, if there is no
electricity or Internet, you can't pay.

## Everyone pays more taxes

Businesses that accept card payments cannot hide them from the
government. So they end up paying more taxes, which will be used to fund
wars or other unpopular policies. When you pay by card, you make the
bank and the government gain power while you lose it.

## Conclusion

Cards leave you poorer, more dependent, less powerful and with less
privacy. That's why I recommend paying in cash or with cryptocurrencies
that allow private transactions, such as [Monero](https://www.getmonero.org/).
Cards would not be so problematic if payments were anonymous and could
not be blocked or restricted by the bank.

[^1]: Banker, S., Dunfield, D., Huang, A. <i lang="lt">et al.</i> Neural mechanisms of credit card spending. <cite>Sci Rep, 11</cite>, 4070 (2021). <https://doi.org/10.1038/s41598-021-83488-3>
