Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2024-02-01 17:07
Lang: es
Slug: casi-un-4-por-ciento-de-los-ordenadores-de-escritorio-usa-gnulinux
Tags: estadística, GNU/Linux
Title: Casi un 4 % de los ordenadores personales usa GNU/Linux
Image: <img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png" alt="" width="1139" height="563" srcset="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png 1139w, /wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024-569x281.png 569w" sizes="(max-width: 1139px) 100vw, 1139px">

Según datos de Statcounter, GNU/Linux es el sistema operativo usado en
el 3,77&nbsp;% de los ordenadores personales (portátiles y
sobremesa)<!-- more -->[^1]. Podemos ver en la gráfica que en enero de
2021 el porcentaje de GNU/Linux era de 1,91&nbsp;%, lo que significa que
en tres años ha doblado su popularidad. Cabe mencionar también que
la cuota de uso de Chrome OS (que usa el núcleo Linux) es del 1,78&nbsp;%.

<a href="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png">
<img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png" alt="">
</a>

Sin embargo, esta estadística no es totalmente representativa, ya que el
código de rastreo de Statcounter está instalado en tan solo 1,5 millones
de sitios web[^2]. Asimismo, algunos usuarios de
GNU/Linux &mdash;los que se preocupan más por la privacidad&mdash; usan
herramientas que cambian el
[`User-Agent`](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/User-Agent)
(un ejemplo es Tor Browser, que siempre dice usar Windows para
camuflarse mejor).

En cualquier caso, se trata de un crecimiento impresionante que
probablemente continúe en los próximos años, ya que GNU/Linux está cada
vez más presente en instituciones educativas y muchos países están
tratando de aumentar su soberanía tecnológica (GNU/Linux es la
forma más barata y segura de hacerlo).

[^1]: <https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-200901-202401>
[^2]: «<i lang="en">Our tracking code is installed on more than 1.5 million sites
    globally</i>»: esto es lo que dicen en <https://gs.statcounter.com/faq#methodology>.
