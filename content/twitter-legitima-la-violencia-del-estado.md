Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-01-03 23:31
Image: <img src="/wp-content/uploads/2018/01/Twitter-legitima-la-violencia-estatal.png" alt="Una policia con la cara de la mascota de Twitter golpeando con una porra a una persona indefensa">
Lang: es
Slug: twitter-legitima-la-violencia-del-estado
Tags: capitalismo, estado, página web, represión, redes sociales, Twitter, violencia
Title: Twitter legitima la violencia del estado

La conocida red social de software privativo Twitter ha creado una nueva
política en la que legitima la violencia estatal.

<!-- more -->

[Esta nueva política](https://blog.twitter.com/official/en_us/topics/company/2017/safetypoliciesdec2017.html)
se hizo efectiva el 18 de diciembre de 2017. En su anuncio de
actualización afirman que la han actualizado «para reducir el contenido
de odio y abusivo». En ella detallan lo que se considera violencia.

> ## Nuevas Reglas sobre Violencia y Daño físico
>
> Amenazas explícitas de violencia o el deseo de daño físico, muerte o
> enfermedad a una individua o grupo de personas es una violación de
> nuestras políticas. Nuestros nuevos cambios incluyen más tipos de
> contenido relacionado:
>
> * [...]
> * Las cuentas que se afilien con organizaciones que usen o promuevan
>   la violencia contra civiles para promover sus causas. Los grupos
>   incluidos en esta política son los que se identifican como tales o
>   participan en actividades &mdash;tanto dentro como fuera de la
>   plataforma&mdash; que incitan la violencia. <mark>Esta política no se
>   aplica a grupos militares u entidades gobernamentales</mark> y
>   consideraremos excepciones para grupos que están actualmente
>   participando en (o hayan participado en) resolución pacífica de
>   conflictos.

(El énfasis lo he añadido yo). En resumen, Twitter legitima la
violencia del estado. No supone ningún problema moral para una empresa
capitalista cuyo único fin es obtener beneficios legitimar el monopolio
de la violencia por parte del estado.

Twitter
[colabora abiertamente](https://transparency.twitter.com/en/information-requests.html)
con estados represivos de todo el mundo facilitándoles información usada
en muchos casos para encarcelar o rastrear a disidentes. Esta nueva
política supone un paso más allá, legitimándoles explícitamente.
