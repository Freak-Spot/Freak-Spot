Author: Хорхе Мальдонадо Вентура
Date: 2017-10-28 18:01
Lang: ru
Modified: 2023-04-23 15:00
Slug: contacto
Status: published
Title: Контактная информация

Мой адрес электронной почты
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net). Используйте мой публичный
<abbr title="GNU Privacy Guard">GPG</abbr> ключ
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc)),
чтобы ваше сообщение не могло быть прочитано другими людьми.
