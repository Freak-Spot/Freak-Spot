Author: Jorge Maldonado Ventura
Category: Notícias
Date: 2018-03-17 23:59
Image: <img src="/wp-content/uploads/2018/03/recaptcha-usado-para-matar.png" alt="Preencher um reCAPTCHA do Google ajuda a matar">
Lang: pt
Slug: al-completar-un-recaptcha-de-google-ayudas-a-matar
Save_as: preencher-reCAPTCHA-do-Google-ajuda-a-matar/index.html
URL: preencher-reCAPTCHA-do-Google-ajuda-a-matar/
Tags: Estados Unidos da América, Google, Pentágono, reCAPTCHA
Title: Preenchendo um reCAPTCHA do Google ajudas a matar

A Google estabeleceu recentemente uma parceria com o
[Pentágono](https://pt.wikipedia.org/wiki/O_Pent%C3%A1gono) para
ajudar a desenvolver inteligência artificial. O projecto, denominado
Maven, envolve o desenvolvimento de um sistema de identificação de
objectos através de imagens de <i lang="en">drones</i>.

Isto significa que o poder do Google sobre as pessoas em todo o mundo
será utilizado pelo império americano para os seus interesses sombrios.
A resolução de um reCAPTCHA de Google [já não implica apenas perigos éticos](/pt/Como-o-Google-explora-com-os-CAPTCHAs/)
relacionados com a exploração económica e a utilização de software
proprietário, mas também uma estreita colaboração no assassinato de
seres humanos. Assassinatos que parecem desumanizados, mas que no final
são pessoas inconscientes (e conscientes) treinando máquinas para matar.

