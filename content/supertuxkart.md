Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2022-07-09 10:45
Lang: es
Slug: videojuego-de-carreras-supertuxkart
Tags: carreras, coches, GNU/Linux, videojuego, Windows
Title: Videojuego de carreras: <cite lang="en">SuperTuxKart</cite>

SuperTuxKart es un videojuego de carreras de gran calidad. Los
personajes son las mascotas de programas libres, como Tux, Gnu y
Wilber (la mascota de GIMP). Tiene un modo historia, varios niveles de
dificultad y permite jugar en línea.

<figure>
<a href="/wp-content/uploads/2022/07/gavroche-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/gavroche-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/gavroche-supertuxkart.png 1024w, /wp-content/uploads/2022/07/gavroche-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
    <figcaption class="wp-caption-text">Selección de personaje</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/07/desafío-modo-historia-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/desafío-modo-historia-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/desafío-modo-historia-supertuxkart.png 1024w, /wp-content/uploads/2022/07/desafío-modo-historia-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
    <figcaption class="wp-caption-text">Animación del modo historia</figcaption>
</figure>

Pero además de las clásicas carreras de coches con objetos, existen modos
de juego diferentes: fútbol, sigue al líder, contrarreloj y batalla. De
estos el más interesante para jugar en línea es fútbol, ya que permite
competir por equipos. Suele haber algún torneo de fútbol en línea cada
mes.

<figure>
<a href="/wp-content/uploads/2022/07/fútbol-supertuxkart.png">
<img src="/wp-content/uploads/2022/07/fútbol-supertuxkart.png" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/07/fútbol-supertuxkart.png 1024w, /wp-content/uploads/2022/07/fútbol-supertuxkart-512x384.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>
    <figcaption class="wp-caption-text">Partido de fútbol</figcaption>
</figure>

El juego se puede instalar en distribuciones de GNU/Linux derivadas de
Debian ejecutando `sudo apt install supertuxkart`.

Abajo dejo el tráiler de la versión más reciente, la 1.3:

<!-- more -->

<video controls="">
<source src="/video/trailer-supertuxkart.mp4" type="video/mp4">
Tráiler de la versión 1.3 de SuperTuxKart
</video>
