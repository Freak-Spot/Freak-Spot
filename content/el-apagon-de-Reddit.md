Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-06-11 10:04
Lang: es
Slug: deja-de-usar-Reddit
Tags: Reddit, Lemmy, Postmill, Lobsters, /kbin, privacidad, software libre
Title: Deja de usar Reddit
Image: <img src="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg" alt="" width="2048" height="1024" srcset="/wp-content/uploads/2023/06/alternativas-a-Reddit.jpeg 2048w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-1024x512.jpg 1024w, /wp-content/uploads/2023/06/alternativas-a-Reddit.-512x256.jpg 512w" sizes="(max-width: 2048px) 100vw, 2048px">

[Miles de comunidades de
Reddit](https://safereddit.com/r/ModCoord/comments/1401qw5/incomplete_and_growing_list_of_participating/)
dejarán de ser accesibles mañana en protesta [contra la decisión de
cobrar millones de dólares a aplicaciones que usan la
API](https://safereddit.com/r/apolloapp/comments/144f6xm/apollo_will_close_down_on_june_30th_reddits/).
Como resultado de esa política, muchas aplicaciones dejarán de funcionar.

Sin embargo, aunque hay gente que está recomendando dejar de usar
Reddit, la protesta está limitada a dos días. El problema de hacer un
boicot temporal es que le manda este mensaje a los dueños:
mucha gente está enfadada, pero después de dos días van a volver y vamos
a seguir ganando dinero. Reddit
[dejó hace años de ser <i lang="en">software</i> libre](https://freakspot.net/raddle-como-respuesta-a-reddit/)
y no va a dejar de censurar la información que considera indeseable.

Apoyo el boicot a Reddit,
pero creo que no debería durar solo dos días; debería ser permanente.
Existen varios programas similares a Reddit que son libres y respetan la
privacidad de sus usuarios: [Lemmy](https://join-lemmy.org/),
[/kbin](https://kbin.pub/),
[Postmill](https://postmill.xyz/),
[Lobsters](https://lobste.rs/), [Tildes](https://tildes.net/)...

[Lemmy](https://join-lemmy.org/) y [/kbin](https://kbin.pub/), a
diferencia de Reddit, son programas libres y federados, por lo que los
administradores de un nodo no pueden censurar información de nodos que
no controlan ni imponerles nada; cada nodo tiene su propia política.
