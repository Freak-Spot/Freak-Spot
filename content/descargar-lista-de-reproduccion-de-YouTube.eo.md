Author: Jorge Maldonado Ventura
Category: Ruzo
Date: 2023-04-13 11:00
Lang: eo
Slug: descargar-lista-de-reproduccion-de-youtube
Save_as: elŝuti-ludliston-el-YouTube/index.html
URL: elŝuti-ludliston-el-YouTube/
Tags: ludlisto, PeerTube, YouTube
Title: Elŝuti ludliston el YouTube
Image: <img src="/wp-content/uploads/2023/04/logo-yt-dlp.png" alt="">

Vi povas uzi la programon [`yt-dlp`](https://github.com/yt-dlp/yt-dlp/).
[Estas kelkaj manieroj instali ĝi](https://github.com/yt-dlp/yt-dlp/#installation), unu el ili
estas pere de la pako-administrilo de via distribuo:

    :::text
    sudo apt install yt-dlp

Por elŝuti ludliston oni simple devas tajpi en terminalo `yt-dlp`, la
URL-n de la ludlisto kaj premi la enigan klavon. Ekzemple:

    :::text
    yt-dlp https://youtube.com/playlist?list=PLvvDxND6LkgB_5dssZod-3BET4_DFRmtU

Ĉi tiu programo ne nur elŝutas videojn el YouTube, sed ankaŭ povas
elŝuti videojn el [multaj retejoj](https://ytdl-org.github.io/youtube-dl/supportedsites.html).
