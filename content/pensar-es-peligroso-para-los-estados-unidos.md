Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2016-09-07 23:50
Lang: es
Slug: pensar-es-peligroso-para-los-estados-unidos
Status: published
Tags: Craig Murray, Estados Unidos, política internacional, Estados Unidos de América
Title: Pensar es peligroso para los Estados Unidos

Los Estados Unidos de América deniegan a Craig Murray sin motivo la
entrada en el país.

Craig Murray fue embajador de Reino Unido en Uzbekistán, pero fue
destituido por criticar las torturas sistemáticas realizadas en ese
país. Según [cuenta en su sitio
web](https://www.craigmurray.org.uk/archives/2016/09/thought-dangerous-usa/),
«las únicas posibles justificaciones para esta negación de entrada es
que he escrito en contra del neoliberalismo, de ataques contra las
libertades públicas y contra políticas exteriores neoconservadoras. Las
personas en la conferencia de Washington \[la conferencia a la que tenía
previsto asistir\] no podrán escucharme hablar ahora». Concluye
irónicamente diciendo, «¡Es demasiado para la tierra de lo libre!».

> "Plainly ideas can be dangerous. So much for the land of the free!".
> —Craig Murray
