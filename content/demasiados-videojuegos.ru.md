Author: Хорхе Мальдонадо Вентура
Category: Видеоигры
Date: 2024-02-02 17:06
Lang: ru
Slug: compras-videojuegos-yo-tengo-mas-de-6000-gratis
Tags: покупки, эмуляция, GNU/Linux, свободное программное обеспечение
Save_as: vy-vsë-eŝe-pokupaete-videoigry-U-menja-estʹ-bolee-6000-i-soveršenno-besplatno/index.html
URL: vy-vsë-eŝe-pokupaete-videoigry-U-menja-estʹ-bolee-6000-i-soveršenno-besplatno/
Title: Вы всё еще покупаете видеоигры? У меня есть более 6000 и совершенно бесплатно
Image: <img src="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png" alt="" width="1441" height="835" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando.png 1441w, /wp-content/uploads/2023/11/captura-de-videojuego-Captain-Comando-720x417.png 720w" sizes="(max-width: 1441px) 100vw, 1441px">

Индустрия видеоигр &mdash; это многомиллиардная индустрия. Однако существуют
тысячи игр, в которые можно играть совершенно бесплатно. Я говорю не
только о бесплатных играх, распространяемых по принципу бесплатного
програмного обеспечения, но и о старых аркадах, консольных играх и т. д.

Если вы используете GNU/Linux, вы можете установить большинство видеоигр,
используя стандартный менеджер пакетов вашего дистрибутива. Другие игры
распространяются посредствам Flatpak, Snap и AppImage или должны
быть скомпилированы вручную. Для поиска бесплатных игр я рекомендую
воспользоваться библиотекой от [LibreGameWiki](https://libregamewiki.org/Main_Page).

Однако у нас есть не только бесплатные игры, но и тысячи старых аркадных
игр, в которые можно играть с помощью<!-- more --> <abbr title="Multiple Arcade Machine Emulator">MAME</abbr>[^1]. Есть также
эмуляторы для консолей, для игр, работающих только на Windows, и т. д.
Многие старые игры очень увлекательны, настолько, что большинство
современных игр являются всего лишь их интерпретациями на современный лад.

Конечно, есть и те, кто предпочитает современные и инновационные видеоигры,
в которые можно играть в высоком разрешении. Проблема в том, что многие
из этих игр являются проприетарными, стоят каких-то баснословных денег
и обычно доступны только на самых распространенных языках. С другой стороны,
игры, распространяемые по принципу бесплатного програмного обеспечения,
всегда можно улучшить, и они часто доступны на самых различных языках.
Я бы предпочел отдать деньги на развитие бесплатных игр, чем тратить
их на то, что я не смогу изменить.

<a href="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png">
<img src="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png" alt="" width="804" height="1080" srcset="/wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball.png 804w, /wp-content/uploads/2023/11/captura-de-videojuego-Capcom-Baseball-402x540.png 402w" sizes="(max-width: 804px) 100vw, 804px">
</a>

Лучший подход &mdash; обзавестись компьютером, который позволяет играть в
любые игры и в любое время, в любом разрешении...
В моем случае такой машиной является компьютер с GNU/Linux.

[^1]: Вы можете установить его с помощью команды `sudo apt install mame` в
    дистрибутивах на базе Debian. Игры (ROMs) можно скачать как на веб-сайте
  <https://archive.org/download/mame-merged/mame-merged/>,
  <https://www.mamedev.org/roms/>, так и из многих других источников.
  В Debian вы должны сохранять игры в каталоге `~/mame/roms`.
