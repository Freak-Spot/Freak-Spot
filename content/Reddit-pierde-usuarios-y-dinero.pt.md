Author: Jorge Maldonado Ventura
Category: Notícias
Date: 2023-06-18 15:49
Lang: pt
Slug: reddit-pierde-usuarios-y-dinero-debido-a-la-protesta-de-sus-usuarios
Save_as: Reddit-perdas-uzantojn-kaj-monon-pro-ignori-la-plendojn-de-siaj-uzantoj/index.html
URL: Reddit-perdas-uzantojn-kaj-monon-pro-ignori-la-plendojn-de-siaj-uzantoj/
Tags: Reddit, Lemmy
Title: Reddit perde utilizadores e dinheiro por ignorar o descontentamento dos seus próprios utilizadores
Image: <img src="/wp-content/uploads/2023/06/reddit-en-llamas.jpg" alt="" width="763" height="639" srcset="/wp-content/uploads/2023/06/reddit-en-llamas.jpg 763w, /wp-content/uploads/2023/06/reddit-en-llamas-381x319.jpg 381w" sizes="(max-width: 763px) 100vw, 763px">

O Reddit está a pagar o preço por ter ignorado o [protesto dos seus
utilizadores contra as últimas alterações da empresa](/pt/deixa-de-utilizar-o-Reddit/).

Por um lado, muitas pessoas mudaram-se para plataformas como o
[Lemmy](https://join-lemmy.org/), que triplicou o seu número de
utilizadores num curto espaço de tempo:

<a href="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png">
<img src="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png" alt="" width="1130" height="320" srcset="/wp-content/uploads/2023/06/usuarios-totales-Lemmy.png 1130w, /wp-content/uploads/2023/06/usuarios-totales-Lemmy-565x160.png 565w" sizes="(max-width: 1130px) 100vw, 1130px">
</a>

Por outro lado, o sítio web do Reddit caiu devido a protestos em que
mais de 7000 fóruns <!-- more -->deixaram de ser públicos[^1] e quatro milhões de pessoas
deixaram de utilizar o sítio[^2]. Numa tentativa de minimizar os danos,
o Reddit retirou das equipas de moderação alguns moderadores que
protestavam, substituindo-os por moderadores obedientes[^3].

No entanto, há fóruns que ainda estão fechados. Há também utilizadores
que carregam conteúdos irrelevantes para a plataforma, de forma a
diminuir o valor da empresa e a continuar o protesto. Um exemplo é o
[fórum r/pic](https://safereddit.com/r/pics), que agora é um «lugar para
fotos nas que John Oliver fica <i lang="en">sexy</i>». Além disso, há
quem esteja a encher a plataforma com vídeos de ruído[^4].

<a href="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png">
<img src="/wp-content/uploads/2023/06/foro-pics-meme-Reddit.png" alt="">
</a>

Todo este descontentamento não passou despercebido a várias empresas de
publicidade, que suspenderam as suas campanhas[^5].

Os moderadores trabalham gratuitamente para a empresa; não foram
ouvidos. Os utilizadores, que também são essenciais, também não foram
ouvidos. O resultado tem sido desastroso para o Reddit: perda de
utilizadores, perda de receitas publicitárias e aumento de conteúdos
inúteis.

[^1]: Peters, J. (12 de junho de 2023). <cite>Reddit crashed because of the growing subreddit blackout</cite>. <https://www.theverge.com/2023/6/12/23758002/reddit-crashing-api-protest-subreddit-private-going-dark>
[^2]: Bonifacic, I. (17 de junho de 2023). <cite>Reddit’s average daily traffic fell during blackout, according to third-party data</cite>. <https://www.engadget.com/reddits-average-daily-traffic-fell-during-blackout-according-to-third-party-data-194721801.html>
[^3]: Divided by Zer0 (17 de junho de 2023). <cite>Mates, today without warning, the reddit royal navy attacked. I’ve been demoded by the admins.</cite>. <https://lemmy.dbzer0.com/post/35555>.
[^4]: Horizon (15 de junho de 2023). <cite>Since the blackout isn't
    working, everyone upload 1gb large videos of noise to reddit
    servers</cite>.
    <https://bird.trom.tf/TheHorizon2b2t/status/1669270005010288640>.
[^5]: Perloff, C. (14 de junho de 2023). <cite>Ripples Through Reddit as Advertisers Weather Moderators Strike</cite>. <https://www.adweek.com/social-marketing/ripples-through-reddit-as-advertisers-weather-moderators-strike/amp/>.
