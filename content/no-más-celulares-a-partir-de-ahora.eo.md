Author: Alyssa Rosenzweig
Category: Privateco
Date: 2017-10-18
Modified: 2023-06-18 09:00
Lang: eo
Slug: no-más-celulares-a-partir-de-ahora
Save_as: sen-poŝtelefonoj/index.html
URL: sen-poŝtelefonoj/
Tags: konsilo, poŝtelefono, edukado, libera programado, Tor
Title: Ne plu poŝtelefonoj

*Ĉi tiu artikolo estas traduko de la angla artikolo [«No cellphones beyond this point»](https://web.archive.org/web/20181026102419/https://rosenzweig.io/blog/no-cellphones.html) de Alyssa Rosezweig, sub la <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.eo"><abbr title="Creative Commons Atribuite-Samkondiĉe 4.0 International">CC BY-SA 4.0</abbr>-permesilo</a>*.

Mi rifuzas kunporti poŝtelefonon — al miaj konfuzitaj amikoj en nia
socio obsedita pri la teĥnologio—, tie ĉi la kialo. Iuj el vi jam
demandis pri mia telofonnumero por skribi al mi. Eble vi estis instruisto
en unu el miaj kursoj demandante, ke mi plenumu iun proprietan
programaron en la kursejo. Eble vi estis familiano zorgita pri tio, ke
en malsekura situacio mi ne povu telefoni por peti helpon.

Mia rifuzo havi poŝtelefonon estas pro kvar rezonoj, malgraŭ tio ke mi
estas agema Interreta uzulo. En ordo de malplej al plej da graveco:

Unue, poŝtelefonoj estas malkomforta por mi. Mia tempo ĉe komputiloj
mi grandege uzas per skribi, programi kaj krei arton; por mi ĉi tiuj
taskoj bezonas grandajn klavarojn aŭ desegnan tabuleton. Ĉi tiu ja ne estas
etika kialo eviti poŝtelefonojn kaj tabuletojn kaj mi agnoskas, ke
multaj homoj havas uzojn pli konvenajn por la etaj aparatoj.

Due, poŝtelefonaj uzantoj kreas poŝtelefonan kulturon. En frakcio da
homa vivotempo, poŝtelefonoj transformiĝis el malestantaj al socie
akceptitaj por esti uzataj dum oni parolas al iu en reala vivo. Ĉi tiu
kulturo ne estas neevitebla por ciferecaj aparatoj — multaj homoj uzas
teĥnologion response, pro kio mi aplaŭdas —, sed ĝi restas deprime
kutima. Se mi havus poŝtelefonon antaŭ mia nazo dum mi simulas paroli al
miaj propraj amikoj, ĝi daŭrigus la ideon, ke tio *estas bona
agmaniero*. Ĉar mi timas, ke mi iĝu iu, kiu malbone uzas teĥnologion
tiumaniere, mi tute evitas kunporti poŝtelefonon por eviti la etikan
riskon.

<p id="privateco">Trie, poŝtelefonoj estas grandaj riskoj por libereco kaj privateco. La
plejparto de poŝtelefonoj en la bazaroj plenumas proprietan operaciumon,
kiel iOS kaj havas amason da proprietaj programoj. Ankaŭ, malsame ol la
plej granda parto de labortablaj kaj tekaj komputiloj, multaj el tiuj
operaciumoj rulas kontrolojn de subskribo. Tio estas, anstataŭigi la
sistemon per libera programaro estas kriptografie maleble kaj kelkfoje
kontraŭleĝe. Nur tio estas sufiĉa kialo eviti tuŝi tiujn aparatojn.</p>

La reala situacio estas bedaŭrinde malpli bona. En kutimaj aparatoj,
estas nur unu ĉefa blato ene, la CPU. La CPU plenumas la operaciumon,
kiel [GNU/Linukson](https://gnu.org/), kaj havas la tutan kontrolon de la
maŝino. Ĝi ne estas tiel en poŝtelefonoj; tiuj aparatoj havas du ĉefajn
blatojn — la CPU-n kaj la bazan bandon —. La unua havas la kutimajn
liberecajn problemojn; la dua estas Interrete konektita nigra skatolo
kun teruraj ebloj. Minimume laŭ la desegno de la poŝtelefonaj retoj, iam
ajn, kiam la poŝtelefono konektas al la interreto (tio estas, la bando
estas enreta), la uzula loko povas esti sekvata per triangulado de
telefonaj turoj. Jam la risko estas neakceptebla por multaj homoj.
Tradicie la telefonaj operacioj estas vundeblaj per observado kaj
tuŝaĉado, ĉar nek alvokoj nek tekstoj estas ĉifritaj. Ankaŭ, por
aldoni doloron al la vundo, malmultaj telefonoj havas acepteblan
izolecon. Tio estas, la CPU, kiu povas plenumi proprietan programaron,
ne kontrolas la bazan bandon, kiu por praktikaj celoj ne povas laŭleĝe
plenumi liberan programaron en Usono. Anstataŭe, en multaj okazoj la baza
bando kontrolas la CPU-n. Ne gravas, se oni uzas ĉifritajn mesaĝojn
per XMPP, se la baza bando simple povas fotografi sen la scio nek
permeso de la sistema operaciumo de la flanko de la CPU. Alternative,
denove, depende de kiel la baza bando estas konektita kun la alio de la
sistemo, ĝi povus havi la eblecojn fore aktivigi la mikrofonon kaj
fotografilon. 33 jaroj poste mondo, en kiu ĉiuj kunportas poŝtelefonon,
preterpasas la terursonĝojn de George Orwell. Eble vi «nenion kaŝas»,
sed almenaŭ al mi ankoraŭ gravas mia privateco. Poŝtelefonoj estas
timigaj. Ne kalkulu je mi.

Fine laŭ la gravaj sekvoj por socio kaj libereco, mi rifuzas eternigi ĉi
tiun sistemon. Mi povus decidi kunporti poŝtelefonon malgraŭ tio,
decidante, ke kiel malinteresa homo mi povas oferi la liberecon pro la
tuja plezuriga kunveno. Sed estante kompleza mi nur aldonus unu al la
grando de la problema, grandan etikan pezaĵon, ĉar uzi la poŝtelefonan
reton kontribuas al la [retefiko](https://eo.wikipedia.org/wiki/Retefiko), kiel la nomo indikas.

Se mi havus mian telefonon ekstere antaŭ aliuloj, ni indikus, ke
«telefonoj estas bonaj». Se iu rigardas al mi kiel etika modelo, ili
ankaŭ povus daŭrigi la uzadon de poŝtelefono.

Se mi permesus al miaj amikoj skribi min anstataŭ uzi pli etikan ilon,
mi indikus, ke «sendi tekstan mesaĝon estas bone» kaj «estas rezone atendi, ke homoj
sendu tekstajn mesaĝojn». Se ili estus en la limo pri la etiko kaj neceso kunporti
poŝtelefonon, tio povus instigi al ili konservi ĝin.

Se mi uzus poŝtelefonon por taskoj en lernejo, mi dirus, ke «lernantoj
en la 21a jarcento devus havi poŝtelefonon». Mi preferus esti la lasta
obstinulo en la lernejo por memorigi al ili, ke tio ne estas etika
supozo.

Se mi ricevas strangan rigardon de miaj konatoj, konfiduloj kaj
instruantoj, mi havas nun la okazon eduki ilin pri libera programaro kaj
privateco. Nur kelkaj homoj konscias pri la riskoj de tiuj «porteblaj
aparatoj de sekvado» kiel [Richard Stallman](https://stallman.org/)
skribus. Tiuj «strangaj momentoj» estas perfektaj okazoj helpi al ili
decidi pli informite.

Kunportante poŝtelefonon mi eternigus ion malbonan. Aktive rifuzante
kunporti ĝin, mi rezistas kaj aktive faras ion bonan.

Do anstataŭ uzi poŝtelefonon, kiuj estas miaj alternativoj?

Por la plejparto de ciferaj taskoj, inklude skribi ĉi tiun afiŝon, mi
uzas tekkomputilon, kiu plenumas [liberan
programaron](https://eo.wikipedia.org/wiki/Libera_programaro). Adicie,
por konekti al la Interreto, mi uzas [Wi-Fi-an
karton](https://en.wikipedia.org/wiki/ath9k), kiu plenumas liberan
integritan mikroprogramaron!

Por paroli al miaj amikoj, mi uzas malcentrajn, malfermitajn
protokolojn, kie eblas. Konkrete oni povas kontakti min per retpoŝto,
XMPP kaj Mastodon. En kelkaj okazoj, kiam tio ne eblas pro la retefiko,
mi uzas liberajn malcentrajn sistemojn kiel IRC. Okaze mi uzas
proprietajn sistemojn, kiuj estis retroprojektitaj por esti uzita per
libera programaro, kiel Discord <ins>[la retroprojekto ligita ne plu
ekzistas!]</ins>. Kie eblas, mi uzas tavolojn de forta ĉifrado
funkciigitajn per libera programaro, kiel GPG kaj OTR, por pli da protekto
kontraŭ privatecaj minacoj. Se loka privateco estas problemo, mi
konektos per [Tor](https://tor.org/). Iu ajn el tiuj metodoj estas
grando paŝo super telefonaj alvokoj, tekstaj mesaĝoj, WhatsApp aŭ
Snapchat. Ĉiuj tiuj kune ŝildos vin kontraŭ la plejparto de la malamikoj.

Por konekti, kiam mi estas ne hejme, mi serĉas publikajn Wi-Fi-retojn,
kiujn oni povas sekurigi per ĉifrado kaj Tor. Se tio ne eblas, mi eble
devas demandi al aliulo pri ria aparato — ĉi tio estas bedaŭrinda, sed
dum la retefiko funkcias, estas etike akceptebla utiligi ĝin. En la
plimulto de la okazoj mi tamen evitos konekti al Interreto for de mia
hejmo; mi estas pli fruktodona eksterrete!

Do, jes, mi vivas sen poŝtelefono. Tio ne ĉiam estas komforta, sed
rendimento, libereco kaj etika konduto ĉiam gravas pli ol komforto.

Mi instigas al vi fari la samon.
