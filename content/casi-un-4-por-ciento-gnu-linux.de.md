Author: Jorge Maldonado Ventura
Category: Nachrichten
Date: 2024-02-03 20:35
Lang: de
Slug: casi-un-4-por-ciento-de-los-ordenadores-de-escritorio-usa-gnulinux
Save_as: fast-4-Prozent-der-PCs-verwenden-GNU-Linux/index.html
URL: fast-4-Prozent-der-PCs-verwenden-GNU-Linux/
Tags: Statistik, GNU/Linux
Title: Fast 4 % der PCs verwenden GNU/Linux
Image: <img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png" alt="" width="1139" height="563" srcset="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png 1139w, /wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024-569x281.png 569w" sizes="(max-width: 1139px) 100vw, 1139px">

Nach Daten von Statcounter wird GNU/Linux auf 3,77&nbsp;% der PCs
(Laptops und Desktops) verwendet<!-- more -->[^1]. Aus der Grafik geht
hervor, dass der Anteil von GNU/Linux im Januar 2021 bei 1,91&nbsp;%
lag, was bedeutet, dass es sich in drei Jahren verdoppelt hat. Es ist
auch erwähnenswert, dass der Anteil von Chrome OS (das den Linux-Kernel
verwendet) 1,78&nbsp;% beträgt.

<a href="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png">
<img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png" alt="">
</a>

Diese Statistik ist jedoch nicht ganz repräsentativ, da der
Tracking-Code von Statcounter nur auf 1,5 Millionen Websites installiert
ist[^2]. Außerdem verwenden einige GNU/Linux-Benutzer &mdash;
diejenigen, die den höchsten Wert auf die Privatsphäre legen &mdash;
Tools, die den [`User-Agent`](https://www.seokratie.de/guide/user-agent/) ändern (ein
Beispiel ist der Tor-Browser, der immer vorgibt, Windows zu verwenden,
um sich besser zu tarnen).

In jedem Fall handelt es sich um ein beeindruckendes Wachstum, das sich
in den kommenden Jahren fortsetzen dürfte, da GNU/Linux in
Bildungseinrichtungen zunehmend präsent ist und viele Länder versuchen,
ihre technologische Souveränität zu erhöhen (GNU/Linux ist der billigste
und sicherste Weg, das zu tun).

[^1]: <https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-200901-202401>
[^2]: «<i lang="en">Our tracking code is installed on more than 1.5 million sites
    globally</i>»: so heißt es in <https://gs.statcounter.com/faq#methodology>.
