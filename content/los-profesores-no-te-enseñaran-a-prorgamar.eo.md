Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2019-07-23
Lang: eo
Slug: los-profesores-no-te-ensenyaran-a-programar
Save_as: profesoroj-ne-lernigos-vin-kodumi/index.html
Tags: instruado, programado, libera programaro
Title: Profesoroj ne lernigos vin kodumi
URL: profesoroj-ne-lernigos-vin-kodumi/

Mirigas min, ke estas homoj kiuj travivis mem kiel funkcias la
lernosistemo kaj ankoraŭ pensas, ke ili povos lerni kodumi per instruado en klasĉambro.

Mi dubegas, ke ili povos lerni ion utilan el instruistoj, kiuj kaŝas
post skribtablo kaj mankas realan sperton pri programado. Estas stulta
intenci lerni ion utilan pri datumbazoj el iu kiu neniam desegnis nek
laboris kun malsimpla datumbazo en la reala mondo kaj nur rediras kiun
aliuloj skribis en lernolibroj.

Dum mia sperto en la labormondo mi konstatis la senutileco de jaroj kaj
jaroj de universitato. Mi, kiu ne lernis kodumado en la universitato,
ege superas al kiuj ĉeestis dum jaroj en klasĉambroj, kiel tio eblas?

<!-- more -->

Mi lernis kodumi el homoj kun multa sperto. Tiuj mentoroj troviĝas en la
sucesaj projektoj de libera programaro. Tio ne signifas, ke oni nenion
povas lerni el libroj kaj instruistoj, sed nur ke ili ne iĝos vi
elstara. Por bone fari retejojn, necesas lerni el kiuj jam faris multajn
retejojn kaj sekvi rian ekzemplo farante multajn retejojn.

En projektoj de libera programaro oni akiras ion grandvaloran: revizioj
kaj sugestoj el tre spertaj kudumuloj. Pro tio mi neniam rekomendos al
kiuj volas iĝi kompetentaj kodumuloj iri al la universitato. Anstataŭ mi
diros al ili, ke ili serĉu sukcesa projekto de libera programaro en la
fako pri kiu ili volas specialigi kaj ke ili engaĝiĝu, utiligante la tre
valorajn konsilojn kaj korektojn el kiuj estas pli spertaj.

La plej grava afero estas serĉi iun kun vera sperto. En firmoj troviĝas
ankaŭ personoj el kiuj eblas lerni, sed ekscesa konkuro kaj devigoj en
la firmoj iĝas malfacile ricevi bonajn konsilojn aŭ halti por observi
kaj lerni.
