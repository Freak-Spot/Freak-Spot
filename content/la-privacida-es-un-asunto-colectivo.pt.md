Author: Jorge Maldonado Ventura
Category: Opinião
Date: 2023-01-31 11:00
Image: <img src="/wp-content/uploads/2019/12/graffiti-camara-vigilancia.jpg" alt="Câmara de segurança">
Lang: pt
Slug: la-privacidad-es-un-asunto-colectivo
Save_as: A-privacidade-é-uma-questão-colectiva/index.html
URL: A-privacidade-é-uma-questão-colectiva/
Tags: conselho, privacidade
Title: A privacidade é uma questão colectiva

Muitas pessoas dão uma explicação pessoal sobre o porquê de protegerem
ou não a sua privacidade. Aqueles que não se importam muito são ouvidos
dizer que não têm nada a esconder. Aqueles que o fazem para se
protegerem de empresas sem escrúpulos, estados repressivos, e assim por
diante. Em ambas as posições é muitas vezes erradamente assumido que a
privacidade é um assunto pessoal, e não é.

A privacidade é tanto um assunto individual como um assunto público. Os
dados recolhidos por grandes empresas e governos são raramente
utilizados numa base individual. Podemos compreender a privacidade como
um direito do indivíduo em relação à comunidade, como diz
[Edward Snowden](https://pt.wikipedia.org/wiki/Edward_Snowden):

> Argumentar que não te preocupas com a privacidade porque não tens
> nada a esconder não é diferente de dizer que não te preocupas com a
> liberdade de expressão porque não tens nada a dizer.

Os teus dados podem ser utilizados para o bem ou para o mal. Os dados
recolhidos desnecessariamente e sem autorização são muitas vezes
utilizados para fins indevidos.

Estados e grandes empresas tecnológicas violam flagrantemente a nossa
privacidade. Muitas pessoas concordam tacitamente argumentando que nada
pode ser feito para o mudar: as empresas têm demasiado poder e os
governos não farão nada para mudar as coisas. E, certamente, essas pessoas
estão habituadas a dar poder às empresas que ganham dinheiro com os seus
dados e assim dizem aos Estados que não vão ser uma pedra no seu sapato
quando quiserem implementar políticas de vigilância de massa. No final,
prejudicam a privacidade daqueles que se preocupam.

A acção colectiva começa com o indivíduo. Cada pessoa deve refletir
sobre se está a fornecer dados sobre si própria que não deve, se está a
encorajar o crescimento de empresas antiprivacidade e, mais importante
ainda, se está a comprometer a privacidade dos que lhe são próximos. **A
melhor maneira de proteger a informação privada é não a divulgar**. Com a
consciência do problema, os projectos em prol da privacidade podem ser
apoiados.

Os dados pessoais são muito valiosos &mdash; tanto que alguns chamam-nos o
«novo petróleo» &mdash; não só porque podem ser vendidos a terceiros,
mas também porque dão poder a quem quer que os tenha. Quando os damos
aos governos, damos-lhes o poder de nos controlarem. Quando os damos às
empresas, estamos a dar-lhes poder para influenciarem o nosso
comportamento. Em última análise, a privacidade é importante porque nos
ajuda a preservar o poder que temos sobre as nossas vidas, que eles
estão tão empenhados em tirar. Eu não vou dar ou vender os meus dados,
e tu?
