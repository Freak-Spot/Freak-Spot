Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2017-10-21 15:48
Image: <img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="">
Lang: es
Slug: viendo-las-modificaciones-de-codigo-en-vim-con-vimgitgutter
Tags: complemento, diff, edición de texto sin formato, Git, modificaciones, Neovim, Vim, vim-gitgutter
Title: Viendo las modificaciones de código en Vim con <code>vim-gitgutter</code>

A veces cuando estamos editando un texto o código fuente es útil ver los
cambios que hemos realizado respecto a la versión anterior. Con
[`vim-gitgutter`](https://github.com/airblade/vim-gitgutter) es posible
hacerlo automáticamente sin salir del editor. `vim-gitgutter` es un
complemento para Vim que muestra los últimos cambios realizados en un
archivo de un repositorio Git.

<!-- more -->

El complemento muestra el tipo de edición que se ha producido en la
columna a la izquierda de la línea. Por defecto, se utilizan tres
símbolos con diferentes colores:

<a href="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png">
<img src="/wp-content/uploads/2017/10/vim-gitgutter_en_acción.png" alt="Imagen mostrando los tres símbolos de vim-gutter">
</a>

- <span style="color: red">–</span> indica que se ha
  eliminado contenido entre las líneas donde está dicho símbolo.
- <span style="color: green">+</span> indica que la línea es nueva.
- <span style="color: yellow">~</span> significa que la línea se ha
  modificado.

Para instalar el complemento necesitamos tener Vim con soporte para
símbolos, puedes comprobarlo ejecutando `:echo has('signs')`. Si
la instrucción anterior devuelve <samp>1</samp>, puedes instalar
`vim-gitgutter` como cualquier otro complemento (ejecuta `:h plugin` en
Vim para más información sobre complementos). También necesitas tener
Git instalado.

Para probar el complemento basta con modificar un archivo que se
encuentre en un repositorio Git. Los símbolos aparecerán a medida que se
realicen modificaciones al archivo. Cuando se realice un nuevo <i lang="en">commit</i>,
los símbolos desaparecen hasta que volvamos a realizar modificaciones.

El complemento tiene un montón de opciones y se puede configurar
bastante para adaptarlo a nuestras necesidades. Para más información
consulta su documentación ejecutando `:h gitgutter`.
