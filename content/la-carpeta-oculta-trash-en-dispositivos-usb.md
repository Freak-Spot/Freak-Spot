Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2016-07-01 23:14
Lang: es
Modified: 2017-02-25 20:36
Slug: la-carpeta-oculta-trash-en-dispositivos-usb
Status: published
Tags: Bash, GNU/Linux, script, Trash, Trisquel, USB, Trisquel Belenos, Trisquel 7
Title: La carpeta oculta .Trash en dispositivos USB

Si trabajas con un dispositivo USB y usas GNU/Linux, quizás te hayas
topado alguna vez con una carpeta oculta llamada `.Trash-1000`,
.`Trash-1001` o con algún nombre similar.
Dentro de este directorio encontrarás un subdirectorio llamado `files` y
otro llamado `info`. En la siguiente imagen puedes ver el contenido de
la carpeta `.Trash-1002` en mi dispositivo USB.

<figure id="attachment_16" style="width: 947px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/07/carpeta-.Trash-USB.png"><img class="size-full wp-image-16" src="/wp-content/uploads/2016/07/carpeta-.Trash-USB.png" alt="carpeta .Trash USB" width="947" height="252" srcset="/wp-content/uploads/2016/07/carpeta-.Trash-USB.png 947w, /wp-content/uploads/2016/07/carpeta-.Trash-USB-300x80.png 300w, /wp-content/uploads/2016/07/carpeta-.Trash-USB-768x204.png 768w" sizes="(max-width: 947px) 100vw, 947px" /></a><figcaption class="wp-caption-text">carpeta .Trash USB</figcaption></figure>

¿Qué es lo que contienen esas carpetas y por qué existen? Cuando borras
algo de tu dispositivo USB usando el ratón o pulsando la tecla
<kbd>Suprimir</kbd>, no lo estás borrando definitivamente: simplemente
se guarda en la carpeta que te he mostrado anteriormente. De esta forma,
puedes estar tranquilo de que, aunque borres algo importante, podrás
recuperarlo si te arrepientes de tu decisión. La carpeta `files`
contiene los archivos, y la carpeta `info`, la ruta que tenían los
archivos en el USB antes de ser «borrados» y su fecha de eliminación.

Para eliminar definitivamente archivos dentro de la carpeta
`.Trash-????`, debes eliminar el archivo que está dentro de la carpeta
`files` y el de la carpeta `info`. Dentro de la carpeta `files`, el
nombre del archivo es el mismo que cuando se eliminó y dentro de la
carpeta `info`, se le ha añadido la extensión `.trashinfo`.

Puedes usar la instrucción `rm` para borrar los archivos de forma
definitiva. Recomiendo usar `rm -i archivos`, si vas a borrar muchos
archivos y quieres asegurarte de que no borras alguno que no deberías.
También puedes borrar la carpeta `.Trash-1002` (o como se llame en tu
caso) con todo su contenido ejecutando la instrucción `rm -r
.Trash-1002` (cambia el nombre de la carpeta si es distinto al tuyo)
sobre la raíz del dispositivo USB. También puedes pulsar
<kbd>Shift</kbd> y <kbd>Suprimir</kbd> al mismo tiempo seleccionado el
archivo que deseas eliminar (no sé si esto funcionará en cualquier
distribución). Tranquilo, la carpeta se volverá a crear cuando borres
cualquier otra cosa. Si quieres borrar solo un archivo, este script te
servirá para eliminar los archivos de los subdirectorios de la carpeta
`.Trash-????`, debes ejecutarlo ubicado en la raíz del dispositivo USB:

    :::bash
    #!/bin/bash

    echo "A continuación se muestra una lista con los archivos de la papelera del USB."
    ls .Trash-1002/files
    echo -en "Introduce el nombre del archivo que quieres borrar.\n>>> "
    read file
    #Si es un directorio, también borra todo su contenido.
    rm -r ".Trash-1002/files/$file" ".Trash-1002/info/$file.trashinfo" && echo "Eliminación realizada con éxito."

Si tu carpeta .Trash-???? no tiene el mismo nombre que la mía debes
cambiarlo en todo el script. A continuación, muestro una imagen con un
ejemplo práctico:

<figure id="attachment_19" style="width: 1011px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/07/script_borrar_de_la_papelera_USB.png"><img class="size-full wp-image-19" src="/wp-content/uploads/2016/07/script_borrar_de_la_papelera_USB.png" alt="Ejecución del script que borra el contenido de la papelera de un dispositivo USB." width="1011" height="428" srcset="/wp-content/uploads/2016/07/script_borrar_de_la_papelera_USB.png 1011w, /wp-content/uploads/2016/07/script_borrar_de_la_papelera_USB-300x127.png 300w, /wp-content/uploads/2016/07/script_borrar_de_la_papelera_USB-768x325.png 768w" sizes="(max-width: 1011px) 100vw, 1011px" /></a><figcaption class="wp-caption-text">Ejecución del script que borra el contenido de la papelera de un dispositivo USB.</figcaption></figure>
