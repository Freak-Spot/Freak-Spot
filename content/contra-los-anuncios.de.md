Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2023-09-21 13:05
Lang: de
Slug: no-pierdas-tu-tiempo-y-dinero-viendo-anuncios
Save_as: vergeude-nicht-dein-Leben-mit-Werbung/index.html
URL: vergeude-nicht-dein-Leben-mit-Werbung/
Tags: Werbung, Rat
Title: Vergeude nicht dein Leben mit Werbung
Image: <img src="/wp-content/uploads/2023/09/distopia-anuncios-editado.png" alt="" width="1387" height="898" srcset="/wp-content/uploads/2023/09/distopia-anuncios-editado.png 1387w, /wp-content/uploads/2023/09/distopia-anuncios-editado-693x449.png 693w" sizes="(max-width: 1387px) 100vw, 1387px">

Du schaust dir ein Video an, das dir gefällt. Das Video wird wieder
unterbrochen.  <span style="color: #E8C244">Eine lästige Werbung</span>.
Zurück zum Video.
Sponsoring. Abonniere. Hinterlasse ein Kommentar...

## Langsamkeit, Umweltverschmutzung, Konsumismus

Digitale Werbung bringt dich dazu, nutzlose Dinge zu kaufen, die du
nicht brauchst, und wertvolle Zeit zu verschwenden. Außerdem langsamen
Anzeigen die Ladung von Webseiten und verursachen enorme
CO<sub>2</sub>-Emissionen (sowohl direkt als auch indirekt).

## Werbung, die töten

Werbung wurde schon immer genutzt, um unsere Wahrnehmung mit Hilfe der
Psychologie zu manipulieren. Werber verkauften Tabak als Symbol der
Befreiung, ungesunde Lebensmittel als wären sie gesund... Dann kamen
Gesundheitsprobleme und Tod für Menschen, die durch diese Anzeigen
überzeugt waren.

## Du bezahlst sie mit deinem Geld und deiner Zeit

Wenn du ein im Internet beworbenes Produkt kaufst, wird der Teil, der
die Unternehmen gekostet hat, die Werbung zu produzieren und zu
verbreiten, von dir bezahlt: mit deiner Zeit und deinem Geld (wir sprechen
von einer Milliardenindustrie).

## Zensur

Es darf keine unkonventionellen politischen Meinungen, keine
Nacktheit o.Ä. geben. Werber gehen nicht gerne Risiken ein, alles
muss kontrolliert werden. Wenn du ihnen nicht gefällst, wird dein Video
kein Geld einbringen, es wird nicht in den Suchergebnissen auftauchen.

Alles, was nicht zum Konsumismus beiträgt, sollte ignoriert werden, weil
es ihnen kein Geld einbringt.

## Keine Privatsphäre

Mit dem Aufkommen zielgerichteter Werbung überwachen viele
Unternehmen alles, was du im Internet tust, und erstellen dadurch
psychologische Profile, die den Werbern ermöglichen,
Manipulationstechniken zu deinen Lasten zu optimieren.

<h2><span style="color: #b5ffb5">Das Leben ohne Werbung</span></h2>

<a href="/wp-content/uploads/2023/09/bosque-falso.png">
<img src="/wp-content/uploads/2023/09/bosque-falso.png" alt="" width="1024" height="989" srcset="/wp-content/uploads/2023/09/bosque-falso.png 1024w, /wp-content/uploads/2023/09/bosque-falso-512x494.png 512w" sizes="(max-width: 1024px) 100vw, 1024px">
</a>

Aus all diesen Gründen blockiere ich Werbung auf Websites, Videos
(einschließlich gesponserte Inhalte) usw. Mit der Zeit, die ich spare,
kann ich Informationen effizienter finden und mir Videos ansehen oder
Artikel lesen, ohne zu warten. So habe in kürzerer Zeit mehr gelernt,
und ich kann mehr Zeit mit dem verbringen, was ich wirklich mag.

Wenn ich etwas kaufe, versuche ich, das mit Bedacht zu tun:

- Was sind die Zutaten?
- Gibt es Optionen von besserer Qualität?
- Wo wurde es hergestellt?
- Wie viel kostet das?
- ...

Wenn mir etwas gefällt und ich es unterstützen möchte, dann tue ich das
direkt (ohne Zwischenhändler), damit das mehr Geld bekommt, als
wenn ich meine Zeit an missbräuchliche Werber gäbe.

Natürlich wird es auch weiterhin Werbung im Internet existieren, denn es
wird immer gesponsorte Artikel, von Unternehmen finanzierte Videos usw.
geben. Zum Glück ist es möglich, viel Werbung mit Programmen wie
[uBlock Origin](https://de.wikipedia.org/wiki/UBlock_Origin) und
[Piped](/de/YouTube-mit-Privatsphäre-mit-Piped/) zu vermeiden.
