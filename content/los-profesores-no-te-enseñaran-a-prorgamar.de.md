Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2019-08-28
Lang: de
Slug: los-profesores-no-te-ensenyaran-a-programar
Save_as: Lehrer-werden-dich-nicht-Programmieren-lehren/index.html
Tags: Erziehung, Programmierung, Freie Software
Title: Lehrer werden dich nicht Programmieren lehren
URL: Lehrer-werden-dich-nicht-Programmieren-lehren/

Es erstaunt mich, dass es Menschen gibt, die selbst erlebt haben, wie
das Bildungssystem funktioniert, und noch denken, dass sie im Unterricht
lernen können werden.

Ich bezweifle sehr, dass sie was Nützliches von Lehrer, die sich hinter
einem Schreibtisch verstecken und keine echte Erfahrung als
Programmierer haben, lernen können. Es ist absurd, wenn man versucht
von Leute, die nie in der realen Welt komplizierten Datenbanken
entwickelt haben, oder mit ihnen arbeiten haben, und nur was andere in
Bücher geschrieben haben wiederholen, etwas Wertvolles über Datenbanken
zu lernen.

Nach meiner Erfahrung in der Arbeitswelt habe ich über der Nutzlosigkeit
von Jahre und Jahre in der Universität erfahren. Ich, ohne
Programmierung in der Universität zu lernen, bin viel besser als die,
die seit Jahren zum Unterricht gehen. Wie ist das möglich?

<!-- more -->

Ich lernte programmieren von sehr erfahrene Leute. Diese Betreuer kann
man in erfolgreichen Freie-Software-Projekten finden. Das bedeutet
nicht, dass man nichts im Bücher oder von Lehrer lernen kann, sondern
nur dass sie dich nicht außergewöhnlich werden. Um ein guter
Programmierer zu werden, ist es nötig von Leute, die viele Webseiten
entwickelt haben, zu lernen und viele Webseiten zu entwickeln, folgend
ihrem Beispiel.

In Freie-Software-Projekte erhält man was unschätzbar: die Überprüfung
und Empfehlungen von sehr erfahrenen Programmierern. Deshalb wird ich
nicht denen, die gute Programmierer werden wollen, empfehlen, an die
Universität zu gehen. Anstatt wird ich den empfehlen, einen
erfolgreichen Freie-Software-Projekt im Fachgebiet, auf dem
sie sich spezialisieren wollen, zu suchen und sich dafür zu engagieren,
ohne die wertvolle Tipps und Korrekturen von den mehr erfahrenen zu
versäumen.

Das Wichtigste ist jemanden mit echter Erfahrung zu suchen. In
Unternehmen gibt es auch Personen von denen man lernen kann, aber wegen
der maßlosen Konkurrenz und der Pflichten in der Firma ist es schwer,
gute Tipps zu bekommen oder etwas Zeit für Beobachtung und Lernen zu
nehmen.
