Author: Jorge Maldonado Ventura
Category: Cinema
Date: 2023-01-04 09:40
Lang: pt
Slug: crear-vídeos-de-ruido-con-ffmpeg
Save_as: criar-vídeos-de-ruído-com-FFmpeg/index.html
URL: criar-vídeos-de-ruído-com-FFmpeg/
Tags: FFmpeg, ruído, vídeos
Title: Criar vídeos de ruído com FFmpeg

[FFmpeg](https://es.wikipedia.org/wiki/FFmpeg) tem filtros que podem
criar ruído de vídeo de forma aleatória. O filtro [`geq`](https://ffmpeg.org/ffmpeg-filters.html#geq) pode criar ruído
de vídeo (com `nullsrc` como fundo branco), enquanto que o filtro [`aevalsrc`](https://ffmpeg.org/ffmpeg-filters.html#aevalsrc)
pode criar ruído de áudio.

Assim, podemos criar um vídeo a preto e branco de `1280x720` <i lang="en">pixels</i> com
o seguinte comando:


```

ffmpeg -f lavfi -i nullsrc=s=1280x720 -filter_complex \
"geq=random(1)*255:128:128;aevalsrc=-2+random(0)" \
-t 10 ruído.mkv
```

<!-- more -->

<video controls="">
  <source src="/video/ruido.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Com o seguinte comando conseguimos o mesmo, mas com cor:

```

ffmpeg -f rawvideo -video_size 1280x720 -pixel_format yuv420p -framerate 30 \
-i /dev/urandom -ar 48000 -ac 2 -f s16le -i /dev/urandom -codec:a copy \
-t 5 ruído-com-cores.mkv
```

Aqui estamos a utilizar `/dev/urandom`, mas também podemos aplicar o filtro
`geq` utilizado anteriormente.

<video controls="">
  <source src="/video/ruido-a-color.webm" type="video/webm">
  <p>Desculpa, o teu navegador não suporta HTML 5. Por favor muda ou
  actualiza o teu navegador.</p>
</video>

Estes comandos podem ser usados para gerar vídeos de exemplo. Também
podem ser utilizados para encher rapidamente com vídeos de lixo os
servidores dos sítios web que aceitam vídeos, que perderão dinheiro a
longo prazo se não os apagarem (ver [«Como destruir o Google»](/pt/como-destruir-o-Google/)), uma vez
que estes vídeos ocupam muito espaço. O último vídeo (5 segundos de
duração) ocupa 106 <abbr title="megabytes">MB</abbr>.
