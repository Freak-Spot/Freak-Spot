Author: Jorge Maldonado Ventura
Category: Ruzoj
Date: 2023-06-24 21:13
Lang: eo
Slug: evitar-que-javascript-modifique-el-portapeles
Save_as: eviti-ke-Ĝavoskripto-modifu-la-tondujon/index.html
URL: eviti-ke-Ĝavoskripto-modifu-la-tondujon/
Tags: Firefox, Ĝavoskripto, tondujo
Title: Eviti, ke Ĝavoskripto modifu la tondujon en Firefox
Image: <img src="/wp-content/uploads/2023/06/JavaScript-modifas-la-tondujon.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/06/JavaScript-modifas-la-tondujon.png 1920w, /wp-content/uploads/2023/06/JavaScript-modifas-la-tondujon-960x540.png 960w, /wp-content/uploads/2023/06/JavaScript-modifas-la-tondujon-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Ĉu iam okazis al vi, ke vi kopiis tekston de retpaĝo kaj, kiam vi
algluis ĝin, aperis teksto modifita, aŭ nenio aperis? Tio okazas, ĉar
tiu paĝo modifis la tondujon.

Por eviti tion vi povas [malebligi
Ĝavoskripton](/eo/malebligi-Ĝavoskripton-en-Firefox-kaj-ĝiaj-derivitaj-retumiloj/)
aŭ agordi Firefox per la jena metodo:

1. En la adresbreto skribu `about:config`.
2. Premu la <kbd>Enigan Klavon</kbd> kaj poste la butonon **Akcepti la
   riskon kaj daŭrigi**.
3. En la serĉo-kampo skribu `dom.event.clipboardevents.enabled`.
4. Alklaku dufoje ĉi tiun opcion aŭ premu ĝian **Baskuligi**-butonon
   por ŝanĝi ĝian valoron al `false`.

<a href="/wp-content/uploads/2023/06/baskuligi-opcion-dom-event-clipboardevents-enable-Firefox.png">
<img src="/wp-content/uploads/2023/06/baskuligi-opcion-dom-event-clipboardevents-enable-Firefox.png" alt="" width="945" height="287" srcset="/wp-content/uploads/2023/06/baskuligi-opcion-dom-event-clipboardevents-enable-Firefox.png 945w, /wp-content/uploads/2023/06/baskuligi-opcion-dom-event-clipboardevents-enable-Firefox-472x143.png 472w" sizes="(max-width: 945px) 100vw, 945px">
</a>

<figure>
<a href="/wp-content/uploads/2023/06/eventoj-de-tondujo-malebligitaj.png">
<img src="/wp-content/uploads/2023/06/eventoj-de-tondujo-malebligitaj.png" alt="" width="934" height="78" srcset="/wp-content/uploads/2023/06/eventoj-de-tondujo-malebligitaj.png 934w, /wp-content/uploads/2023/06/eventoj-de-tondujo-malebligitaj-467x39.png 467w" sizes="(max-width: 934px) 100vw, 934px">
</a>
    <figcaption class="wp-caption-text">Ĝi devas aspekti tiel</figcaption>
</figure>

Tiel la retejoj, kiujn vi vizitos ne povos modifi vian tondujon. Sciu,
ke kelkaj retaplikaĵoj, kiuj modifas la tondujon per Ĝavoskripto (kiel
[Collabora](https://es.wikipedia.org/wiki/Collabora_Online)), ne plu
povos fari tion, do ilia kapablo alglui ne plu ĝuste funkcios.
