Author: Jorge Maldonado Ventura
Category: Bash
Date: 2016-07-22 19:04
Lang: es
Slug: gksudo-en-vez-de-sudo-para-aplicaciones-graficas
Status: published
Tags: gksu, gksudo, root, sudo, superusuario
Title: gksudo en vez de sudo para aplicaciones gráficas

¿Que diferencia hay entre `sudo` y `gksudo`? La principal es que `sudo`
está hecho para aplicaciones de texto que ejecutas en la Terminal y
`gksudo` o `gksu` (son equivalentes), para aplicaciones gráficas.

Si eres un usuario de GNU/Linux, probablemente alguna vez habrás
utilizado `sudo` para abrir una aplicación gráfica. También te habrás
dado cuenta de que la mayoría de las veces no da ningún problema.
Entonces, ¿por qué deberías usar `gksu?`

Cuando lanzas un programa con sudo, estás utilizando la configuración
del usuario y ejecutando el programa siendo el superusuario. Pero si
utilizas, `gksudo` estás utilizando el programa como superusuario y
cargando la configuración de superusuario (root). La mayoría de las
veces no supone ningún problema utilizar sudo para abrir programas
gráficos con privilegios, pero [otras veces
sí](http://www.psychocats.net/ubuntu/graphicalsudo).

Si vas a realizar tareas administrativas, recomiendo hacerlo todo desde
la Terminal con `sudo`, porque es más rápido y más seguro. Para
encontrar más información, lee la página de manual de `gksu.`
