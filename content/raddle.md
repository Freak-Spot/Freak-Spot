Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2017-09-22 14:41
Image: <img src="/wp-content/uploads/2017/09/reddit-reemplazado-por-raddle.png" alt="Cara de la mascota de Reddit tapada por la rana de Raddle">
Lang: es
Slug: raddle-como-respuesta-a-reddit
Tags: censura, corporaciones, debate, discusión, páginas web, Reddit, Raddle, votaciones,
Title: Raddle como respuesta a Reddit

[Raddle](https://raddle.me/) está aquí como respuesta a las decisiones
anticomunitarias de la corporación Reddit, que [recientemente decidió
dejar de publicar su código fuente como <i lang="en">software</i>
libre](https://www.reddit.com/r/changelog/comments/6xfyfg/an_update_on_the_state_of_the_redditreddit_and/).
Además, está planteando algunos problemas de privacidad:

- Las cookies de Reddit [pueden rastrear las acciones de usuarias en diferentes
  dominios](https://www.reddit.com/help/privacypolicy?v=b51465c2-761d-11e7-9160-0e433a32d3b8)
- Al estar Reddit ubicado en los Estados Unidos de América,
  <a href="https://www.siliconrepublic.com/life/reddit-warrant-canary-disappearance">el <abbr title="Federal Bureau of Investigation">FBI</abbr> ha registrado</a>
  y puede registrar en el futuro toda la información que desee

La principal razón para la creación de Raddle parece ser, sin embargo,
la censura y la eliminación de cuentas a grupos de izquierda, según se
explica en la [página de wiki «History»](https://raddle.me/wiki/history)
de Raddle.

## ¿Qué cambia Raddle respecto a Reddit?

<a href="/wp-content/uploads/2017/09/Raddle-foros.png">
<img src="/wp-content/uploads/2017/09/Raddle-foros.png" alt="La visión general de Raddle es sencilla" width="1004" height="814" srcset="/wp-content/uploads/2017/09/Raddle-foros.png 1004w, /wp-content/uploads/2017/09/Raddle-foros-502x407.png 502w" sizes="(max-width: 1004px) 100vw, 1004px">
</a>

<!-- more -->

Raddle es <i lang="en">software</i> libre, colaborativo y no rastrea a las usuarias. No
tiene ningún oscuro interés económico al no estar controlado por una
empresa privada, sino que se concibe como un espacio comunitario.
Asimismo, cuenta con un servicio oculto de Tor:
<http://c32zjeghcp5tj3kb72pltz56piei66drc63vkhn5yixiyk4cmerrjtid.onion/>.

Como es un proyecto colaborativo, se pueden incluir nuevas
características tras pasar el visto bueno de la comunidad en el [foro
meta](https://raddle.me/f/meta).

Originalmente, Raddle se llamaba Raddit, pero [cambió de
nombre](https://raddle.me/f/meta/6779/we-ve-rebranded-to-raddle-me) para
no ser comparado con Reddit y evitar posibles problemas con registrantes
de dominios.

## ¿Cómo funciona Raddle?

<figure>
    <a href="/wp-content/uploads/2017/09/discusión-en-Raddle.png">
    <img src="/wp-content/uploads/2017/09/discusión-en-Raddle.png" alt="" width="1335" height="849" srcset="/wp-content/uploads/2017/09/discusión-en-Raddle.png 1335w, /wp-content/uploads/2017/09/discusión-en-Raddle-667x424.png 667w" sizes="(max-width: 1335px) 100vw, 1335px">
    </a>
    <figcaption class="wp-caption-text">Discusión en Raddle</figcaption>
</figure>

Raddle es un sitio donde compartir artículos y generar discusiones
y votaciones. El contenido se organiza en torno a foros. Una se puede
suscribir a distintos foros para estar pendiente de sus novedades. Se
pueden utilizar diferentes filtros para ver el contenido que nos
interesa: por novedades, votos positivos, votos negativos, actividad.

Alguna información sobre el servicio en sí se encuentra en el wiki de
Raddit, que cualquiera puede editar, a excepción de algunas páginas
bloqueadas (como la política de privacidad).

La apariencia de Raddle se puede personalizar muy fácilmente eligiendo
algunos de los temas existentes o creando uno propio. Raddle está
traducido a varios idiomas, incluido el castellano.

<a href="/wp-content/uploads/2017/09/usando-Raddle.png">
<img src="/wp-content/uploads/2017/09/usando-Raddle.png" alt="En Raddle se puede votar de forma positiva y negativa" width="1000" height="868" srcset="/wp-content/uploads/2017/09/usando-Raddle.png 1000w, /wp-content/uploads/2017/09/usando-Raddle-500x434.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
