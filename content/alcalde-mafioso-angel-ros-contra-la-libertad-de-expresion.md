Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2016-07-09 00:15
Lang: es
Modified: 2017-08-19 20:04
Slug: alcalde-mafioso-angel-ros-contra-la-libertad-de-expresion
Status: published
Tags: alcalde, Àngel Ros, España, libertad de expresión, Lleida, PSC
Title: Alcalde mafioso Àngel Ros contra la libertad de expresión

Àngel Ros es el alcalde de Lleida. [No tiene reparo en homenajear a
fascistas](http://www.elpuntavui.cat/article/3-politica/17-politica/798548-la-cup-portara-angel-ros-a-lonu-per-lbanalitzacio-del-feixismer.html)
ni en censurar a quienes le critican. Invierte mucho dinero en pagar a
periódicos para lavar su imagen después de sus pueriles fechorías.

<a href="/wp-content/uploads/2016/07/Àngel-Ros-fascista.jpg"><img class="aligncenter size-full wp-image-188" src="/wp-content/uploads/2016/07/Àngel-Ros-fascista.jpg" alt="Àngel Ros homenajea al fascista Hellín" width="576" height="457" srcset="/wp-content/uploads/2016/07/Àngel-Ros-fascista.jpg 576w, /wp-content/uploads/2016/07/Àngel-Ros-fascista-300x238.jpg 300w" sizes="(max-width: 576px) 100vw, 576px" /></a>

La última víctima de este personaje ha sido la libertad de expresión. La
censura se ha hecho válida, gracias a la fiscalía, contra una canción de
crítica al alcalde. El escrito al juez pedía «medidas urgentes y
cautelares que eviten la difusión del vídeo por las redes sociales»
argumentando que «el contenido del vídeo atenta contra la persona del
alcalde, su honor y su familia, pudiendo además ser un delito de los ».
La fiscalía pide 1 año y 3 meses de prisión para Pablo Hasél y Ciniko
(nombres artísticos), que tendrán el juicio el 23 de noviembre de este
año.

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698.png">
  <img class="aligncenter size-full wp-image-48" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698.png" alt="Àngel Ros contra la libertad de expresión" width="1379" height="1064" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698.png 1379w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698-300x231.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698-768x593.png 768w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-09-022959-e1468024529698-1024x790.png 1024w" sizes="(max-width: 1379px) 100vw, 1379px">
</a>

Aquí podéis escuchar la canción satírica.

<audio class="wp-audio-shortcode" preload="none" style="width: 100%;" controls="controls"><source type="audio/mpeg" src="/wp-content/uploads/2016/07/Prozaks-Menti-Ros.mp3?_=1" /><a href="/wp-content/uploads/2016/07/Prozaks-Menti-Ros.mp3">/wp-content/uploads/2016/07/Prozaks-Menti-Ros.mp3</a></audio>

Uno de los raperos, Pablo Hasél, responde: «Dice sentirse amenazado por
una canción, es cuanto menos ridículo. En todo caso le molestan las
verdades incómodas que se dicen, sobre todo cuando cada vez más lo
tienen calado y saben cómo las gasta este cacique especulador. Nosotros
nos sentimos amenazados por sus políticas llevadas a los hechos, por eso
nos sentimos peor que amenazados, nos sentimos jodidos por la miseria y
precariedad que crean. Por ello tenemos rabia y por ello lo hemos
denunciado. No son amenazas de muerte, es decir lo que creemos que
merece y si ni siquiera se puede desear algo a alguien, es que en este
Estado no hay libertad ni para sentir». Las rimas más agresivas son las
que desean la muerte al edil, como «Te mereces un tiro» o «se merece un
navajazo en el abdomen».

«El alcalde Àngel Ros me ha denunciado por una canción. La canción se
titula *Menti-Ros* y está sacada con el grupo Prozaks en clave medio
cómica denunciando sus prácticas mafiosas en el ayuntamiento y sus
políticas contra la justicia social. La canción es junto a un compañero
y la cruzada del alcalde contra mí es tal, que hasta me ha denunciado
por frases suyas. Pero que no se equivoque, yo también defiendo la
libertad de expresión de mi compañero y no escurriré el bulto por esas
frases. No le basta con prohibirme conciertos como hizo recientemente en
las fiestas mayores obligando a quienes me habían contratado a no
dejarme subir al escenario, ahora la inquisición llega al punto de
exigir que borre una canción. Tampoco le basta con mandar a sus Mossos
D’Esquadra a detenerme en medio de un restaurante por defenderme de la
agresión de unos nazis».

Así pretende este señor silenciar a quienes le critican. Incluso ha
recibido [acusaciones de corrupto de su propio partido (cesó de su cargo
a la
denunciante)](http://vozpopuli.com/actualidad/59084-marta-camps-revela-toda-la-supuesta-corrupcion-del-ayuntamiento-de-lerida).
En vez de como un alcalde, actúa como un cruel empresario que, según
parece, gana cientos de miles euros con [operaciones de dudosa
reputación con su antiguo jefe en la
Caixa](http://lagrancorrupcion.blogspot.com.es/2014/01/angel-ros-alcalde-de-lerida-renuncia-al.html),
rodeado de denuncias de fraude, mientras la mitad de los jóvenes de su
ciudad están en el desempleo.

<a href="/wp-content/uploads/2016/07/Ladrón.png"><img class="aligncenter size-full wp-image-190" src="/wp-content/uploads/2016/07/Ladrón.png" alt="Àngel Ros corrupto y ladrón" width="576" height="428" srcset="/wp-content/uploads/2016/07/Ladrón.png 576w, /wp-content/uploads/2016/07/Ladrón-300x223.png 300w" sizes="(max-width: 576px) 100vw, 576px" /></a>

El alcalde de Lleida dice tener miedo, a pesar de ir siempre acompañado
de escoltas, y se siente una víctima, tanto que ha pedido también una
orden de alejamiento. Una persona que utiliza la vida pública para
hacerse rico debería ser capaz de asumir alguna crítica. A mí me parece
deplorable denunciar a unos raperos por componer una canción y al mismo
tiempo alabar a reyes corruptos. A miles de familias han dañado sus
políticas, muchas de ellas desean que este personaje muera (quizá con
razón o quizá no). ¿Pero puedes decirlo si el Estado español te condena
por ello? Yo opino que hay que denunciar todo ataque contra la libertad
de expresión y Pablo Hasél también: «No ha topado con un joven sumiso,
ha topado con un joven dispuesto a denunciarlo y a defender la libertad
de expresión. Ceder ante su inquisición sería renunciar a nuestro
derecho a decir lo que pensamos y a tener rabia ante las injusticias de
las que se lucra».

Es necesario denunciar estos hechos porque el siguiente censurado puede
ser cualquiera. Por favor, protestad contra este alcalde compartiendo
este artículo y luchando en las calles. Enviad también una crítica o
pedidle al alcalde la retirada de la denuncia a través de su dirección
de contacto: <http://alcalde.paeria.cat/es/contacta.aspx>. Asimismo,
protestad en contra del uso del <i lang="en">software</i> privativo (Flash) en el sitio
web del ayuntamiento de Lleida (un portal que solo debería utilizar
<i lang="en">software</i> libre al ser público).

A continuación, dejo varias de las fuentes de información utilizadas:

-   <http://www.lavanguardia.com/internacional/20160622/402684820863/pablo-hasel-queda-en-libertad-pero-la-audiencia-nacional-le-abre-otra-causa.html>
-   <http://ccaa.elpais.com/ccaa/2015/03/04/catalunya/1425468611_030885.html>
-   <http://www.elpuntavui.cat/article/3-politica/17-politica/798548-la-cup-portara-angel-ros-a-lonu-per-lbanalitzacio-del-feixismer.html>
-   <http://www.elmundo.es/cataluna/2015/03/29/5516fefc22601d96458b456b.html>
-   <http://www.teinteresa.es/catalunya/barcelona/Ros-razonables-insuficiente-Ada-Colau_0_1400860013.html>
-   <http://lagrancorrupcion.blogspot.com.es/2014/01/angel-ros-alcalde-de-lerida-renuncia-al.html>
-   <http://www.elmundo.es/cataluna/2015/03/16/5505eb72e2704ea6468b456b.html>
-   <http://vozpopuli.com/actualidad/59084-marta-camps-revela-toda-la-supuesta-corrupcion-del-ayuntamiento-de-lerida>
-   <http://infolagrancorrupcion.blogspot.com.es/2015/05/narcis-serra-angel-ros-catalunya-caixa.html>
-   <http://www.elmundo.es/cataluna/2015/03/31/5519a20422601da9338b4572.html?cid=MNOT23801&s_kw=el_alcalde_de_lleida_admite_que_pago_la_comida_navidena_del_psc_con_fondos_municipales>
-   <http://www.europapress.es/catalunya/noticia-alcalde-lleida-denuncia-rapero-pablo-hasel-amenazas-muerte-cancion-20141205114147.html>

