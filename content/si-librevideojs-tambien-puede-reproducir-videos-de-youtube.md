Author: Jorge Maldonado Ventura
CSS: librevideojs/librevideojs-material/libre-skin-teal.css
Category: Desarrollo web
Date: 2017-01-14 11:36
JS: cliplibrejs.dev.js (top)
Lang: es
Modified: 2021-12-05
Slug: si-librevideojs-tambien-puede-reproducir-videos-de-youtube
Status: published
Tags: Cybersy, LibreVideoJS, LibreVideoJS HTML player, reproductor, software libre, Video, videos, Wordpress, YouTube
Title: Sí, LibreVideoJS también puede reproducir vídeos de YouTube

Me preguntaron si el reproductor LibreVideoJS era capaz de reproducir
vídeos de YouTube. La respuesta es sí. Solo necesitas la URL del vídeo.

Pero claro, YouTube no te proporciona la URL del vídeo, porque es un
sitio web restrictivo y privativo. Aún así, no es difícil obtenerla. Una
manera sencilla de obtener la URL de un vídeo es usar
[youtube-dl](http://rg3.github.io/youtube-dl/) con el parámetro `-g` o
`--get-url`. Con el parámetro `--get-thumbnail` podemos obtener la URL
de la imagen que se muestra en YouTube antes de reproducir el vídeo. Con
la opción `--all-subs`, podemos descargar también todos los subtítulos
disponibles del vídeo.

<!-- more -->

Con esas URLs podemos insertar un vídeo cómodamente usando LibreVideoJS.
En Wordpress es muy sencillo usando el complemento[LibreVideoJS HTML
Player](https://es.wordpress.org/plugins/librevideojs-html5-player/),
como veis en la siguiente imagen.
<a href="/wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118.png"><img class="aligncenter size-full wp-image-654" src="/wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118.png" alt="Usando LibreVideoJS HTML Player" width="1866" height="856" srcset="/wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118.png 1866w, /wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118-300x138.png 300w, /wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118-768x352.png 768w, /wp-content/uploads/2017/01/Captura-de-pantalla-de-2017-01-14-121118-1024x470.png 1024w" sizes="(max-width: 1866px) 100vw, 1866px"/></a>
Como ejemplo, he utilizado un vídeo de Richard Stallman de YouTube. A
continuación, dejo también por escrito las instrucciones que he
utilizado...

    :::bash
    # Obtiene la URL original del vídeo
    youtube-dl --get-url http://www.youtube.com/watch?v=9sJUDx7iEJw

    # Obtiene el thumbnail del vídeo
    youtube-dl --get-thumbnail http://www.youtube.com/watch?v=9sJUDx7iEJw

Aquí está el vídeo..., pero hay un problema: las URLs de los vídeos de
YouTube cambian cada cierto tiempo. Con lo cual tendríamos que estar
actualizando la URL del vídeo diariamente o en la frecuencia que YouTube
tenga establecida. Por esta razón el vídeo insertado de YouTube ya no se
ve.

<video id="librevideo" class="cliplibre-js-responsive-container librevjs-hd cliplibre-js librevjs-libre-skin" controls preload="none" data-setup="{}">
  <source src="https://r18---sn-h5q7dn7k.googlevideo.com/videoplayback?mime=video%2Fwebm&pl=16&source=youtube&key=yt6&nh=IgpwcjAxLm1hZDAxKgkxMjcuMC4wLjE&expire=1484438745&sparams=clen%2Cdur%2Cei%2Cgir%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cnh%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&ei=eWh6WJ23CMeciwbrlI2gAg&itag=43&signature=76E786941A709617ADE10D20A9B7882CC64B25B2.D2243DA5693F0A758A666A0AB6767FB6B1A7FC09&ratebypass=yes&clen=8083220&ip=95.123.193.170&ms=au&mt=1484416858&mv=m&dur=0.000&id=o-ALO18YreFHOCsIQaT9nIi8A8xg5XwlE8HxlXm8eIBJ62&ipbits=0&mm=31&mn=sn-h5q7dn7k&initcwndbps=1341250&gir=yes&upn=jg29z_8f44w&requiressl=yes&lmt=1298995162069711" type="video/webm">
  <p class="no_html5">Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

La solución a este problema es descargar el vídeo y subirlo a nuestro
propio servidor o subirlo a un sitio web que no modifique la URL de los
vídeos cada cierto tiempo.

Yo recomiendo usar una instancia de
[MediaGoblin](http://mediagoblin.org/), porque es más rápido, más
sencillo, no altera la URL de los vídeos y es <i lang="en">software</i> libre.
