Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2024-01-28 11:30
Lang: es
Slug: la-privacidad-que-da-cerrar-el-pico
Tags: consejos
Title: La privacidad que da cerrar el pico
Image: <img src="/wp-content/uploads/2024/01/tucán.jpg" alt="" width="1920" height="1285" srcset="/wp-content/uploads/2024/01/tucán.jpg 1920w, /wp-content/uploads/2024/01/tucán-960x642.jpg 960w, /wp-content/uploads/2024/01/tucán-480x321.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Hay cosas que no debemos divulgar: el jaqueo de un sitio web,
la compra de criptomonedas y de oro, un robo, etc. Para evitar ser
identificados hay que usar tecnologías que permiten el anonimato (como
[Tor](https://es.wikipedia.org/wiki/Tor_(red_de_anonimato)) y [redes privadas virtuales](https://es.wikipedia.org/wiki/Red_privada_virtual)), [pagar en efectivo](/jam%C3%A1s-pagues-con-tarjeta/) y no mostrar
documentos identificativos, eso está claro. Sin embargo, muchas veces
olvidamos algo muy básico: cerrar el pico.

Crear una identidad digital anónima, por ejemplo, es muy sencillo: te
registras con un nombre de usuario en una web usando una VPN o Tor y
subes contenido. Perder ese anonimato, por otro lado, también es muy
fácil: basta revelar algún dato relacionado con tu identidad real. Esas
identidades digitales pueden tener datos, pero deben ser falsos. La
manera de escribir también puede revelar nuestra identidad real, así que
es recomendable hacer un esfuerzo para cambiar el estilo de redacción.

<img src="/wp-content/uploads/2024/01/silencio.jpg" alt="">

Las personas con las que interactuamos no deben saber nada que queramos
mantener oculto, incluso aunque sean personas de confianza. Nunca
sabemos si esas personas que hoy son de confianza lo seguirán siendo
mañana. El estafador que vendió la Torre Eiffel dos veces, [Victor
Lustig](https://es.wikipedia.org/wiki/Victor_Lustig), fue detenido
porque su amante se puso celosa de una relación que tenía con otra mujer
y decidió delatarlo.

Claro, muchas veces queremos fardar de nuestras proezas. Queremos
presumir de nuestros jaqueos, queremos ostentar... Mejor no hacerlo.
Es mejor ser humilde e inventarse alguna escusa. «Ese dinero me lo dio
mi padre»; «tengo criptomonedas, pero poca cosa»... En resumen, lo mejor
es cerrar el pico si no quieres que te pillen y tampoco quieres ser el
punto de mira. No seas el tonto que presume de comprar 100 gramos de oro
y se encuentra al día siguiente su casa desvalijada.
