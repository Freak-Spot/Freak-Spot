Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-03-13
Lang: es
Slug: cómo-ver-páginas-web-censuradas
Tags: censura, libertad de información
Title: Cómo ver páginas web censuradas

Existen formas de evitar la censura con fines políticos de ciertas
páginas web en Internet, lo que tanto en Europa como en Rusia está
ocurriendo actualmente. Es posible usar
[Tor](https://www.torproject.org/), una [red privada
virtual](https://es.wikipedia.org/wiki/Red_privada_virtual), acceder a
copias de páginas web (usando la [Wayback
Machine](https://archive.org/), por ejemplo), enviar y recibir
contenido censurado por correo electrónico o servicios de mensajería,
etc.
