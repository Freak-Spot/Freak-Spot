Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2022-03-29
Lang: es
Slug: ciberguerra-contra-inocentes
Tags: gestor de paquetes, npm, política
Title: Víctimas inocentes de una ciberguerra: sobre el <i lang="en">malware</i> inyectado en node-ipc

El [ataque a la cadena de
suministro](https://es.wikipedia.org/wiki/Ataque_a_cadena_de_suministro)
usado por el programa
[node-ipc](https://github.com/RIAEvangelist/node-ipc), dependencia de
muchos otros programas, ha hecho saltar las alarmas en la comunidad del
<i lang="en">software</i> libre. El <i lang="en">malware</i> creaba un archivo con un
mensaje de protesta en el escritorio y borraba todos los archivos de los
ordenadores que tuvieran una IP rusa o bielorrusa.

Obviamente, al ser este programa libre, se podía comprobar lo que
hacía antes de descargarlo, pero mucha gente no suele hacerlo en la
práctica. El ataque ha demostrado que las actualizaciones automáticas de
programas no son seguras en gestores de paquetes de lenguajes de
programación, en particular de [npm](https://es.wikipedia.org/wiki/Npm),
que ya ha protagonizado incidentes similares en el pasado.

Este ataque ha supuesto una pérdida de confianza por parte de la
comunidad del <i lang="en">software</i> libre en el programador Brandon
Nozaki Miller. El ataque con el mensaje de paz es realmente un acto de
sabotaje, no contra las autoridades militares de Rusia y Bielorrusia,
sino contra la gente que vive en esos países, o que usa una [red privada
virtual](https://es.wikipedia.org/wiki/Red_privada_virtual) con una IP
de esos países, y desarrolla programas informáticos. ¿Demonizar y
atacar a toda la población de un país conducirá a la paz?
