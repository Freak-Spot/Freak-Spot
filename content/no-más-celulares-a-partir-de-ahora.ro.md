Author: Belgin
Category: Intimitate
Date: 2017-10-19
Lang: ro
Slug: no-más-celulares-a-partir-de-ahora
Save_as: fără-telefon-mobil/index.html
Tags: telefon mobil, software liber
URL: fără-telefon-mobil/
Title: Fără telefoane celulare de acum înainte

*[Traducere în limba română de Belgin](https://web.archive.org/web/20180501100032/https://rosenzweig.io/blog/no-cellphones.ro.html) sub licență <a href="https://creativecommons.org/licenses/by-sa/4.0/"><abbr title="Creative Commons Attribution-ShareAlike 4.0 International">CC BY-SA 4.0</abbr></a>*.

Refuz să car un telefon celular - pentru prietenii mei nedumeriți din lumea noastră tehno-centrată, urmează de ce. Câțiva dintre voi m-au întrebat deja care-i numărul meu de telefon cu scopul de a-mi trimite un mesaj text. Poate ai fost un profesor din unul dintre cursurile mele, cerându-mi să rulez programe nelibere în timpul orelor. Poate ai fost un membru al familiei, îngrijorat că într-o situație primejdioasă, nu aș fi capabilă să cer ajutor.

Sunt patru raționamente în spatele refuzului meu de a avea un telefon celular, în ciuda faptului că sunt o utilizatoare avidă a Internetului. Urmează, în ordine crescătoare a semnificativității, argumentele:

În primul rând, electronicele portabile nu îmi sunt confortabile. Majoritatea timpului petrecut de mine în fața calculatorului implică scriere, programare și artă; pentru mine, aceste sarcini necesită o tastatură întreagă sau tablete pentru desenare. Desigur, acesta nu este un motiv etic pentru a refuza telefoanele portabile și recunosc că mulți oameni au întrebuințări mai potrivite formelor compacte ale acestor calculatoare portabile.

În al doilea rând, utilizatorii telefoanelor celulare creează o cultură a acestora. Într-o fracțiune din viața unui om, celularele s-au transformat din inexistente, în folosibile în timp ce vorbești cu cineva față în față. Această cultură nu este inevitabilă pentru electronicele digitale - mulți oameni utilizează tehnologia responsabil, lucru pentru care îi admir - cu toate acestea, se întâlnește foarte frecvent. Dacă aș avea un telefon în fața nasului în timp ce mă prefăceam să vorbesc cu prietenii mei, aș fi continuat să perpetuez noțiunea că aceast comportament este în regulă. Mă tem să devin cineva care utilizează greșit tehnologia în acest fel, așa că evit să car un telefon celular pentru a evita riscul etic.

În al treilea rând, telefoanele celulare prezintă riscuri grave față de libertate și intimitate. Vasta majoritate a telefoanelor portabile de pe piață ruleaeză sisteme de operare proprietare, cum ar fi „iOS” și sunt îndesate cu programe nelibere. De asemenea, spre deosebire de calculatoare de birou sau portabile, multe din aceste sisteme de operare ale telefoanelor rulează verificări de semnătură. Acest lucru înseamnă că este imposibil din punct de vedere criptografic și în unele cazuri chiar legal, să fie înlocuit sistemul cu unul liber. Acest lucru în sine este o motivație suficientă pentru a refuza să ating aceste dispozitive.

Realitatea este, din păcate, și mai dură. Electronicele convenționale au un singur procesor, unitatea centrală de procesare (CPU). CPU-ul rulează sistemul de operare, cum ar fi GNU/Linux și este în control deplin al calculatorului. Nu este așa și pentru telefoanele celulare; aceste dispozive au două procesoare principale - CPU-ul și modemul. Primul are problemele obișnuite de libertate, iar cel din urmă este o cutie neagră conectată la internet cu o serie de capabilități înfricoșătoare. În cel mai puțin rău caz, din pricina modului în care a fost proiectată rețeaua de telefonie celulară, oricând telefonul este conecatat la aceasta (însemnând, când modemul este activ), utilizatorul poate fi localizat prin triangularea turnurilor celulare. Deja, riscul este prea mare pentru unele persoane. Operațiile telefonice tradiționale sunt vulnerabile la supraveghere și interferență, întrucât nici apelurile, nici mesajele scrise nu sunt criptate. Mai mult, foarte puține telefoane portabile prezintă izolare acceptabilă a modemului. Acest lucru înseamnă că CPU-ul, chiar dacă rulează programe libere, nu controlează modemul, iar din motive practice, este ilegal ca acesta din urmă să ruleze programe libere în Statele Unite. În multe cazuri este invers, modemul controlează CPU-ul. Nu contează dacă se folosește mesageria criptată prin XMPP dacă modemul poate pur și simplu să facă o captură de ecran fără ca CPU-ul să știe sau să-și dea acordul. Alternativ, în funcție de modul în care modemul este conectat la restul sistemului, acesta poate facilita activarea de la distanță a microfonului sau a camerei video. 33 de ani mai târziu, o lume în care toată lumea cară un telefon celular depășește până și coșmarurile lui George Orwell. Poate că „nu ai nimic de ascuns”, dar mie îmi pasă de intimitatea mea. Telefoanele celulare sunt înfiorătoare. Le refuz.

În cele din urmă, socotind implicațiile periculoase pentru societate și libertate, refuz să perpetuez acest sistem. Aș putea decide să car un telefon celular oricum, luând decizia că, fiind o persoană plicticoasă, pot să-mi sacrific libertatea în numele confortului instantaneu mulțumitor. Dar, prin complacere, aș adăuga unu la problemă, o sarcină etică grea când utilizarea rețelei celulare contribuie și la efectul de rețea, după cum sugerează numele.

Dacă aș avea telefonul scos în fața altora, aș semnala că „telefoanele portabile sunt în regulă”. Dacă cineva m-ar avea ca model etic, acesta ar continua să folosească un celular.

Dacă le-aș permite prietenilor mei să îmi trimită mesaje telefonice scrise în loc să folosească un mediu mai etic de comunicație, aș semnala că „mesajele scrise pe celular sunt în regulă” și că „este rezonabil să aștepți ca oamenii să trimită mesaje telefonice”. Dacă unii ar fi indeciși față de implicațiile etice și de nevoia de a folosi un celular, acest lucru i-ar putea determina să-l păstreze.

Dacă aș folosi un telefon pentru activitățile didactice, aș semnala că „studenții secolului 21 ar trebui să aibă celulare”. Prefer să le amintesc că aceasta nu este o alegere etică.

Dacă primesc priviri nedumerite de la cunoștințe, colegi și profesori, acum am oportunitatea să-i educ despre programe libere și intimitate. Foarte puțini oameni sunt conștienți despre aceste „dispozitive portabile de supraveghere”, precum ar scrie Richard Stallman. Aceste „momente de nedumerire” sunt oportunități perfecte pentru a-i ajuta să ia o decizie mai informată.

Cărând un telefon portabil, aș perpetua ceva rău. Prin refuzul activ de a purta unul, protestez și fac ceva bine.

Dacă nu folosesc un telefon celular, care-mi sunt alternativele?

Pentru majoritatea sarcinilor digitale, inclusiv scrierea acestei postări, folosesc un calculator portabil ce rulează programe libere. Ca bonus, pentru a mă conecta la Internet, folosesc un card Wi-Fi care rulează un microprogram liber.

Pentru a comunica cu prietenii mei, folosesc protocoale necentralizate și cu specificație deschisă, acolo unde este posibil. Mai precis, sunt disponibilă pe poșta electronică, XMPP și Mastodon. În unele cazuri unde acestea nu sunt disponibile din cauza efectului de rețea, folosesc sisteme centralizate, cum ar fi IRC. Ocazional, utilizez sisteme proprietare care au fost eliberate prin inginerie inversă, cum ar fi Discord. Acolo unde este posibil, adaug criptare puternică implementată prin programe libere, cum ar fi GPG și OTR, pentru protecție împotriva amenințărilor la intimitate. Dacă este importantă confidențialitatea locației în care mă aflu, mă conectez prin Tor. Oricare dintre aceste măsuri reprezintă un pas major înainte de la apelurile telefonice, mesajele scrise telefonice, Whatsapp sau Snapchat. Toate aceste măsuri împreună te vor apăra de majoritatea adversarilor.

Pentru a mă conecta când nu sunt acasă, caut rețele Wi-Fi publice, care pot fi făcute sigure prin adăugarea criptării și Tor. Dacă aceasta nu este o opțiune, s-ar putea să cer cuiva să-mi împrumute electronicele - acesta este un caz neplăcut, dar cât timp efectul de rețea există, este acceptabil din punct de vedere etic să-l exploatăm. Majoritatea timpului când nu sunt acasă, evit să mă conectez la Internet; sunt mai productivă fără!

Așa că da, supraviețuiesc fără telefon portabil. Nu este tot timpul confortabil, dar productivitatea, libertatea și comportarea etică biruiesc confortul oricând.

Te încurajez să faci la fel.
