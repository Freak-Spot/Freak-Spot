Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2017-10-04 03:11
Lang: es
Slug: la-era-oscura-digital
Tags: censura, digital, era, futuro, restricciones, oscura, tecnología
Title: La era oscura digital

¿Estamos inmersas en una *era oscura digital*? La [Edad
Oscura](https://es.wikipedia.org/wiki/Edad_Oscura) se caracterizó por la
ausencia de fuentes que explicaran los cambios sociales transcurridos
entre el colapso de la civilización micénica y la [Época
Arcaica](https://es.wikipedia.org/wiki/%C3%89poca_arcaica). Con el
término *era oscura digital* se podrían referir en el futuro
perfectamente a este periodo de la historia de la tecnología.

¿Dónde están los antiguos sistemas operativos, páginas web,
videojuegos...? Muchos programas han desaparecido tras quebrar una
empresa o han sido abandonados por motivos económicos. Sin embargo, el
valor histórico de las obras perdidas es incalculable.

<!-- more -->

Los [*derechos* de
autora](https://es.wikipedia.org/wiki/Derecho_de_autor) (que se
se deberían llamar *restricciones*) y las
[patentes](https://es.wikipedia.org/wiki/Patente) han provocado el
monopolio de las corporaciones sobre el producto de sus empleadas, la
desaparición de obras digitales y la criminalización de la copia y la
distribución de programas, entre otras cosas.

Hay ya varias organizaciones sin ánimo de lucro dedicadas a la
preservación de contenidos digitales como páginas web, vídeos, audio,
programas, código fuente de programas... Sin embargo, es imposible
preservar la mayor parte del contenido con las restricciones que imponen
los estados, las empresas y las limitaciones técnicas.

El contenido audiovisual y los programas de <i lang="en">software</i> privativo son los
mayores damnificados de esta pérdida de información.

La [W3C](https://es.wikipedia.org/wiki/World_Wide_Web_Consortium) ha
formalizado recientemente con estándares dictados por grandes
corporaciones las [restricciones de contenidos
digitales](https://es.wikipedia.org/wiki/Gesti%C3%B3n_de_derechos_digitales)
en la red, algunos estados plantean más restricciones al flujo de
información: la censura está por todas partes.

Sin embargo, hay muchos focos de resistencia: <i lang="en">software</i> libre,
plataformas colaborativas, redes sociales descentralizadas. Estamos
asistiendo a un cambio de paradigma que aún no se ha extendido
lo suficiente.

Ante este panorama solo la conciencia, el boicot y la acción directa
pueden cambiar las cosas a mejor.
