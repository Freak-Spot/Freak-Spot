Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2017-02-28 01:11
Lang: es
Slug: reflexión-sobre-los-medios-de-comunicación
Tags: reflexión, medios de comunicación, libros, Internet, televisión, software libre
Title: Reflexión sobre los medios de comunicación

Hoy en día hay muchos medios de comunicación que antes no existían. El
más importante para mí es el ordenador. Con los ordenadores podemos
hacer casi cualquier cosa: ver la tele, escuchar música, leer, etc.
Internet es maravilloso. También se puede experimentar la sensación de
comunidad a través de foros y *chats*[^*].

No me desagrada ver la televisión, pero lo mejor para mí es ver
exactamente lo que yo quiero a través de Internet.

Naturalmente no todo es perfecto. También hay personas malvadas que
escriben información falsa. También hay empresas y gobiernos que quieren
saberlo todo sobre nosotros. Ellos hablan sobre seguridad y una mejor
experiencia para el usuario, pero solo quieren dinero y dominación. Por
eso solo apoyo el <i lang="en">software</i> libre. Por desgracia, Internet es peligroso
para la gente que no conoce estos problemas.

<!-- more -->

Ver la televisión, sin embargo, es peor para mí, porque los espectadores
no tienen poder alguno. Quizás quiero ver una película en particular,
pero no está en la televisión. Solo puedo ver un par de programas, pero
nunca cuando yo quiero. Casi todo está manipulado, y hay muchos
anuncios. ¿Para qué necesito un televisor cuando tengo libros y un
ordenador?

¿Cuál es el futuro de los medios de comunicación? Supongo que aparecerán
nuevos medios de comunicación. Espero que la gente no olvide los libros.

[^*]:
    Una conversación escrita realizada de manera instantánea mediante el
    uso de <i lang="en">software</i> entre dos o más personas conectadas a una red.
