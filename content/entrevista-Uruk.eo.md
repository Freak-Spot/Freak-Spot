Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2018-07-01 05:36
Image: <img src="/wp-content/uploads/2018/06/Uruko.svg" alt="">
Lang: eo
Modified: 2018-07-03 01:36
Slug: entrevista-uruk
Tags: intervidiĝo, GNU/Linukso, libera programaro, projekto, Triskelo, Uruko
Title: Intervidiĝo pri Uruko Projekto

La Projekto Uruko anigas multajn utilajn programojn kaj
GNU/Linukso-distribuon bazita sur Trisquel. Mi intervidiĝis Hayder
Majid, unu el la estigintoj, por koni pli.

<!-- more -->

**Ĉi povas vi perfavore nolonge prezenti la projecto Uruko kaj la
Uruko-GNU/Linukso distribuo?**

La projekto naskiĝis 2016, sed la historio komencis tago antaŭe ĉe tio
jaro. Mi kaj mia amiko alimiracle estis pensata pri krei plenan liberan
distribuon, por satigî niajn necesojn, kaj por havigi al alioj, do ni
kunsidi kun malgranda teamo kun la sama ideo, kaj ni rapide unuiĝos. La
teamo distribuis la laboron mise, kaj la projekto fiaskis. Poste,
ĝis 2016, ni faris iuj programon kiel Uruk cleaner, [ikonan etoson
Masalla](https://www.gnome-look.org/p/1012467), [UPMS](https://notabug.org/alimiracle/UPMS) kaj alian aĵojn, do ni decidis fari ĉi tiujn programojn
sub unu projekto. Ni nomis ĝin la Projekto-Uruko, kaj faris homoniman
plenan liberan distribuon, kiu respektas la liberon kaj la privatecon da
uzantoj.

**Kio estas la estinteco da projekta kontribuantoj kaj iliaj motivo?**

Ni havas multaj personoj, kiu nin helpas kun la Projekto Uruko, kaj kiu
kunhavas niajn celumojn kaj motivojn pri libera programaro, sed precipe
havas ni du tipoj de kontribuantoj: La unua estas la projekta teama
membro. Ili havas projektoj sub Uruko Projekto aŭ programita en unu aŭ
pli Uruka subprojektoj, kiu faris substancan aliiĝon je la Projekto
Uruko, kaj la aktiva teamaj membroj estas la sekvanta:

- Ali Abdul Ghani (Ali Miracle): programisto kaj fondinto de la
  Projekto Uruko kaj programisto de Uruko-GNU/Linukso kaj aliaj
  projektoj.
- Hayder Majid (Hayder Ctee): programainĝenierado, programisto,
  fasonisto, fondinto de la Projekto Uruko kaj Uruko-GNU/Linukso kaj
  aliaj projektoj.
- Rosa: Programisto, pako-administristo kaj programisto de la
  Projekto Uruko kaj Uruko-GNU/Linukso.
- Ahmed Nourllah: Programisto de la Uruko-Projekto kaj ĉefa programisto
  de la Uruka vikio.

La dua estas la alia tipo de kontribuanto, kiu eble subtenas la projekto
tradukanta aŭ skribanta kodon, pakanta, k.t.p.

**Kiel uzulo de Uruko-GNU/Linukso, mi plenumas ĝi kun malnova Lenovo
Thinkpad X60 tekomputilo, kaj tamen havas ebenan sperton de uzanto.
Dankon pro fari la estoson Masalla,
kiun vi menciis antaŭ. Estando Uruko-GNU/Linukso distribuo bazita sur
Trisquel, mi dimandi min kial vi ne elektis Debiano kiel bazo aŭ
konlaboris direkte kun Triskelo anstataŭ estigi novan distribuon.**

Kiel Rosa diris, "vi ne povas manĝigi unu tipon de kuko al tutaj homoj".

Ni elektis Triskelo ĉar ni kredas pri la filosofo de libera programaro.
Triskelo estas tuta libera distribuo, kiu satiĝas niajn celojn sed ne
niajn bezonojn, do ni faris nian distribuon. Se vi provis Urukon kaj
Triskelon antaŭe, vi trovos iujn malsamojn inter ambaŭ, do Uruko estas
pli akomodebla kaj havas multajn programojn faritaj de la Uruka teamo
(kiel upms, ucc kaj aliaj programoj). Ni ankaŭ akceptas la opinion el la
komunumo kiel la bazo de niaj eldonaj, kaj mi pensas ke ni influas
Triskelon, sed nerekte: finfine, vi povas rigardi ĝin en Triskelo 8, kiu
uzas la fenestrilon de MATE kiel defaŭlta kaj VLC kiel la spektilo [Uruk
faris ĝin antaŭ].

**Ĉu vi klopodas gajni agnoskon de la Free Software Foundation kaj
esti adicita al la listo de distribuoj kiuj estas tute liberaj?**

Jes, ni serioze klopodas adici Uruko-GNU/Linukson al la listo de liberaj
distribuoj, sed ĝi necesas multon da tempo, oŭ kiel mi amiko Ali diris
«mil jaroj [rideto]»

**La lasta eldono estis GNU/Linux Uruk 2.0, bazita sur Triskelo 8. Kioj
estas via plano por la estonteco?**

Ni planas multon por la futuro, kiel adici kelkajn novajn fenestrilojn
al niaj Uruko-GNU/Linukso eldonoj, pliboniĝi Projektan infrastrukturon,
adici novajn programon kreiĝitaj de la projekta teamo, kaj aliaj
surprizoj [okulsigno].
