Author: Jorge Maldonado Ventura
Category: Edição de textos
Date: 2023-11-22 23:36
Lang: pt
Slug: numerar-líneas-de-poema-cada-verso-y-cada-5-versos
Tags: GNU/Linux, Perl, sed, poesia, nl
Title: Numerar linhas de poema, a cada 5 versos e a cada verso
Save_as: numerar-linhas-de-poema-a-cada-5-versos-e-a-cada-verso/index.html
URL: numerar-linhas-de-poema-a-cada-5-versos-e-a-cada-verso/
Image: <img src="/wp-content/uploads/2023/11/poema-o-pirata-numerado.png" alt="">

Raramente os versos dos poemas que encontramos na Internet são
numerados. A numeração não é necessária para a leitura, mas é muito útil
para a análise e comentário de um poema longo. Neste artigo, mostro como
numerar um ficheiro de texto (no qual devemos ter colado o poema copiado
da Internet).

Como [exemplo de ficheiro](/wp-content/uploads/2023/11/O-Pirata_Gonçalves-Dias.txt) utilizo o poema *O pirata*, de Gonçalves Dias.
Se quisermos numerar todas as suas linhas, basta executar o seguinte comando:

    :::bash
    $ nl O-Pirata_Gonçalves-Dias.txt

Aqui esta o resultado:

    :::text
         1	Nas asas breves do tempo
         2	Um ano e outro passou,
         3	E Lia sempre formosa
         4	Novos amores tomou.

         5	Novo amante mão de esposo,
         6	De mimos cheia, lh'of'rece;
         7	E bela, apesar de ingrata,
         8	Do que a amou Lia se esquece.

         9	Do que a amou que longe pára,
         [...]

<!-- more -->

Convenientemente, o `nl`[^1] não numera linhas
em branco por defeito.

E se quisermos numerar apenas a cada cinco linhas? Então eliminamos os
números que não são múltiplos de cinco. Podemos encomendar este trabalho
ao Perl[^2]:

    :::bash
    nl O-Pirata_Gonçalves-Dias.txt | perl -pe 's/(^ *[0-9]*[12346789]\b)/" " x length($1)/gei'

Este é o resultado:

    :::text
           	Nas asas breves do tempo
           	Um ano e outro passou,
           	E Lia sempre formosa
           	Novos amores tomou.

          5	Novo amante mão de esposo,
           	De mimos cheia, lh'of'rece;
           	E bela, apesar de ingrata,
           	Do que a amou Lia se esquece.

           	Do que a amou que longe pára,
         10	Do que a amou, que pensa nela,
         [...]

O que a expressão regular anterior faz é substituir os números que não
são múltiplos de cinco no início da linha por caracteres em branco. Uma
vez que o `nl` introduz vários caracteres em branco no início da linha,
utilizamos `^ *`. Para evitar um desajuste de caracteres, calculamos o
comprimento da seleção, que substituímos por espaços (`" " x length($1)`).

Pode-se fazer isso de forma mais simples com o `sed`, mas haveria um
ligeiro desajuste de caracteres; porém, uma vez que `nl` usa tabulações
por defeito depois dos números, não é perceptível.

    :::bash
    nl O-Pirata_Gonçalves-Dias.txt | sed 's/^ *[[:digit:]]*[1|2|3|4|6|7|8|9]\b/   /'


[^1]: Pertence ao pacote `coreutils`, que já está instalado nas diferentes distribuições GNU/Linux.
[^2]: Executa `sudo apt install perl` se não o tens instalado (para
    distribuições derivadas do Debian).
