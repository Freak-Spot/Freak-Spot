Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2022-02-04 22:00
Lang: es
Slug: superburbuja
Tags: burbuja, colapso, descentralización, tecnología
Title: Superburbuja: ¿cambio tecnológico, económico y social?
Image: <img src="/wp-content/uploads/2022/02/stock-crash.jpg" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/02/stock-crash.jpg 1024w, /wp-content/uploads/2022/02/stock-crash-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

Hay una superburbuja a punto de estallar: el precio de la vivienda ha
aumentado un montón, las acciones están muy sobrevaloradas, los alimentos
son más caros, el precio del petróleo se ha disparado. ¿Qué pasará
cuando estalle la burbuja?

Algunos dicen que la sociedad industrial está
[colapsando](https://es.wikipedia.org/wiki/Colapso_social); otros
piensan que pronto habrá una
[depresión](https://es.wikipedia.org/wiki/Depresi%C3%B3n_(econom%C3%ADa)).
Orientar el sistema hacia un crecimiento económico sin fin cuando
vivimos en un planeta con recursos limitados es algo absurdo. Sin
embargo, el sistema capitalista depende de este crecimiento para
mantenerse a flote. Algunos proponen colonizar el espacio para evitar el
colapso del capitalismo.

A corto plazo aumentará la pobreza. Quienes no tengan propiedades que
generen riqueza (activos) tendrán que vender su fuerza de trabajo a un
menor precio (si es que encuentran un trabajo cuando aumente más el
desempleo), robar o depender de limosnas. Quienes tienen capital o
propiedades donde cultivar comida, acceso a agua potable y vivienda lo
tienen más fácil. ¿Puede ser a largo plazo una oportunidad para fomentar
otro tipo de valores?

El aumento de las temperaturas ya ha desencadenado ciclos de
retroalimentación que provocan un mayor aumento de las temperaturas (lo
que está aumentando los eventos meteorológicos extremos, el nivel del
mar, la desertificación, etc.). Por ejemplo, al derretirse los polos, se
refleja menos radiación solar, por lo que aumenta la temperatura; al
disolverse la capa glaciar, se libera metano (un gas de efecto
invernadero) a la atmósfera; al desaparecer bosques y arrecifes de
coral, se absorbe menos CO<sub>2</sub>.

Existen tecnologías que favorecen la acumulación de capital en pocas
manos y suponen un enorme coste energético, y existen tecnologías
[descentralizadas](https://es.wikipedia.org/wiki/Descentralizaci%C3%B3n),
libres y eficientes. Los problemas que tenemos no son ni mucho menos
únicamente tecnológicos, pero la tecnología está aumentándolos.
¿Favorecerá el estallido de la superburbuja un cambio tecnológico,
económico y social?

Nada dura para siempre.

<small>[Imagen *Coit Tower fresco - the stock crash*](https://live.staticflickr.com/1/679924_59bcaf5217_b.jpg) por Zac Appleton.</small>
