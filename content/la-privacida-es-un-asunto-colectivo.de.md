Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2023-01-30 18:16
Image: <img src="/wp-content/uploads/2019/12/graffiti-camara-vigilancia.jpg" alt="Graffiti einer Überwachungskamera">
Lang: de
Slug: la-privacidad-es-un-asunto-colectivo
Save_as: Privatsphäre-ist-eine-kollektive-Angelegenheit/index.html
URL: Privatsphäre-ist-eine-kollektive-Angelegenheit/
Tags: Rat, Privatsphäre
Title: Privatsphäre ist eine kollektive Angelegenheit

Viele Menschen geben eine persönliche Erklärung dafür ab, warum sie ihre
Privatsphäre schützen oder nicht. Diejenigen, denen das nicht so wichtig
ist, sagen, sie haben nichts zu verbergen. Diejenigen, die sich darum
kümmern, tun dies, um sich vor skrupellosen Unternehmen, repressiven
Staaten und so weiter zu schützen. In beiden Positionen wird oft
fälschlicherweise angenommen, dass die Privatsphäre eine persönliche
Angelegenheit ist, was nicht der Fall ist.

Die Privatsphäre ist sowohl eine individuelle als auch eine öffentliche
Angelegenheit. Die von großen Unternehmen und Regierungen gesammelten
Daten werden nur selten auf individueller Basis verwendet. Wir können
die Privatsphäre als ein Recht des Einzelnen in Bezug auf die
Gemeinschaft verstehen, wie [Edward Snowden](https://de.wikipedia.org/wiki/Edward_Snowden) sagt:

> Die Behauptung, dass einem die Privatsphäre egal ist, weil man
> nichts zu verbergen hat, ist nichts anderes als die Behauptung, dass
> einem die Redefreiheit egal ist, weil man nichts zu sagen hat.

Deine Daten können zum Guten oder zum Schlechten verwendet werden.
Unnötigerweise und ohne Erlaubnis erhobene Daten werden oft für
schlechte Zwecke verwendet.

Staaten und große Technologieunternehmen verletzen unverhohlen unsere
Privatsphäre. Viele Menschen geben stillschweigend zu, dass sich daran
nichts ändern lässt: die Unternehmen haben zu viel Macht, und die
Regierungen werden nichts unternehmen, um etwas zu ändern. Und natürlich
sind diese Menschen daran gewöhnt, die Macht an Unternehmen abzugeben,
die mit ihren Daten Geld verdienen, und sagen den Staaten damit, dass
sie ihnen kein Dorn im Auge sein werden, wenn sie eine Politik der
Massenüberwachung umsetzen wollen. Letztlich wird dadurch die
Privatsphäre derjenigen beeinträchtigt, denen es wichtig ist.

Kollektives Handeln beginnt mit dem Einzelnen. Jeder sollte darüber
nachdenken, ob er Daten herausgibt, die er nicht herausgeben sollte, ob
er das Wachstum datenschutzfeindlicher Unternehmen fördert und, was am
wichtigsten ist, ob er die Privatsphäre seiner Freunde. Der beste Weg,
private Informationen zu schützen, ist, sie nicht weiterzugeben. Wenn
das Problembewusstsein vorhanden ist, können Projekte zum Schutz der
Privatsphäre unterstützt werden.

Personenbezogene Daten sind sehr wertvoll &mdash; so wertvoll, dass manche sie
als das „neue Öl“ bezeichnen &mdash; nicht nur, weil sie an Dritte verkauft
werden können, sondern auch, weil sie denjenigen, die sie besitzen,
Macht verleihen. Wenn wir sie den Regierungen überlassen, geben wir
ihnen die Macht, uns zu kontrollieren. Wenn wir sie an Unternehmen
weitergeben, geben wir ihnen die Macht, unser Verhalten zu beeinflussen.
Letztendlich ist die Privatsphäre wichtig, weil sie uns hilft, die Macht
zu bewahren, die wir über unser Leben haben und die sie uns so gerne
nehmen möchten. Ich werde meine Daten nicht weitergeben oder verkaufen,
und du?
