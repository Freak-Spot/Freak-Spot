Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2016-07-15 12:52
Lang: es
Modified: 2017-03-30 16:41
Slug: instalacion-de-un-servidor-glamp
Status: published
Tags: Apache, GLAMP, GNU/Linux, instalación GLAMP, MySQL, PHP, phpMyAdmin, Trisquel, Trisquel Belenos, Trisquel 7
Title: Instalación de un servidor GLAMP

Introducción {#introducción style="text-align: center;"}
------------

GLAMP es un acrónimo que define un sistema infraestructura de internet
que incluye las siguientes herramientas: GNU, Linux, Apache, MySQL y PHP
o Python o Perl. Realmente GNU y Linux ya están instalados, se utiliza
esa notación para distinguir GLAMP de MAMP (de Mac OS) y WAMP (de
Windows). Se suele usar GLAMP para realizar tareas relacionadas con la
programación de aplicaciones web o para tareas que necesiten de un
servidor HTTP como del de Apache.

La instalación la voy a realizar en una distribución de GNU/Linux
llamada Trisquel, pero el proceso es similar en otras distribuciones.
Trisquel está basada en Ubuntu, así que los pasos son idénticos o casi
idénticos en ambas.

Instalación {#instalación style="text-align: center;"}
-----------

Abrimos `tasksel` (instálalo si tu distribución no lo trae instalado)
con permisos de superusuario (`sudo tasksel`). Encontraremos una
pantalla que pone «Selección de programas».

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-134703.png"><img class="aligncenter size-full wp-image-143" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-134703.png" alt="tasksel (Trisquel)" width="577" height="413" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-134703.png 577w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-134703-300x215.png 300w" sizes="(max-width: 577px) 100vw, 577px"></a>

Veréis que la opción sobre la que estáis situados se marca con un color
diferente; quizá tengáis algunas opciones marcadas con un asterisco.
Tenéis que seleccionar `GLAMP web server` (puede que no tenga
exactamente el mismo nombre dependiendo de la distribución. Como regla
general, si pone LAMP es lo que buscas). Ahora pulsáis la barra
espaciadora (veréis aparece un asterisco) y después *Enter*. Enseguida
se iniciará el instalador.

Durante la instalación os preguntará por la contraseña que queréis que
tenga MySQL, escoged la de vuestra elección y continuará la instalación.

Comprobad ahora si se puede acceder a localhost con vuestro navegador.
Deberá aparecer esta página web o una similar:

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601.png"><img class="aligncenter size-full wp-image-144" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601.png" alt="Apache localhost on Trisquel" width="1920" height="1044" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601.png 1920w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601-300x163.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601-768x418.png 768w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-140601-1024x557.png 1024w" sizes="(max-width: 1920px) 100vw, 1920px" /></a>

Podéis comprobar también si MySQL se ha instalado correctamente
intentando acceder a MySQL con la contraseña especificada durante la
instalación: `mysql -u root -p` (tras introducir esto en la Terminal, os
preguntará la contraseña para acceder).

Ahora vamos a instalar phpMyAdmin con la instrucción
`sudo apt-get install phpmyadmin`. Nos preguntará por el servidor que
queremos elegir para configurar phpMyAdmin, elegimos apache2. También
debemos decir que sí queremos que nos configure la base de datos
automáticamente con «dbconfig-common» . Luego nos pedirá la contraseña
de la base de datos.

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141706.png"><img class="aligncenter size-full wp-image-145" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141706.png" alt="Captura de pantalla de 2016-07-15 14:17:06" width="577" height="413" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141706.png 577w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141706-300x215.png 300w" sizes="(max-width: 577px) 100vw, 577px" /></a>

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141835.png"><img class="aligncenter size-full wp-image-146" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141835.png" alt="instalando phpmyadmin" width="577" height="413" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141835.png 577w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-141835-300x215.png 300w" sizes="(max-width: 577px) 100vw, 577px" /></a>

Ahora podemos probar si phpMyAdmin se ha instalado, yendo a
[localhost/phpmyadmin](http://localhost/phpmyadmin) con nuestro
navegador. Nos deberá aparecer una pantalla que nos permita iniciar
sesión. Aquí debemos introducir `root` (o nuestro usuario de MySQL si
habéis creado ya uno) y su contraseña.

<figure id="attachment_148" style="width: 1920px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838.png"><img class="wp-image-148 size-full" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838.png" alt="main screen phpMyAdmin" width="1920" height="1044" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838.png 1920w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838-300x163.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838-768x418.png 768w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142838-1024x557.png 1024w" sizes="(max-width: 1920px) 100vw, 1920px"></a><figcaption class="wp-caption-text">La pantalla que aparece al iniciar sesión en phpMyAdmin</figcaption></figure>

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142943.png"><img class="aligncenter size-full wp-image-149" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142943.png" alt="login phpMyAdmin" width="981" height="706" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142943.png 981w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142943-300x216.png 300w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-142943-768x553.png 768w" sizes="(max-width: 981px) 100vw, 981px" /></a>

Ahora debéis cambiarle el nombre al archivo HTML que aparece cuando
vamos a <http://localhost> con en el navegador (`sudo mv
/var/www/html/index.html /var/www/html/index_de_apache.html`).  Esto lo
hago para no ver el molesto mensaje de bienvenida cada vez que voy a
localhost con el navegador. En vez de eso, veremos los archivos del
directorio raíz de nuestro servidor GLAMP: `var/www/html`.

<figure id="attachment_150" style="width: 620px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144207.png"><img class="size-full wp-image-150" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144207.png" alt="Se verá algo como esto cuando estemos en localhost" width="620" height="506" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144207.png 620w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144207-300x245.png 300w" sizes="(max-width: 620px) 100vw, 620px" /></a><figcaption class="wp-caption-text">Se verá algo como esto cuando estemos en localhost</figcaption></figure>

Ahora debemos cambiar el usuario y el grupo de la carpeta `html`. El
grupo que debemos especificar es `www-data`, que es el que utiliza
Apache. Lo debemos hacer de forma recursiva para que todo el contenido
de la carpeta también cambie de propietario y de grupo. La instrucción
es la siguiente:

```
sudo chown -R $USER:www-data /var/www/html
```

Puede que quieras modificar también los permisos de esta carpeta según
tus necesidades.

<a href="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144455.png"><img class="aligncenter size-full wp-image-151" src="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144455.png" alt="Captura de pantalla de 2016-07-15 14:44:55" width="577" height="173" srcset="/wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144455.png 577w, /wp-content/uploads/2016/07/Captura-de-pantalla-de-2016-07-15-144455-300x90.png 300w" sizes="(max-width: 577px) 100vw, 577px"></a>
