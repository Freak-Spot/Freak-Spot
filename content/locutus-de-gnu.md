Author: Jorge Maldonado Ventura
Category: Cine
Date: 2017-02-01 00:03
Lang: es
Modified: 2021-11-02
Slug: locutus-de-gnu
Status: published
Tags: GNU/Linux, gracioso, humor, software libre, software privativo, Star Trek, video
Title: Locutus de GNU

Encontré [esta
parodia](https://archive.org/details/libreweb/locutus.webm)
muy graciosa. Para entenderla al menos debéis saber qué son el <i
lang="en">software</i>
libre y el <i lang="en">software</i> privativo. Espero que os guste.

<!-- more -->

<video controls poster="/wp-content/uploads/2017/01/Bildschirmfoto-vom-2017-02-01-00-58-04.png">
  <source src="https://ia903102.us.archive.org/7/items/libreweb/locutus.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>
