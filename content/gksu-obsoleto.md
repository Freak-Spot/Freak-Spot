Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2018-04-23 15:56
Lang: es
Slug: gksu-está-obsoleto
Tags: Debian 10, Debian Buster, gksu, gksudo, permisos, root, seguridad, sudo, superusuario, Trisquel 9
Title: gksu está obsoleto

[gksu](/gksudo-en-vez-de-sudo-para-aplicaciones-graficas/)
ha sido eliminado de los repositorios de Debian Buster. Es decir,
Debian y sus distribuciones derivadas no podrán usar la instrucción gksu
como antes (en Trisquel 9 no será posible ejecutar `gksu`).

gksu ha estado obsoleto durante algunos años. El modo recomendado de
ejecutar aplicaciones gráficas con permisos elevados es usando
[PolicyKit](https://wiki.debian.org/PolicyKit).

En resumen, si antes ejecutábamos `gksu gedit /etc/hosts`, ahora tenemos
que ejecutar `gedit admin:///etc/hosts`. gksu llevaba sin mantenimiento
desde 2014, lo cual [Debian consideró una vulnerabilidad de
seguridad](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=892768).
