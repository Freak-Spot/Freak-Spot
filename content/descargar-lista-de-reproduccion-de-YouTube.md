Author: Jorge Maldonado Ventura
Category: Trucos
Date: 2023-04-13 10:00
Lang: es
Slug: descargar-lista-de-reproduccion-de-youtube
Tags: lista de reproducción, PeerTube, YouTube
Title: Descargar lista de reproduccion de YouTube
Image: <img src="/wp-content/uploads/2023/04/logo-yt-dlp.png" alt="">

Podemos usar el programa [`yt-dlp`](https://github.com/yt-dlp/yt-dlp/).
[Hay varias formas de
instalarlo](https://github.com/yt-dlp/yt-dlp/#installation), una de
ellas es usando el gestor de paquetes de nuestra distribución:

    :::text
    sudo apt install yt-dlp

Para descargar una lista de reproducción, simplemente hay que poner en
una terminal `yt-dlp` seguido de la URL de la lista de reproducción y
pulsar <kbd>Enter</kbd>. Por ejemplo:

    :::text
    yt-dlp https://youtube.com/playlist?list=PLvvDxND6LkgB_5dssZod-3BET4_DFRmtU

Este programa no solo descarga vídeos de
YouTube, también puede descargar vídeos de [un montón de páginas
web](https://ytdl-org.github.io/youtube-dl/supportedsites.html).
