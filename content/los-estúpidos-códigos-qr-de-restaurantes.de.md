Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2022-11-18 09:00
Lang: de
Slug: los-estúpidos-códigos-qr-en-restaurantes
Save_as: die-dumme-QR-Codes-in-Restaurants/index.html
URL: die-dumme-QR-Codes-in-Restaurants/
Tags: QR-kodo, restaurantes
Title: Die dumme QR-Codes in Restaurants
Image: <img src="/wp-content/uploads/2022/11/hombre-chupando-móvil.png" alt="Hombre chupando móvil" width="960" height="1200" srcset="/wp-content/uploads/2022/11/hombre-chupando-móvil.png 960w, /wp-content/uploads/2022/11/hombre-chupando-móvil-480x600.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Früher war es üblich, in ein Restaurant zu gehen, auf die Speisekarte zu
schauen und zu bestellen: so einfach war das. Heute gibt es in vielen
Geschäften nicht einmal mehr eine physische Speisekarte; sie gehen davon
aus, dass der Kunde ein „intelligentes“ Telefon und eine
Internetverbindung hat. In diesem Fall wird vom Kunden erwartet, dass er
die Kamera benutzt und den
[QR-Code](https://eo.wikipedia.org/wiki/Rapidresponda_kodo) scannt, was
ihn auf eine Webseite führt, die die Privatsphäre nicht respektiert, oft
lange Ladezeiten hat und in vielen Fällen nicht intuitiv ist.

## Es ist ineffizient und verschmutzt mehr

Das Laden einer Webseite mit Bildern auf einen entfernten Server für
jeden Kunden ist umweltschädlich. Mit einer physischen Speisekarte wird
kein Strom verschwendet, die Menschen können die Karte unbegrenzt
wiederverwenden... Wenn es kein Internet gibt oder die Batterie leer
ist, wie kannst du dann das Menü mit dem QR-Code anzusehen?

## Ohne Privatsphäre

Wenn wir eine Website besuchen, hinterlassen wir eine digitale
Spur. Wenn wir QR-Codes verwenden, um die Speisekarte anzusehen, können
Unternehmen, Regierungen usw. wissen, dass wir zu einem bestimmten
Zeitpunkt die Speisekarte eines bestimmten Restaurants geschaut
haben.

Die Kunden verlieren auch ihre Privatsphäre, wenn sie mit Karte statt
mit Bargeld bezahlen, aber das ist ein anderes Thema.


<a href="/wp-content/uploads/2022/11/nein-danke-QR-Restaurants.png">
<img src="/wp-content/uploads/2022/11/nein-danke-QR-Restaurants.png" alt="" width="1200" height="626" srcset="/wp-content/uploads/2022/11/nein-danke-QR-Restaurants.png 1200w, /wp-content/uploads/2022/11/nein-danke-QR-Restaurants-600x313.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">
</a>

## Besser ohne QR-Code

Ich habe kein „intelligentes“ Mobiltelefon und ich gehe nicht gerne in
Restaurants. Wenn ich in einem Restaurant esse, frage ich nach der
physischen Speisekarte. Wenn sie ihn mir nicht geben, müssen sie es mir
sagen, denn ich habe keine Möglichkeit, den QR-Code zu sehen. Das meiste
Essen in Restaurants ist ungesund, die Arbeiter werden oft ausgebeutet,
es werden viele Lebensmittel verschwendet, es gibt nur wenige vegane
Optionen usw. Die Restaurants haben viele Probleme. Die Verwendung
des QR-Codes für Speisekarten ist nur ein weiterer Schritt in die
falsche Richtung, aber sehr leicht zu bekämpfen, indem man sich weigert,
ein „intelligentes“ Telefon zum Scannen eines dummen QR-Codes zu
benutzen.
