Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2020-04-17 22:45
Lang: es
Slug: luz-roja
Tags: verso
Title: Luz roja
JS: quiere-morir.js (bottom)

Mirlo, clava en mí tu mirada.  
Castaño, clava en mí tus erizos.  
Llevadme lejos de la luz azul.

Hay un lugar donde la luna ilumina,  
donde la noche susurra tu nombre,  
que nos pide unir nuestros cuerpos.  
¿Sientes las sombras que abrasan?  
¿Escuchas el canto del bosque?

Roja la luz, hazla vivir.  
Azul la luz, hazla morir.
