Author: Jorge Maldonado Ventura
Category: Privacidade
Date: 2023-01-29 00:00
Lang: pt
Slug: cómo-recoger-información-de-uso-de-usuarios-de-redes-sociales-ingeniería-social
Save_as: Como-investigar-pessoas-através-das-redes-sociais/index.html
URL: Como-investigar-pessoas-através-das-redes-sociais/
Tags: engenharia social, Facebook, Instagram, redes sociais, TikTok, Twitter
Title: Como investigar pessoas através das redes sociais
Image: <img src="/wp-content/uploads/2023/01/secreto-mujer.png" alt="" width="1200" height="844" srcset="/wp-content/uploads/2023/01/secreto-mujer.png 1200w, /wp-content/uploads/2023/01/secreto-mujer-600x422.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Poucos detectives informáticos prestam atenção aos padrões de utilização
das redes sociais, que podem revelar desejos profundos, disposições de
ânimo, etc. O algoritmo das redes <!-- more -->sociais[^1] faz isto. Então como
podemos tirar partido disso quando investigamos uma pessoa?

[^1]: Por outro lado, existem algumas redes sociais como o
    [Mastodon](https://joinmastodon.org/pt-BR) que não têm um algoritmo
    de recomendação e não recolhem grandes quantidades de dados de
    utilizadores.

Ao pesquisar uma pessoa utilizando redes sociais, é necessário também
recolher informações de utilização, e isto pode ser feito seguindo-as
com uma conta isolada. Com este método, o algoritmo recomendará coisas
relacionadas com os seus interesses, a sua disposição, etc., coisas que
a pessoa nem sequer publicou.

<img src="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg" alt="" width="6016" height="4000" srcset="/wp-content/uploads/2023/01/chica-en-blanco-y-negro.jpg 6016w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-3008x2000.jpg 3008w, /wp-content/uploads/2023/01/chica-en-blanco-y-negro-1504x1000.jpg 1504w" sizes="(max-width: 6016px) 100vw, 6016px">

Obter o endereço IP e os dados técnicos[^2] da pessoa é bastante fácil:
basta guiá-la para um URL num servidor que tenhas, e para o fazer tem de
a fazer clicar numa ligação ou carregar uma imagem ou vídeo que tenhas para
esse fim, o que não é difícil se conhecer os seus interesses. Se
tivermos o seu correio eletrónico, podemos utilizar um
[<i lang="en">web beacon</i>](https://pt.wikipedia.org/wiki/Web_beacon) (ou seja, uma
imagem invisível).

[^2]: De onde acede à Internet, que dispositivo utiliza, que línguas
    configurou, que versões do navegador e do sistema operativo utiliza,
    etc.

Se quisermos obter mais informações, temos de obter os pedidos HTTP da
pessoa sob investigação. Por vezes os dados podem ser comprados a
fornecedores de Internet ou na [Internet profunda](https://pt.wikipedia.org/wiki/Deep_web_e_surface_web), outras vezes as forças
de segurança governamentais ou agências de inteligência têm acesso a
estes dados. Além disso, a [engenharia social](https://pt.wikipedia.org/wiki/Engenharia_social_(seguran%C3%A7a)) poderia ser utilizada no
mundo real, comprando dados de sítios web que a pessoa utiliza para
obter o seu número de telefone real, etc.

A única forma de se proteger de sites de redes sociais abusivas e do
acesso à sua informação por parte de <i lang="en">hackers</i>, governos, etc., é não os
utilizar. Podes reduzir os riscos utilizando-os de forma limitada: sem
JavaScript (que muitas redes sociais abusivas não permitem), através do
[Tor](https://pt.wikipedia.org/wiki/Tor_(rede_de_anonimato)), apenas
postar ou apenas ler, não clicar em publicações que chamam a tua
atenção, não dizer do que gostas, passar sempre a mesma quantidade de
tempo em cada publicação para não mostrar preferência por nenhum deles
(o que é complicado), etc.

Em suma, é fácil investigar pessoas que não são utilizadores, mas que
são utilizadas, uma vez que são o produto de empresas que comercializam
as suas vidas, os seus desejos, as suas disposições, etc. A soberania
tecnológica pode ser aumentada tendo o teu próprio sítio web, em vez de
depender de uma rede social, ou utilizando uma rede social livre que
respeite a privacidade dos utilizadores.
