Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-07-04 19:36
Lang: es
Modified: 2018-07-05 01:49
Slug: asaltadas-por-defender-la-privacidad
Tags: Alemania, asalto, estado, organización, privacidad, Zwiebelfreunde
Title: Asaltadas por defender la privacidad

Integrantes de la asociación
[Zwiebelfreunde](https://www.zwiebelfreunde.de/), dedicada a la defensa
y la educación sobre la privacidad, han sido víctimas de registros
policiales, sin motivo justificable. Fueron registrados el 20 de junio
el local de la asociación y las viviendas de integrantes del comité
ejecutivo. También [registraron los espacios del Chaos Computer Club en
Augsburg](https://www.spiegel.de/netzwelt/web/hausdurchsuchungen-bei-netzaktivisten-chaos-computer-club-kritisiert-polizeivorgehen-a-1216463.html).

La justificación que han dado las fuerzas del estado es que había un
sitio web que llamaba a realizar acciones contra la reunión anual del
partido de extrema derecha
[AfD](https://es.wikipedia.org/wiki/Alternativa_para_Alemania). Dicho
sitio web no tiene nada que ver con la asociación ni con las personas a
las que han registrado. La supuesta conexión es el uso del servidor de
correo de Riseup (organización a la que la asociación Zwiebelfreunde
solo apoya económicamente) [por el sitio web
sospechoso](https://augsburgfuerkrawalltouristen.noblogs.org/impressum/)
de una *futura* acción criminal.

Las activistas por la privacidad son muchas veces víctimas
injustificadas de invasiones a la privacidad por parte de las fuerzas
represivas de los estados. En este caso las personas registradas lo han
sido como testigas; tratadas como criminales.
