Author: Jorge Maldonado Ventura
Date: 2018-08-08 18:14
Modified: 2023-07-27 13:00
Lang: eo
Save_as: regularo-pri-uzado/index.html
Slug: política-de-uso
Status: hidden
Title: Regularo pri uzado
Url: regularo-pri-uzado/

Oni akceptas sugestojn kaj aliigojn de ĉi tiu regularo adresitaj per la
[koncernaĵo-administrilo de Freak Spot](https://notabug.org/Freak-Spot/Freak-Spot/issues)
aŭ per [nia kontakta retpoŝto](/eo/pages/contacto.html).

## Kondiĉaro pro uzado

Ni ne akceptas la respondecon pri la eblaj problemoj derivitaj de la
uzado de nia retpaĝaro. Ni akceptas kritikojn kaj korrektojn por pliboni
la paĝaron kaj solvi la eblajn erarojn, kiujn ni havigu.

Freak Spot respektas la opiniojn, kritikojn kaj proponojn esprimitaj en
komentoj. Ni havas la rajton forigi la spamojn.

Kiam ebla, ni penas fari la paĝaron alirebla al pli granda nombro de
homoj eble: al malkapablaj homoj; al kiu retumas kun Ĝavaskripto
maleblita; al homoj, kiu uzas teksto-retumilojn; kun multa trafiklimigo,
ktp.

Kie ne notita kontraŭon, la permesiloj de Freak Spot estas la
[CC0](https://creativecommons.org/publicdomain/mark/1.0/deed.eo) de
publika havaĵo, por la enhavo (tekstoj, bildoj, videoj, komentoj...), kaj la
<abbr title="Affero General Public License, version 3">AGPLv3</abbr> de
libera programaro, por la programaro. Freak Spot ankaŭ uzas
programojn faritajn de aliaj, kiuj povas esti sub alia liberprograma permesilo, konsultu [informon pri
permesiloj](https://notabug.org/Freak-Spot/Freak-Spot#informaci%C3%B3n-de-licencias)
por pli informo pri la programaj permesiloj.

## Regularo pri privateco

Freak Spot uzas [Nginx](https://eo.wikipedia.org/wiki/Nginx)-servilon, kiu
gastigas en dosieroj aliro-datumojn: <abbr title="Interreta
Protokolo">IP</abbr>n, uzitan retumilon, daton de vizito, ktp. Ĉi tiu
informo povus esti uzita por trovi vian identecon. Por retumi kiel eble
plej private, oni povas aliri al retejo en
<http://63xpbju6u6kzge3k5mobwivob2seui4ka26l2iboraw5lxz262brgjad.onion/>
per la retumilo [Tor Browser](https://www.torproject.org/).

Pri la artikoloj oni povas fari anonimajn komentojn, kun falsaj nomoj aŭ
kun realaj datumoj. Okaze, ke iu volu forigi aŭ korekti komenton, ri devus
[kontakti nin](/eo/pages/contacto.html)
demonstranta, kiu vi skribis la komenton, en tiu kazo ni indikos, ke oni
eliminis aŭ modifis la komenton kiel ni trovis tion bone, ĉiam penante
esti plej travidebla eble kun la aliaj partoprenantoj de la konversacio
kaj evitante perdi la ĉirkaŭtekston de aliaj komentoj.
