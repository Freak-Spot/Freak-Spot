Author: Jorge Maldonado Ventura
CSS: asciinema-player.css
Category: Videojuegos
Date: 2018-02-04 19:38
Image: <img src="/wp-content/uploads/2018/02/VimGameSnake.png" alt="">
JS: asciinema-player.js (top)
Lang: es
Slug: videojuego-de-la-serpiente-en-vim
Tags: Neovim, serpiente, Vim, Vim-Snake, VimGameSnake
Title: Videojuego de la serpiente en Vim

En el clásico [juego de la
serpiente](https://es.wikipedia.org/wiki/La_serpiente_(videojuego))
manejas una serpiente que debe comer alimentos para crecer. Aquí os
presento dos variantes disponibles para  el
editor de texto Vim.

<!-- more -->

Los videojuegos mostrados son complementos para Vim. Se pueden
instalar fácilmente como cualquier otro complemento. Para aprender a
instalar complementos con Vim, lee la sección de ayuda correspondiente
(`:h plugins`). Yo los instalé con
[Vundle](https://github.com/VundleVim/Vundle.vim) sin problemas.

La primera variante se llama
[VimGameSnake](https://github.com/johngrib/vim-game-snake). He aquí una
demostración:

<asciinema-player src="/asciicasts/VimGameSnake.json">
Lo siento, asciinema-player no funciona sin JavaScript.
</asciinema-player>

Al ejecutar `:VimGameSnake` se inicia la partida. El tamaño del mapa
coincide con el tamaño de la terminal en que se ejecuta. Los controles
son las teclas de movimiento de Vim: <kbd>h</kbd>, <kbd>j</kbd>,
<kbd>k</kbd>, <kbd>l</kbd>.

<figure>
    <a href="/wp-content/uploads/2018/02/Vim-Snake.png">
    <img src="/wp-content/uploads/2018/02/Vim-Snake.png" alt="">
    </a>
    <figcaption class="wp-caption-text">Variante Vim Snake</figcaption>
</figure>

Existe también otra variante algo más antigua llamada Vim Snake.
Funciona con un truco bastante peculiar: como usa `ControlHold`, si no
se pulsa ninguna tecla durante un tiempo, la instrucción automática
("autocommand") se activa. Esto tiene el inconveniente de que si
mantienes una tecla pulsada durante mucho tiempo, el juego se detiene.

<asciinema-player src="/asciicasts/Vim-Snake.json">
Lo siento, asciinema-player no funciona sin JavaScript.
</asciinema-player>

A diferencia de la anterior versión, esta muestra la puntuación,
actualiza la posición del cursor y permite ajustar el tamaño del mapa
modificando varias variables:

    ::vim
    let g:snake_update = 125  " Milisegundos entre fotogramas
    let g:snake_rows = 20  " Filas del mapa
    let g:snake_cols = 50  " Columnas del mapa
