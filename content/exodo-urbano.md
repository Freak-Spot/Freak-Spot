Author: Jorge Maldonado Ventura
Category: Literatura
Date: 2017-07-30 00:53
Lang: es
Modified: 2018-07-13 21:30
Slug: éxodo-urbano
Tags: ciudad, campo, éxodo, prosa, relato
Title: Éxodo urbano

La luz se escondía tras la verde montaña; tuvo suficiente tiempo para
alcanzar su pequeño y tranquilo refugio. Ella vivía con mucha paz y
armonía con la naturaleza.

Un día encontró a otra persona en el bosque que había cerca de su
refugio mientras recogía bayas. Ella se acercó para platicar con la
desconocida, que miraba desorientada en todas las direcciones. Al
percatarse de su presencia, la saludo contenta, y comenzaron a charlar.

Ella venía de una ciudad en busca de un lugar tranquilo donde poder
vivir en paz y libertad. Ambas se comprendieron muy bien desde el
principio, así que acordaron cooperar y vivir juntas.

Las historias que contaba de la ciudad eran extrañas. Allí no había
libertad. Los animales no corrían ni volaban libremente, sino que tenían
correas en los cuellos y respiraban aire sucio. Las personas que
dirigían a esos animales cuadrúpedos también tenían amas a las que
debían servir.

Cuando llegó a la montaña y conoció a su nueva compañera, descubrió una
forma de vida maravillosa. Conoció el placer de observar las estrellas,
de respirar aire puro, de moverse libremente...
