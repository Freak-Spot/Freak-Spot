Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2020-01-25
Lang: es
Slug: más-tecnología-más-contaminación
Tags: contaminación, tecnología industrial
Title: Más tecnología para resolver los problemas creados por la tecnología

Dicen por ahí que la principal causa del calentamiento global, la
tecnología industrial, es también su solución.

En varias publicaciones que hoy he hojeado en una biblioteca me he
enterado de que con ciertas nuevas tecnologías se podría lograr una
mayor eficiencia que reduzca la contaminación y las emisiones.

Convenientemente omiten el coste ambiental y de recursos no renovables
que supone la producción, el despliegue, el uso o el mantenimiento de la
tecnología maravillosa de turno. Las revistas y periódicos con un gran
número de lectores tienen detrás una financiación que determina en gran
medida su línea editorial. Es obvio que no van a criticar a quienes los
financian, sino que van a decir solo cosas buenas de ellos. Me refiero a
las grandes empresas tecnológicas, que tanto dinero mueven.

Que va a gastar más electricidad, que va a usar materiales no
renovables durante la producción, que si es perjudicial para la salud:
esas objeciones no existen. Miran solo lo bueno, aunque sea
insignificante comparado con lo malo.

A más tecnología industrial, más contaminación. No lo digo yo, lo dicen
los datos. Esos son también macrodatos (o como dicen los «entendidos»,
*big data*), pero parece que no los procesan muy bien.
