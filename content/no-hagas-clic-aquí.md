Author: Jorge Maldonado Ventura
CSS: articles/no-hagas-clic-aquí.css
Category: Desarrollo web
Date: 2017-04-28 16:29
Lang: es
Slug: no-hagas-clic-aquí
Tags: consejos, enlaces, páginas web
Title: No hagas clic aquí

Los enlaces deben ser descriptivos, ¿no?

<!-- more -->

Hay veces que vemos en páginas web enlaces que ponen «haz clic aquí»,
«pincha aquí» u otro texto que no aporta nada.

Los enlaces se aprecian visualmente y son reconocidos por lectores de
pantalla. No hace falta decirle al usuario que tiene que «pinchar en el
enlace» como si fuera idiota. Además, utilizar expresiones de este tipo
no es adecuado, ya que no todo el mundo utiliza un ratón cuando navega
por Internet. Veamos varios ejemplos mal hechos y cómo
se pueden corregir (los ejemplos mal hechos aparecen tachados; las
correcciones, sin tachar):

<div class="two-text-comparison">
  <p>
    <s>Haz clic <a href="https://ia601403.us.archive.org/26/items/AllCreativeWorkIsDerivative/AWID2_Feb09_2010_H264_24fps.ogv">aquí</a>
    para ver el vídeo <a href="https://ia601403.us.archive.org/26/items/AllCreativeWorkIsDerivative/AWID2_Feb09_2010_H264_24fps.ogv">All Creative Work Is Derivative</a></s>.
  </p>
  <p>
    Ve el vídeo <a href="https://ia601403.us.archive.org/26/items/AllCreativeWorkIsDerivative/AWID2_Feb09_2010_H264_24fps.ogv">All Creative Work Is Derivative</a>.
  </p>
</div>

<div class="two-text-comparison">
  <p>
    <s>Puedes encontrar imágenes del oso polar
    <a href="https://upload.wikimedia.org/wikipedia/commons/6/66/Polar_Bear_-_Alaska_%28cropped%29.jpg">aquí</a>
    y
    <a href="https://upload.wikimedia.org/wikipedia/commons/2/20/Ursus_maritimus_us_fish.jpg">aquí</a>.</s>
  </p>
  <p>
    Imágenes de un
    <a href="https://upload.wikimedia.org/wikipedia/commons/6/66/Polar_Bear_-_Alaska_%28cropped%29.jpg">oso polar adulto</a>
    y
    <a href="https://upload.wikimedia.org/wikipedia/commons/2/20/Ursus_maritimus_us_fish.jpg">dos crías</a>.
  </p>
</div>

También es recomendable no utilizar URLs completas, especialmente si son
muy largas:

<div class="two-text-comparison">
  <p>
    <s>El código fuente de este sitio web es <i lang="en">software</i> libre y se encuentra en
    <a href="https://notabug.org/Freak-Spot/Freak-Spot">https://notabug.org/Freak-Spot/Freak-Spot</a>.</s>
  </p>
  <p>
    El código fuente de este sitio web es <i lang="en">software</i> libre y se encuentra en
    <a href="https://notabug.org/Freak-Spot/Freak-Spot">NotABug</a>.
  </p>
</div>
