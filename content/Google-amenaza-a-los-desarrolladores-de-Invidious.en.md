Author: Jorge Maldonado Ventura
Category: News
Date: 2023-06-10 11:20
Lang: en
Slug: google-amenaza-a-los-desarrolladores-de-invidious
Tags: Google, Invidious, privacy, free software, YouTube
Save_as: Google-threats-Invidious-developers/index.html
URL: Google-threats-Invidious-developers/
Title: YouTube threats Invidious developers
Image: <img src="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png" alt="" width="1200" height="740" srcset="/wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious.png 1200w, /wp-content/uploads/2023/06/abogado-de-los-Simpsons-YouTube-amenazando-Invidious-600x370.png 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Invidious developers [have received an email from
YouTube](https://github.com/iv-org/invidious/issues/3872) asking them to
stop offering the program. According to YouTube's legal team, they are
in violation of YouTube API terms of use, which is impossible, as
Invidious does not use YouTube API.

They also claim that Invidious “is being offered on
[invidious.io](https://invidious.io/)”, which is also untrue, because
that website does not host any instance of Invidious. As of today there
are 40 websites hosting public instances of Invidious, over which the
Invidious team has no control, as Invidious uses the free
[AGPL](https://www.gnu.org/licenses/agpl-3.0.html) license. Even if
Invidious were illegal in the United States, it is hosted on the [Tor
network](https://en.wikipedia.org/wiki/Tor_(network)), the [I2P
network](https://geti2p.net/en/) and in many countries, making it
virtually impossible to make Invidious disappear. In addition, its code
can be found on several development platforms and is on lots of
computers.

Invidious has neither agreed to YouTube's API terms of service nor
YouTube's terms of service. YouTube allows access to content hosted on
its servers via the HTTP protocol, so Invidious is not committing a
computer crime; it is simply safeguarding the right to privacy and
freedom.

Google (the company that controls YouTube), on the other hand, [does not
respect privacy](https://stallman.org/google.html#surveillance),
[censors](https://stallman.org/google.html#censorship), [requires the
use of proprietary
software](https://stallman.org/google.html#nonfree-software),
[exploits its users](/en/como-explota-Google-con-CAPTCHAs/), [develops artificial intelligence software for
military
purposes](/en/by-completing-a-Google-reCAPTCHA-you-are-helping-to-kill/),
[has a huge ecological
impact](https://time.com/5814276/google-data-centers-water/), to name
just a few examples. That's why there are people who think that [Google
should be destroyed](/en/how-to-destroy-Google/).

Fortunately, even if Invidious were to disappear, there are other free
projects such as [Piped](/en/YouTube-privately-with-Piped/),
[NewPipe](https://newpipe.net/) and
[`youtube-dl`](https://youtube-dl.org/). Will Google also threaten the
developers of these projects and their millions of users?
