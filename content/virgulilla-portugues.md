Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2021-09-05
Modified: 2022-05-29 17:00
Lang: es
Slug: virgulilla-portugues
Tags: GNU/Linux, portugués, teclado, virgulilla
Title: Virgulilla de portugués (ã, õ) en teclado español de GNU/Linux

Para escribir una virgulilla se usa la combinación
<kbd>AltGr</kbd>+<kbd>4</kbd>. Sin embargo, si queremos añadirle la
virgulilla, por ejemplo, a la a (ã) o a la e (õ), como sucede en portugués, no
podemos usando esa combinación, ya que inmediatamente aparece solo el
símbolo de la virgulilla (~). Existe la opción de pulsar la combinación
<kbd>AltGr</kbd>+<kbd>¡</kbd>, soltar y pulsar <kbd>a</kbd> para
escribir ã, pero si prefieres hacerlo con la tecla <kbd>4</kbd>, aquí te
explico cómo.

Para poder escribir ã y õ debemos modificar el archivo
`/usr/share/X11/xkb/symbols/es` (son necesarios permisos de
administrador). En la línea de la tecla 4 debemos cambiar `asciitilde`
por `dead_tilde`; es decir, cambiar esto...

    :::text
        key <AE04>	{ [         4,     dollar,   asciitilde,       dollar ]	};

por esto...

    :::text
        key <AE04>	{ [         4,     dollar,   dead_tilde,       dollar ]	};

Una vez guardada la modificación es necesario reiniciar el sistema de
entrada del núcleo (`sudo udevadm trigger --subsystem-match=input
--action=change`) o simplemente reiniciar el ordenador para que los
cambios tengan efecto.

Ahora se puede pulsar <kbd>AltGr</kbd>+<kbd>4</kbd>, soltar las teclas y
pulsar <kbd>a</kbd> para escribir ã. Si solo quieres escribir la
virgulilla (~), puedes pulsar <kbd>AltGt</kbd>+<kbd>ñ</kbd>, dos veces
<kbd>AltGr</kbd>+<kbd>4</kbd> o <kbd>AltGr</kbd>+<kbd>4</kbd> y
<kbd>espacio</kbd>.
