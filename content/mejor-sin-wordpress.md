Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2017-02-09 18:51
Lang: es
Modified: 2017-02-17 20:07
Slug: mejor-sin-wordpress
Tags: programación, Wordpress, generador de páginas estáticas, generador de sitios web estáticos, rendimiento, eficiencia, comodidad, desventajas
Title: Mejor sin Wordpress

Desde su nacimiento en junio de 2016, este blog ha usado Wordpress. Tras
casi un año de uso, he podido aprender el funcionamiento de Wordpress.
Incluso he modificado un tema y he desarrollado dos complementos para
Wordpress.

Durante ese periodo he conocido algunas de las desventajas de Wordpress:

- La administración del blog requiere el inicio de sesión: supone una
  vía de entrada más para atacar el sitio web.
- La dependencia de Wordpress hacia una base de datos MySQL supone una
  posible vía de ataque al sitio web y un requerimiento que reduce la
  flexibilidad del sitio web (no todos los servidores web tienen MySQL
  instalado).
- No se puede publicar y gestionar todo el contenido en un repositorio
  con un sistema de control de versiones como Git.
- El diseño de Wordpress apenas tiene en cuenta la eficiencia. Es
  complicado hacer un complemento eficiente, y casi nadie lo hace.
  Muchas veces los complementos introducen código JavaScript y CSS en
  páginas que no lo requieren.

En un blog como Freak Spot sería mucho más sencillo y eficiente servir
directamente páginas en formato HTML, y no depender tanto de PHP y
MySQL. Además, esto podría permitir un fácil control del contenido del
blog utilizando Git.

Con el uso de páginas estáticas  se solucionarían
todos estos problemas. Sin embargo, la migración presenta algunas
dificultades:

- Elegir o crear un generador de sitios web estáticos
- Preservar todo el contenido (artículos, páginas, imágenes...)
- Preservar y seguir permitiendo los comentarios
- Preservar las categorías y las palabras clave
- Reemplazar los complementos de Wordpress utilizados
- Reemplazar el buscador de Wordpress
- Preservar las URLs originales

Para realizar la migración pretendo utilizar Pelican, un generador de
páginas estáticas escrito en Python. Ya he comenzado a migrar el
contenido, aunque aún queda mucho trabajo por hacer. En este repositorio
se encuentra el código fuente del futuro blog:
<https://notabug.org/Freak-Spot/Freak-Spot>.
