Author: Jorge Maldonado Ventura
Category: Meinung
Date: 2017-02-28 01:11
Modified: 2021-11-09 15:00
Lang: de
Slug: reflexión-sobre-los-medios-de-comunicación
Tags: Bücher, Fernsehen, freie Software, Internet, Kommentar, Medien
Title: Kommentar zu den Medien

Heutzutage gibt es viele Medien, die vorher nicht existierten. Das
wichtigste für mich ist der Rechner. Mit dem Computer können wir fast
alles machen: fernsehen, Musik hören, lesen usw. Internet ist
wunderbar. Man kann auch das Gefühl von Gemeinschaft erleben, durch
Foren und *Chats*[^1].

Ich sehe nicht ungern fern, aber am liebsten schaue ich mir genau an,
was ich will, durch das Internet.

Natürlich ist nicht alles perfekt. Es gibt auch bosärtige Personen, die
falsche Nachrichten schreiben. Es gibt auch Firmen und Regierungen, die
alles über uns wissen möchten. Sie sprechen über Sicherheit und bessere
Nutzerfahrung, aber sie wollen nur Geld und Herrschaft. Darum
unterstütze ich nur Freie Software. Leider ist das Internet gefährlich
für Leute, die diese Probleme nicht kennen.

Fernsehen ist aber schlechter für mich, weil die Zuschauer keine Macht
haben. Vielleicht möchte ich einen besonderen Film sehen, aber er ist
nicht im Programm. Ich kann nur ein paar gute Sendungen sehen, aber nie,
wenn ich will. Fast alles ist manipuliert, und es gibt viel Werbung.
Warum brauche ich einen Fernseher, wenn ich Bücher und einen Computer
habe?

Was ist die Zukunft der Medien? Ich vermute, dass neue Medien erscheinen
werden. Ich hoffe, dass die Leute nicht die Bücher vergessen.

[^1]:
    Bezeichnet die elektronische Kommunikation in Echtzeit durch
    Software zwischen zwei oder mehr Personen in einem Netzberk.
