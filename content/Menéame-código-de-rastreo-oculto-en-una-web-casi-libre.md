Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2018-05-24 15:00
Modified: 2021-11-02
Lang: es
Slug: menéame-código-de-rastreo-oculto
Tags: AGPL, AGPLv3, empresa, español, licencias, Menéame, página web
Title: Menéame contiene código de rastreo oculto y contradice su licencia

Hace poco descubrí que el sitio web [Menéame](https://www.meneame.net/)
contiene código privativo de rastreo. El caso es bastante particular
porque se trata de una página web que usa <i lang="en">software</i> libre bajo la
licencia
[Affero General Public License, version 3](https://www.gnu.org/licenses/agpl-3.0.en.html),
la cual trata de garantizar que el <i lang="en">software</i> de páginas web solo se pueda
distribuir de forma libre, proporcionando acceso al código fuente y
notificando de las modificaciones realizadas.

<!-- more -->

<figure>
    <a href="/wp-content/uploads/2018/05/pie-de-página-Menéame.png">
    <img src="/wp-content/uploads/2018/05/pie-de-página-Menéame.png" alt="" width="1049" height="483" srcset="/wp-content/uploads/2018/05/pie-de-página-Menéame.png 1049w, /wp-content/uploads/2018/05/pie-de-página-Menéame-524x241.png 524w" sizes="(max-width: 1049px) 100vw, 1049px">
    </a>
    <figcaption class="wp-caption-text">Enlaces a la licencia y
    a <em>parte</em> del código fuente</figcaption>
</figure>

Al darme cuenta de que la [página web](https://www.meneame.net/)
contenía código de rastreo de Google que no aparecía en el
enlace que dice contener el código fuente,
[abrí una incidencia del problema](https://github.com/gallir/Meneame/issues/119)
en el repositorio del supuesto código fuente de la página web:

> He visto que la página principal de Menéame contiene funciones para el
> rastreo, concretamente Google Analytics. Sin embargo, no las encuentro
> en el código fuente. He encontrado algo que me da una pista, pero me
> hace temer que el código esté oculto.

    {% try_include 'private/stats.html' %}
    {% try_include 'private/header.html' %}

> Ocultar el código de un programa licenciado con AGPLv3 supone un
> incumplimiento de la licencia. Peor aún es el hecho de que la gente
> está ejecutando software privativo de Google que compromete la
> privacidad y ni siquiera puede verlo.

Mi incidencia se basa en dos hechos que suponen el incumplimiento de la
licencia:

1. Se ha incluido código fuente privativo y ofuscado en una versión
   derivada del código fuente original.
2. No se ha proporcionado el código fuente de la versión derivada.

Pero además de que se ignoró el problema, recibí unas «simpáticas»
respuestas un tanto contradictorias:

> Tampoco Google Analytics está "ofuscado", es su código propio, se
> copia y se pega. Independientemente de que para ti sea legible o no.

> Creo que, o bien estoy hablando con un troll, o bien con alguien que
> no tiene ni idea de como funciona un proyecto web, así que voy a dejar
> aquí la conversación :)

Tras responder correctamente a sus respuestas proporcionando citas
literales a la licencia que desmienten sus afirmaciones, quien me
responde me llama [trol](https://es.wikipedia.org/wiki/Trol_(Internet))
e ignorante.

> NO EXISTE MODIFICACIÓN DEL CÓDIGO FUENTE. El código fuente que tenemos
> en el servidor es el mismo que tenemos en el repositorio, pero hay
> ficheros adicionales que no están cubiertos por la licencia ya que no
> son ficheros del repositorio.

Sigue en su empeño, contradiciéndose, y además ahora [usando
mayúsculas](https://culturainternet.wordpress.com/2007/08/29/escribir-todo-en-mayuscula/).
Así le contesto a esto último:

> Así define la licencia AGPLv3 una modificación:
> > To "modify" a work means to copy from or adapt all or part of the
> > work in a fashion requiring copyright permission, other than the
> > making of an exact copy. The resulting work is called a "modified
> > version" of the earlier work or a work "based on" the earlier work.

No he reproducido toda la conversación debido a su extensión, para
leerla se puede acceder a
[la página de la incidencia](https://github.com/gallir/Meneame/issues/119).

¿Cómo se puede remediar este problema? Cualquier persona que posea los
derechos de autoría del código fuente (quien haya escrito parte del
código fuente si no ha renunciado a los derechos) puede requerir
jurídicamente que se cumpla la licencia. Otro punto de defensa para las
usuarias es reclamar que no se engañe a las visitantes haciéndoles
pensar que el código es libre cuando no lo es, quizá haya también una
forma de denunciar esto legalmente.

En cualquier caso, decir que un programa es <i lang="en">software</i> libre cuando no lo
es es un acto de muy mala fe. Si el código fuente no tiene alguna otra
artimaña, es posible que alguien realice una versión derivada que sí
respete las libertades de las usuarias (aunque [la ausencia de
documentación](https://github.com/gallir/Meneame/issues/4) es un posible
obstáculo). Lo más sencillo sería que lo remediaran las propias
desarrolladoras.

Tras esta toma de contacto con Menéame no me apetece usarlo ni
recomendar su uso a nadie. Hay muchas páginas web similares que respetan
la libertad y la privacidad. Investigaré formas de acción para evitar
que engañen a alguien, agradezco cualquier consejo o ayuda. Espero que
este artículo también sirva para hacer presión, dar a conocer el
problema y para que más gente aprenda sobre los mal llamados «derechos
de autor» y el
[izquierdo de copia](https://es.wikipedia.org/wiki/Copyleft).
