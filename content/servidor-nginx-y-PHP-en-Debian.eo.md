Author: Jorge Maldonado Ventura
Category: GNU/Linukso
Date: 2022-04-15 20:00
Lang: eo
Modified: 2022-05-14 16:48
Save_as: instali-Nginx-servilon-kun-PHP-en-Debiano-11/index.html
URL: instali-Nginx-servilon-kun-PHP-en-Debiano-11/
Slug: instalar-servidor-nginx-con-php-en-debian
Tags: Debiano, Debiano 11, Nginx, PHP
Title: Instali Nginx-servilon kun PHP en Debiano 11

En ĉi tiu artikolo mi montras kiel instali Nginx-servilon, kiu povas
plenumi PHP-programojn en Debiano 11.

Unue oni devas instali la sekvajn pakojn:

    :::bash
    sudo apt install nginx php php-fpm

Poste oni devas malkomenti la sekvajn liniojn de la defaŭlta agorda
dosiero de Nginx (`/etc/nginx/sites-available/default`):

    :::text
	#location ~ \.php$ {
	#	include snippets/fastcgi-php.conf;
	#
	#	# With php-fpm (or other unix sockets):
	#	fastcgi_pass unix:/run/php/php7.4-fpm.sock;
	#	# With php-cgi (or other tcp sockets):
	#	fastcgi_pass 127.0.0.1:9000;
	#}

Tiel estas la rezulto[^1]:

    :::text
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;

		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
		# With php-cgi (or other tcp sockets):
		#fastcgi_pass 127.0.0.1:9000;
	}

Sekve oni devas kontroli se la sintakson de la agorda dosiero estas
ĝusta per `sudo nginx -t`. Se ĝi ne montras eraron, lanĉu la servon
de <!-- more -->PHP-FPM[^1] kaj reŝargu la agordojn de Nginx:

    :::text
    sudo systemctl enable php7.4-fpm
    sudo systemctl start php7.4-fpm
    sudo systemctl reload nginx


[^1]: Se la versio de PHP de la servo de PHP-FPM, kiun vi instalis
  malsamas, agordu ĝin.

Fine ŝanĝu la permesojn, por ke nia uzanto povu aliri al la dosierujo de
la loka servilo:

    :::text
    sudo chown -R $USER:www-data /var/www/html

Vi povas kontroli, se PHP plenumas en via retumilo kreante testan
programon nomitan `testo.php` en `/var/www/html` ekzemple:

    :::php
    <?php
    echo 'PHP plenumas en ĉi tiu servilo.';

Se vi malfermos la adreson <http://localhost/testo.php> en la retumilo,
vi devos vidi paĝon kun la antaŭa mesaĝo.
