Author: Jorge Maldonado Ventura
Category: Noticias
Date: 2019-08-29
Lang: es
Slug: Gitlab-integra-Invisible-Captcha
Tags: CAPTCHA, GitLab, reCAPTCHA, spam
Title: Gitlab integra Invisible Captcha como alternativa a los CAPTCHAs de Google

Ahora GitLab permite usar un
<abbr title="Completely Automated Public Turing test to tell Computers and Humans Apart">CAPTCHA</abbr>
libre invisible llamado
[Invisible Captcha](https://github.com/markets/invisible_captcha)
en lugar de reCAPTCHA.
[Esta funcionalidad ha sido integrada recientemente](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/31625).

Esto supone una mejora respecto a [la situación
anterior](/gitlab-recaptcha-privativo/), ya que muchas
instancias de GitLab podrán empezar a usar Invisible Captcha y
deshabilitar reCAPTCHA. Al ser un CAPTCHA invisible, Invisible Captcha
proporciona además una mejor experiencia de uso.
