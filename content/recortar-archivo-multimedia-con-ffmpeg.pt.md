Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-10-27 13:45
Lang: pt
Slug: recortar-archivo-multimedia-con-FFmpeg
Save_as: aparar-ficheiros-multimédia-com-o-FFmpeg/index.html
URL: aparar-ficheiros-multimédia-com-o-FFmpeg/
Tags: som, ffmpeg, vídeo
Title: Aparar ficheiros multimédia com o FFmpeg
Image: <img src="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg" alt="" width="1200" height="826" srcset="/wp-content/uploads/2022/06/pinchadiscos-ffmpeg.jpeg 1200w, /wp-content/uploads/2022/06/pinchadiscos-ffmpeg.-600x413.jpg 600w" sizes="(max-width: 1200px) 100vw, 1200px">

Se só queres editar um ficheiro multimédia para aparar o seu início, fim
aŭ ambos, o mais rápido é usar o FFmpeg. O FFmpeg pode ser instalado em
distribuições baseadas em Debian com `sudo apt install ffmpeg`.

Se só queres eliminar os 10 primeiros segundos dum ficheiro multimédia,
basta com executar o FFmpeg assim:

    :::text
    ffmpeg -i canção.mp3 -ss 10 canção2.mp3

Depois de `-i` especifica-se o ficheiro que queremos editar
(`canção.mp3`); `-ss` seguido de  `10` indica os segundos que queremos
tirar; finalmente especifica-se o nomo do novo ficheiro,
`canção2.mp3`.

Se queremos eliminar tanto o início quanto o fim, podemos addicionar
o argumento `-to`:

    :::text
    ffmpeg -i canção.mp3 -ss 15 -to 04:10 canção2.mp3

Depois de `-to` deve haver uma posição, neste caso o minuto 4 e o
segundo 10 (`04:10`). Também é possível usar `-t`, que para obter o
mesmo resultado seria usado assim:

    :::text
    ffmpeg -i canção.mp3 -ss 15 -t 235 canção2.mp3

`-t` indica que se gravará hasta passados 235 segundos no novo ficheiro. Neste
caso, esses 235 novos segundos serão gravados despois de pular os 15
primeiros.
