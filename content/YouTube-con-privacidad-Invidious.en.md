Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2020-10-08 16:20
Modified: 2022-10-29 21:00
Lang: en
Slug: youtube-con-privacidad-con-invidious
Tags: add-on, Firefox, Google, Invidious, privacy, YouTube
Title: YouTube with privacy: with Invidious
Image: <img src="/wp-content/uploads/2020/10/home-page-Invidious.png" alt="" width="1108" height="809" srcset="/wp-content/uploads/2020/10/home-page-Invidious.png 1108w, /wp-content/uploads/2020/10/home-page-Invidious-554x404.png 554w" sizes="(max-width: 1108px) 100vw, 1108px">

As it is already well known, Youtube isn't free software and it doesn't
respects your privacy, but unfortunately some videos are only found
there. In this article I present you Invidious, a simple way to watch
YouTube videos without executing proprietary software from Google.

Invidious is a free and lightweight interface for YouTube that is made
with software freedom in mind. These are some of its features:

- No ads
- It's free software, [source code](https://github.com/iv-org/invidious) under the [AGPLv3](https://www.gnu.org/licenses/agpl-3.0.html) license
- It has a search engine
- Doesn't need a Google account to save subscriptions
- Supports captions
- Very customizable
- Allows embedding videos from Invidious in your page, like the following...

<!-- more -->

<iframe id='ivplayer' width='640' height='360' src='https://inv.zzls.xyz/embed/Ag1AKIl_2GM' style='border:none;'></iframe>

As it is free software, there is no unique version. That means that you
can install Invidious in your server or choose between the available
[public
instances](https://api.invidious.io/).

I invite you to [test how it works](https://invidious.snopyta.org/).
Down below I show you images with some of the things you can do with
invidios.

<figure>
<a href="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png">
<img src="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png" alt="" width="1000" height="774" srcset="/wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious.png 1000w, /wp-content/uploads/2020/01/iniciando-sesión-en-una-instancia-de-invidious-500x387.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
</a>
    <figcaption class="wp-caption-text">You can login without a Google
    account.</figcaption>
</figure>

<figure>
    <a href="/wp-content/uploads/2020/10/Invidious-subscriptions.png">
    <img src="/wp-content/uploads/2020/10/Invidious-subscriptions.png" alt="" width="1366" height="711" srcset="/wp-content/uploads/2020/10/Invidious-subscriptions.png 1366w, /wp-content/uploads/2020/10/Invidious-subscriptions-683x355.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
    </a>
    <figcaption class="wp-caption-text">You can view the latest videos
    from your subscriptions</figcaption>
</figure>

<figure>
    <a href="/wp-content/uploads/2020/10/playing-Invidious-video.png">
    <img src="/wp-content/uploads/2020/10/playing-Invidious-video.png" alt="" width="1920" height="1038" srcset="/wp-content/uploads/2020/10/playing-Invidious-video.png 1920w, /wp-content/uploads/2020/10/playing-Invidious-video-960x519.png 960w, /wp-content/uploads/2020/10/playing-Invidious-video-480x259.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">
    </a>
    <figcaption class="wp-caption-text">It has a dark theme</figcaption>
</figure>

There are Firefox add-ons that convert YouTube links into Invidious links.
[Privacy Redirect](https://addons.mozilla.org/en-US/firefox/addon/privacy-redirect/) also works for
[Nitter](/en/Check-Twitter-with-free-software-and-privacy-with-Nitter/)
and [Bibliogram](/en/check-Instagram-with-privacy-and-free-software/).
