Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2022-05-14 16:30
Lang: de
Save_as: Nginx-Server-mit-PHP-in-Debian-11-Installieren/index.html
URL: Nginx-Server-mit-PHP-in-Debian-11-Installieren/
Slug: instalar-servidor-nginx-con-php-en-debian
Tags: Debian, Debian 11, Nginx, PHP
Title: Nginx-Server mit PHP in Debian 11 Installieren

In diesem Artikel zeige ich, wie kann man einen Nginx-Server
installieren, der PHP-Programme in Debian 11 ausführen kann.

Zuerst müssen die folgenden Pakete installiert werden:

    :::bash
    sudo apt install nginx php php-fpm

Dann müssen die folgende Zeilen der voreingestellten Nginx-Konfigurationsdatei
(`/etc/nginx/sites-available/default`) entkommentiert werden:

    :::text
	#location ~ \.php$ {
	#	include snippets/fastcgi-php.conf;
	#
	#	# With php-fpm (or other unix sockets):
	#	fastcgi_pass unix:/run/php/php7.4-fpm.sock;
	#	# With php-cgi (or other tcp sockets):
	#	fastcgi_pass 127.0.0.1:9000;
	#}

So sollte die aussehen[^1]:

    :::text
	location ~ \.php$ {
		include snippets/fastcgi-php.conf;

		# With php-fpm (or other unix sockets):
		fastcgi_pass unix:/run/php/php7.4-fpm.sock;
		# With php-cgi (or other tcp sockets):
		#fastcgi_pass 127.0.0.1:9000;
	}

Dann muss man überprüfen, ob die Syntax in der Konfigurationsdatei
korrekt ist. Wenn kein Fehler auftritt, starte den <!-- more -->
PHP-FPM-Dienst[^1] und lade die Nginx-Konfiguration neu:

    :::text
    sudo systemctl enable php7.4-fpm
    sudo systemctl start php7.4-fpm
    sudo systemctl reload nginx


[^1]: Falls die PHP-Version des PHP-FPM-Servers, den du installierst
  hast, nicht die gleiche ist, stell die ein.

Schließlich müssen wir die Berechtigungen ändern, so dass unser Nutzer
auf den Ordner der lokalen Server zugreifen kann:

    :::text
    sudo chown -R $USER:www-data /var/www/html

Wir können testen, ob PHP auf unserem Server läuft, indem wir z. B. ein
Testprogramm namens `test.php` in `/var/www/html` erstellen:

    :::php
    <?php
    echo 'PHP läuft in diesem Server.';

Wenn wir die Adresse <http://localhost/test.php> in unserem Browser
öffnen, sollte eine Seite mit der oben geschriebenen Nachricht angezeigt
werden.
