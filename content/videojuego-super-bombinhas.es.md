Author: Jorge Maldonado Ventura
Category: Videojuegos
Date: 2021-11-06
Modified: 2021-11-07
Lang: es
Slug: videludo-Super-Bombinhas
Save_as: videojuego-Super-Bombinhas/index.html
URL: videojuego-Super-Bombinhas/
Tags: cultura libre, GNU/Linux, plataformas, videojuego, Windows
Title: Videojuego <cite lang="pt">Super Bombinhas</cite>
Image: <img src="/wp-content/uploads/2021/11/Super-Bombinhas.png" alt="">

Una bombas vivientes son las protagonistas de este videojuego de
plataformas. Bomba Azul es la única bomba que no fue capturada por el
malvado Gaxlon. Debes salvar a las otras bombas y al rey Aldan. Cada
bomba tiene una habilidad especial: Bomba Amarela puede correr
rápidamente y saltar más que las otras, Bomba Verde puede explotar, el
rey Aldan puede detener el tiempo, etc. Después de salvar a una bomba
puedes cambiar a ella durante la aventura.

En el juego hay 7 regiones muy diferentes. Cada una de ellas tiene un
enemigo final. Debes usar de forma inteligente todas las bombas para
avanzar.

El juego también tiene un editor de niveles para crear nuevos niveles.

![Editor de niveles](/wp-content/uploads/2021/11/Super-Bombinhas-level-editor.gif)

Como una imagen vale más que mil palabras, os muestro a continuación un
vídeo corto:

<!-- more -->

<video controls preload="none" poster="/wp-content/uploads/2021/11/Super-Bombinhas.png" data-setup="{}">
  <source src="/video/super-bombinhas-es.webm" type="video/webm">
  <p>Lo siento, tu navegador no soporta HTML 5. Por
  favor, cambia o actualiza tu navegador</p>
</video>

Puedes instalar el juego en GNU/Linux y en Windows. Para instalarlo en
distribuciones basadas en Debian, debes descargar el archivo `.deb` más
nuevo de la [página de
publicaciones](https://github.com/victords/super-bombinhas/releases) y
ejecutarlo.
