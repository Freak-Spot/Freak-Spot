Author: Jorge Maldonado Ventura
Category: Edición de textos
Date: 2016-08-31 23:36
Lang: es
Modified: 2017-02-25 20:34
Slug: no-escribais-espacios-a-final-de-linea
Status: published
Tags: consejos, edición de texto sin formato, editores de texto, espacios a final de línea, Python, programación, trailing whitespaces, Vim
Title: No escribáis espacios a final de línea

Este artículo va dirigido a programadores y a personas que editan texto
sin formato. Los espacios a final de línea son algo molesto e inútil
cuando la gente los pone sin pensar. Muchas veces porque no utilizan un
buen editor que les señale dónde hay espacios a final de línea.

<figure style="width: 599px" class="wp-caption aligncenter"><a href="/wp-content/uploads/2016/08/Captura-de-pantalla-de-2016-09-01-013156.png"><img class="wp-image-303 size-full" src="/wp-content/uploads/2016/08/Captura-de-pantalla-de-2016-09-01-013156.png" alt="Textos a final de línea resaltados en Vim" width="599" height="405" srcset="/wp-content/uploads/2016/08/Captura-de-pantalla-de-2016-09-01-013156.png 599w, /wp-content/uploads/2016/08/Captura-de-pantalla-de-2016-09-01-013156-300x203.png 300w" sizes="(max-width: 599px) 100vw, 599px" /></a><figcaption class="wp-caption-text">Así veo yo los espacios a final de línea</figcaption></figure>

A continuación, expongo algunas de las razones por las que son un
problema:

- Hacen que el tamaño de los archivos sea mayor.
- Hacen difícil la navegación por el código. Por ejemplo, cuando
  pulsas la tecla Fin, lo que esperas es llegar a la última letra de
  la línea. Si el código o el texto que estás editando tiene
  caracteres al final de línea, puedes acabar varios espacios detrás
  del texto que quieres editar.
- Pueden ocasionar errores muy difíciles de detectar. Por ejemplo, en
  Python,

        :::python
        print('Hola\
            Mundo')

  produce un error.

      File "", line 1
          print('Hola\
                    ^
      SyntaxError: EOL while scanning string literal

- Si introduces espacios a final de línea, estás cambiando el
  contenido del fichero innecesariamente. En la mayoría de sistemas de
  control de versiones esto es algo muy difícil de ver y puede
  generar problemas.

La mayoría de editores de texto permiten solucionar este problema. Si
utilizas Vim, puedes eliminar todos los espacios a final de línea de un
fichero con la siguiente orden: `:%s/\s\+$//e`.

Probablemente haya alguna razón más para no usar espacios de línea que
tú conozcas y yo no conozca. Dímela en los comentarios para que la añada
a la lista de razones de este artículo y cuéntame los problemas que te
han ocasionado los espacios al final de línea.
