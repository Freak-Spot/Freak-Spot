Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2019-04-20 04:12
Image: <img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="">
Lang: es
Slug: combinar-grep-y-less-con-color
Tags: Bash, color, grep, intérprete, interfaz de línea de órdenes, less, terminal
Title: Combinar <code>grep</code> y <code>less</code> con color

Suelo usar [Grep](https://www.gnu.org/software/grep/) para buscar un
texto exacto en archivos. Usando `grep -R texto` dentro de un
directorio, puedo localizar todos sus archivos en los que se encuentra
dicho texto.

<a href="/wp-content/uploads/2019/04/grep-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-con-color.png" alt="Cuando simplemente ejecuto algo como «grep -R 'Freak Spot'», los colores se ven correctamente">
</a>

Cuando hay muchas coincidencias es más cómodo usar `less` para moverse
por los resultados. El problema es que al ejecutar
`grep -R texto | less` los colores ya no se ven.

<!-- more -->

<a href="/wp-content/uploads/2019/04/grep-y-less-sin-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-sin-color.png" alt="Al ejecutar «grep -R texto | less» no se ve en color, sino que se ven en letra y en el mismo color los códigos ANSI de color (como «[35m»)">
</a>

Grep no muestra colores por defecto, sino que está configurado en la
mayoría de sistemas GNU/Linux con un alias en el archivo `.bashrc`, o en
el equivalente en otros intérpretes de instrucciones, para detectar
cuándo mostrar colores y cuándo no: `alias grep='grep --color=auto'`.
Cuando pasa el resultado de su ejecución a otro programa mediante una
tubería, Grep deja de mostrar colores, porque los códigos usados para
mostrar colores en las terminales añaden caracteres no existentes que
pueden producir resultados inesperados. Para mostrar colores también en
este caso debemos añadir `--color=always`. También debemos pasarle la
opción `-r` a `less` para que este muestre los caracteres de color.
Queda así la instrucción: `grep --color=always -R texto | less -r`.

<a href="/wp-content/uploads/2019/04/grep-y-less-con-color.png">
<img src="/wp-content/uploads/2019/04/grep-y-less-con-color.png" alt="Ejecutando «grep --color=always -R texto | less -r» se ven bien los colores">
</a>

Puesto que es tedioso escribir tanto, es buena idea añadir esta
función al archivo `.bashrc` (recuerda que es necesario
[recargar la configuración de Bash](/recargar-la-configuracion-de-bash-bashrc/)
para poder usar la función):

    :::bash
    lgrep ()
    {
      grep --color=always -R "$1" | less -r
    }

De esta forma solo es necesario escribir `lgrep texto` para que se
muestren los resultados con color en `less`.
