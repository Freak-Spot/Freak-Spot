Author: Jorge Maldonado Ventura
Category: Bash
Date: 2020-10-21
Lang: eo
Slug: apagar-ordenador-automáticamente-tras-finalizar-proceso
Save_as: malŝalti-komputilon-post-fino-de-procezo/index.html
URL: malŝalti-komputilon-post-fino-de-procezo/
Tags: malŝalti, do, done, GNU/Linukso, pgrep, PID, procezo, lista operacio, ps, shutdown, sleep, test, lertaĵo, wget, while
Title: Malŝalti komputilon post fino de procezo

Okaze ni volas malŝalti la komputilon, sed ni estas plenumanta taskon
(ekzemple elŝuton, ĝisdatigon, ktp.). Ni povas atendi ĝian finon por
malŝalti la komputilon mane aŭ ni povas diri al la komputilo, ke ĝi
malŝaltu sin, kiam la procezo finu. En ĉi tiu artikolo mi montras kiel
fari la aŭtomatan solvon.

<!-- more -->

Ĉi tiu artikolo povas esti danĝera, se vi kopias la komandon mane kaj
eraras, ĉar vi povas malŝalti la komputilon. Mi rekomendas legi kaj
kompreni la instruojn ĉi tie montritajn antaŭ kopii ilin laŭlitere.

Ni supozu, ke ni komencis elŝuton per la programo
[wget](https://www.gnu.org/software/wget) kaj ni scias, ke la elŝuto
finos proksimume post unu horo. Ni povus do programi la malŝaltadon de
la komputilo post unu horo per la komando `shutdown +60`, kvankam ĉiam
estas rekomendinda lasi iom pli da tempo.

Ĉi tiu solvo havas gravan problemon: se iel la tasko prokrastas, povas
esti, ke la elŝuto ne finu. Por solvi ĉi tiun problemon, ni malŝaltos la
komputilon, kiam la tasko finu.

Al ĉiu tasko apartenas proceza identigilo (mallongigite <abbr title="Process ID">PID</abbr>).
Por atingi la procezan nombron de programo, ni povas uzi la
pgrep-komandon, donante al ĝi kiel argumento la nomon de la programo.

<pre><samp>$ pgrep wget
3204</samp></pre>

La programo liveras la elirstaton 0 (sukceso) kaj montras la rezulton de
la plenumado (<samp>3204</samp>). Se estas neniu plenumanta procezo, kiu
enhavas la ĉenon `wget`, pgrep liveras la elirstaton 1.

Poste ni skribos `while`-iteracion por kontroli, se pgrep liveras 0 kiel
elirstato ĉiun kvinan sekundon. Se ĝi ne faras tion, ni malŝaltos la
komputilon. En la malsupra kodo ni uzas la listan operacion `||`. La
komando antaŭ `||` ĉiam plenumos; la malantaŭa nur plenumos, se la
antaŭa fiaskas. Ĉar pgrep montras la komandan eligon ekrane kaj mi ne
volas vidi ĝin ĉiun kvinan sekundon, mi alidirektas ĝin al `/dev/null`
per `>` (tio forigas ĝin).

    :::bash
    while true; do
        pgrep wget > /dev/null || shutdown now
        sleep 5
    done

Se oni plenumas ĉi tiujn komandojn kelkfoje, estas rekomendinde
programi skripton, kiu povas ricevi argumenton por specifi la programon,
kiun ni volas kontroli.

La antaŭa metodo havas problemon en kazoj, kiam pgrep liveras pli ol unu
rezulto, sed oni povas solvi ĝin kontrolante nur la procezan
identigilon, pri kiu ni estas interesitaj kaj kontrolante, ke ĝi ankoraŭ
plenumas. Ni supozu, ke ni nur volas halti la procezon kun la identigilo
1091 en la jenaj ekzemploj.

Ni povas kontroli, se procezo ankoraŭ plenumas analizante la eligon de
la ps-programo per la opcio `-q` kaj la proceza nombro kiel ĝia
argumento.

    :::bash
    while true; do
        ps -q 1091 > /dev/null || shutdown now
        sleep 5
    done

En GNU/Linukso, la procezoj kreas dosierujon, kies nomo estas la
procezidentigilo, en la dosierujo `/proc/`. Tio estas al la procezo kun
la identigilo 1091 atribuas la dosiero `/proc/1019`. Kiam la procezo
finas, tiu dosiero malaperos.

    :::bash
    while true; do
        test -d /proc/1091 || shutdown now
        sleep 5
    done

Per ambaŭ antaŭaj metodoj estas simple programi la malŝaltadon de la
komputilo post fino de tasko. Kvankam ne estas tre probabla, konsideru,
ke iu nova procezo povus preni la identigilon de la procezo de la
kontrolita programo &mdash; ekzemple tio eblas per la integrita en Bash
`exec`-komando.
