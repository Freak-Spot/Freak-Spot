Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2017-12-14 01:59
Image: <img src="/wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser.png" alt="" width="1000" height="354" srcset="/wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser.png 1000w, /wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser-500x177.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
Lang: es
Slug: recordar-urls-onion
Tags: Abrowser, Deep web, Firefox, Iceweasel, marcadores, .onion, privacidad, Tor, Tor Browser
Title: Recordar URLs <code>.onion</code>


Recordar servicios ocultos es complicado, ya que tienen muchos
caracteres aleatorios y son largos, como
<http://duskgytldkxiuqc6.onion/>. Pronto será aún más complicado, ya que
[las URLs se hacen más
largas](https://blog.torproject.org/tors-fall-harvest-next-generation-onion-services)
para evitar problemas de privacidad. ¿Cómo podemos recordar estas URLs
tan extrañas?

<!-- more -->

Tor Browser permite utilizar marcadores para guardar las páginas que
queremos recordar. Se puede añadir una página a la lista de marcadores
usando la interfaz gráfica o el atajo de teclado
<kbd>Ctrl</kbd>+<kbd>D</kbd>. Una vez guardado un marcador, puedes
empezar a escribir en la barra de URL el título de la página (el que
aparece normalmente en la pestaña de la página, corresponde a la
etiqueta <abbr title="HyperText Markup Language">HTML</abbr> `<title>`),
y deberán aparecer las URLs de marcadores cuyos títulos coincidan con lo
escrito. Basta con seleccionar la página a la que queremos ir a
continuación.

<figure>
    <a href="/wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser.png">
    <img src="/wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser.png" alt="" width="1000" height="354" srcset="/wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser.png 1000w, /wp-content/uploads/2017/12/marcadores-en-barra-URLs-Tor-Browser-500x177.png 500w" sizes="(max-width: 1000px) 100vw, 1000px">
    </a>
    <figcaption class="wp-caption-text">Marcadores en barra de URLs en
    Tor Browser</figcaption>
</figure>

También se puede añadir una barra de marcadores como barra lateral o
superior. Aparecerán entonces iconos y títulos de páginas para que
podamos acceder a ellas fácilmente pulsándolas.

Si queremos cambiar el nombre de un marcador, importar la lista de
marcadores, exportarla o realizar otras opciones avanzadas, debemos
entrar en la **Barra de herramientas de marcadores** desde el menú de
marcadores.

![Mostrando cómo acceder en Tor a la barra de herramientas de marcadores](/wp-content/uploads/2017/12/Tor-barra-herramientas-marcadores.gif)

También podemos anotar las URLs de otras maneras: un listado de páginas
en formato HTML, una hoja en una libreta, un gestor de contraseñas, etc.
