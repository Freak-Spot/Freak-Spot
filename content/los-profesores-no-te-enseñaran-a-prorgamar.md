Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2019-07-23 08:38
Lang: es
Slug: los-profesores-no-te-ensenyaran-a-programar
Tags: enseñanza, programación, software libre
Title: Los profesores no te enseñarán a programar

Me sorprende que haya gente que ha experimentado de primera mano cómo
funciona el sistema educativo y sigan pensando que podrán aprender a
programar asistiendo a clases.

Dudo mucho que puedan aprender algo de mucha utilidad de docentes que se
esconden tras un escritorio y carecen de experiencia real en la
programación. Es absurdo pretender aprender algo útil sobre bases de
datos de alguien que jamás a diseñado o trabajado con una base de datos
compleja en el mundo real y solo se dedica a repetir lo que han escrito
otros en libros de texto.

Durante mi experiencia en el mundo laboral he podido comprobar de
primera mano la inutilidad de años y años de universidad. Yo, que no he
estudiado programación en la universidad, supero con creces a quienes
han pasado años asistiendo a clases, ¿cómo es posible?

<!-- more -->

Yo he aprendido a programar de gente con sobrada experiencia. Esos
mentores se encuentran en los proyectos exitosos de <i lang="en">software</i> libre. Eso
no quiere decir que no se pueda aprender nada de libros ni de profesores
y que sean completamente inútiles, sino solo que no te volverán
sobresaliente. Para ser bueno haciendo páginas web, es necesario
aprender de quienes han hecho muchísimas páginas web y seguir su ejemplo
haciendo muchas páginas web.

En los proyectos de <i lang="en">software</i> libre se obtiene algo de incontable valor:
la revisión y las sugerencias de desarrolladores con mucha experiencia.
Por ello yo nunca recomendaré a quienes quieran convertirse en
programadoras competentes ir a la universidad. En su lugar, les diré que
busquen un proyecto de <i lang="en">software</i> libre exitoso en el área en la que
quieran especializarse y se involucren, sin desaprovechar los
valiosísimos consejos y correcciones de quienes tienen más experiencia.

La clave está en buscar a alguien con experiencia de verdad. En una
empresa se pueden encontrar también personas de quienes aprender, pero
una excesiva competitividad y presión dentro las empresas hace
que a veces sea difícil recibir buenas recomendaciones o detenerse para
observar y aprender.
