Author: Jorge Maldonado Ventura
Category: Privatsphäre
Date: 2022-11-18 16:25
Lang: de
Slug: Cómo-destruir-Google
Save_as: wie-zerstört-man-Google/index.html
URL: wie-zerstört-man-Google/
Tags: direkte Aktion, Anzeigen, Boykott, Google, Privatsphäre
Title: Wie zerstört man Google
Image: <img src="/wp-content/uploads/2022/11/aniquilar-a-Google.png" alt="" width="960" height="540" srcset="/wp-content/uploads/2022/11/aniquilar-a-Google.png 960w, /wp-content/uploads/2022/11/aniquilar-a-Google-480x270.png 480w" sizes="(max-width: 960px) 100vw, 960px">

Das Geschäftsmodell von Google basiert auf der Sammlung persönlicher
Daten von Nutzern, dem Verkauf dieser Daten an Dritte und der Schaltung
von Anzeigen. Das Unternehmen [beteiligt sich auch an Spionageprogrammen](https://de.wikipedia.org/wiki/PRISM),
[entwickelt <abbr title="künstliche Inteligenz">KI</abbr>-Programme für
militärische Zwecke](/de/du-hilfst-zu-töten-indem-du-ein-Google-reCAPTCHA-ausfüllst/) und [beutet seine Nutzer aus](/de/wie-beutet-Google-mit-CAPTCHAs-aus/)...

Es ist eines der mächtigsten Unternehmen der Welt. Doch Google ist ein
Riese auf tönernen Füßen, der vernichtet werden kann.

## Schluss mit seinem Werbeeinnahmen

Google verdient Geld, indem es auf der Grundlage der von seinen Nutzern
gesammelten Informationen personalisierte Anzeigen schaltet. Wenn die
Nutzer keine Anzeigen sehen, verdient Google kein Geld. Das Blockieren
von Anzeigen ist eine Möglichkeit, das Tracking zu verhindern und Google
Geld zu entziehen, aber wenn du Seiten von Google besuchst, erhält
Google immer noch Informationen, die es an Werbekunden verkaufen kann.
Am einfachsten ist es also, [Anzeigen zu blockieren](/bloqueo-de-anuncios-elementos-molestos-y-rastreadores-ublock-origin/) und Google-Seiten zu
meiden.

Eine andere Möglichkeit ist, alle Anzeigen mit der
[AdNauseam](https://adnauseam.io/)-Erweiterung anzuklicken, die sie
ebenfalls vor uns verbirgt, so dass wir sie nicht als störend empfinden.
Diese Methode bedeutet, dass Google weniger Geld mit Anzeigenklicks
verdient und dass die Google-Server etwas mehr ausgelastet sind
(minimal, aber es erhöht die Kosten).

## Googles Server mit Mist füllen

Google lässt fast alles auf seine Server hochladen (Videos, Dateien
usw.). Wenn die auf die Server hochgeladenen Inhalte viel Platz
beanspruchen und Müll sind, der die Nutzer von den Diensten abschreckt
(Videos mit Roboterstimmen, die Unsinn reden, Hunderte von Videos mit
Rauschen, die Gigabyte um Gigabyte beanspruchen), steigen die Kosten für
die Wartung der Server und der Gewinn des Unternehmens sinkt.

Wenn es sich um eine weltweit koordinierte Aktion mehrerer Nutzer
handelt, müsste Google anfangen, das Hochladen von Videos
einzuschränken, Leute einzustellen, um Scheißvideos zu finden, Personen
und IP-Adressen zu sperren usw., was seine Verluste erhöhen und seine
Gewinne verringern würde.

Ich kann zum Beispiel jede Stunde 15-minütige Videos erstellen und sie
automatisch oder halbautomatisch auf YouTube hochladen. Die Videos
sollten viel Platz beanspruchen. Je mehr Auflösung, je mehr Farben, je
mehr Tonvielfalt, je mehr Bilder pro Sekunde, desto mehr Geld wird en
YouTube ausgeben, um diese Videos auf seinen Servern zu halten.

Das Video, das ich unten zeige, [wurde automatisch mit `ffmpeg` erstellt](/de/Rauschvideos-mit-FFmpeg-erstellen/).
Es ist nur zwei Sekunden lang, nimmt aber 136 MB in Anspruch. Ein
ähnliches 15-minütiges Video würde 61,2 GB beanspruchen.

<!-- more -->

<video controls>
  <source src="/video/basura.mp4" type="video/webm">
  <p>Entschuldigung, dein Browser unterstützt nicht HTML 5. Bitte wechsle
  oder aktualisiere deinen Browser.</p>
</video>

Es wäre notwendig, Variationen von Zufallsvideos, Schrottvideos von
Robotern, die in seltsamen Sprachen sprechen, zu erstellen...
Gleichzeitig sollten die Nutzer ermutigt werden, zu anderen Plattformen
wie [PeerTube](https://joinpeertube.org/de/), Odysee usw. zu wechseln, die datenschutzfreundlicher sind.

Auf dieselbe Weise könnten wir Google Drive mit Scheißdateien füllen. Wir
müssten wahrscheinlich mehrere Konten verwenden und versuchen, nicht zu
viel Aufmerksamkeit von Google auf uns zu ziehen. Es können
Minderheitensprachen, legitim aussehende Dateien usw. verwendet werden.

Damit der Angriff auf Google die größtmögliche Wirkung hat, sollten
mehrere verschiedene Konten mit unterschiedlichen IPs verwendet werden,
und alles sollte zwischen mehreren Personen koordiniert werden. Es gibt
kein Gesetz, das das Hochladen von zufällig generierten Videos und
Dateien verbietet, und selbst wenn es eines gäbe, könnte man es anonym
tun, um nicht identifiziert zu werden.

## Nutz ihre Server über einen Proxy, ohne etwas zu bezahlen

Du kannst Programme wie [Piped](/de/YouTube-mit-Privatsphäre-mit-Piped/), [Invidious](/de/YouTube-mit-Privatsphäre-mit-Invidious/) oder [NewPipe](https://newpipe.net/) verwenden.
YouTube gibt Geld aus, indem es Videodateien über das Internet
versendet. Auf diese Weise sammelt es keine persönlichen Daten von uns,
während es Geld verliert, und wir können die Videos genießen. 😆

## Boycot ihre Produkte

Kauf nichts, was Google Geld bringt. Wenn du bereits ein Google-Produkt
hast, kannst du Maßnahmen ergreifen, um so wenig Daten wie möglich an
Google zu senden. Du kannst zum Beispiel [den Captive Portal Check unter
Android deaktivieren](https://www.kuketz-blog.de/geloest-deaktivierung-captive-portal-check-unter-android-oreo/).

Wenn du ein Telefon brauchst, kannst du ein dummes Telefon, ein
GNU/Linux-Telefon oder eines mit einer freien Android-Distribution
kaufen.

## Propaganda gegen Google

Dieser Artikel ist ein Beispiel dafür. ✊
