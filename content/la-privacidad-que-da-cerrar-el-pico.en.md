Author: Jorge Maldonado Ventura
Category: Privacy
Date: 2024-01-28 23:59
Lang: en
Slug: la-privacidad-que-da-cerrar-el-pico
Save_as: be-quiet-to-protect-your-privacy/index.html
URL: be-quiet-to-protect-your-privacy/
Tags: advice
Title: Be quiet to protect your privacy
Image: <img src="/wp-content/uploads/2024/01/tucán.jpg" alt="" width="1920" height="1285" srcset="/wp-content/uploads/2024/01/tucán.jpg 1920w, /wp-content/uploads/2024/01/tucán-960x642.jpg 960w, /wp-content/uploads/2024/01/tucán-480x321.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

There are things we shouldn't disclose: that we cracked a website,
bought cryptocurrencies and gold, robbed something, etc. To avoid being
identified we must use technologies that allow anonymity (such as
[Tor](https://en.wikipedia.org/wiki/Tor_(network))
and [virtual private networks](https://en.wikipedia.org/wiki/Virtual_private_network)), [pay in cash](/en/why-you-should-never-pay-by-card/), and not show IDs,
that's clear. However, we often forget something very basic:
to be quiet.

Creating an anonymous digital identity, for example, is very simple: you
register with a username on a website using a VPN or Tor and upload
content. Losing that anonymity, on the other hand, is also very easy:
just reveal some data related to your real identity. These digital
identities can have data, but they must be false. The way we write can
also reveal our real identity, so it's recommended to make an effort to
change your writing style.

<img src="/wp-content/uploads/2024/01/silencio.jpg" alt="">

The people you interact with shouldn't know anything you want to keep
hidden, even if they are people you trust. You never know if those people
you trust today will still be trustworthy tomorrow. The con man who sold the
Eiffel Tower twice, [Victor Lustig](https://en.wikipedia.org/wiki/Victor_Lustig), was arrested because his mistress
became jealous of a relationship he was having with another woman and
decided to rat him out.

Of course, many times we want to brag about our feats. We want to brag
about our hacks, we want to show off.... Better not to do it. It's
better to be humble and make up some excuse. “That's money my father
gave me”; “I have cryptocurrencies, but not much”... In short,
the best thing to do is to keep your mouth shut if you don't want to get
caught and you don't want to be in the spotlight. Don't be the fool who
brags about buying 100 grams of gold and the next day finds his house
ransacked.
