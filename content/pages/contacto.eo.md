Author: Jorge Maldonado Ventura
Date: 2017-10-28 18:01
Modified: 2023-04-23 15:00
Lang: eo
Slug: contacto
Status: published
Title: Kontakto

Mia retadreso estas
[jorgesumle@freakspot.net](mailto:jorgesumle@freakspot.net). Uzu mian
publikan <abbr title="GNU Privacy Guard">GPG</abbr>-ŝlosilon
([4BF5 360D 50C8 4085 5648 644E 1200 84A8 5F2F 6B50](/Jorge_jorgesumle@freakspot.net-0x120084A85F2F6B50-pub.asc)),
por ke aliaj personoj ne povu legi la mesaĝon.
