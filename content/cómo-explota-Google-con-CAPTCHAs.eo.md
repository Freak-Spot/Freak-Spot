Author: Jorge Maldonado Ventura
Category: Opinio
Date: 2017-05-05 14:20
Image: <img src="/wp-content/uploads/2017/05/reCAPTCHA-libros.png" alt="reCAPTCHA used to digitalize texts">
Lang: eo
Slug: como-explota-Google-con-CAPTCHAs
Tags: CAPTCHA, ekspluatado, Google, reCAPTCHA, trudaĵo
Title: Kiel Google ekspluatas per CAPTCHA-j
Save_as: kiel-Google-ekspluatas-per-CAPTCHA-j/index.html
URL: kiel-Google-ekspluatas-per-CAPTCHA-j/

<abbr title="Completely Automated Public Turing test to tell Computers and
Humans Apart">CAPTCHA</abbr> (komplete aŭtomata publika testo de Turing
por distingi komputilojn kaj homojn) estas testo uzata por distingi
inter komputiloj kaj homoj. Ili estas ĉefe uzita por eviti trudvarbajn
mesaĝojn.

Programo, kiu faras tiun ĉi teston estas reCAPTCHA, kiu esits publikigita
la 27-an de majo de 2007 kaj akirita de Google<!-- more --> en setembro de 2009[^1].
Ĉi tiu programo estas uzita en retejoj tutmonde.

reCAPTCHA estis multege uzita por bitigi tekstojn danke al la senkosta laboro
de milionoj da uzantoj, kiuj povis identigi vortojn nekomprenatajn por la
komputiloj. Plejparto de la uzantoj ne scias, ke Google profitas de ilia
laboro; al aliuloj ne gravas.

![reCAPTCHA uzita por ciferecigi tekston](/wp-content/uploads/2017/05/reCAPTCHA-libros.png)

La artikolo [«Deciphering Old Texts, One Woozy, Curvy Word at a
Time»](https://www.nytimes.com/2011/03/29/science/29recaptcha.html)
publikita en la gezeto The New York Time informis, ke la Interretaj
uzantoj jam finis la literigadon per reCAPTCHA de la dosieraro de tiu
gazeto, eldonita ekde 1851. Laŭ la tiama aŭtoro de reCAPTCHA diris, la
uzantoj, aŭ pli bone dirite «uzatoj», tage malĉifris cirkaŭ 200
milionojn da <abbr title="Completely Automated Public Turing test to tell
Computers and Humans Apart">CAPTCHA</abbr>j, daŭrante po 10 sekundojn
por solvi iun. Tia laboro tage egalvaloras al 500 000 laborhoroj[^2].

CAPTCHA povas esti uzita ne nur por malfacile kompreneblaj tekstoj, sed
ankaŭ por bildoj. Exemple, Google ankaŭ uzas reCAPTCHA-jn por identigi
bildojn de vendejoj, trafiksignaloj, ktp., faritaj por Google Maps.
Ankaŭ la reCAPCHA-j de Google estas uzitaj por aliaj celoj, kiujn la
uzantoj ne konas.

![reCAPTCHA farita de Google Maps](/wp-content/uploads/2017/05/reCAPTCHA-Google-Maps.png)

Nur Google scias la profiton, kiun ĝi gajnas per ĉi tiu ekspluatada
sistemo. Tute ne eblas kontroli reCAPTCHA, ĉar ĝi estas proprieta
programaro, kaj tiuj, kiuj uzas ĝin, ne havas povon. Ĉio, kion ili povas
fari estas rifuzi solvi reCAPTCHA-jn kiel tiujn de Google por ne plu
esti uzatoj.

Google uzas nun metodon de identigo, kiu ŝparas tempon al ilia uzatoj
kaj evitas, ke ili devu deĉifri tekstojn kaj bildojn. La uzatoj nune nur
devas premi butonon. Ĉi tiu mekanismo trudas la plenumadon de
Ĝavoskripta kodo nekonata de la uzatoj. Google povas akiri multege da
informo el la uzatoj, kiuj uzu ĉi tiun mekanismon, kiun ĝi verŝajne poste
vendos kontraŭ grandega kvanto da mono.

![reCAPTCHA button](/wp-content/uploads/2017/05/Recaptcha_anchor@2x.gif)

Krome, reCAPTCHA diskriminacias handikapulojn kaj uzantojn de [Tor](https://eo.wikipedia.org/wiki/Tor_(programaro)):
unuflanke la defioj prezentitaj al handikapuloj estas pli longaj kaj pli
malfacile solveblaj; aliflanke tiu, kiu private retumas la reton devas
solvi pli malfacilajn defiojn kaj bezonas pli da tempo.

La
[artikolo](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html)
skribita de la kreinto de reCAPTCHA[^3], kiam ĝi estis aĉetita de Google
diras:

> Plibonigante la haveblecon kaj alireblecon de ĉiuj informoj en la
> Interreto vere gravas por ni, do ni deziras plibonigi ĉi tiun
> teknologion kune kun la skipo de reCAPTCHA.

Tamen la laboro farita uzante reCAPTCHA ne estas disponebla nek alirebla
en la plejparto de la okazoj. La datenoj estas prezentitaj per maniero,
kiu mone favoras Google kaj aliajn firmaojn. La uzatoj, kiuj helpis
ciferigi la dosieraron de The New York Times devas pagi vidante
reklamojn, kiam ili konsultas la dosieraron, kiun ili mem helpis
ciferigi sen akiri nenion pro la laboro.

[^1]:
    [reCAPTCHA](https://en.wikipedia.org/wiki/ReCAPTCHA). Wikipedia. Konsultita en 2017-5-5.
[^2]:
    [Deciphering Old Texts, One Woozy, Curvy Word at a Time](http://www.nytimes.com/2011/03/29/science/29recaptcha.html) (2011-3-28). The New York Times. Konsultita 2017-5-5.
[^3]:
    [Teaching computers to read: Google acquires reCAPTCHA](https://googleblog.blogspot.com/2009/09/teaching-computers-to-read-google.html) (2009-9-16). Official Google Blog. Konsultita 2017-5-5.
