Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2024-01-28 19:30
Lang: eo
Slug: la-privacidad-que-da-cerrar-el-pico
Tags: konsiloj
Save_as: por-privateco-fermu-la-buŝon/index.html
URL: por-privateco-fermu-la-buŝon/
Title: Por privateco, fermu la buŝon
Image: <img src="/wp-content/uploads/2024/01/tucán.jpg" alt="" width="1920" height="1285" srcset="/wp-content/uploads/2024/01/tucán.jpg 1920w, /wp-content/uploads/2024/01/tucán-960x642.jpg 960w, /wp-content/uploads/2024/01/tucán-480x321.jpg 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Estas aferoj, kiujn vi ne devas diskonigi: kodrompon de retejo, aĉeton de
ĉifrovalutoj aŭ de oro, ŝtelon, ktp. Por eviti, ke oni identigu vin, vi
devas uzi teknologiojn, kiuj ebligas la anonimecon
(kiel [Tor](https://eo.wikipedia.org/wiki/Tor_(programaro)) kaj [virtualaj privataj retoj](https://eo.wikipedia.org/wiki/VPN)),
[pagi per kontanta mono](/eo/neniam-pagu-per-karto/) kaj eviti montri
identigajn dokumentojn, tio kompreneblas. Tamen multfoje ni forgesas ion
tre esencan: fermi la buŝon.

Krei anoniman ciferecan identecon, ekzemple, tre facilas: vi
registriĝas per uzantnomo en retejo uzante VPN-on aŭ Tor kaj alŝutas
aferojn. Perdi tiun anonimecon, aliflanke, ankaŭ tre facilas: vi nur
devas riveli iun datenon rilatan al via vera identeco. Tiuj ciferecaj
identecoj povas havi datenojn, sed ili devas esti falsaj. La maniero,
per kiu vi skribas, ankaŭ povus malkaŝi vian veran identecon, do estas
rekomendinde peni ŝanĝi la skribstilon.

<img src="/wp-content/uploads/2024/01/silencio.jpg" alt="">

Homoj, kun kiuj vi interagas, devas scii nenion, kion vi volas sekrete
teni, eĉ se ili estas fidindaj. Ni neniam scias, se tiuj homoj, kiuj
hodiaŭ estas fidindaj, daŭre restos tiel morgaŭ. La trompisto, kiu
dufoje vendis la Eiffel-Turon, [Victor Lustig](https://eo.wikipedia.org/wiki/Victor_Lustig),
estis arestita, ĉar lia amatino estis ĵaluza pro rilato, kiun li havis kun
alia virino, kaj decidis denunci lin.

Ni ja multfoje volas fanfaroni pri niaj atingoj. Ni volas laŭdi nin mem
pro niaj kodrompoj, ni volas paradi... Ne faru tion. Pli bone estas esti
humila kaj elpensi pretekston. «Mia patro donis al mi tiun monon»; «mi
havas ĉifrovalutojn, sed malmulton»... Resume, pli bonas fermi la buŝon,
se vi ne volas esti kaptita kaj ankaŭ ne volas esti la centro de la
atento. Ne estu la stultulo, kiu paradas post aĉeti 100 gramojn da oro
kaj trovas la sekvan tagon sian domon forrabita.
