Author: Jorge Maldonado Ventura
Category: Kino
Date: 2019-07-24
Image: <img src="/wp-content/uploads/2017/07/caracol.png" alt="" width="1366" height="768" srcset="/wp-content/uploads/2017/07/caracol.png 1366w, /wp-content/uploads/2017/07/caracol-683x384.png 683w" sizes="(max-width: 1366px) 100vw, 1366px">
Lang: eo
Slug: caracoles-cortometraje
Tags: Audacity, Blender, filmeto, libera kulturo, distopio, Garage Band, GIMP, Krita, surrealismo, Synfig
Save_as: limakoj-filmeto/index.html
Title: Limakoj: distopia filmeto
URL: limakoj-filmeto/

<!-- more -->

<video controls poster="/wp-content/uploads/2017/07/caracol.png">
  <source src="/temporal/caracoles.mp4" type="video/mp4">
  <p>Pardonu, via retumilo ne subtenas HTML 5. Bonvolu ŝanĝi aŭ ĝisdatigi vian retumilon.</p>
</video>

Pepe estas junulo, kiu, laca pro sia labortago, volas iri al sia domo por
resti. En ria distopia mondo plena de surealismo eniras en iaspecan
limako-buson, kie ri neeviteble perdiĝas en siaj pensoj.

La video havas la permesilon <a href="https://creativecommons.org/licenses/by/3.0/deed.eo"><abbr title="Creative Commons Atribuite 3.0 Neadaptita">CC BY 3.0</abbr></a> kaj estis atingita el
[Goblin Refuge](https://goblinrefuge.com/mediagoblin/u/guayabitoca/m/caracoles-snails/).
