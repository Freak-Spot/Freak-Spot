Author: Jorge Maldonado Ventura
Category: Textbearbeitung
Date: 2020-08-17 19:00
Lang: de
Slug: diff-highlight-un-mejor-diff-para-git
Save_as: diff-highlight-ein-besseres-diff-für-Git/index.html
URL: diff-highlight-ein-besseres-diff-für-Git/
Tags: diff, diff-highlight, Git, GNU/Linux, Kommandozeile
Title: diff-highlight: ein besseres diff für Git
Image: <img src="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de.png" alt="" width="1499" height="504" srcset="/wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de.png 1499w, /wp-content/uploads/2020/08/git-diff-highlight_vs_git-diff-estándar.de-749x252.png 749w" sizes="(max-width: 1499px) 100vw, 1499px">

Das voreingestellte [diff](https://de.wikipedia.org/wiki/Diff)-Programm
von Git lässt viel zu wünschen übrig.

<!-- more -->

<a href="/wp-content/uploads/2020/08/git_diff-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_diff-ejemplo.png" alt="" width="838" height="705" srcset="/wp-content/uploads/2020/08/git_diff-ejemplo.png 838w, /wp-content/uploads/2020/08/git_diff-ejemplo-419x352.png 419w" sizes="(max-width: 838px) 100vw, 838px">
</a>

Klar, wir können `git --word-diff` ausführen, um die Änderungen besser
zu sehen, aber damit ignorieren wir manchmal Informationen. In diesem
Fall, zum Beispiel, zeigt es nicht die hintere Leerzeichen vor den
HTML-Tags.

<a href="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png">
<img src="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png" alt="" width="941" height="467" srcset="/wp-content/uploads/2020/08/git_word-diff_ejemplo.png 941w, /wp-content/uploads/2020/08/git_word-diff_ejemplo-470x233.png 470w" sizes="(max-width: 941px) 100vw, 941px">
</a>

Und mit anderen diff-Programmen? Lass uns diff-highlight nachsehen...

<a href="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png">
<img src="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png" alt="" width="859" height="705" srcset="/wp-content/uploads/2020/08/git-diff-highlight-ejemplo.png 859w, /wp-content/uploads/2020/08/git-diff-highlight-ejemplo-429x352.png 429w" sizes="(max-width: 859px) 100vw, 859px">
</a>

Letzteres ist besser, oder? Wie kann man es konfigurieren? Ich entdeckte
wie [in einer Webseite, wo ich nur die Befehle
verstand](https://qiita.com/SakaiYuki/items/2814d417d0bec59046bb), was
genug war. Ich erkläre dich wie, denn ich weiß es nun.

Zuerst braucht man die Git-Version 2.9 oder höher.

    :::bash
    sudo apt update
    sudo apt install git
    git --version  # Muss gleich oder größer als Version 2.9

Obwohl du schon die nötige Git-Version installiert hast, muss man noch
diff-highlight mit Ausführungsrechten konfigurieren und eine Symbolische
Verknüpfung in einem Ordner erstellen, der im `$PATH`-Umgebungsvariable
ist. Hier erstelle ich eine Symbolische Verknüpfung in
`/usr/local/bin/`...

    :::bash
    sudo chmod +x /usr/share/doc/git/contrib/diff-highlight/diff-highlight
    sudo ln -s /usr/share/doc/git/contrib/diff-highlight/diff-highlight /usr/local/bin/diff-highlight

Nun können wir `diff-highlight` ausführen, wenn wir wollen, ohne Git zu
nutzen. Wir müssen die Datei `.gitconfig` konfigurieren, so dass jedes
Mal, wenn wir `git diff` laufen lassen, dieses Programm benutzt wird,
entweder für ein bestimmtes Project oder für jedes. Weil ich es immmer
nutzen will, füge ich die folgende Zeile in `~/.gitconfig` ein:

    :::config
    [core]
        pager = diff-highlight | less -r

Der Einfachheit halber führe ich den Auslass von less durch ein Rohr.

Fertig! Falls wegen eines komisches Grund du Sehnsucht nach das alte
diff hast, kannst du die Datei `.gitconfig` zurückstellen.
