Author: Jorge Maldonado Ventura
Category: Novaĵoj
Date: 2022-06-17 15:36
Lang: eo
Slug: Julian-Assange-será-extraditado-al-pais-que-planeo-asesinarlo
Save_as: Julian-Assange-estos-ekstradicita-al-lando-kiu-planis-mortigi-lin/index.html
URL: Julian-Assange-estos-ekstradicita-al-lando-kiu-planis-mortigi-lin/
Tags: Usono, Julian Assange, Britio
Title: Julian Assange estos ekstradicita al lando, kiu planis mortigi lin
Image: <img src="/wp-content/uploads/2022/06/free-assange-2.webp" alt="" width="824" height="464" srcset="/wp-content/uploads/2022/06/free-assange-2.webp 824w, /wp-content/uploads/2022/06/free-assange-2.-412x232. 412w" sizes="(max-width: 824px) 100vw, 824px">

La [britia registraro decidas
ekstradicii](https://nitter.42l.fr/wikileaks/status/1537726323858219009)
al [Julian Assange](https://eo.wikipedia.org/wiki/Julian_Assange) al
Usono, [la lando, kiu planis mortigi
lin](https://www.jornada.com.mx/notas/2021/09/26/mundo/revelan-que-la-cia-planeo-secuestrar-y-asesinar-a-assange-en-londres/);
iliaj advokatoj apelacios al Alta Kortumo.

La trakto ricevita de Assange en Britia malliberejo estas fia:
psikologiaj torturoj, kiuj kaŭzis al li ekstreman streson kaj igis, ke
li deziru memmortigi, [laŭ la observoj de pli ol sesdek kuracistoj kaj
la speciala raportisto de Unuiĝintaj
Nacioj](https://www.lavanguardia.com/internacional/20191125/471840750638/sesenta-medicos-temen-assange-pueda-morir-prision.html).
En Usono li povas eĉ preni punojn, kiuj entute estu 175 jaroj sub la
Spionada Leĝo. Assange agis kiel ĵurnalisto publikante misuzojn de povo
kaj militkrimojn.
