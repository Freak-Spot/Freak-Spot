Author: Хорхе Мальдонадо Вентура
Category: Bash
Date: 2024-02-25 14:00
Lang: ru
Slug: aliases-para-agilizar-tareas-en-bash
Save_as: psevdonimy-dlja-uproŝenija-rešenija-zadač-v-Bash/index.html
URL: psevdonimy-dlja-uproŝenija-rešenija-zadač-v-Bash/
Tags: .bashrc, .bash_aliases, алиас, настройка Bash
Title: Псевдонимы для упрощения решения задач в Bash

Псевдонимы или как их еще называют, алиас, как следует из названия,
используются для вызова команды под другим именем. Команда, к которой
применен псевдоним, будет работать так же, как если бы она была
вызвана напрямую. Например, если я хочу перейти в родительский
каталог с помощью команды `..`, мне достаточно создать псевдоним
в терминале с помощью следующей команды: `alias ..='cd ...'`.

Вероятно, у вас уже создано несколько псевдонимов, и вы об этом не
знаете. Если вы выполните `alias`, вы увидите уже определенные
псевдонимы. Эти псевдонимы определены в файле `.bashrc`, куда вы
можете добавлять свои собственные псевдонимы (не забудьте [перезагрузить
конфигурацию Bash](/en/reload-Bash-configuration/) после их добавления,
чтобы вы могли начать использовать их без перезагрузки компьютера).
Но если вы хотите добавить много псевдонимом, и что не менее важно,
хотите различать их, какие из них ваши, желательно вынести их в
отдельный файл.

В файле `.bashrc` вы, вероятно, найдете похожие строки:

    :::bash
    # Alias definitions.
    # You may want to put all your additions into a separate file like
    # ~/.bash_aliases, instead of adding them here directly.
    # See /usr/share/doc/bash-doc/examples in the bash-doc package.

    if [ -f ~/.bash_aliases ]; then
        . ~/.bash_aliases
    fi

Это подразумевает, что при каждом запуске Bash загружает псевдонимы,
найденные в файле `~/.bash_aliases`, если он существует. Если у вас
еще нет этого файла, создайте его и добавьте несколько псевдонимов,
которые помогут вам в повседневной работе. Они сэкономят вам много
времени в долгосрочной перспективе.

Вот несколько полезных псевдонимов:

    :::bash
    alias ....='cd ../../..'
    alias ...='cd ../..'
    alias ..='cd ..'
    alias install='sudo apt-get install'
    alias search='apt-cache search'
    alias update='sudo apt-get update && sudo apt-get upgrade'

У меня есть репозиторий <https://notabug.org/jorgesumle/bash_aliases> со
всеми моими псевдонимами, посмотрите на него и скопируйте те, что
покажутся вам наимболее полезными.
