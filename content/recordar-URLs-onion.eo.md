Author: Jorge Maldonado Ventura
Category: Privateco
Date: 2020-12-09 05:00
Lang: eo
Slug: recordar-urls-onion
Save_as: memori-.onion-URL-jn/index.html
URL: memori-.onion-URL-jn/
Tags: Abrowser, Firefox, Iceweasel, legosignoj, .onion, privateco, Tor, Tor Browser
Title: Memori <code>.onion</code>-URL-jn
Image: <img src="/wp-content/uploads/2020/12/legosignoj-de-Tor-Browser.png" alt="" width="1002" height="478" srcset="/wp-content/uploads/2020/12/legosignoj-de-Tor-Browser.png 1002w, /wp-content/uploads/2020/12/legosignoj-de-Tor-Browser-501x239.png 501w" sizes="(max-width: 1002px) 100vw, 1002px">

Memori kaŝitajn servojn estas komplike, ĉar ili havas multajn hazardajn
signojn kaj estas longaj, kiel
<http://duskgytldkxiuqc6.onion/>. Kiel povas ni memori tiel strangajn
URL-jn?

<!-- more -->

Tor Browser lasas uzi legosignojn por konservi la paĝojn, kiujn ni volas
memori. Oni povas aldoni paĝon al la listo de legosignoj per la grafika
fasado aŭ la klavarokomando <kbd>Ctrl</kbd>+<kbd>D</kbd>. Kiam jam estas
konservita la legosigno, vi povas skribi en la adresbreto la paĝan
titolon (kiu kutime aperas en la paĝa langetobreto, rilatas al la
`<title>`-etikedo de <abbr title="HyperText Markup Language">HTML</abbr>),
kaj devos aperi la URL-j de legosignoj kies titoloj kongruu kun la skribito.
Sufiĉas elekti la paĝon, al kiu ni poste volas iri.

<figure>
<a href="/wp-content/uploads/2020/12/legosignoj-de-Tor-Browser.png">
<img src="/wp-content/uploads/2020/12/legosignoj-de-Tor-Browser.png" alt="" width="1002" height="478" srcset="/wp-content/uploads/2020/12/legosignoj-de-Tor-Browser.png 1002w, /wp-content/uploads/2020/12/legosignoj-de-Tor-Browser-501x239.png 501w" sizes="(max-width: 1002px) 100vw, 1002px">
</a>
    <figcaption class="wp-caption-text">Legosignoj en la adresbreto en
    Tor Browser</figcaption>
</figure>

Ankaŭ eblas aldoni breton de legosignoj kiel flanka aŭ suba panelo.
Aperos tiam bildetoj kaj paĝaj titoloj, por ke ni povu iri al ili facile
premante ilin.

Se ni volas alinomi legosignon, importi la liston de legosignoj,
eksporti ĝin aŭ fari aliajn opciojn specialajn, ni devas iri al la
**Biblioteko** el la menuo de legosignoj aŭ premante je <kbd>Ctrl</kbd>
kaj <kbd>o</kbd> samtempe.

![Montrante kiel iri en Tor Browser al la Biblioteko](/wp-content/uploads/2020/12/biblioteko-de-legosignoj-en-Tor-Browser.gif)

Ni povas ankaŭ skribi la URL-jn alie: per listo de paĝoj en HTML-aranĝo,
paĝo en notlibro, administrilo por pasvortoj, ktp.
