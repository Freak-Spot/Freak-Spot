Author: Jorge Maldonado Ventura
Category: Sin categoría
Date: 2022-04-17 00:00
Lang: es
Slug: la-competencia-informacional-en-la-sociedad-de-la-información-problemas-y-soluciones
Tags: competencias digitales, privacidad, software libre
Title: La competencia informacional en la «sociedad de la información»: problemas y soluciones

## 1. Introducción

La competencia informacional tiene una gran importancia en la sociedad
actual, en la que la vida de las personas está mediada por tecnologías
de todo tipo y hay una ingente cantidad de información a su disposición.
El objetivo de este ensayo es presentar esta importante cuestión,
analizar brevemente cuál es la situación actual y determinar cómo
mejorar las competencias de quienes integran nuestra sociedad.

<!-- more -->

## 2. Alfabetización informacional versus alfabetización digital

La sociedad del norte global actual no se puede entender sin las
tecnologías informáticas que han pasado a formar parte de nuestras
vidas. Ya no son solo los jóvenes quienes usan masivamente las
tecnologías; muchos mayores ya poseen un móvil inteligente, ordenadores,
tabletas, lectores de libros electrónicos, etc. En EE. UU., por ejemplo,
el 61 % de la población de más de 65 años posee un móvil
inteligente[^1]. Cabe, sin embargo, preguntarse si realmente hacen un
uso eficiente de la tecnología para obtener información de calidad.

[^1]: Pew Research Center: Internet, Science & Tech. <cite>Mobile Fact Sheet</cite>.
  23 de noviembre de 2021 [consulta: 23 de febrero de 2022]. Disponible
  en: <https://www.pewresearch.org/internet/fact-sheet/mobile/>.

Con la adopción de las tecnologías informáticas han ido surgiendo
términos como *alfabetismo* o *alfabetización digital*, *sociedad de la
información* y *alfabetización informacional*, entre otros. Empezaremos
describiendo la sociedad de la información, un concepto acuñado por el
sociólogo Yoneji Masuda en su obra <cite>La sociedad informatizada como
sociedad post-industrial</cite> (publicada originalmente en japonés en 1980).
El término hace referencia a la importancia social que se le concede a
la comunicación y a la información en la sociedad actual, donde se
involucran las relaciones sociales, económicas y culturales[^2]. Sin
embargo, cabe señalar que la sociedad de la información no se da en todo
el mundo, al menos no de igual manera: según los datos de la
International Telecommunication Union (Unión Internacional de
Telecomunicaciones), 2,9 mil millones de personas nunca han utilizado
Internet[^3].

[^2]: ALFONSO SÁNCHEZ, I. R. La Sociedad de la Información, Sociedad del
  Conocimiento y Sociedad del Aprendizaje. Referentes en torno a su
  formación. <cite>Bibliotecas. Anales de Investigación, 12</cite> (2),
  2016, p. 236.  Disponible en:
  <https://dialnet.unirioja.es/servlet/articulo?codigo=5766698>.
[^3]: International Telecommunication Union. <cite>Statistics</cite>.
  2021 [consulta: 17 de abril de 2022]. Disponible en: <https://www.itu.int/en/ITU-D/Statistics/Pages/stat/default.aspx>.

Por otro lado, la alfabetización digital y alfabetización informacional
pueden entenderse casi como sinónimos en la sociedad de la información,
pues el acceso a la información está mediado por las tecnologías
informáticas. Los dos tipos de alfabetización son inseparables[^4]. Es
por ello que los utilizaré como sinónimos en este ensayo. El término
alfabetización digital hace énfasis en el uso de las tecnologías,
mientras que el de alfabetización informacional es más aséptico. Una
definición de alfabetización informacional es «la capacidad de una
persona para saber cuándo y por qué necesita información, dónde
encontrarla, y cómo evaluarla, utilizarla y comunicarla de manera
ética»[^5]. Se puede tener una mayor o menor alfabetización informacional.

[^4]: HOVIOUS, A. <cite>Digital vs. Information Literacy</cite>. Designer Librarian.
  24 de abril de 2014 [consulta: 3 de abril de 2022]. Disponible en:
  <https://designerlibrarian.wordpress.com/2014/04/24/digital-vs-information-literacy/>.
[^5]: Editores de Wikipedia. <cite>Alfabetización informacional</cite>. 2021 [consulta: 3 de marzo de 2022]. Disponible en:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

## 3. Los problemas presentes en la «sociedad de la información»

### 3.1. Limitaciones técnicas

Sin duda, la infraestructura informática determina la capacidad de
aprovechar al máximo las competencias digitales aplicadas a la búsqueda
de información. Por ejemplo, determinados equipos antiguos no pueden
hacer uso de determinados programas informáticos, existen redes de
información inaccesibles desde ciertos países, hay navegadores web
(normalmente obsoletos o no actualizados) que no pueden cargar
correctamente determinadas páginas web, etc.

La capacidad de superar esos obstáculos depende, entre otras cosas,
de las condiciones socioeconómicas y de nuestro lugar de residencia.
Por muy hábiles que seamos es imposible obtener cierta información
cuando en donde nos encontramos no hay acceso a electricidad o a
Internet. Si no tenemos dinero para comprar un equipo informático y
no podemos acceder a uno público, como sucede en muchos países
pobres, será imposible siquiera hacer uso de las competencias
digitales. Si no tenemos esas limitaciones, como sucede en los
países desarrollados, podremos hacer uso de las tecnologías y
podremos hablar de alfabetización digital.

Algunas empresas tienen comportamientos que exacerban el problema. La
obsolescencia programada, que reduce la vida útil de los productos
tecnológicos, provoca un mayor desembolso económico a largo plazo,
volviendo la tecnología menos accesible. Existen legislaciones que
pretenden solucionar esta situación, como la ley francesa 2015-992[^6], que
puede castigar con dos años en la cárcel o con cuantiosas multas.

[^6]: <cite>LOI n° 2015-992 du 17 août 2015 relative à la transition
  énergétique pour la croissance verte (1)</cite>. 17 de agosto de 2015
  [consulta: 17 de abril de 2022]. Disponible en:
  <https://www.legifrance.gouv.fr/loda/id/JORFTEXT000031044385/>.

### 3.2. Competencias insuficientes

Ahora bien, que tengamos los medios no quiere decir que tengamos las
competencias. Para desarrollar dichas competencias debemos aprender a
usar las tecnologías y a usar estrategias que nos permitan hacer frente
a los obstáculos.

El sistema educativo actual se está enfocando mucho en la
adquisición de esas competencias, pero de forma errada según muchos
críticos que advierten de la pérdida de privacidad de los menores de
edad al imponer plataformas como Google y Microsoft[^7]. No se han
fomentado plataformas respetuosas con la privacidad, descentralizadas,
autogestionadas y libres, sino todo lo contrario: se depende, sobre
todo, de empresas estadounidenses que restringen el conocimiento, con lo
que dificultan el aprendizaje[^8]. Por otro lado,
solo una de cada cuatro empresas formaba a sus empleados en competencias
digitales en 2019[^9]; todavía muchísima gente usa tecnologías que les
convierte en «usados», y no en «usuarios»[^10], al carecer de las
libertades que ofrecen los programas libres[^11]; proliferan las
noticias falsas[^12]; el espacio digital está plagado de monopolios[^13]; etc.

[^7]: MANUEL; RAQUEL. <cite>Competencias digitales con mirada crítica</cite>. 25 de
  junio de 2021 [consulta: 30 de marzo de 2022]. Disponible en:
  <https://commons.wikimedia.org/wiki/File:EsLibre_2021_P25_-_Manu,_Raquel_-_Competencias_digitales_con_mirada_cr%C3%ADtica.webm>.
[^8]: STALLMAN, R. M. <cite>Software libre y educación</cite>. Sitio web del sistema
  operativo GNU. 2021 [consulta: 30 de marzo de 2022]. Disponible en:
  <https://www.gnu.org/education/education.es.html>.
[^9]: RODELLA, F. <cite>Solo una de cada cuatro empresas forma a sus empleados
  en competencias digitales</cite>. El País. 2 de julio de 2019 [consulta: 30
  de marzo de 2020]. Disponible en:
  <https://elpais.com/retina/2019/07/02/tendencias/1562062119_351000.html>.
[^10]: STALLMAN, R. M. <cite>Glossary</cite>. Richard Stallman’s
  personal site. 2020 [consulta: 4 de marzo de 2022]. Disponible en:
  <https://stallman.org/glossary.html>.
[^11]: STALLMAN, R. M. <cite>¿Qué es el Software Libre?</cite> Sitio web
  del sistema operativo GNU. 2019 [consulta: 4 de marzo de 2022].
  Disponible en: <https://www.gnu.org/philosophy/free-sw.es.html>.
[^12]:CARVALHO, D. <cite>Por que as pessoas acreditam em fake news</cite>, segundo a
  psicologia social. Blog Política na Cabeça. 25 de junio de 2019
  [consulta: 2 de abril de 2022]. Disponible en:
  <https://www.blogs.unicamp.br/politicanacabeca/2019/06/25/fake-news-por-que-as-pessoas-acreditam-em-noticias-falsas-segundo-a-psicologia-social/>.
[^13]: DEL CASTILLO, CARLOS. <cite>Multinacionales digitales. Monopolio,
  control, poder: negocio</cite>. El Diario. 15 de agosto de 2020 [consulta: 30
  de marzo de 2020]. Disponible en:
  <https://www.eldiario.es/tecnologia/multinacionales-digitales-monopolio-control-negocio_130_6137536.html>.

Las noticias falsas han proliferado gracias a redes sociales
centralizadas como Facebook, que ganan dinero con la atención y
radicalizan por ello a los usuarios con noticias falsas, según las
revelaciones de Frances Haugen[^14]. La información se
comparte muy rápido, y la gente dedica cada vez menos tiempo a analizar
la información. No se fomenta el debate ni el análisis crítico, sino el
posicionamiento (positivo compartiendo, dándole a «me gusta», comentando
positivamente; negativo dándole a «no me gusta», no compartiendo,
comentando negativamente) ante un hecho impactante que nos ofende, nos
llama la atención, activa nuestras emociones, pues eso es lo que hará
que la gente pase más tiempo frente a la pantalla, llenando así los
bolsillos de las empresas, que pueden mostrar más anuncios y recopilar
más información de los usuarios. El
resultado es una sociedad polarizada, mal informada y con poco espíritu
crítico. No hay una red social popular basada en el debate respetuoso,
aportando fuentes, sin contenido excesivamente llamativo, etc.,
precisamente porque no es rentable.

[^14]: MILMO, D. <cite>Facebook revelations: what is in cache of
  internal documents?</cite> The Guardian. 25 de octubre de 2021
  [consulta: 7 de abril de 2022]. Disponible en:
  <https://www.theguardian.com/technology/2021/oct/25/facebook-revelations-from-misinformation-to-mental-health>.

### 3.3. El mito de los nativos digitales

Pensky, quien acuñó el término de
«nativos digitales»[^15], no realizó una investigación extensa sobre esta
generación, sino que se basó en simples observaciones, como en el número
de horas que los universitarios pasan frente a pantallas (pero sin
aportar fuentes) y en el hecho de que los jóvenes hayan estado inmersos
en la tecnología digital durante toda su vida. Solo a partir de esas
observaciones no se puede afirmar que los jóvenes entendían lo que
estaban haciendo con estos aparatos, o que lo estaban haciendo de forma
efectiva y eficiente. Por el contrario, varias investigaciones[^16]
nos muestran que esos «nativos digitales» nacidos a partir de 1984 no
tienen un conocimiento profundo de las tecnologías, sino que solo tienen
habilidades básicas de ofimática, de envío de mensajes de texto, de uso
de redes sociales como Facebook y de navegación por Internet. Rowlands
<i lang="lt">et al.</i>[^17] concluyeron que «muchas anotaciones profesionales,
escritos populares y presentaciones de PowerPoint sobreestiman el
impacto de las TIC en la juventud, y que la presencia ubicua de la
tecnología en sus vidas no ha resultado en una mejora de las capacidades
de retención, búsqueda o evaluación de información».

[^15]: PRENSKY, M. Digital Native, Digital Immigrants. <cite>On the Horizon.
  2001, 9</cite> (5). Disponible en:
  <https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf>.
[^16]: BULLEN, M. <i lang="lt">et al.</i> <cite>The digital learner at BCIT and implications for
  an e-strategy</cite>. Octubre de 2008 [consulta: 17 de abril de 2022]. Disponible en:
  <https://app.box.com/s/fxqyutottt>.
[^17]: ROWLANDS, I. <i lang="lt">et al.</i> The Google generation: The information
  behaviour of the researcher of the future. <cite>Aislib proceedings: New
  Information Perspectives</cite>. 2008, 60, pp. 290-310. Disponible en:
  <http://dx.doi.org/10.1108/00012530810887953>.

Las empresas intentan crear una dependencia de los alumnos a
programas privativos ofreciéndolos de forma gratuita. Estos
programas suelen recopilar datos personales para venderlos, se
lucran con anuncios y tienen una licencia restrictiva que se debe
pagar[^8]. Los jóvenes, desconocedores de las
verdaderas intenciones de las empresas, son una presa fácil. No son
unos genios, sino que están aprendiendo, y pueden aprender mal. La
intención de esas empresas no es que aprendan, sino generarles una
dependencia a sus productos.

El mito de los nativos digitales es algo pernicioso porque
intenta hacer ver que el problema de la subcompetencia digital no existe
o se resolverá a medida que las generaciones más jóvenes vayan
reemplazando a las más mayores.

### 3.4. Censura

Existen factores políticos que influyen en la aparición de monopolios
informativos y tecnológicos, aunque hay algunos intentos por parte de
algunos sectores para remediar esto. Respecto a los resultados mostrados
por los buscadores también se aprecia una gran influencia política. Por
ejemplo, la Unión Europea obliga a los buscadores a censurar resultados
que infringen los derechos de autor, que no respetan el derecho al
olvido y, recientemente, se han censurado los medios de comunicación
estatales rusos[^18]. Por otro lado, China
también censura muchos resultados ofrecidos por buscadores disponibles
en su territorio[^19].

[^18]: <cite>União Europeia e gigantes da web censuram as agências RT e
  Sputnik</cite>. Pragmatismo Político. 3 de marzo de 2022 [consulta: 30 de
  marzo de 2022]. Disponible en:
  <https://www.pragmatismopolitico.com.br/2022/03/uniao-europeia-web-censuram-agencias-rt-sputnik.html>.
[^19]: Editores de Wikipedia. Censura de Internet en la República Popular
  China. 2022 [consulta: 30 de marzo de 2022]. Disponible en:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

Algunos buscadores pueden decidir también censurar todos los
resultados sobre un determinado tema, tipo de contenido, etc., sin
importar la ubicación geográfica de los usuarios. Es el caso de
DuckDuckGo, que ya no muestra en su lista de resultados los sitios
web relacionados con «desinformación rusa»[^20].
La mayoría de buscadores no suelen admitir abiertamente realizar
acciones de este tipo y no tienen ningún tipo de transparencia.
Google acumula decenas de millones en multas por alterar los
resultados para su propio beneficio[^21]. Casi todo el
mundo usa Google: un 91,56 % en marzo de 2022, según Statcounter[^22],
siguiéndole Bing con un 3,1 %. Existe un motor de búsqueda
distribuido y libre llamado YaCy, pero su uso es marginal y sus
resultados no son muy buenos para redes tan grandes como Internet,
por lo que es mucho más usado en <i lang="en">intranets</i>.

[^20]: WEINBERG, G. Mensaje en la red social Twitter. 10 de marzo de
  2022 [consulta: 30 de marzo de 2022]. Disponible en:
  <https://nitter.snopyta.org/yegg/status/1501716484761997318>.
[^21]: RAYÓN, A. <cite>Los monopolios de Google</cite>. Deia.eus, 4 de
  abril de 2021 [consulta: 2 de abril de 2022]. Disponible en:
  <https://www.deia.eus/vivir-on/contando-historias/2021/04/04/monopolios-google/1110752.html>.
[^22]: <cite>Search Engine Market Share Worldwide</cite>. Statcounter. Marzo de 2022
  [consulta: 30 de marzo de 2020]. Disponible en:
  <https://gs.statcounter.com/search-engine-market-share>.

Es muy caro desarrollar un buscador para Internet, así como también es
caro mantenerlo. Por ello casi todos los buscadores pertenecen a grandes
empresas.

### 3.5. La información desaparece

Por otro lado, mucha información también desaparece por motivos
económicos, de poca difusión de la página, etc. Justamente lo mismo pasó
con los libros antiguos: de Grecia y Roma nos han llegado solo algunas
obras populares, y poquísimas impopulares. Los archivistas, bibliotecarios,
lectores, etc., preservaron esas obras, por lo que podemos encontrar
copias en bibliotecas y en Internet a día de hoy. En Internet, alojar
una página cuesta dinero: tanto el coste del servidor como el del
dominio, a no ser que se opte por usar un protocolo alternativo, como el
servicio oculto de Tor (con lo que solo se paga por el servidor), o se
dependa en el alojamiento proporcionado por otro individuo, organización
o empresa (normalmente, a cambio de poner anuncios, apoyado por
donaciones o simplemente como un acto típico de un mecenas). Muchísimas
páginas han desaparecido porque no se han mantenido los servidores o no
se ha pagado el dominio. Si las obras no tenían mucho interés,
probablemente se perdieron las copias. Esa información puede ser también
relevante para nosotros, y es posible que alguien la haya archivado. Por
eso, es bueno que sepamos que existen archivos de páginas web y de
archivos multimedia como Internet Archive.

A fecha de abril de 2022, Internet Archive tiene más de 670 mil millones de
páginas web; 34 millones de libros y textos; casi ocho millones de
películas, vídeos y programas de televisión; 800 000 programas
informáticos, 14 millones de archivos de audio, 4,3 millones de
imágenes, entre otras cosas, normalmente agrupadas en colecciones,
disponibles en varios formatos y con metadatos[^23]. Internet Archive es una
organización sin ánimo de lucro. También existen otros proyectos con el
mismo objetivo (como [archive.today](https://archive.ph/)) e iniciativas
de carácter estatal, como el proyecto Memento[^24] (de los Estados Unidos).

[^23]: <cite>Internet Archive: Digital Library of Free & Borrowable Books,
  Movies, Music & Wayback Machine</cite>. 2022 [consulta: 6 de abril de 2022].
  Disponible en: <https://archive.org/>
[^24]: Editores de Wikipedia. <cite>Memento Project</cite>. 15 de noviembre de 2021 [consulta: 17 de abril de 2022]. Disponible en
  <https://en.wikipedia.org/wiki/Memento_Project>.

## 4. Cómo aumentar la alfabetización informacional

Las habilidades en el uso de programas informáticos y la capacidad de
plantear consultas de búsqueda para resolver problemas es también algo
muy importante en nuestra sociedad de la información. Las habilidades de
búsqueda no solo consisten en buscar en un buscador, sino también en
navegar por menús, por la documentación del programa, participar en
foros, etc. Es importante también saber qué versión del programa en
cuestión estamos usando, pues puede haber soluciones en Internet para
una versión que no usamos y no nos sirve.

En definitiva, cuanto más elevado sea el conocimiento de informática,
más desarrolladas están nuestras capacidades de búsqueda de información.
Si sabemos qué es un buscador web y cómo funciona, podemos realizar
mejores búsquedas; si sabemos cómo usar un servidor <i
lang="en">proxy</i>, podremos evitar las restricciones de acceso a la
información según la ubicación geográfica (normalmente país) o dirección
IP; si conocemos los prejuicios y las limitaciones de los buscadores,
podremos contrastar resultados de búsqueda y elegir el buscador más
apropiado para cada consulta; si consultamos archivos web, podremos ver
cómo ha cambiado la información a lo largo del tiempo y páginas que ya no
son accesibles; etc.

Para desarrollar esas habilidades es necesaria la educación y la
práctica. De la misma forma que no se aprende a tocar la guitarra en un
día, tampoco se aprende a usar un ordenador y a acceder con eficacia a
la información en un día. La familia, las empresas y la comunidad
educativa tienen un papel esencial para fomentar el desarrollo de esas
habilidades.

Cuando necesitamos obtener cierta información y nos encontramos
obstáculos tenemos una posibilidad de aprender a evitarlos. Podemos
investigar cómo hacer frente a esos obstáculos, cómo usar estrategias para
obtener información más relevante. Alguien que ha superado esos
obstáculos puede ayudarnos a hacer lo mismo. El papel de la comunidad de
usuarios es importantísimo, así como el de los docentes.

Sin embargo, no avanzaremos en esas habilidades si nos es suficiente con
lo que sabemos o pensamos que no hay ningún problema con la información
obtenida. Una persona atrapada en la burbuja de las redes sociales,
consumiendo información falsa y comprando como resultado de la enorme
industria de la publicidad en Internet, puede ignorar estas habilidades
y pensar que es experta en el uso de las tecnologías informáticas. Puede
ser víctima del efecto Dunning-Kruger, que David Dunnung resumió así:
«Si eres incompetente, no puedes saber que eres incompetente… Las
habilidades que necesitas para producir una respuesta correcta son
exactamente las habilidades que necesitas para reconocer qué es una
respuesta correcta»[^25].

[^25]:MORRIS, E. <cite>The Anosognosic’s Dilemma: Something’s Wrong but You’ll
  Never Know What It Is (Part 1)</cite>. Opinionator. The New York Times. 20 de
  junio de 2010 [consulta: 8 de abril de 2022]. Disponible en:
  <https://web.archive.org/web/20220331180557/https://opinionator.blogs.nytimes.com/2010/06/20/the-anosognosics-dilemma-1/>.

Lo mejor es tener habilidades que permitan adaptarnos a todo. No basta
con saber usar un buscador, un programa… concreto, sino que es necesario
conocer sus rasgos comunes. Así, si actualizan el programa o nos
encontramos en un sistema operativo diferente con un programa diferente,
podremos seguir usándolo correctamente, sin grandes complicaciones o
contratiempos.

## 5. Conclusión

La «sociedad de la información» en general está bastante desinformada.
Sin embargo, las habilidades que forman parte de la alfabetización
digital se pueden adquirir con educación y práctica. El sistema
educativo ha ido aumentando con los años la alfabetización digital, pero
no lo suficiente en los niveles educativos básicos. Quienes adquieran un
mayor nivel de competencias informacionales podrán participar mejor de
esta sociedad.
