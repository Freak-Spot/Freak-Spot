Author: Jorge Maldonado Ventura
Category: Opinión
Date: 2023-05-08 00:12
Modified: 2023-07-13 10:00
Lang: es
Slug: el-problema-monocultural-de-mastodon
Tags: Fediverso, Mastodon, descentralización, redes sociales
Title: El problema monocultural de Mastodon

Las recientes acciones de Eugen Rochko (conocido como `Gargron` en el
Fediverso), el director ejecutivo de la [asociación sin ánimo de lucro de
Mastodon](https://joinmastodon.org/es/about) y principal desarrollador
del [programa Mastodon](https://github.com/mastodon/mastodon), han hecho que
algunas personas [se preocupen](https://mastodon.social/@atomicpoet/110300833929868645) por la [enorme influencia](https://mstdn.social/@feditips/110260432218416976) que Mastodon (el proyecto de <i lang="en">software</i> y la organización sin ánimo de lucro) tiene sobre el resto del Fediverso.

Bien. *Deberíamos* estar preocupados.

El programa Mastodon es lo que la mayoría de la gente en el Fediverso usa.
El nodo más grande, `mastodon.social`, aloja más de 200&nbsp;000
cuentas activas mientras escribo este artículo. Esto es aproximadamente
una décima parte de *[todo el Fediverso](https://the-federation.info/)*,
en un *único nodo*. Peor aún, el programa Mastodon es a menudo identificado
como toda la red social, ocultando el hecho de que el Fediverso es [un
sistema mucho más amplio compuesto por programas mucho más diversos](https://axbom.com/fediverse/).

Esto tiene consecuencias negativas ahora y podría tener consecuencias
peores más adelante. Lo que también me molesta mucho es que he visto
algo parecido antes.

## Lo que vi en el OStatus-verso

Hace años tenía una cuenta en un precursor del Fediverso. Se basaba
principalmente en el programa StatusNet (desde entonces renombrado como
[GNU Social](https://es.wikipedia.org/wiki/GNU_Social)) y el [protocolo
OStatus](https://es.wikipedia.org/wiki/OStatus). El nodo más grande con
diferencia era `identi.ca` &mdash;donde yo tenía mi cuenta&mdash;.
También había un montón de otros nodos, y había otros proyectos de
<i lang="en">software</i> que también implementaron OStatus
&mdash;destacando [Friendica](https://friendi.ca/)&mdash;.

Para el propósito de este artículo, llamaremos a esa red social
«OStatus-verso».

Comparado con el Fediverso actual, el OStatus-verso era minúsculo. No
tengo cifras concretas, pero mi estimación aproximada es de entre
100&nbsp;000 y 200&nbsp;000 cuentas activas en un buen día (si tienes
las cifras concretas,
[dímelas](https://freakspot.net/pages/contacto.html) y actualizaré este
artículo con mucho gusto). Tampoco tengo las cifras exactas de
`identi.ca`, pero mi estimación aproximada es que tenía entre
10&nbsp;000 y 20&nbsp;000 cuentas activas.

Es decir, alrededor de una décima parte de la red social.

El OStatus-verso era pequeño pero animado. Había debates, hilos y
etiquetas. Tenía grupos una década antes de que el proyecto de
<i lang="en">software</i>
Mastodon implementara los grupos. Tenía aplicaciones (de escritorio)
&mdash;¡Aún echo de menos la facilidad de uso de [Choqok](https://choqok.kde.org/)!&mdash;. Y después de insistir
un poco, incluso pude convencer a un ministerio polaco para que tuviera
[presencia oficial en
él](https://web.archive.org/web/20121122021505/http://identi.ca/maic).
Hasta donde yo sé, este es el primer ejemplo de una institución
gubernamental que tiene una cuenta oficial en una red social
descentralizada gestionada por <i lang="en">software</i> libre.

## Identipocalipsis

Un día, Evan Prodromou, administrador de `identi.ca` (y creador original
del <i lang="en">software</i> StatusNet), [decidió redesplegarlo como un
nuevo servicio, que ejecutaba `pump.io`](https://lwn.net/Articles/544347/).
Se supone que el nuevo <i lang="en">software</i> sería
[mejor y más ágil](https://opensource.com/life/13/7/pump-io). Se creó un
nuevo protocolo porque OStatus tenía limitaciones muy reales.

Solo había un inconveniente: ese nuevo protocolo era incompatible con el
resto del OStatus-verso. Le arrancó las entrañas a esa red social.

Las personas con cuentas en `identi.ca` perdieron sus conexiones en
todos los nodos compatibles con OStatus. Las personas con cuentas en
otros nodos perdieron el contacto con las personas en `identi.ca`,
algunas de las cuales eran muy populares en el OStatus-verso (¿te
suena?...).

Resultó que si un nodo es una décima parte de toda la red social, muchas
conexiones sociales pasan *por* él. Aunque existían otros nodos, de
repente una gran parte de los usuarios activos *se esfumaron*. Muchos
grupos quedaron prácticamente en silencio. Incluso si uno tenía una
cuenta en un nodo diferente y contactos en otros nodos, muchas caras
conocidas simplemente desaparecieron. Yo dejé de usarlo poco después.

Desde mi punto de vista, esta única acción nos hizo retroceder al menos
cinco años, si no diez, en lo que respecta a la promoción de las redes
sociales descentralizadas. El redespliegue  de `identi.ca` fracturó el
OStatus-verso no solo en el sentido de las conexiones sociales, sino
también en el del protocolo y la comunidad de desarrolladores. Como dijo
[`pettter`](https://mastodon.acc.umu.se/@pettter), un veterano del
OStatus-verso:

<blockquote>Creo que un pequeño matiz sobre el gran golpe es que no
solo impactó cortando las conexiones sociales, sino también en la
fragmentación de protocolos y en la fragmentación de los esfuerzos de
los desarrolladores en reconstruir una y otra vez los bloques básicos de
una web social federada. Quizás fue un paso necesario para que volvieran
a unirse en el diseño de AP [ActivityPub], pero personalmente no lo
creo.</blockquote>

Por supuesto, Evan tenía todo el derecho a hacerlo. Era un servicio que
él llevaba, <i lang="lt">pro bono</i>, en sus propios términos, con su
propio dinero. Pero eso no cambia el hecho de que destrozó el
OStatus-verso.

Creo que debemos aprender de esta historia. Una vez que lo hagamos,
[*deberíamos* preocuparnos por el enorme tamaño de
`mastodon.social`](https://social.lol/@mcg/110302174281035077). Debería
preocuparnos la aparente monocultura del programa Mastodon en el
Fediverso. Y también debería preocuparnos la identificación de todo
el Fediverso con solo «Mastodon».

## El precio de crecer a lo grande

Existen costes y riesgos reales relacionados con crecer tanto como
`mastodon.social`. Esos costes y, sobre todo, esos riesgos afectan tanto
al propio nodo como al Fediverso en general.

La moderación en el Fediverso se centra en gran medida en los
nodos. Un solo nodo gigantesco es [difícil de moderar
eficazmente](https://mastodon.social/@Gargron/109383947978442853),
especialmente si tiene registros abiertos (como `mastodon.social`
actualmente). Como nodo estrella, promocionado directamente en las
aplicaciones móviles oficiales, atrae muchos registros nuevos
&mdash;también [unos cuantos bastantes
problemáticos](https://tech.lgbt/@asahi95/110310499987010451)&mdash;.

Al mismo tiempo, esto también hace que sea más difícil para los
administradores y moderadores de *otros* nodos tomar decisiones de
moderación sobre `mastodon.social`.

Si un administrador de un nodo diferente decide que la moderación
de `mastodon.social` es deficiente por cualquier razón, ¿debería
silenciarlo o incluso defederarse de este (como algunos ya han hecho,
aparentemente), negando así a los miembros de su nodo el acceso a muchas
personas populares que tienen cuentas allí? ¿O deberían mantener ese
acceso, arriesgándose a exponer a su propia comunidad a acciones
potencialmente dañinas?

El enorme tamaño de `mastodon.social` hace que cualquier decisión de
este tipo por parte de otro nodo se convierta inmediatamente en un
gran problema. Es una forma de poder: «claro, puedes defederarte de
nosotros si no te gusta cómo moderamos, ¡pero sería una *pena* que la
gente de tu nodo perdiera el acceso a una décima parte de todo el
Fediverso!". Como [dice el sitio web de GoToSocial](https://gotosocial.org/):

<blockquote>Tampoco creemos que los nodos estrella con miles y
miles de usuarios sean muy buenos para el Fediverso, ya que tienden a la
centralización y pueden convertirse fácilmente en «demasiado grandes
como para ser bloqueados».</blockquote>

¡Ojo, *no* estoy diciendo que esta dinámica de poder se explote consciente
y deliberadamente! Pero es innegable que existe.

Ser un gigantesco nodo estrella también significa que `mastodon.social`
tiene más probabilidades de ser blanco de acciones maliciosas. En varias
ocasiones durante los últimos meses, por ejemplo, [ha sufrido ataques de
denegación de servicio](https://mastodon.social/@Gargron/109781490892884305). Un par
de veces [dejó de funcionar por
eso](https://status.mastodon.social/clf5ti6d818413wsobkvn2zruj). La
resiliencia de un sistema federado se basa en la eliminación de grandes
puntos de fallo, y `mastodon.social` es uno enorme hoy en día.

El tamaño de ese nodo y que sea un objetivo jugoso también
significa que hay que tomar ciertas decisiones difíciles. Por ejemplo,
al ser un objetivo probable de ataques de denegación de servicio, ahora
está detrás de Fastly. [Esto es un
problema](https://spheron.medium.com/the-fastly-internet-outage-shows-the-fallibility-of-leaving-so-much-power-in-the-hands-of-so-few-a234a1bd9fe1)
desde la perspectiva de la privacidad y desde la perspectiva de la
centralización de la infraestructura de Internet. También es un
problema que los nodos más pequeños evitan por completo simplemente por
ser más pequeños y, por tanto, objetivos menos interesantes para que
alguien los derribe con ataques de denegación de servicio.

## Aparente monocultura

Aunque el Fediverso no es exactamente una monocultura, está demasiado
cerca de serlo. Mastodon, la organización sin ánimo de lucro, tiene una
gran influencia en todo el Fediverso. Esto hace que las cosas sean tensas
para los usuarios de la red social, los desarrolladores del programa
Mastodon y otros proyectos de programas federados, y también para los
administradores de nodos.

Mastodon no es el único proyecto de <i lang="en">software</i> federado en el Fediverso, ni el
primero. Por ejemplo, [Friendica](https://friendi.ca/) existe desde hace
una década y media, mucho antes de que el programa Mastodon recibiera su
primer <i lang="en">commit</i> de Git. ¡Hay nodos de Friendica (por
ejemplo, [`pirati.ca`](https://pirati.ca/)) que a día de hoy funcionan dentro del Fediverso y
que habían formado parte del OStatus-verso hace una década!

Pero llamar a todo el Fediverso «Mastodon» hace que parezca que solo
existe el programa Mastodon en el Fediverso. Esto lleva a la gente a
exigir que se añadan características a Mastodon y a pedir cambios que a
veces ya han sido implementados por otro programa federado.
[Calckey](https://calckey.org) ya
<a href="https://i.calckey.cloud/notes/994oo646qk">tiene <i lang="en">toots</i> de citas</a>.
Friendica tiene [conversaciones con hilos y texto con formato](https://friendi.ca/about/features/).

Identificar a Mastodon con todo el Fediverso también es malo para los
desarrolladores del programa Mastodon. Se encuentran bajo presión para
implementar funcionalidades que podrían no encajar del todo con el
programa Mastodon. O se encuentran lidiando con dos grupos de
usuarios que se hacen oír: uno que pide una determinada funcionalidad,
y otro que insiste en que no se implemente por ser un cambio demasiado
grande. Muchas de estas situaciones probablemente podrían resolverse más
fácilmente poniendo claramente un límite e indicando a los usuarios
otros programas federados se ajusten mejor a su caso de uso.

Finalmente, Mastodon es actualmente (medido en usuarios activos y en
número de nodos) la implementación más popular del protocolo
ActivityPub. Cada implementación tiene sus peculiaridades. Con el
tiempo y con las nuevas funcionalidades que se implementen, la
implementación de Mastodon podría tener que alejarse más de la
especificación estricta. Es tentador, después de todo: ¿por qué pasar
por un arduo proceso de estandarización de las extensiones de protocolo
si de todos modos eres el nodo más grande?

Si eso ocurre, ¿tendrán que seguirlo todas las demás implementaciones,
siendo así arrastradas, pero sin realmente poder decidir qué cambios se
aplican a la especificación <i lang="lt">de facto</i>? ¿Creará esto más
tensiones entre los desarrolladores del <i lang="en">software</i>
Mastodon y los desarrolladores de otros proyectos de
<i lang="en">software</i> federado?

La mejor solución a «a Mastodon le falta la funcionalidad X» no es
siempre «Mastodon debería implementar la funcionalidad X». A menudo
puede ser mejor utilizar un programa federado diferente, más adecuado
para una tarea o comunidad en particular. O trabajar en una
extensión del protocolo que permita que una funcionalidad
particularmente popular sea implementada de forma fiable por tantos
nodos como sea posible.

[Pero eso solo puede funcionar si todo el mundo tiene claro que Mastodon
es solo una parte de una red social más grande: el
Fediverso](https://mstdn.social/@rysiek/109543826226766974). Y que *ya
tenemos muchas opciones* en lo que respecta a programas federados, y en
lo que respecta a los nodos individuales, y en lo que respecta a
las aplicaciones móviles.

Lamentablemente, eso parece ir en contra de las recientes decisiones de
Eugen, que van hacia un modelo bastante jerárquico (no del todo
integrado en una jerarquía de arriba a abajo, pero yendo a esa
dirección) de aplicaciones móviles oficiales de Mastodon que promueven
el nodo estrella `mastodon.social`. Y eso es algo preocupante, en mi
opinión.

## Un enfoque mejor

Quiero dejar claro que no estoy abogando aquí por congelar el desarrollo
de Mastodon y no implementar nunca nuevas funcionalidades. También estoy
de acuerdo en que el proceso de registro tiene que ser mejor y más ágil
de lo que era antes, y que hay que implementar muchos cambios en la
interfaz de usuario y mejorar su experiencia. Pero todo esto puede y
debe hacerse de forma que mejore la resiliencia del Fediverso, en lugar
de socavarla.

## Grandes cambios

Mi lista de grandes cambios necesarios para Mastodon y el Fediverso
sería:

<ol>
    <li><strong>Cerrar los registros en <code>mastodon.social</code></strong>, ahora.<br>Ya es
    un riesgo y un nodo demasiado grande para el resto del Fediverso.</li>
    <li><strong>Volver todavía más sencilla la migración de perfiles, también
    entre diferentes tipos de nodos</strong><br><a href="https://docs.joinmastodon.org/user/moving/">En Mastodon, la migración de perfil actualmente solo mueve seguidores</a>.
    A quien sigues, los marcadores y las listas de bloqueados y silenciados se
    pueden mover manualmente. Las publicaciones y las listas no se
    pueden mover &mdash;y eso es un gran problema para mucha gente, ya
    que les mantiene atados al primer nodo en el que se
    registraron&mdash;. No es imposible (yo he movido mi perfil dos
    veces y me ha ido perfectamente). Pero es demasiada fricción.
    Afortunadamente, otros proyectos de programas federados también
    están trabajando para permitir las migraciones de publicaciones.
    Pero no va a ser una solución rápida y fácil, ya que el diseño de
    ActivityPub hace que sea muy difícil mover publicaciones entre
    nodos.</li>
    <li><strong>Por defecto, las aplicaciones oficiales deberían ofrecer a los
    nuevos usuarios un nodo aleatorio de entre una pequeña lista de
    nodos verificados</strong><br>Al menos algunos de estos nodos promovidos no
    deberían estar controlados por la asociación sin ánimo de lucro de
    Mastodon. Idealmente, algunos nodos deberían ejecutar
    <em>programas federados diferentes</em> siempre que utilicen una API de
    cliente compatible.</li></ol>

## ¿Qué puedo hacer yo?

Estas son las cosas que nosotros podemos hacer por nuestra cuenta, como
personas que utilizamos el Fediverso:

<ul>
    <li><strong>Considera la posibilidad de salir de <code>mastodon.social</code></strong> si tienes una cuenta allí. Hay que reconocer que es un gran paso, pero también es algo que puedes hacer y que ayuda más directamente a solucionar la situación. Yo <a href="https://mastodon.social/@rysiek">migré de <code>mastodon.social</code></a> hace años, y nunca miré atrás.</li>
    <li><strong>Considera la posibilidad de utilizar un nodo basado en
    un proyecto de <i lang="en">software</i>
    diferente</strong><br>Cuanta más gente migre a nodos que utilicen
    otro programa federado distinto a Mastodon, más equilibrado y
    resiliente será el Fediverso. Estamos escuchando muchas opiniones
    positivas sobre <a href="https://calckey.social/">Calckey</a>, por ejemplo.
    <a href="https://gotosocial.org/">GoToSocial</a> también parece
    interesante.</li>
    <li><strong>Recuerda que el Fediverso no es solo Mastodon</strong><br>
    Las palabras importan. Cuando se habla del Fediverso, llamarlo «Mastodon» solo hace más difícil lidiar con los problemas que mencioné anteriormente.</li>
    <li><strong>Si puedes, apoya otros proyectos que no sean los oficiales de Mastodon</strong><br>
    En este momento el proyecto del programa Mastodon tiene un montón de
    colaboradores, un equipo de desarrollo estable y suficiente
    financiación sólida para continuar con seguridad durante mucho
    tiempo. ¡Eso es estupendo! Pero no se puede decir lo mismo de otros
    proyectos cercanos al Fediverso, como las aplicaciones móviles
    o los programas federados independientes. Para tener un Fediverso
    diverso y resistente, necesitamos asegurarnos de que estos proyectos
    también reciben apoyo, también financiero.</li>
</ul>

## Pensamientos finales

En primer lugar, el Fediverso es una red social mucho más resiliente,
más viable a largo plazo, más segura y más democratizada que cualquier
red social cerrada y centralizada. Incluso con el problema
monocultural de Mastodon, sigue sin ser (y [no puede
ser](https://www.pcmag.com/opinions/whats-happening-to-twitter-could-never-happen-to-mastodon))
propiedad de una sola empresa o persona ni estar bajo su control.
También creo que es una opción mejor y más segura que las [redes sociales
que se disfrazan como descentralizadas y lo son solo de boquilla,
como BlueSky](https://rys.io/en/167.html).

De manera muy significativa, puede decirse que el OStatus-verso fue una
versión temprana del Fediverso; como he explicado antes, algunos
nodos que entonces formaron parte de aquel siguen funcionando y
formando parte del Fediverso hoy en día. En otras palabras, el Fediverso
lleva ya una década y media funcionando y *ha sobrevivido* al
Identipocalipsis, aunque resultó gravemente dañado por este,
siendo testigo [tanto del nacimiento como del prematuro fallecimiento de
Google+](https://rys.io/en/162.html).

Creo que el Fediverso es mucho más resiliente a día de hoy que el OStatus-verso antes
del redespliegue de `identi.ca`. Es un orden de magnitud (al menos)
mayor en términos de base de usuarios. Hay docenas de proyectos de
programas federados diferentes y decenas de miles de nodos activos.
También hay [instituciones](https://social.network.europa.eu/explore)
importantes que invierten en su futuro. No debería cundir el pánico por
todo lo que he escrito antes. Pero sí creo que deberíamos estar
preocupados.

No considero malintencionadas las recientes acciones de Eugen (como hacer que
las aplicaciones oficiales de Mastodon dirijan a nuevas personas hacia
`mastodon.social`) ni las acciones pasadas de Evan (redesplegar
`identi.ca` en `pump.io`). Y creo que nadie debería. Esto es
difícil, y todos estamos aprendiendo sobre la marcha, tratando de
hacerlo lo mejor posible con el tiempo limitado que tenemos disponible y
recursos restringidos en nuestras manos.

Evan pasó a ser uno de los principales creadores de [ActivityPub](https://www.w3.org/TR/activitypub/), el
protocolo con el que funciona el Fediverso. Eugen inició el
proyecto de <i lang="en">software</i> Mastodon, que, creo con
convicción, permitió que el Fediverso floreciera hasta convertirse en lo
que es hoy. Realmente aprecio su trabajo y reconozco que es imposible
hacer algo en el ámbito de las redes sociales sin que alguien opine
sobre ello.

Eso no significa, sin embargo, que no podamos escrutar estas
decisiones y que no debamos tener estas opiniones.

<span style="text-decoration:underline">Este artículo es una traducción del artículo «[Mastodon monoculture problem](https://rys.io/en/168.html)»
publicado por Michal "rysiek" Woźniak bajo la licencia [CC BY-SA
4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.es).</span>
