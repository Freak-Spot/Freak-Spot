Author: Jorge Maldonado Ventura
Category: Opinion
Date: 2022-02-04 22:00
Lang: en
Slug: superburbuja
Save_as: super-bubble/index.html
URL: super-bubble/
Tags: bubble, collapse, decentralization, technology
Title: Super bubble: technological, economic and social change?
Image: <img src="/wp-content/uploads/2022/02/stock-crash.jpg" alt="" width="1024" height="768" srcset="/wp-content/uploads/2022/02/stock-crash.jpg 1024w, /wp-content/uploads/2022/02/stock-crash-512x384.jpg 512w" sizes="(max-width: 1024px) 100vw, 1024px">

There is a super bubble about to burst: housing prices have gone through
the roof, stocks are way overvalued, food is more expensive, the price
of oil has skyrocketed. What will happen when the bubble bursts?

Some say industrial society is
[collapsing](https://en.wikipedia.org/wiki/Societal_collapse); others
think there will soon be a
[depression](https://en.wikipedia.org/wiki/Economic_depression). To
orient the system towards endless economic growth when we live on a
planet with limited resources is absurd. Yet the capitalist system
depends on this growth to stay afloat. Some propose colonising space to
prevent the collapse of capitalism.

In the short term, poverty will increase. Those without
wealth-generating property (assets) will have to sell their labour power
at a lower price (that’s if they can find a job when unemployment rises
further), steal or rely on handouts. Those who have capital or property
on which to grow food, access to clean water and housing have it easier.
Could this be an opportunity in the long run to foster other kinds of
values?

Rising temperatures have already triggered feedback loops that lead to
further temperature increases (which are increasing extreme weather
events, sea levels, desertification, etc.). For example, as the poles
melt, less solar radiation is reflected, so temperatures rise; as the
ice cap dissolves, methane (a greenhouse gas) is released into the
atmosphere; as forests and coral reefs disappear, less CO<sub>2</sub> is absorbed.

There are technologies that favour the accumulation of capital in a few
hands and entail enormous energy costs, and there are decentralised,
free and efficient technologies. The problems we have are far from being
solely technological, but technology is adding to them. Will the
bursting of the super bubble lead to technological, economic and social
change?

Nothing lasts forever.

<small>[Image *Coit Tower fresco - the stock crash*](https://live.staticflickr.com/1/679924_59bcaf5217_b.jpg) by Zac Appleton.</small>
