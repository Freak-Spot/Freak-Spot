Author: Jorge Maldonado Ventura
Category: Notícias
Date: 2024-02-02 00:08
Lang: pt
Slug: casi-un-4-por-ciento-de-los-ordenadores-de-escritorio-usa-gnulinux
Save_as: quase-4-por-cento-dos-computadores-pessoais-corre-GNU-Linux/index.html
URL: quase-4-por-cento-dos-computadores-pessoais-corre-GNU-Linux/
Tags: estatística, GNU/Linux
Title: Quase 4% dos computadores pessoais corre GNU/Linux
Image: <img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png" alt="" width="1139" height="563" srcset="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024.png 1139w, /wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-entre-2009-y-2024-569x281.png 569w" sizes="(max-width: 1139px) 100vw, 1139px">

De acordo com os dados do Statcounter, o GNU/Linux é o sistema operativo
utilizado em 3,77% dos computadores pessoais (portáteis e
<i lang="en">desktops</i>)<!-- more -->[^1]. Podemos ver no gráfico que em janeiro de
2021 a quota do GNU/Linux era de 1,91%, o que significa que em três anos
a sua popularidade duplicou. Também vale a pena mencionar que a quota do
Chrome OS (que utiliza o núcleo Linux) é de 1,78%.

<a href="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png">
<img src="/wp-content/uploads/2024/02/estadística-usuarios-de-GNULinux-2016-hasta-2024.png" alt="">
</a>

No entanto, esta estatística não é totalmente representativa, uma vez
que o código de rastreio do Statcounter está instalado em apenas 1,5
milhões de sítios[^2]. Além disso, alguns utilizadores do GNU/Linux
&mdash; aqueles que se preocupam mais com a privacidade &mdash; usam
ferramentas que alteram o
[`User-Agent`](https://developer.mozilla.org/pt-BR/docs/Web/HTTP/Headers/User-Agent)
(um exemplo é o Tor Browser, que sempre diz que corre no Windows para
se camuflar melhor).

De qualquer forma, trata-se de um crescimento impressionante que
provavelmente continuará nos próximos anos, uma vez que o GNU/Linux está
cada vez mais presente nas instituições de ensino e muitos países estão
a tentar aumentar a sua soberania tecnológica (o GNU/Linux é a forma
mais barata e segura de a atingir).

[^1]: <https://gs.statcounter.com/os-market-share/desktop/worldwide/#monthly-200901-202401>
[^2]: «<i lang="en">Our tracking code is installed on more than 1.5 million sites
    globally</i>»: isto é o que dizem em <https://gs.statcounter.com/faq#methodology>.
