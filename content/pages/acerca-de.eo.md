Author: Jorge Maldonado Ventura
Date: 2016-07-28 17:43
Lang: eo
Modified: 2023-12-10 23:10
Save_as: pri/index.html
Slug: acerca-de
Status: published
Title: Pri Freak Spot
URL: pri/

Ni skribas pri liberaj programoj kaj libera kulturo kun sendependa
vidpunkto. La problemo estas, ke multaj retejoj kun la sama temo
dependas de reklamantoj. Ili ne morale povas defendi la liberecon
de programoj el senpartia perspektivo, ĉar ili postulas plenumi la
proprietan fontkodon de reklamoj kaj ne kritikas reklamantajn firmaojn.

Kreita je junio de 2016, ĉi tiu retejo klopodas esti alernativo. Estas
farita de kaj por la komunumo, do nature [ni akceptas
kontrubuojn](https://notabug.org/Freak-Spot/Freak-Spot/src/master/README.markdown#colaboraci%C3%B3n).
