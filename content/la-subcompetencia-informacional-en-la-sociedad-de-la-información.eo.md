Author: Jorge Maldonado Ventura
Category: Senkategoria
Date: 2022-04-17 00:00
Lang: eo
Slug: la-competencia-informacional-en-la-sociedad-de-la-información-problemas-y-soluciones
Save_as: la-kompetenteco-pri-informo-en-la-informa-socio-problemoj-kaj-solvoj/index.html
URL: la-kompetenteco-pri-informo-en-la-informa-socio-problemoj-kaj-solvoj/
Tags: cifereca kompetenteco, privateco, liberaj programoj
Title: La kompetenteco pri informo en la «informa socio»: problemoj kaj solvoj

## 1. Enkonduko

La kompetentenco pri informo estas tre grava en la nuntempa socio, kie
la homaj vivoj estas peritaj per ĉiuspecaj teknologioj kaj ekzistas ega
kvanto da informo je ilia dispono. La celo de ĉi tiu eseo estas prezenti
ĉi tiun gravan temon, mallonge analizi la nuntempan situacion kaj
determini kiel plibonigi la kompetentecon de la homoj en nia socio.

<!-- more -->

## 2. Informa kompetenteco kontraŭ cifereca kompetenteco

La socio de la evoluintaj landoj ne povas esti komprenita sen la
informaj teknologioj, kiuj iĝis parto de niaj vivoj. Nun ne nur
junuloj grandskale uzas teknologiojn; multaj maljunuloj jam havas
saĝtelefonon, komputilojn, tabulkomputilojn, bitlegilojn, ktp. En Usono
ekzemple 61 % de la loĝantaro pli aĝa ol 65 havas saĝtelefonon[^1].
Tamen indas demandi, ĉu ili vere efike uzas la teknologion por akiri
kvalitan informon.

[^1]: Pew Research Center: Internet, Science & Tech. <cite>Mobile Fact Sheet</cite>.
  23 de novembro de 2021 [aliro: 23 de februaro de 2022]. Disponebla
  en: <https://www.pewresearch.org/internet/fact-sheet/mobile/>.

Per la adopto de la ciferecaj teknologioj naskiĝis terminoj kiel
*cifereca kompetenteco*, *informa socio* kaj *kompetenteco pri informo*,
inter aliaj. Ni komencos priskribante la informan socion, koncepton
kreitan de la sociologo Yoneji Masuda en lia verko <cite>La informa
socio kiel postindustria socio</cite> (origine eldonita en la japana en
1980). La termino aludas la socian gravecon donitan al la komunikado kaj
al la informo en la nuntempa socio, kie miksiĝas sociaj, ekonomiaj kaj
kulturaj rilatoj[^2]. Tamen indas rimarkigi, ke la informa socio ne
ekzistas en la tuta mondo, almenaŭ ne sammaniere: laŭ la datenoj de la
International Telecommunication Union (Internacia Telekomunika Unio),
2,9 miliardoj da homoj neniam uzis la Interreton[^3].

[^2]: ALFONSO SÁNCHEZ, I. R. La Sociedad de la Información, Sociedad del
  Conocimiento y Sociedad del Aprendizaje. Referentes en torno a su
  formación. <cite>Bibliotecas. Anales de Investigación, 12</cite> (2),
  2016, p. 236. Disponebla en:
  <https://dialnet.unirioja.es/servlet/articulo?codigo=5766698>.
[^3]: International Telecommunication Union. <cite>Statistics</cite>.
  2021 [aliro: 17-a de aprilo de 2022]. Disponebla en: <https://www.itu.int/en/ITU-D/Statistics/Pages/stat/default.aspx>.

Aliflanke la cifereca kompetenteco kaj la informa kompetenteco povas
esti komprenataj preskaŭ kiel sinonimoj en la informa socio, ĉar la
aliro al la informo estas perita per ciferecaj teknologioj. La du specoj
de kompetentecoj estas nedisigeblaj[^4]. Estas pro tio, ke mi uzos ilin
kiel sinonimojn en ĉi tiu eseo. La termino cifereca kompetenteco emfazas
la uzadon de teknologioj, dum informa kompetenteco estas pli neŭtra.
Difino pri cifereca kompetenteco estas «la kapablo de homo scii kiam kaj
kial ri bezonas informon, kie trovi ĝin kaj kiel taksi, uzi kaj komuniki
ĝin etike»[^5]. Oni povas havi pli aŭ malpli da cifereca kompetenteco.

[^4]: HOVIOUS, A. <cite>Digital vs. Information Literacy</cite>. Designer Librarian.
  24-a de aprilo de 2014 [aliro: 3-a de aprilo de 2022]. Disponebla en:
  <https://designerlibrarian.wordpress.com/2014/04/24/digital-vs-information-literacy/>.
[^5]: Editores da Wikipédia. <cite>Alfabetización informacional</cite>. 2021 [aliro: 3-a de marto de 2022]. Disponebla en:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

## 3. La nuntempaj problemoj en la «informa socio»

### 3.1. Teknikaj limigoj

La komputila infrastrukturo sendube determinas la kapablon plej bone
utiligi la ciferecajn kompetentecojn aplikitajn al la serĉo de informo.
Ekzemple iuj malnovaj aparatoj ne povas uzi iujn komputilajn programojn,
estas retoj de informo nealireblaj el iuj landoj, estas retumiloj
(precipe malaktualaj aŭ ne-ĝisdatigitaj), kiuj ne povas bone ŝargi iujn
retejojn, ktp.

La kapablo preterpasi tiujn barojn dependas, inter aliaj aferoj, de la
sociekonomiaj kondiĉoj kaj de nia loĝloko. Ne gravas nian kompetentecon,
ne eblas akiri iujn informojn, kiam ne estas aliro al elektro aŭ al
Interreto, kie ni loĝas. Se ni ne havas monon por aĉeti komputilon kaj
ne povas aliri al iu publika, kiel okazas en multaj malriĉaj landoj, eĉ
ne eblos uzi la ciferecajn kompetentecojn. Se ni ne havas tiajn
limigojn, kiel okazas en la evoluintaj landoj, ni povos uzi la
teknologiojn kaj ni povos paroli pri cifereca kompetenteco.

Kelkaj firmaoj havas kondutojn, kiuj grandigas la problemon. La planita
malnoviĝo, kiu malpliigas la utilan vivon de la teknologiaj produktoj,
longatempe kaŭzas pli granda elspezo, igante la teknologion malpli
alirebla. Ekzistas leĝaro por solvi ĉi tiun problemon, kiel la franca
leĝo 2015-992[^6], kiu povas puni per dujara malliberigo aŭ kostaj
monpunoj.

[^6]: <cite>LOI n° 2015-992 du 17 août 2015 relative à la transition
  énergétique pour la croissance verte (1)</cite>. 17 de aŭgusto de 2015
  [aliro: 17-a de aprilo de 2022]. Disponebla en:
  <https://www.legifrance.gouv.fr/loda/id/JORFTEXT000031044385/>.

### 3.2. Nesufiĉaj kompetentecoj

Tamen, ke ni havu la rimedojn ne signifas, ke ni havas la
kompetentecojn. Por disvolvi tiajn kompetentecojn ni devas lerni uzi la
teknologiojn kaj uzi strategiojn, kiuj ebligas al ni alfronti la barojn.

La nuntempa edukada sistemo koncentriĝas en la akirado de tiuj
kompetentecoj, sed per malĝusta maniero laŭ multaj kritikantoj, kiuj
avertas pri la perdo de privateco de neplenaĝuloj postulante platformojn
kiel Guglon kaj Microsoft-on[^7]. Ne estis kuraĝigitaj platformoj amikaj
de la privateco, malcentraj, memadministritaj kaj liberaj, sed la malo:
oni dependas precipe de usonaj firmaoj, kiuj limigas la scion kaj tial
malfaciligas la lernadon[^8]. Aliflanke, nur unu el kvar firmaoj trejnis
siajn dungitojn pri ciferecaj kompetentecoj en 2019[^9]; ankoraŭ multege
da homoj uzas teknologiojn, kiuj igas ilin «uzataj», kaj ne
«uzantoj»[^10], ĉar ili malhavas la liberecojn, kiujn la liberaj
programoj donas[^11]; la falsaj novaĵoj disvastiĝas[^12]; la cifereca
spaco estas plena de monopoloj[^13], ktp.

[^7]: MANUEL; RAQUEL. <cite>Competencias digitales con mirada crítica</cite>. 25-a de
  junio de 2021 [aliro: 30-a de marto de 2022]. Disponebla en:
  <https://commons.wikimedia.org/wiki/File:EsLibre_2021_P25_-_Manu,_Raquel_-_Competencias_digitales_con_mirada_cr%C3%ADtica.webm>.
[^8]: STALLMAN, R. M. <cite>Software libre y educación</cite>. Retejo de
    la GNU-operaciumo. 2021 [aliro: 30-a de marto de 2022]. Disponebla
    en: <https://www.gnu.org/education/education.es.html>.
[^9]: RODELLA, F. <cite>Solo una de cada cuatro empresas forma a sus empleados
  en competencias digitales</cite>. El País. 2-a de julio de 2019 [aliro: 30
  de março de 2020]. Disponebla en:
  <https://elpais.com/retina/2019/07/02/tendencias/1562062119_351000.html>.
[^10]: STALLMAN, R. M. <cite>Glossary</cite>. Richard Stallman’s
  personal site. 2020 [aliro: 4-a de março de 2022]. Disponebla en:
  <https://stallman.org/glossary.html>.
[^11]: STALLMAN, R. M. <cite>¿Qué es el Software Libre?</cite> Retejo de
  la GNU-operaciumo. 2019 [aliro: 4-a de marto de 2022].
  Disponebla en: <https://www.gnu.org/philosophy/free-sw.es.html>.
[^12]:CARVALHO, D. <cite>Por que as pessoas acreditam em fake news</cite>, segundo a
  psicologia social. Blog Política na Cabeça. 25-a de junio de 2019
  [aliro: 2-a de aprilo de 2022]. Disponebla en:
  <https://www.blogs.unicamp.br/politicanacabeca/2019/06/25/fake-news-por-que-as-pessoas-acreditam-em-noticias-falsas-segundo-a-psicologia-social/>.
[^13]: DEL CASTILLO, CARLOS. <cite>Multinacionales digitales. Monopolio,
  control, poder: negocio</cite>. El Diario. 15-a de aŭgusto de 2020
  [aliro: 30-a
  de marto de 2020]. Disponebla en:
  <https://www.eldiario.es/tecnologia/multinacionales-digitales-monopolio-control-negocio_130_6137536.html>.

La falsaj novaĵoj disvastiĝas danke al centraj sociaj retejoj, kiel
Facebook, kiuj gajnas monon per la atento kaj tiel radikaligas la
uzantojn per falsaj novaĵoj, laŭ la riveloj e Frances Haugen[^14]. La
informo estas tre rapide diskonigita, kaj la homoj estas malpli kaj
malpli da tempo analizante informon. La debato kaj la kritika analizo
ne estas kuraĝigitaj, sed la donado de opinioj (pozitivaj per diskonigo,
ŝato, pozitivaj komentoj; negativaj per manko de ŝato, de diskonigo, per
negativaj komentoj) pri ŝoka afero, kiu ofendas nin, vekas nian atenton,
aktivas niajn emociojn, ĉar estas tio, kio igas, ke homoj estu pli da
tempo kontraŭ ekrano, riĉigante tiel la firmaojn, kiuj povas montri pli da
reklamoj kaj kolekti pli da informo de la uzantoj. La rezulto estas
socio frakciigita, malbone informita kaj kun malmulte da kritika
spirito. Ne estas populara socia retejo bazita sur la respekta debato,
kiu donas informfontojn, sen tro da okulfrapa enhavo, ktp., precize ĉar
tio ne estas profitdona.

[^14]: MILMO, D. <cite>Facebook revelations: what is in cache of
  internal documents?</cite> The Guardian. 25-a de oktobro de 2021
  [aliro: 7-a de aprilo de 2022]. Disponebla en:
  <https://www.theguardian.com/technology/2021/oct/25/facebook-revelations-from-misinformation-to-mental-health>.

### 3.3. La mito de la ciferecaj denaskuloj

Pensky, kiu kreis la terminon «ciferecaj denaskuloj»[^15], ne faris
vastan esploron pri tiu generacio, sed li baziĝis sur simplaj observoj,
kiel la nombro de horoj, kiuj la universitataj studantoj pasiĝas kontraŭ
ekranoj (sed sen provizi fontojn), kaj la fakto, ke la junuloj estis
ĉirkaŭitaj per cifereca teknologio dum sia tuta vivo. Sole per ĉi tiuj
observoj oni ne povas aserti, ke la junuloj komprenis tion, kion ili
faris per tiuj aparatoj, aŭ ke ili efike kaj rendimente faris tion.
Male la esploro montras al ni, ke tiuj «ciferecaj denaskuloj» naskitaj
post 1984 ne havis profundan scion de la teknologioj, sed nur baza
kompetenteco de labortablo, de sendado de mesaĝoj, de uzado de sociaj
retejoj kiel Facebook kaj de retumado en la Interreto. Rowlands
<i lang="lt">et al.</i>[^17] konkludis, ke «multaj profesiaj komentoj,
popularaj skribaĵoj kaj PowerPoint-prezentoj troigas la efekton de la
komputilaj teknologioj en la junularo, kaj ke la ĉiea ĉeesto de la
teknologio en iliaj vivoj ne kaŭzis plibonigon de la kapabloj de reteno,
serĉo aŭ takso de informo».

[^15]: PRENSKY, M. Digital Native, Digital Immigrants. <cite>On the Horizon.
  2001, 9</cite> (5). Disponebla en:
  <https://www.marcprensky.com/writing/Prensky%20-%20Digital%20Natives,%20Digital%20Immigrants%20-%20Part1.pdf>.
[^16]: BULLEN, M. <i lang="lt">et al.</i> <cite>The digital learner at BCIT and implications for
  an e-strategy</cite>. Oktobro de 2008 [aliro: 17-a de aprilo de 2022].
  Disponebla en:
  <https://app.box.com/s/fxqyutottt>.
[^17]: ROWLANDS, I. <i lang="lt">et al.</i> The Google generation: The information
  behaviour of the researcher of the future. <cite>Aislib proceedings: New
  Information Perspectives</cite>. 2008, 60, p. 290-310. Disponebla en:
  <http://dx.doi.org/10.1108/00012530810887953>.

La firmaoj klopodas krei dependecon de la studantoj al proprietaj
programoj, donante ilin senkoste. Ĉi tiuj programoj ofte kolektas
personajn datenojn por vendi ilin, profitas per reklamoj kaj havas
limigan permesilon, kion oni devas pagi[^8]. La maljunuloj, kiuj ne
scias la verajn intencojn de la firmaoj, estas facilaj predoj. Ili ne
estas geniuloj, sed lernas, kaj ili povas malbone lerni. La intenco de
tiuj firmaoj ne estas, ke ili lernu, sed generi dependecon al iliaj
produktoj.

La mito de la ciferecaj denaskuloj estas io damaĝa, ĉar ĝi klopodas
ŝajnigi, ke la problemo de la cifereca subkompetenteco ne ekzistas aŭ
estos solvita, kiam la pli junaj generacioj anstataŭigas la plej
maljunaj.

### 3.4. Cenzuro

Estas politikaj faktoroj, kiuj instigas la aperon de teknologiaj
kaj informaj monopoloj, kvankam estas kelkaj klopodoj fare de iuj
branĉoj por solvi ĉi tiun situacion. Rilate al la rezultoj montritaj de
la serĉiloj oni observas grandan politikan influon. Ekzempla la Eŭropa
Unio devigas al la serĉiloj cenzuri rezultojn, kiuj malobservas la
aŭtorrajton, kiuj ne respektas la rajton esti forgesita, kaj lastatempe
estis cenzuritaj la rusaj ŝtataj komunikiloj[^18]. Aliflanke Ĉinio ankaŭ
cenzuras multajn rezultojn oferitajn de la serĉiloj disponeblaj en sia
teritorio[^19].

[^18]: <cite>União Europeia e gigantes da web censuram as agências RT e
  Sputnik</cite>. Pragmatismo Político. 3-a de marto de 2022 [aliro: 30-a de
  marto de 2022]. Disponebla en:
  <https://www.pragmatismopolitico.com.br/2022/03/uniao-europeia-web-censuram-agencias-rt-sputnik.html>.
[^19]: Redaktistoj de Vikipedio. Censura de Internet en la República Popular
  China. 2022 [aliro: 30-a de marto de 2022]. Disponebla en:
  <https://es.wikipedia.org/wiki/Censura_de_Internet_en_la_Rep%C3%BAblica_Popular_China>.

Kelkaj serĉiloj ankaŭ povas decidi cenzuri ĉiujn la rezultojn pri
specifa temo, speco de enhavo, ktp., sendepende de la geografia situo de
la uzantoj. Estas ĉi tio, kion faras DuckDuckGo, kiu ne plu montras en
sia listo de rezultoj retejojn rilatajn al la «rusa misinformado»[^20].
La plejparto de la serĉiloj ne kutimas malferme agnoski fari ĉi tiajn
agojn kaj havas nenian travideblecon. Google amasigas dekojn da
monpunojn pro ŝanĝi la rezultojn por sia propra profito[^21]. Preskaŭ
ĉiuj uzas Guglon: 91,56 % en marto de 2022, laŭ Statcounter[^22], kaj
ĝin sekvas Bing kun 3,1 %. Estas libera kaj malcentra serĉilo nomita
YaCy, sed ĝi ne estas multe uzata kaj ĝiaj rezultoj ne estas bonaj por
retoj tiel grandaj kiel la Interreto, do ĝi estas multe pli uzata en
intraretoj.

[^20]: WEINBERG, G. Mensaje en la red social Twitter. 10-a de marto de
  2022 [aliro: 30-a de marto de 2022]. Disponebla en:
  <https://nitter.snopyta.org/yegg/status/1501716484761997318>.
[^21]: RAYÓN, A. <cite>Los monopolios de Google</cite>. Deia.eus. 4-a de
  aprilo de 2021 [aliro: 2-a de aprilo de 2022]. Disponebla en:
  <https://www.deia.eus/vivir-on/contando-historias/2021/04/04/monopolios-google/1110752.html>.
[^22]: <cite>Search Engine Market Share Worldwide</cite>. Statcounter. Marto de 2022
  [aliro: 30-a de marto de 2020]. Disponebla en:
  <https://gs.statcounter.com/search-engine-market-share>.

Tre multekostas disvolvi Interretan serĉilon, kaj same multekostas
konservi ĝin. Tial preskaŭ ĉiuj serĉiloj apartenas al grandaj firmaoj.

### 3.5. La informo malaperas

Aliflanke multaj informoj ankaŭ malaperas pro ekonomiaj kialoj, pro la
manko de mono, pro malmulte da disvastiĝo de la retejo, ktp. Same okazis
al la malnovaj libroj: el Grekio kaj Romio nur alvenis al ni kelkaj
popularaj verkoj, kaj malmultege da nepopularaj. La arkivistoj,
bibliotekistoj, legintoj, ktp., konservis tiujn verkojn, do ni nuntempe
povas trovi kopiojn en bibliotekoj kaj en la Interreto. En Interreto
gastigi retejon kostas monon: kaj la koston de la servilo kaj tiun de la
domajno, krom se oni elektas uzi alternativan protokolon, kiel tiun de
la kaŝita servo de Tor (per kiu oni nur pagas por la servilo), aŭ oni
dependas de la gastigado donita de alia homo, organizaĵo aŭ firmao
(kutime kontraŭ la meto de reklamoj, subtene de donacoj aŭ simple kiel ago
propra de mecenato). Multege da retejoj malaperis, ĉar oni ne konservis
la servilojn aŭ oni ne pagis la domajnon. Se la verkoj ne vekis multe da
intereso, eble la kopioj perdiĝis. Tiu informo ankaŭ povas esti grava
por ni, kaj eble iu arkivis ĝin. Do estas bone, ke ni sciu, ke estas
arkivoj de retejoj kaj de plurmediaj dosieroj kiel Internet Archive.

Je aprilo de 2022, Internet Archive havas pli da 670 miliardoj da
retejoj; 34 milionoj da libroj kaj tekstoj; preskaŭ ok milionoj da
filmoj, videoj kaj televidprogramoj; 800 000 komputilaj programoj, 14
milionoj da aŭdaĵdosieroj, 4,3 milionoj da bildoj, inter aliaj aferoj,
kutime arigitaj per kolektoj, disponeblaj en kelkaj aranĝoj kaj kun
metadatenoj[^23]. Internet Archive estas neprofitcela organizo. Ankaŭ
ekzistas aliaj samcelaj projektoj (kiel
[archive.today](https://archive.ph/)) kaj registaraj iniciatoj, kiel la
Memento-projekto[^24] (de Usono).

[^23]: <cite>Internet Archive: Digital Library of Free & Borrowable Books,
  Movies, Music & Wayback Machine</cite>. 2022 [aliro: 6-a de aprilo de 2022].
  Disponebla en: <https://archive.org/>
[^24]: Redaktistoj de Vikipedio. <cite>Memento Project</cite>. 15-a de novembro de 2021 [aliro: 17-a de aprilo de 2022]. Disponebla en:
  <https://en.wikipedia.org/wiki/Memento_Project>.

## 4. Kiel pliigi la kompetentecon pri informo

La kompetentecoj pri la uzado de programoj kaj la kapabloj prezenti
informpetojn por solvi problemojn estas ankaŭ gravaj en nia informa
socio. La serĉkapabloj ne nur konsistas el la serĉado per serĉilo, sed
ankaŭ el retumi tra programaj menuoj, partopreni en diskutejoj, ktp.
Ankaŭ gravas scii, kiu programa versio ni uzas, ĉar povas esti solvojn en
Interreto por versio, kiun ni ne uzas kaj kiu ne utilas al ni.

Resume, ju pli alta la nivelo de cifereca kompetenteco, des pli
evoluita estas nia kapableco serĉi informon. Se ni scias, kio estas
retumilo kaj kiel ĝi funkcias, ni povas pli bone serĉi; se ni scias kiel
uzi prokuran servilon, ni povas eviti la limigojn de aliro al la informo
laŭ la geografia situo (kutime laŭ lando) aŭ IP-adreso; se ni scias la
antaŭjuĝon de la serĉiloj, ni povos kompari la serĉrezultojn kaj elekti
la plej taŭgan serĉilon por ĉiu informpeto; se ni konsultas retajn
arkivojn, ni povos vidi, kiel ŝanĝis la informon tra la tempo, kaj
paĝojn, kiuj ne plu estas alireblaj; ktp.

Por disvolvi tiujn kompetentecojn bezonatas la edukado kaj la praktiko.
Same kiel oni ne lernas ludi la gitaron en unu tago, oni ankaŭ ne lernas
uzi komputilon kaj efike aliri al la informo en unu tago. La familio, la
firmaoj kaj la edukada komunumo havas esencan rolon por kuraĝigi la
disvolvadon de tiuj kapabloj.

Kiam ni bezonas akiri iun informon kaj ni trovas barojn, ni havas ŝancon
lerni eviti ilin. Ni povas esplori, kiel alfronti tiujn barojn, kiel uzi
strategiojn por akiri pli rilatan informon. Iu, kiu preterpasis tiujn
barojn, povas helpi nin fari la samon. La rolo de la komunumo de uzantoj
tre gravas, same kiel tiu de la lernigantoj.

Tamen ni ne antaŭeniros per tiu kapabloj, se sufiĉas al ni tio, kion ni
scias, aŭ ni pensas, ke estas neniu problemo en la informo akirita. Homo
kaptita en la veziko de la sociaj retejoj, konsumante falsajn informojn
kaj aĉetante pro la ega reklamindustrio en Interreto, povas ignori ĉi
tiujn kapablojn kaj pensi, ke ri estas sperta pri la uzo de la
komputilaj teknologioj. Ri povas esti viktimo de la efiko
Dunning-Kruger, kiun David Dunning tiel resumis: «Se vi estas
nekompetenta, vi ne povas scii, ke vi estas nekompetenta... La kapabloj,
kiujn vi bezonas por produkti ĝustan respondon estas ekzakte la
kapabloj, kiujn vi bezonas por rekoni, kio estas ĝusta respondo»[^25].

[^25]:MORRIS, E. <cite>The Anosognosic’s Dilemma: Something’s Wrong but You’ll p
  Never Know What It Is (Part 1)</cite>. Opinionator. The New York Times. 20-a de
  junio de 2010 [aliro: 8-a de aprilo de 2022]. Disponebla en:
  <https://web.archive.org/web/20220331180557/https://opinionator.blogs.nytimes.com/2010/06/20/the-anosognosics-dilemma-1/>.

Plej bone estas havi kompetentecojn, kiuj ebligas al ni adaptiĝi al ĉio.
Ne sufiĉas scii, kiel uzi specifan serĉilon, programon, ktp., sed
necesas koni iliajn komunajn ecojn. Tiumaniere, se la programo estos
ĝisdatigita aŭ ni troviĝas en alia operaciumo kun malsama programo, ni
povos daŭre uzi ĝin sen grandaj komplikaĵoj kaj malfacilaĵoj.

## 5. Konkludo

La «informa socio» estas ĝenerale tre misinformita. Tamen la kapabloj,
kiuj apartenas al la cifereca kompetenteco, povas esti akiritaj per
edukado kaj praktiko. La eduka sistemo kreskigis la ciferecan
kompetentecon dum la jaroj, sed ne sufiĉe en bazaj edukaj niveloj. Tiuj,
kiuj akiru pli altan nivelon de informaj kapabloj, povos pli bone
partopreni en ĉi tiu socio.
