Author: Jorge Maldonado Ventura
Category: Truques
Date: 2023-06-25 15:53
Lang: pt
Slug: evitar-que-javascript-modifique-el-portapeles
Save_as: evitar-que-o-JavaScript-modifique-a-área-de-transferência-no-Firefox/index.html
URL: evitar-que-o-JavaScript-modifique-a-área-de-transferência-no-Firefox/
Tags: área de transferência, Firefox, JavaScript
Title: Evitar que o JavaScript modifique a área de tranferência no Firefox
Image: <img src="/wp-content/uploads/2023/06/JavaScript-modificando-a-área-de-transferência.png" alt="" width="1920" height="1080" srcset="/wp-content/uploads/2023/06/JavaScript-modificando-a-área-de-transferência.png 1920w, /wp-content/uploads/2023/06/JavaScript-modificando-a-área-de-transferência-960x540.png 960w, /wp-content/uploads/2023/06/JavaScript-modificando-a-área-de-transferência-480x270.png 480w" sizes="(max-width: 1920px) 100vw, 1920px">

Alguma vez te aconteceu copiares texto de uma página e quando o colas
aparece um texto modificado, ou mesmo nada? Isso acontece porque a
página em questão executa um código JavaScript que modifica a área de
transferência.

Para evitar isto, podes
[desativar o JavaScript](/desahabilitar-javascript-comodamente-firefox-y-derivados/)
ou alterar as definições do Firefox da seguinte forma:

1. Na barra de endereço, digita `about:config`.
2. Aperta <kbd>Enter</kbd> e clica em **Aceitar o risco e continuar**.
3. Na caixa de pesquisa, escreve `dom.event.clipboardevents.enabled`.
4. Clica duas vezes nessa preferência ou clica no seu botão **Alternar**
   para mudar o seu valor para `false`.

<a href="/wp-content/uploads/2023/06/alternar-preferência-dom-event-clipboardevents-enable-Firefox.png">
<img src="/wp-content/uploads/2023/06/alternar-preferência-dom-event-clipboardevents-enable-Firefox.png" alt="" width="1169" height="304" srcset="/wp-content/uploads/2023/06/alternar-preferência-dom-event-clipboardevents-enable-Firefox.png 1169w, /wp-content/uploads/2023/06/alternar-preferência-dom-event-clipboardevents-enable-Firefox-584x152.png 584w" sizes="(max-width: 1169px) 100vw, 1169px">
</a>

<figure>
<a href="/wp-content/uploads/2023/06/eventos-da-área-de-transferência-do-Firefox-desativados.png">
<img src="/wp-content/uploads/2023/06/eventos-da-área-de-transferência-do-Firefox-desativados.png" alt="" width="949" height="55" srcset="/wp-content/uploads/2023/06/eventos-da-área-de-transferência-do-Firefox-desativados.png 949w, /wp-content/uploads/2023/06/eventos-da-área-de-transferência-do-Firefox-desativados-474x27.png 474w" sizes="(max-width: 949px) 100vw, 949px">
</a>
    <figcaption class="wp-caption-text">Deve ficar assim</figcaption>
</figure>

Desta forma, as páginas web que visitares não poderão modificar a tua área
de transferência. Tem em atenção que algumas aplicações web que
modificam a área de transferência com JavaScript (como o [Collabora](https://es.wikipedia.org/wiki/Collabora_Online)),
não poderão mais fazer isso, pelo que a sua funcionalidade de colar
deixará de funcionar corretamente.
