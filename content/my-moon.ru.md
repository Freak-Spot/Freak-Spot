Author: Хорхе Мальдонадо Вентура
Category: Кинематограф
Date: 2023-02-12 23:00
Image: <img src="/wp-content/uploads/2017/04/My-Moon-poster.jpg" alt="Астронавт в короткометражном фильме "Моя Луна" class="attachment-post-thumbnail wp-post-image">
Lang: ru
Slug: my-moon
Save_as: moja-luna/index.html
URL: moja-luna/
Tags: Blender, короткометражный фильм, свободная культура, Inkscape, Krita, OpenMPT, видео, YouTube
Title: <cite>Моя луна</cite>: короткометражный фильм, созданный с помощью свободного программного обеспечения

<!-- подробнее -->

<video controls preload="none" poster="/wp-content/uploads/2017/04/My-Moon-poster.jpg">
  <source src="/temporal/My_Moon_(Blender_short_film).mp4" type="video/mp4">
  <p>К сожалению, ваш браузер не поддерживает HTML 5. Пожалуйста,
  воспользуйтесь другим или обновите свой текущий браузер</p>
</video>

Это видео, загруженное на YouTube с названием <cite>Моя луна (Короткометражный фильм
созданный в Blender)</cite>, был снят Ником Продановым в рамках дипломной работы,
без привлечения какого либо финансирования и с использованием свободного программного
обеспечения. ПО распространяется под лицензией <a href="https://creativecommons.org/licenses/by/4.0/deed.ru"><abbr title="Creative Commons Attribution 4.0 International">CC BY 4.0</abbr></a>.
