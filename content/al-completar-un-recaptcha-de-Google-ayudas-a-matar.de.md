Author: Jorge Maldonado Ventura
Category: Nachrichten
Date: 2018-03-17 23:55
Image: <img src="/wp-content/uploads/2018/03/recaptcha-usado-para-matar.png" alt="Completing a Google reCAPTCHA to train an artificial intelligence that will help US drones to kill">
Lang: de
Slug: al-completar-un-recaptcha-de-google-ayudas-a-matar
Save_as: du-hilfst-zu-töten-indem-du-ein-Google-reCAPTCHA-ausfüllst/index.html
URL: du-hilfst-zu-töten-indem-du-ein-Google-reCAPTCHA-ausfüllst/
Tags: USA, Google, Das Pentagon, reCAPTCHA
Title: Du hilfst zu töten, indem du ein Google-reCAPTCHA ausfüllst

Google hat sich kürzlich mit [dem Pentagon](https://de.wikipedia.org/wiki/Pentagon) zusammengetan, um es bei der
Entwicklung künstlicher Intelligenz zu unterstützen. Das Projekt mit der
Bezeichnung Maven umfasst die Entwicklung eines Systems zur
Identifizierung von Objekten anhand von Drohnenbildern.

Das bedeutet, dass die Macht von Google über Menschen auf der ganzen
Welt vom US-Imperium für seine dunklen Interessen genutzt wird. Nun
birgt das Ausfüllen eines Google reCAPTCHA nicht nur [ethische
Gefahren](/de/wie-beutet-Google-mit-CAPTCHAs-aus/)
im Zusammenhang mit wirtschaftlicher Ausbeutung und der Verwendung
proprietärer Software, sondern auch eine enge Zusammenarbeit bei der
Ermordung von Menschen. Tode, die entmenschlicht erscheinen, aber
letztendlich sind es unbewusste (und bewusste) Menschen, die Maschinen
zum Töten trainieren.
