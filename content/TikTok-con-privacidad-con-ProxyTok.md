Author: Jorge Maldonado Ventura
Category: Privacidad
Date: 2022-08-18 00:00
Modified: 2022-10-29 14:00
Lang: es
Slug: TikTok-con-privacidad-con-ProxiTok
Tags: TikTok
Title: TikTok con privacidad con ProxiTok
Image: <img src="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-etiqueta-stallman-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">

TikTok es una red centralizada y que requiere el uso de
<i lang="en">software</i> privativo. Consultar TikTok con el navegador
de forma decente sin perder privacidad o libertad es prácticamente
imposible... a no ser que usemos otra interfaz, como
[ProxyTok](https://proxitok.pabloferreiro.es/), la cual describo en este
artículo.

La interfaz de ProxiTok es sencilla. Se puede ir a contenido popular
(**<i lang="en">Trending</i>**), descubrir usuarios (**<i lang="en">Discover</i>**) y buscar por
nombre de usuario, etiqueta, URL de TikTok, identificador de música e
identificador de vídeo.

<figure>
<a href="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/buscar-por-nombre-de-usuario-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Buscar por nombre de usuario</figcaption>
</figure>

<figure>
<a href="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png">
<img src="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png" alt="" width="1922" height="1034" srcset="/wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok.png 1922w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-961x517.png 961w, /wp-content/uploads/2022/08/perfil-de-recetas-veganas-en-ProxiTok-480x258.png 480w" sizes="(max-width: 1922px) 100vw, 1922px">
</a>
    <figcaption class="wp-caption-text">Perfil de <a href="https://proxitok.herokuapp.com/@recetasveganas">@recetasveganas</a>. También se puede seguir por RSS.</figcaption>
</figure>

El proyecto se inició el 1 de enero de 2022, por lo que es bastante
joven y se espera que se le vayan añadiendo más funcionalidades. Para
redireccionar automáticamente a ProxiTok se puede instalar la extensión
[LibRedirect](https://libredirect.github.io/), que también evita otras
páginas web nocivas para la privacidad.

Existen [varias instancias
públicas](https://github.com/pablouser1/ProxiTok/wiki/Public-instances),
siendo posible instalar una en tu propio servidor.
