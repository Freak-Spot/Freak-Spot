Author: Jorge Maldonado Ventura
Category: GNU/Linux
Date: 2023-02-09 14:56
Lang: en
Slug: bloquear-sitios-web-etchosts
Save_as: block-websites-with-hosts-file/index.html
URL: block-websites-with-hosts-file/
Tags: ads, block, GNU/Linux, websites, privacy
Title: Block websites: <code>hosts</code> file
Image: <img src="/wp-content/uploads/2023/02/example-blocking-with-hosts-file.png" alt="" width="960" height="784" srcset="/wp-content/uploads/2023/02/example-blocking-with-hosts-file.png 960w, /wp-content/uploads/2023/02/example-blocking-with-hosts-file-480x392.png 480w" sizes="(max-width: 960px) 100vw, 960px">

To block websites you can use a browser extension (such as [Block
Site](https://add0n.com/block-site.html)), a [proxy server](https://en.wikipedia.org/wiki/Proxy_server) (such as
[Squid](https://en.wikipedia.org/wiki/Squid_(software))), but there is
also the option of editing the `hosts` file, a method that consumes very
little RAM and, unlike the browser extension, will work for any browser
or program <!-- more -->that connects to the Internet[^1].

## How does this method works?

You just have to add lines in the following format to the `hosts` file
(`/etc/hosts` in GNU/Linux) with the pages you want to block:

    :::text
    0.0.0.0 ejemplo.com

This line causes that every time you try to connect to the site
`example.com` instead you connect to the IP address `0.0.0.0.0`, which
is a non-routable address that is used to designate an unknown, invalid
or not applicable destination. The result, after restarting the network
service (`sudo systemctl restart NetworkManager` in Debian) and the web
browser is the following:

<a href="/wp-content/uploads/2023/02/example-blocking-with-hosts-file.png">
<img src="/wp-content/uploads/2023/02/example-blocking-with-hosts-file.png" alt="" width="960" height="784" srcset="/wp-content/uploads/2023/02/example-blocking-with-hosts-file.png 960w, /wp-content/uploads/2023/02/example-blocking-with-hosts-file-480x392.png 480w" sizes="(max-width: 960px) 100vw, 960px">
</a>


It is also possible to redirect to another website, although most
browsers will not let you see the website, as this has been used in the
past for [phishing](https://en.wikipedia.org/wiki/Phishing).

If you want to block a lot of websites, adding hundreds of pages by hand
is not practical. That is why there are people and projects that compile
lists of sites. One example is the [list managed by Steven
Black](https://github.com/StevenBlack/hosts/), which provides block
lists according to subject matter, such as pornography, fake news and
gambling. Simply copy and paste from the list you want into your `hosts`
file.

[^1]: This method won't work for programs that connect to the Internet
    using [Tor](https://en.wikipedia.org/wiki/Tor_(network)), because
    the DNS is resolved inside the Tor network.
