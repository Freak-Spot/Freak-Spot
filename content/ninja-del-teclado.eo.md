Author: Jorge Maldonado Ventura
Category: Tekstoprilaboro
Date: 2023-02-10 18:00
Modified: 2023-03-04 13:51
Lang: eo
Slug: combinaciones-de-teclado-y-tecla-Componer-GNU/Linux
Save_as: skribi-ĉiun-ajn-signon-per-la-klavaro-en-GNU-Linukso-rapide/index.html
URL: skribi-ĉiun-ajn-signon-per-la-klavaro-en-GNU-Linukso-rapide/
Tags: agordoj, kunigo-klavo, GNU/Linukso, Cinammon, klavaro, Debiano, Debiano 11, Unicode
Title: Skribi ĉiun ajn signon per la klavaro en GNU/Linukso, rapide
Image: <img src="/wp-content/uploads/2020/04/ninja-del-teclado.jpg" alt="">

Pro via laboro aŭ io ajn vi devas skribi kelkfoje signojn, kiujn vi ne
trovas pentritaj en la klavaro (¹, «, —, ä, ĉ, ß, ŭ, ktp.), aŭ vi havas
rompitan klavon. Kion vi faras?<!-- more -->

1. Serĉi la signon en la Interreto, kopii kaj alglui ĝin.
2. Uzi funkciojn de la tekst-redaktilo por enmeti specialajn signojn.
3. Serĉi la Unikodan kodon en la Interreto kaj enmetu ĝin per
   klavo-kombinaĵo de via operaciumo[^1].
4. Lerni kiel oni skribas tiujn strangajn signojn uzante
   facile memoreblajn klavo-kombinaĵojn kaj uzi ilin en la estonteco.

Se via respondo estis la 1-a, 2-a aŭ 3-a, vi devus legi plu; la plej
bona solvo estas la 4-a, se vi jam kelkfoje bezonis enmeti strangajn
signojn.

Por ke vi sciu, mi montras jen listeton de kombinaĵoj, kiujn mi uzas en
la hispana klavaro en GNU/Linukso[^2]:

- <kbd>Desktra alt-klavo</kbd> + <kbd>z</kbd> = «
- <kbd>Alt Gr</kbd> + <kbd>x</kbd> = »
- <kbd>Majuskliga klavo</kbd> + <kbd>^</kbd>, poste nombro = ¹
- <kbd>Kunigo-klavo</kbd> + <kbd>_</kbd>, poste nombro = ₁
- <kbd>Dekstra alt-klavo</kbd> + <kbd>v</kbd> = “
- <kbd>Dekstra alt-klavo</kbd> + <kbd>b</kbd> = ”
- <kbd>Dekstra alt-klavo</kbd> + <kbd>Mayús</kbd> + <kbd>v</kbd> = ‘
- <kbd>Dekstra alt-klavo</kbd> + <kbd>Mayús</kbd> + <kbd>b</kbd> = ’
- <kbd>Kunigo-klavo</kbd>, dann <kbd>-</kbd>, dann <kbd>-</kbd>, dann <kbd>-</kbd> = &mdash;

Se vi serĉis kiel skribi unu el tiuj signoj eble vi jam solvis la
problemon. «Sed atendu!», eble vi demandas...

- Kiel mi povas scii tiujn kombinaĵojn?
- Kio estas la kunigo-klavo?

## Vidi la klavo-kombinaĵojn

En la menuo de klavar-aranĝoj, almenaŭ en la Cinnamon-fenestrilo, vi
trovos opcion por montri la mapon de klavoj. Se vi iom serĉas en la
klavar-agordoj, vi trovos la mapon de klavoj. Ankaŭ ekzistas la komando
`xmodmap -pke`.

<figure>
<a href="/wp-content/uploads/2023/02/Esperanta-klavar-aranĝo-en-Debian-testing-Cinnamon.png">
<img src="/wp-content/uploads/2023/02/Esperanta-klavar-aranĝo-en-Debian-testing-Cinnamon.png" alt="" width="1464" height="648" srcset="/wp-content/uploads/2023/02/Esperanta-klavar-aranĝo-en-Debian-testing-Cinnamon.png 1464w, /wp-content/uploads/2023/02/Esperanta-klavar-aranĝo-en-Debian-testing-Cinnamon-732x324.png 732w" sizes="(max-width: 1464px) 100vw, 1464px">
</a>
    <figcaption class="wp-caption-text">Esperanta klavar-aranĝo en Debian testing (Cinnamon)</figcaption>
</figure>

En la bildo ni vidas, ke klavo havas ĝis kvar signoj. En la fizika
klavaro, kiun vi antaŭe havas, eble ne estas tiomaj pentritaj. Ni rigardu
specifan klavon, tiun kiu korespondas al la v litero. Ni vidas, ke en
tiu klavo aperas kelkajn signojn, tiel oni skribas ĉiujn:

- *Malsupre maldekstre (**v**)*. Simple tajpu tiun klavon
- *Supre maldekstre (**V**)*. <kbd>Majuskliga klavo</kbd> + *klavo*
- *Malsupre dekstre (**“**)*. <kbd>Dekstra alt-klavo</kbd> + *klavo*
- *Supre dekstre (**‘**)*. <kbd>Dekstra alt-klavo</kbd> + <kbd>Majuskliga klavo</kbd> + *klavo*

## Kunigo-klavo

Aperas problemo, kiam ne ekzistas la klavo, kiun ni volas skribi, en nia
klavar-aranĝo. Eble ĝi estas signo kiel —, → aŭ alia ajn.

Ĉi-okaze, ni devas agordi la [kunigo-klavon](https://eo.wikipedia.org/wiki/Kunigo-klavo),
se ĝi ne estas agordita en nia klavaro. Por fari tion ni povas agordi
ĝin en la klavar-agordoj de nia distribuo aŭ modifi la dosieron
`/etc/default/keyboard` aldonante al la variablo `XKBOPTIONS` la klavon,
kiun ni volas uzi (`lalt`, por maldekstra alt-klavo; `rwin`, dekstra
vindoza klavo; `lwin`, maldekstra vindoza klavo...). Si mi volus uzi la
dekstran alt-klavon kiel kunigan klavon, mi ŝanĝus tiel la dosieron
`/etc/default/keyboard`:

    :::diff
    - XKBOPTIONS=""
    + XKBOPTIONS="compose:ralt"

Kiam ĝi estu agordita, provu kombinaĵon kiel <kbd>Kunigo-klavon</kbd>
poste <kbd>-</kbd> poste <kbd>></kbd> (ĝi devus skribi →). Ni povas
serĉi la implicitajn signojn kun iliaj respektivaj klavo-kombinaĵoj en
la dosierujo `/usr/share/X11/locale/` &mdash; per rikura serĉo pere de
`grep` (`grep -R signo`) tio facilas &mdash;. Ni povas trovi la agorda
aranĝo en la manlibra paĝo [Compose(5)](https://man.archlinux.org/man/Compose.5).
Se ne ekzistas klavo-kombinaĵo por la signo, kiun ni volas skribi, ni
povas agordi ĝin kreante `~/.XCompose`-dosieron.

## Estas multe pli...

Mi ne volis detale paroli pri mortaj klavoj kaj aliaj aferoj pri la
klavaro. Danke al ĉi tiu artikolo verŝajne estos pli simpla por vi trovi
pli da informo, se ĉi tio estis malmulte.

Mi esperas, ke per tio, kion mi lernigis al vi en ĉi tiu artikolo, vi
povu skribi multajn strangajn signojn per ninĵa lerteco. ☺

[^1]: En GNU/Linux-operaciumoj kun la X-fenestrosistemo vi devas teni
  <kbd>Ctrl</kbd>+<kbd>Shift</kbd> premitaj kaj preni <kbd>u</kbd>,
  malpremi <kbd>Ctrl</kbd> kaj <kbd>Shift</kbd>, skribi
  la deksesuman kodon kaj, finfine, premi <kbd>Enigan klavon</kbd>.
[^2]: La signo + signifas, ke oni devas preni la klavojn samtempe. Kiam
    mi diras «poste», oni devas preni ĝin sinsekve. Post la = aperas la
    rezulto de la klavo-kombinaĵo.
