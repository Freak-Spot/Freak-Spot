Author: Jorge Maldonado Ventura
Category: Desarrollo web
Date: 2016-11-10 19:30
Modified: 2023-01-16 10:00
Lang: es
Slug: por-que-algunas-urls-acaban-con-una-barra
Status: published
Tags: direcciones web, http, https, Localizador de recursos uniforme, página principal, páginas web, protocolo, URI, url, URLs
Title: ¿Por qué algunas URLs acaban con una barra?

Probablemente, te habrás cruzado con URLs que acaben en una barra (como
[https://freakspot.net/](https://freakspot.net/) o [/](/), directorio raíz del servidor) y con otras que no (como esta:
<https://www.gnu.org/gnu/gnu.html>). ¿Cuál es la diferencia? ¿Es
importante?

Una
[URL](https://es.wikipedia.org/wiki/Localizador_de_recursos_uniforme) es
básicamente una dirección a un recurso. Las URLs no hacen referencia
únicamente a páginas web, sino también a otros tipos de recursos.
Algunos ejemplos de esquemas URL son `http`, `https`, `ftp`, `telnet`,
`data` y `mailto`. En este artículo me estoy refiriendo a páginas web
que usan el esquema `http` o el `https`.

**Las direcciones URL que acaban en una barra se refieren a un
directorio; las que no, hacen referencia a un archivo**. Cuando clicas
el enlace <https://freakspot.net>, el servidor se da cuenta de que la
dirección requerida no es un archivo y se dirige a
[https://freakspot.net/](https://freakspot.net/). Allí encuentra un archivo principal llamado
`index.html` o *index con otra extensión* y muestra su contenido.

Consecuentemente, la carga de una página es más rápida cuando utilizamos
enlaces a páginas principales acabados en barras (por ejemplo,
[/](/)) o cuando enlazamos al nombre del archivo
(por ejemplo, <http://www.ejemplo.com/index.html>).
